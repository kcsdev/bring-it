<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'c14_wp12');

/** MySQL database username */
define('DB_USER', 'c14_wp12');

/** MySQL database password */
define('DB_PASSWORD', 'P*L)&Y9~S#48*^5');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'q1BD0XPELsM4jJkctPxx5aahhn3zjxJ6SxKnkTloUMytD9uFFbrUHQiYxR1TeN8f');
define('SECURE_AUTH_KEY',  'NL9FJmRxPF241maQ7zZuwxrdtuuVEF9uk5UH88jBt3qUWaDfzGufsdvQrDuEKLEt');
define('LOGGED_IN_KEY',    'wj16lK2trdSHs34AouMF4DBZbDCeJpW5qBfugYHsKq1mSdSHgAWB0EPRxUodulDt');
define('NONCE_KEY',        'ps4Le1cypieKOnglzwB4KHPSE4F9X9LDpGh6iBYwjUGFxw79q4jaJSgi1XlD5L0g');
define('AUTH_SALT',        'QFLHrKsnszxFJRTwP3T3L2gIxGJapEZBkWl4x14Py8Arsbvt3ktyQEUCNXwuti1l');
define('SECURE_AUTH_SALT', 'BKflcmuNqZLByvL7HRsgMzR0Z30bv3fJzGM4kpywQIuc1UBTEAglu8AlxTwNtI9l');
define('LOGGED_IN_SALT',   'oXEYuo2xbEMYFOFu4Hj88BkOvQIjOL5mTRxouIEOs9HrdBSz9qKa0xXjiUVPO8o4');
define('NONCE_SALT',       'G3YOs1ifgnqLdtgJAfqbWLZgcn8GO4eronNK5dLRxhH6xX9EF1vpFUY5Vewxp7Qn');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0777);define('FS_CHMOD_FILE',0666);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);
define('WP_DEBUG_LOG', true);
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

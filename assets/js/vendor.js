jQuery(document).ready(function($){ 
    
    
    function changeRegionStatus(regionId,cityId,rStatus){
        $.ajax({
            url:"/wp-admin/admin-ajax.php",
            type:'POST',
            data:'action=enable_region_action&checked_region_id='+regionId+'&city_id='+cityId+'&turn='+rStatus, 
            success:function(result){ 
                //console.log($("div.city-brick").find($("[city="+cityId+"]")));
                if($("div.city-brick").find($("[city="+cityId+"]"))){ 
                    if(rStatus=='off'){
                        $('input[name=minimum-order-price-'+cityId+'_'+regionId+']').prop('disabled',true);
                        $('input[name=shipping-cost-'+cityId+'_'+regionId+']').prop('disabled',true);
                        $('input[name=free-shipping-minimum-order-price-'+cityId+'_'+regionId+']').prop('disabled',true);
                        $('input[name=shipping-time-'+cityId+'_'+regionId+']').prop('disabled',true);
                    }else{
                        $('input[name=minimum-order-price-'+cityId+'_'+regionId+']').prop('disabled',false);
                        $('input[name=shipping-cost-'+cityId+'_'+regionId+']').prop('disabled',false);
                        $('input[name=free-shipping-minimum-order-price-'+cityId+'_'+regionId+']').prop('disabled',false);
                        $('input[name=shipping-time-'+cityId+'_'+regionId+']').prop('disabled',false);
                    }
                }
            }
        });
    }
    
    function savingButton(){
        $('#save-regions').click(function(e){
            e.preventDefault();
            $.ajax({
                url:"/wp-admin/admin-ajax.php",
                type:'POST',
                data:'action=save_city_table_action',
                success:function(result){
                    $('#region_table').remove();
                    $(result).insertAfter('#select_city');
                }
            });
        });
    }
    function openCityTable(cityId){
        $.ajax({
            url:"/wp-admin/admin-ajax.php",
            type:'POST',
            data:'action=get_list_of_city_regions_action&city_id='+cityId, 
            success:function(result){ 

                $('#region_table').remove();
                $(result).insertAfter('#select_city');
                //**Saving button Funtion**//
                savingButton();
                //**************************//
                $('input[name=region-status]').change(function(){   // Check/Uncheck Region ******************************************
                    var rStatus = $(this).val();
                    //console.log(rStatus);
                    var regionId = $(this).siblings('input[name="region_id"]').val();
                    //console.log(regionId);
                    if(rStatus == 'on'){
                        rStatus = 'off';
                        $(this).val(rStatus);
                        changeRegionStatus(regionId,cityId,rStatus);
                    }else{
                        rStatus = 'on';
                        $(this).val(rStatus);
                        changeRegionStatus(regionId,cityId,rStatus);
                    }

                });
            }
        });   
    }
    
    $("#add_city").click(function(e){ // Add city Buttom on Click ******************************************
        e.preventDefault();
        $.ajax({
            url:"/wp-admin/admin-ajax.php",
            type:'POST',
            data:'action=get_list_of_cities_action', 
            success:function(result){ 
                $('#select_city').remove();
                $(result).insertAfter('#add_city');
                $("select[name = select_city]").change(function(){  // City Selection on Change ******************************************
                    var cityId = ($(this).find("option:selected").val());
                    openCityTable(cityId);
                });
            }
        });
        
    });
    
});
<?php
add_action('init', 'additional_product');
$tax_name = "";
function additional_product()
{ 
    $single = "Additional Product";
    $multi = "Additional Products";
    $func_name = "additional_product";
    global $tax_name;
    
  $labels = array(
    'name' =>  __( $single ),
    'singular_name' =>  __( $single ),
    'add_new' =>  __( 'Add New'),
    'add_new_item' =>  __( 'Add New '.$single),
    'name_admin_bar'=> __( 'Add New '.$single),
    'edit_item' =>  __( 'Edit' .$single),
    'new_item' =>  __( 'Add '.$single),
    'view_item' =>  __( 'View '.$single),
    'search_items' =>  __( 'Search ' . $multi),
    'not_found' =>   __( $multi . ' not found'),
    'not_found_in_trash' =>  __( $multi . ' not found in trash'),
    'menu_name' =>  __( $single)
  );

  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'additional_post',
    'capability' => 'additional_post',
    'map_meta_cap' => true,
    'has_archive' => true,
    'hierarchical' => true,
    'menu_position' => 5,
    'supports' => array('title','thumbnail'),
//    'taxonomies' => array( $tax_name ) 
  );
  register_post_type($func_name,$args); 
  flush_rewrite_rules();

}
if($tax_name != ""){ 
    add_action( 'init', 'add_'.$single.'_categories', 0 );
    function add_additional_product_categories(){ 
        $labels = array(
            'name'                       => _x( $single .' Categories', 'taxonomy general name' ),
            'singular_name'              => _x( $single .' Category', 'taxonomy singular name' ),
            'search_items'               => __( 'Search '.$multi.' Categories' ),
            'popular_items'              => __( 'Popular '.$multi.' Categories' ),
            'all_items'                  => __( 'All '.$multi.' Categories' ),
            'parent_item'                => null,
            'parent_item_colon'          => null,
            'edit_item'                  => __( 'Edit '.$single.' Category' ),
            'update_item'                => __( 'Update '.$single.' Category' ),
            'add_new_item'               => __( 'Add New '.$single.' Category' ),
            'new_item_name'              => __( 'New '.$single.' Category Name' ),
            'separate_items_with_commas' => __( 'Separate '.$multi.' Categories with commas' ),
            'add_or_remove_items'        => __( 'Add or remove '.$single.' Categories' ),
            'choose_from_most_used'      => __( 'Choose from the most used '.$single.' Categories' ),
            'not_found'                  => __( 'No '.$multi.' Categories found.' ),
            'menu_name'                  => __( $single.' Categories' ),
        );

        $args = array(
            'hierarchical'          => false,
            'labels'                => $labels,
            'show_ui'               => true,
            'show_admin_column'     => true,
            'update_count_callback' => '_update_post_term_count',
            'query_var'             => true,
            'rewrite'               => array( 'slug' => $tax_name )
        );

        register_taxonomy( $tax_name, $single, $args );
    }
}
?>
<?php
add_action('init', 'addons');
function addons()
{
  $labels = array(
    'name' =>  __( 'Addons'),
    'singular_name' =>  __( 'Addon'),
    'add_new' =>  __( 'Add New'),
    'add_new_item' =>  __( 'Add New Addon'),
    'edit_item' =>  __( 'Edit Addon'),
    'new_item' =>  __( 'Add Addon'),
    'view_item' =>  __( 'View Addon'),
    'search_items' =>  __( 'Search Addons'),
    'not_found' =>   __( 'Addons not found'),
    'not_found_in_trash' =>  __( 'Addons not found in trash'),
    'menu_name' =>  __( 'Addon')
  );

  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'query_var' => true,
    'rewrite' => true,
      
    'capability_type' => 'addon_post_type',
    'map_meta_cap' => true,
    'capability_type' => 'post',
      
    'has_archive' => true,
    'hierarchical' => true,
    'menu_position' => 5,
    'supports' => array('title','thumbnail'),
    'taxonomies' => array( 'addon_cat' ),
//    'map_meta_cap' => true,
//    'capability_type' => array('addon_post_type','post')
    
    'capability' =>array('publish_addon','edit_addon','delete_addon'),
  );
  register_post_type('addon',$args); 
  flush_rewrite_rules();

}

add_action( 'init', 'add_addon_categories', 0 );
function add_addon_categories(){
    $labels = array(
		'name'                       => _x( 'Addons Categories', 'taxonomy general name' ),
		'singular_name'              => _x( 'Addon Category', 'taxonomy singular name' ),
		'search_items'               => __( 'Search Addons Categories' ),
		'popular_items'              => __( 'Popular Addons Categories' ),
		'all_items'                  => __( 'All Addons Categories' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Addon Category' ),
		'update_item'                => __( 'Update Addon Category' ),
		'add_new_item'               => __( 'Add New Addon Category' ),
		'new_item_name'              => __( 'New Addon Category Name' ),
		'separate_items_with_commas' => __( 'Separate Addons Categories with commas' ),
		'add_or_remove_items'        => __( 'Add or remove Addon Categories' ),
		'choose_from_most_used'      => __( 'Choose from the most used Addon Categories' ),
		'not_found'                  => __( 'No Addons Categories found.' ),
		'menu_name'                  => __( 'Addon Categories' ),
	);

	$args = array(
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'addon_cat' )
	);

	register_taxonomy( 'addon_cat', 'addon', $args );
}
?>
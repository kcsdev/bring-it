<?php

?><!DOCTYPE html>
<html>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>BringIt - Management System</title>

    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/vendor/vendor-styles.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/vendor/vendor-login-styles.css" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>


</head>

<body <?php body_class(); ?>>

<div class="page-wrapper login clearfix">
    <header class="h-section down-space-24 login">
        <img src="<?php bloginfo('stylesheet_directory') ?>/assets/bringit-top-logo.png" class="item-img" alt="" />
    </header>



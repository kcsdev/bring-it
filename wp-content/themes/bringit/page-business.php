<?php
/**
Template Name:  BringIt Business Page
 */

get_header();

function bringid_business() {
    wp_enqueue_style( 'bringid_business', '/wp-content/themes/bringit/css/bringid_business.css' );
    
}
add_action( 'bringid_business', '/wp-content/themes/bringit/css/bringid_business.css' );
add_action( 'wp_enqueue_scripts', 'bringid_business' );
?>

   <div class="bb-container">
        <header class="bb-header">
            <div>
               <img title="logo" />
            </div>
        </header>
        <nav>
            <ul>
                <li>מלאי</li>
                    <ul>
                        <li>שתייה</li>
                        <li>תוספות</li>
                        <li>תוספות אישיות</li>
                        <li> אוכל נוסף</li>
                        <li>מחיר פיצה</li>
                    </ul>
                        <li>מבצעים</li>
                            <ul>
                                <li>מצבעים שלי</li>
                            </ul>
                        <li>הגדרות</li>
                           <ul>
                               <li>עלה לוגו חדש</li>
                               <li>הגדרות משלוח</li>
                               <li>שינויים פרטים אישיים</li>
                               <li>הגדרות אתר אישי</li>
                           </ul>   
                        <li>סטטיסטיקה</li>
                            <ul>
                                 <li>הראה את כל ההזמנות</li>
                                 <li>מה הכי נמכר באתר</li>
                                 <li>מה הכי נמכר אצלי</li>
                                 <li>הרווחים שלי</li>
                            </ul>
                        <li>אני עובד</li>
                        <img title="on/off"/>
                </ul>
        </nav>
                
        <div>
            <h3>בחר אזורי חלוקה</h3>
            <input type="button" value="הוסף"/>
            <select>
                <option>בחר</option>
            </select>
        </div>
                
        <div>
            <input type="button" value="ערוך"/>
        </div>
                
        <div>
            <input type="button" value="בטל"/>
            <input type="button" value="שמור"/>
        </div>
                
        <div class="ui-page-business-sidebar">
            <div>
                <select>
                    <option>הוסף זמן משלוח</option>
                    <input type="button" value="אפס ברירת מחדל"/>
                </select>
            </div>
            <input type="button" value="הזמנות "/>
       </div>
                        
       <div class="orders">
            <div>אבן עזרא 10</div>
       </div>    
                    
   </div>
            


<?php
get_footer();
?>
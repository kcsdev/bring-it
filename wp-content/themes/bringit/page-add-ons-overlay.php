<?php
/**
Template Name: Add Ons Overlay page
 */
get_header('blank'); ?>

    <div class="bi-section clearfix">
        <div class="bi-container add-ons">

            <p class="pizzeria-name">פיצה איטליה</p>

            <div class="page-title"></div>

            <div class="item">
                <img src="<?php bloginfo('stylesheet_directory') ?>/assets/temp/adds-on-1.jpg" alt="" />
                <p class="name">לחם שום</p>
                <p class="descr">חביתית או חמיטה, הנקראת גם בלועזית פנקייק או קרפ</p>
                <div class="bottom">
                    <span>11.90 &#8362;</span>
                </div>
                <ul class="controls">
                    <li class="plus"></li>
                    <li class="total">0</li>
                    <li class="minus zero"></li>
                </ul>
            </div>

            <div class="item">
                <img src="<?php bloginfo('stylesheet_directory') ?>/assets/temp/adds-on-2.jpg" alt="" />
                <p class="name">מיני פנקייקס</p>
                <p class="descr">חביתית או חמיטה, הנקראת גם בלועזית פנקייק או קרפ</p>
                <div class="bottom">
                    <span>16.50 &#8362;</span>
                </div>
                <ul class="controls">
                    <li class="plus"></li>
                    <li class="total">0</li>
                    <li class="minus zero"></li>
                </ul>
            </div>

            <button class="close-me" onclick="location='/purchase-sign-in/';"></button>
        </div>
    </div>

    <script type="text/javascript">

        (function( $ ) {
            $(document).ready(function(){
                $(".add-ons .item .controls .plus").bind("click", function(){
                    $self = $(this);
                    $self.next().text(parseInt($self.next().text()) + 1);
                    if(parseInt($self.next().text()) > 0 && $self.siblings(".minus").hasClass("zero")){
                        $self.siblings(".minus").removeClass("zero")
                    }
                })

                $(".add-ons .item .controls .minus").bind("click", function(){
                    $self = $(this);
                    var currentNum = parseInt($self.prev().text());
                    if(currentNum > 0){
                        $self.prev().text(currentNum - 1);
                        if(parseInt($self.prev().text()) <= 0){
                            $self.prev().text("0");
                            $self.addClass("zero");
                        }
                    }
                });
            });
        })( jQuery );

    </script>
<?php get_footer('blank'); ?>
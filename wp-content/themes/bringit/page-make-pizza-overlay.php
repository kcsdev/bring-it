<?php
/**
Template Name: Make Pizza Overlay page
 */
get_header('blank'); ?>

    <div class="bi-section clearfix">
        <div class="bi-container mkp">

            <form class="pizza-components">
                <div class="mkp-work-pad overlay clearfix">
                    <div id="makePizzaWorkpad"></div>
                </div>
            </form>

        </div>
    </div>

    <script type="text/javascript">
        jQuery.noConflict();

        (function( $ ) {
            $(document).ready(function(){
                $("#makePizzaWorkpad").load("/pv-make-pizza/?modal=1");
            });
        })( jQuery );

    </script>
<?php get_footer('blank'); ?>
<?php
/**
 * The template for displaying the blank footer
 *
 * Contains the closing of the "#main" div and all content after.
 *
 * @package BringIt
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
</div><!-- #main -->

<?php wp_footer(); ?>

<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/js/jquery.simplemodal.js"></script>

</body>
</html>

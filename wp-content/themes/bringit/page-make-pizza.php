<?php
/**
Template Name: Make Pizza page
 */
get_header(); ?>
<script>var addons=[];</script>
    <div class="bi-section clearfix">
        <div class="bi-container enlarged mkp">
            <div class="mask"></div>

            <form class="pizza-components">
                <div class="mkp-work-pad clearfix">
                    <div class="mkp-page-title"></div>

                    <div id="makePizzaWorkpad"></div>
                </div>
            </form>



            <?php /* ------- Shopping cart sidebar ------- */ ?>
            <div class="mkp-left-sidebar">
                <div class="toggle-button">&lt;&lt;</div>
                <img src="<?php bloginfo('stylesheet_directory') ?>/assets/bike.png" alt="" class="bike" />

                <div class="shopping-cart">
                    <div id="shoppingCartContent"></div>
                </div>

            </div>
            <?php /* ------- /Shopping cart sidebar ------- */ ?>
        </div>
    </div>

    <script type="text/javascript">
        
        jQuery.noConflict();

        (function( $ ) {
            $(document).ready(function(){
                $("#makePizzaWorkpad").load("/pv-make-pizza/?r=" + (new Date()).getTime(),function(){                              
                                        
                    $("#add-pizza-and-remain").bind("click",function(){
                        event.preventDefault(); // preventing from the button to reload the page.
                        var addson = []; // creating new array to contain all selected addson for the pizza
                        
                        jQuery("#loading-animation").show(); // Displaying an animation while the ajax is working.

                        $(".selection > li input").each(function(){ // loops through the li-s of the addson and saves them to an array
                            var addon = {}; // Creating a new empty addon object
                            if($(this).is(":checked")){ // if an input is CHECKED (addon checkbox or a radio button);
                                addon.location = $(this).attr("addon-location"); // saving the location of an addon that was chosen
                                addon.WPID = $(this).attr("addon-WPID"); // saving the WPID of an addon that was chosen
                                
                                addson.push(addon); // Adding the new object to the array of addson
                            } // end if
                            
                        }); // end each

                        var RAWjsonAddson = JSON.stringify(addson); // Stringifying the array of addson
                        var jsonAddson = RAWjsonAddson.replace(/\\/g, ''); // removing all the backslashes before sending it to the php function

                        jQuery.ajax({ 

                            url:"/wp-admin/admin-ajax.php",
                            type:'POST',
                            data:'action=add_pizza_to_cart_action&addson='+jsonAddson, 
                            success:function(result){ 

                                console.log("Add pizza and reload");
                                console.log(result);
                                jQuery("input").each(function(){
                                    jQuery(this).data("checked",false);
                                    jQuery(this).data("waschecked",false);
                                    jQuery(this).prop("checked",false);
                                    addons = [];
                                });
                        //                                jQuery("#makePizzaWorkpad").load("/pv-make-pizza/");
                            jQuery("#shoppingCartContent").load("/pv-shopping-cart-content/");
                        //                                location.reload();
                            },
                            complete:function(){
                                jQuery("#loading-animation").hide();
                                jQuery(".pizza-layer").css("display","none");
                            }

                        });
                    });
                });
                
                $("#shoppingCartContent").load("/pv-shopping-cart-content/",function(){                     

                                        
                });
            });

            $(".mkp-left-sidebar .toggle-button").bind("click", function(){
                $self = $(this);
                var objMenu = $(".mkp-left-sidebar");
                if($self.hasClass("open")) {
                    objMenu.animate({"left":"-310px"}, 200, function(){
                        $(".bi-section .bi-container.mkp .mask").fadeOut(200, function(){
                            $self.removeClass("open").text('<<');
                            objMenu.css({"left":""});
                        });
                    })
                } else {
                    $(".bi-section .bi-container.mkp .mask").fadeIn(200, function(){
                        objMenu.animate({"left":"0px"}, 200, function(){
                            $self.addClass("open").text('>>');
                        });
                    });
                }
            });
        })( jQuery );

    </script>
<?php get_footer(); ?>
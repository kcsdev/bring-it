<?php
    $ref = $_GET['ref'];
    if($ref == "newArea");
    $locality = $_GET['locality'];
    $regions = Address::getRegionsByCity($locality);
?>
<table class="tab-data" id = "new-area-table" city-name = "<?php echo $locality; ?>">
    <colgroup>
        <col style="width: 10%;" />
        <col style="width: 18%;" />
        <col style="width: 18%;" />
        <col style="width: 18%;" />
        <col style="width: 18%;" />
        <col style="width: 18%;" />
    </colgroup>
    <thead>
    <tr>
        <th>אזור
            <span class="icon-tooltip static-tooltip" data-tooltip-class="blue" data-tooltip-text="טבלה עם אפשרויות סינון- לפי תאריכים. על מנת שאוכל להגיע לכל הזמנה קודמת שהייתה לי שנה אחורה.">?</span></th>
        <th>מבצע משלוח לאיזור זה
            <span class="icon-tooltip static-tooltip" data-tooltip-class="blue" data-tooltip-text="טבלה עם אפשרויות סינון- לפי תאריכים. על מנת שאוכל להגיע לכל הזמנה קודמת שהייתה לי שנה אחורה.">?</span></th>
        <th>מינימום הזמנה באתר (&#8362;)
            <span class="icon-tooltip static-tooltip" data-tooltip-class="blue" data-tooltip-text="טבלה עם אפשרויות סינון- לפי תאריכים. על מנת שאוכל להגיע לכל הזמנה קודמת שהייתה לי שנה אחורה.">?</span></th>
        <th>עלות משלוח (&#8362;)
            <span class="icon-tooltip static-tooltip" data-tooltip-class="blue" data-tooltip-text="טבלה עם אפשרויות סינון- לפי תאריכים. על מנת שאוכל להגיע לכל הזמנה קודמת שהייתה לי שנה אחורה.">?</span></th>
        <th>משלוח חינם החל מ- (&#8362;)
            <span class="icon-tooltip static-tooltip" data-tooltip-class="blue" data-tooltip-text="טבלה עם אפשרויות סינון- לפי תאריכים. על מנת שאוכל להגיע לכל הזמנה קודמת שהייתה לי שנה אחורה.">?</span></th>
        <th>זמן משלוח (דקות)
            <span class="icon-tooltip static-tooltip" data-tooltip-class="blue" data-tooltip-text="טבלה עם אפשרויות סינון- לפי תאריכים. על מנת שאוכל להגיע לכל הזמנה קודמת שהייתה לי שנה אחורה.">?</span></th>
    </tr>
    </thead>
    <tbody>
        
    <?php foreach($regions as $region): ?>
    <tr class = "region-row" region-name = "<?php echo $region->name; ?>" >
        <td>
            <label>
                <?php echo $region->name; ?>
            </label>
        </td>
        <td class="align-center"><input type="checkbox" class="v-align-middle" id = "is_shipping_<?php echo $region->name; ?>" name = "is_shiping" /></td>
        <td><input type="text" class="tab-data-field" id = "minimum-order-price_<?php echo $region->name; ?>" name = "minimum-order-price" /></td>
        <td><input type="text" class="tab-data-field" id = "shipping-price_<?php echo $region->name; ?>" name = "shipping-price" /></td>
        <td><input type="text" class="tab-data-field" id = "minimum-order-price-for-free-shipping_<?php echo $region->name; ?>" name = "minimum-order-price-for-free-shipping" /></td>
        <td><input type="text" class="tab-data-field" id = "shipping-time_<?php echo $region->name; ?>" name = "shipping-time" /></td>
    </tr>
    <?php endforeach; ?> 

    </tbody>
</table>
<ul class="locality-buttons">
    <li>
        <button type="submit" 
                id = "new-area-save" >שמור</button>
        <img class = "save-area-animation" id = "add-area-animation" src = "<?php echo LOADING_GIF ?>" />
    </li>
    <li>
        <button type="reset" 
                onclick="<?php if($ref=='newArea'){ ?>jQuery('#localityNewData').html('');<?php } ?>; jQuery('#<?php echo $ref; ?>').toggle(400);" >
            בטל
        </button>
    </li>
</ul>
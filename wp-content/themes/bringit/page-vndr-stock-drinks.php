<?php
get_header('vendor');
?>
    <div class="h-section down-space">
        <ul class="breadcrumbs">
            <li><a>מלאי</a></li>
            <li class="current"><a>שתיה</a></li>
        </ul>
    </div>

    <div class="h-section content-pane down-space clearfix">
        <form onsubmit="return false;">
        <table class="stock-list down-space">
            <colgroup>
                <col style="width:50%;" />
                <col style="width:25%;" />
                <col style="width:25%;" />
            </colgroup>
            <thead>
            <tr>
                <th>
                    שם
                </th>
                <th>
                    האם זמין לרכישה באתר?
                </th>
                <th>
                    מחיר
                </th>
            </tr>
            </thead>
            <?php $drinks = Drink::getAll(); ?>
            <?php foreach($drinks as $drink): ?>
            <?php if($drink->inStock()): ?>
                <?php $disabled = ""; ?>
                <?php $checked = "checked"; ?>
            <?php else: ?>
                <?php $disabled = "disabled"; ?>
                <?php $checked = ""; ?>
            <?php endif; ?>
            <?php $price = $drink->getVendorsPrice(); ?>
            <tr>
                <td class="product-name">
                    <?php echo $drink->getTitle(); ?>
                </td>
                <td class="align-center">
                    <input class = "vndr-drink-in-stock" 
                           type="checkbox" 
                           name="<?php echo $drink->getWPID(); ?>-stock-status" 
                           id="<?php echo $drink->getWPID(); ?>-stock-status" 
                           <?php echo " " . $disabled . " "; ?> 
                           <?php echo $checked; ?>
                           />
                </td>
                <td class="align-center">
                    <input class = "vndr-drink-price" 
                           type="text" 
                           drink-WPID = "<?php echo $drink->getWPID(); ?>" 
                           name="<?php echo $drink->getWPID(); ?>-price" 
                           id="<?php echo $drink->getWPID(); ?>-price" 
                           maxlength="5" 
                           value = "<?php echo $price; ?>" />
                    &#8362;
                </td>
            </tr>
            <?php endforeach; ?>
            
        </table>
            <div class="clearfix">
                <button type="submit" class="list-save" id = "vndr-stock-drinks-save">שמירה</button>
                <img src = "<?php echo LOADING_GIF; ?>" class = "save-area-animation-absolute" id = "update-drinks-button" />
            </div>
        </form>
    </div>

<script type="text/javascript">
    jQuery(document).ready(function($){
       
    });
</script>
<?php
get_footer('vendor');
?>
<?php
/**
 * The template for displaying the blank header
 *
 * Displays all of the head element and everything up until the "#main" div.
 *
 * @package BringIt
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <?php wp_head(); ?>

    <style type="text/css">
        html { overflow-y: auto; }
    </style>
</head>

<body <?php body_class(); ?> style="background: transparent none !important;">
<div id="main">

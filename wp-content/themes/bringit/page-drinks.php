<?php
/**
Template Name: Drinks page
 */
get_header(); ?>

    <div class="bi-section">
        <div class="bi-container clearfix drinks">
            <div class="mask"></div>

            <div class="drinks-pane clearfix">
                <div class="title">
                    <img src="<?php bloginfo('stylesheet_directory') ?>/assets/drinks-title.png" alt="" />
                </div>

                <div id="drinkTiles" class="drink-tiles"></div>
            </div>

            <div class="drinks-left-sidebar">
                <div class="toggle-button">&lt;&lt;</div>
                <img src="<?php bloginfo('stylesheet_directory') ?>/assets/bike.png" alt="" class="bike" />
                <div class="shopping-cart">
                    <div id="shoppingCartContent"></div>
                    
                </div>
                <button type="button" class="checkout" onclick="location='/vieworder/';"></button>
            </div>

        </div>


    </div>
    <script type="text/javascript">
        jQuery(function ($) {
           

            $(".drinks-left-sidebar .toggle-button").bind("click", function(){
                $self = $(this);
                var objMenu = $(".drinks-left-sidebar");
                if($self.hasClass("open")) {
                    objMenu.animate({"left":"-310px"}, 200, function(){
                        $(".bi-section .bi-container.drinks .mask").fadeOut(200, function(){
                            $self.removeClass("open").text('<<');
                            objMenu.css({"left":""});
                        });
                    })
                } else {
                    $(".bi-section .bi-container.drinks .mask").fadeIn(200, function(){
                        objMenu.animate({"left":"0px"}, 200, function(){
                            $self.addClass("open").text('>>');
                        });
                    });
                }
            });

            $(document).ready(function(){
                $("#drinkTiles").load("/pv-drinks/");
                $("#shoppingCartContent").load("/pv-shopping-cart-content/");
            });
        });
    </script>
<?php get_footer(); ?>
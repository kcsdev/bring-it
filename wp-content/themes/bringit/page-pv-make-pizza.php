
<?php $categories  = Addon::getCategories(); ?>
<!--<form class="pizza-components">-->
<!--    <div class="mkp-work-pad clearfix">-->
<!--        <div class="mkp-page-title"></div>-->

        <?php /* ------- Components menu ------- */ ?>
        <ul class="mkp-categories">
            <?php foreach($categories as $category): ?>
            <?php $addons  = Addon::getAddonsByCategory($category); ?>
            <?php $category_image = get_field("image","addon_cat_" . $category->term_id); ?>
            <li class="section">
                <a class="pz-components-title" data-section-title="<?php echo $category->slug; ?>"><img src="<?php echo $category_image; ?>" alt="" /></a>
                <ul class="pz-components <?php echo $category->slug; ?> close">
                    <?php foreach($addons as $addon): ?>
                    <li class="clearfix">
                        <?php echo $addon->getTitle(); ?>
                        <?php render_selection_group($addon->getName(),$addon->getWPID(),$addon->getTitle()); ?>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </li>
           
            <?php endforeach; ?>
        </ul>
        <?php /* ------- /Components menu ------- */ ?>

        <?php /* ------- Pizza ------- */ ?>
        <div class="mkp-pizza-base">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/make-pizza-base.png" class="base-layer" alt="" />


            <?php /* PIZZA LAYERS -- SHOULD BE GENERATED WITH PHP CODE */ ?>
            <?php $addons = Addon::getAll(); ?>
            <?php foreach($addons as $addon): ?>
                <?php $_addon = $addon->getName(); ?>
                <div class="pizza-layer whole_<?php echo $_addon; ?>" style = "z-index: <?php echo get_field("layer_order_number",$addon->getWPID()); ?> !important;"></div>
                <div class="pizza-layer l-half_<?php echo $_addon; ?>"></div>
                <div class="pizza-layer r-half_<?php echo $_addon; ?>"></div>
                <div class="pizza-layer nw_<?php echo $_addon; ?>"></div>
                <div class="pizza-layer ne_<?php echo $_addon; ?>"></div>
                <div class="pizza-layer sw_<?php echo $_addon; ?>"></div>
                <div class="pizza-layer se_<?php echo $_addon; ?>"></div>
            <?php  endforeach; ?>
            <?php /* /PIZZA LAYERS -- SHOULD BE GENERATED WITH PHP CODE */ ?>
            

            <?php if(isset($_GET['modal']) && !empty($_GET['modal']) && $_GET['modal'] == '1') { ?>
                <button type="button" class="close-me" onclick="parent.closeMe();">סיימתי!</button>
            <?php } else { ?>
            <div class="mkp-order-buttons">
                <script>
                    
                    
// onclick = "update_pb_cart_and_redirect(event);"
                </script>
                 
                <button type="submit" 
                        id = "add-pizza-and-remain" 
                        class="first-button">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/order-btns-add-button.png" alt="" />
                </button>
                <button type="button" id = "add-pizza-and-continue-to-drinks" onclick = "checkBundlesForPizzas()" ><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/order-btns-drink-button.png" alt="" /></button>
            </div>
            <?php } ?>
        </div>
        <?php /* ------- /Pizza ------- */ ?>

<!--    </div>-->
<!--</form>-->

<script type="text/javascript">
    jQuery.noConflict();

    (function( $ ) {
        // ******* Pizza components sections
        $('body').on('click', '.pz-components-title', function(){
            var $self = $(this);
            if($('.pz-components.' + $self.data('section-title')).hasClass('open')){
                $('.pz-components.' + $self.data('section-title')).slideUp(400, function(){
                    $('.pz-components.' + $self.data('section-title')).removeClass('open').css({'display':''});
                });
            }else{
                if($('.pz-components.open').length > 0){
                    $($('.pz-components.open')[0]).slideUp(400, function(){
                        $('.pz-components').removeClass('open').css({'display':''});
                        $('.pz-components.' + $self.data('section-title')).slideDown(400, function(){
                            $('.pz-components.' + $self.data('section-title')).addClass('open').css({'display':''});
                        });
                    });
                }else{
                    $('.pz-components.' + $self.data('section-title')).slideDown(400, function(){
                        $('.pz-components.' + $self.data('section-title')).addClass('open').css({'display':''});
                    });
                }
            }
        });
        // ******* /Pizza components sections

        // ******* Radio buttons click
        $('body').on('click', 'input.component-selection', function(){
            var $self = $(this);
            $(".pizza-layer." + $self.data("layer-name")).toggle();     //TOGGLES LAYERS
            var radioName = $self.attr('name');
            var component = $self.data('component');
            if ($self.data('waschecked') == true)
            { 
                $self.prop('checked', false);
                $('input[name="' + radioName + '"]').data('waschecked', false);                
            }
            else {
                $('input[name="' + radioName + '"]').data('waschecked', false);
                $self.data('waschecked', true);
                
            }

            // reset quarters
            $('.quarters-group_' + component).prop('checked', false);
            $('.quarter-' + radioName).css({'background-position':'-52px 0'});

            var selectedComponents = '';
            for(var i = 0; i < $('input.component-selection').length; i++){
                if($($('input.component-selection')[i]).is(':checked')){
                    selectedComponents += $($('input.component-selection')[i]).attr('name') + '-' + $($('input.component-selection')[i]).val() + ' ';
                }
            }

            var $sel;
            var $pzLayer;
            $("input.component-selection").each(function(){
                $sel = $(this);
                $pzLayer = $(".pizza-layer." + $sel.data("layer-name"));
                if($sel.data('waschecked') == true){
                    $pzLayer.fadeIn();
                }else{
                    $pzLayer.fadeOut();
                }
            });
            for(var i = 0; i < jQuery("input.component-selection-qgroup").length; i++){
                $sel = jQuery(jQuery("input.component-selection-qgroup")[i]);
                $pzLayer = jQuery(".pizza-layer." + $sel.data("layer-name"));
                if($sel.is(':checked')) {
                    $pzLayer.fadeIn();
                }else{
                    $pzLayer.fadeOut();
                }
            }
        });
        // ******* /Radio buttons click

        // ******* Check boxes (quarters)
        $('body').on('click', 'input.component-selection-qgroup', function(){
            var $self = $(this);
            var radioName = $self.attr('name');
            var component = $self.data('component');
            $('.component-selection-' + component).prop('checked', false).data('waschecked', false);
            var quarters = '';
            for(var i = 0; i < $('.quarters-group_' + component).length; i++){
                if($($('.quarters-group_' + component)[i]).is(':checked')){
                    quarters += '1';
                }else{
                    quarters += '0';
                }
            }

            var $quarterLabel = $('.quarter-' + radioName);
            switch(quarters){
                // from the left to right: NW - SW - SE - NE
                case '0000': $quarterLabel.css({'background-position':'-52px 0'}); break;   //blank
                case '1000': $quarterLabel.css({'background-position':'-78px 0'}); break;   //NW
                case '0100': $quarterLabel.css({'background-position':'-390px 0'}); break;   //SW
                case '0010': $quarterLabel.css({'background-position':'-338px 0'}); break;   //SE
                case '0001': $quarterLabel.css({'background-position':'-286px 0'}); break;   //NE
                case '1001': $quarterLabel.css({'background-position':'-572px 0'}); break;   //NW+NE
                case '0011': $quarterLabel.css({'background-position':'-182px 0'}); break;   //NE+SE
                case '0110': $quarterLabel.css({'background-position':'-598px 0'}); break;   //SE+SW
                case '1100': $quarterLabel.css({'background-position':'-130px 0'}); break;   //NW+SW
                case '0111': $quarterLabel.css({'background-position':'-494px 0'}); break;   //BLANK - NW
                case '1110': $quarterLabel.css({'background-position':'-416px 0'}); break;   //BLANK - NE
                case '1101': $quarterLabel.css({'background-position':'-442px 0'}); break;   //BLANK - SE
                case '1011': $quarterLabel.css({'background-position':'-468px 0'}); break;   //BLANK - SW
                case '1010': $quarterLabel.css({'background-position':'-520px 0'}); break;   //NW+SE
                case '0101': $quarterLabel.css({'background-position':'-546px 0'}); break;   //NE+SW
                case '1111': $quarterLabel.css({'background-position':'-234px 0'}); break;   //The whole pizza
            }

            showLayers();
        });
        // ******* /Check boxes (quarters)

        // ******* showLayers() function
        function showLayers(){
            var $sel;
            var $pzLayer;
            for(var i = 0; i < jQuery("input.component-selection").length; i++){
                $sel = jQuery(jQuery("input.component-selection")[i]);
                $pzLayer = jQuery(".pizza-layer." + $sel.data("layer-name"));
                if($sel.data('waschecked') == true){
                    $pzLayer.fadeIn();
                }else{
                    $pzLayer.fadeOut();
                }
            }
            for(var i = 0; i < jQuery("input.component-selection-qgroup").length; i++){
                $sel = jQuery(jQuery("input.component-selection-qgroup")[i]);
                $pzLayer = jQuery(".pizza-layer." + $sel.data("layer-name"));
                if($sel.is(':checked')) {
                    $pzLayer.fadeIn();
                }else{
                    $pzLayer.fadeOut();
                }
            }
        }
        // ******* /showLayers() function

        // ******* Opens/hides the quarters group
        $('body').on('click', '.quarter-select', function(e){
            e.stopPropagation();
            $self = $(this);
            if($('.quarter-' + $self.data('selection-quarter')).hasClass('quarters-visibility')){
                $('.quarters-group').addClass('quarters-visibility');
                $('.quarter-' + $self.data('selection-quarter')).removeClass('quarters-visibility');
            }else{
                $('.quarter-' + $self.data('selection-quarter')).addClass('quarters-visibility');
            }
        });

        $('body').on('click', function(){
            $('.quarters-group').addClass('quarters-visibility');
        });
        // ******* /Opens/hides the quarters group

        $(document).ready(function(){
            // ******** Restores the selections status for the quarters groups
            for(var i = 0; i < $('.quarter.quarter-select').length; i++){
                var component = $($('.quarter.quarter-select')[i]).data('selection-quarter');
                var quarters = '';
                for(var j = 0; j < $('.quarters-group_' + component).length; j++) {
                    if($($('.quarters-group_' + component)[j]).is(':checked')){
                        quarters += '1';
                    }else{
                        quarters += '0';
                    }
                }
                switch(quarters){
                    // from the left to right: NW - SW - SE - NE
                    case '0000': $($('.quarter.quarter-select')[i]).css({'background-position':'-52px 0'}); break;   //blank
                    case '1000': $($('.quarter.quarter-select')[i]).css({'background-position':'-78px 0'}); break;   //NW
                    case '0100': $($('.quarter.quarter-select')[i]).css({'background-position':'-390px 0'}); break;   //SW
                    case '0010': $($('.quarter.quarter-select')[i]).css({'background-position':'-338px 0'}); break;   //SE
                    case '0001': $($('.quarter.quarter-select')[i]).css({'background-position':'-286px 0'}); break;   //NE
                    case '1001': $($('.quarter.quarter-select')[i]).css({'background-position':'-572px 0'}); break;   //NW+NE
                    case '0011': $($('.quarter.quarter-select')[i]).css({'background-position':'-182px 0'}); break;   //NE+SE
                    case '0110': $($('.quarter.quarter-select')[i]).css({'background-position':'-598px 0'}); break;   //SE+SW
                    case '1100': $($('.quarter.quarter-select')[i]).css({'background-position':'-130px 0'}); break;   //NW+SW
                    case '0111': $($('.quarter.quarter-select')[i]).css({'background-position':'-494px 0'}); break;   //BLANK - NW
                    case '1110': $($('.quarter.quarter-select')[i]).css({'background-position':'-416px 0'}); break;   //BLANK - NE
                    case '1101': $($('.quarter.quarter-select')[i]).css({'background-position':'-442px 0'}); break;   //BLANK - SE
                    case '1011': $($('.quarter.quarter-select')[i]).css({'background-position':'-468px 0'}); break;   //BLANK - SW
                    case '1010': $($('.quarter.quarter-select')[i]).css({'background-position':'-520px 0'}); break;   //NW+SE
                    case '0101': $($('.quarter.quarter-select')[i]).css({'background-position':'-546px 0'}); break;   //NE+SW
                    case '1111': $($('.quarter.quarter-select')[i]).css({'background-position':'-234px 0'}); break;   //The whole pizza
                }
            }
            // ******** /Restores the selections status
            var $sl;
            var $pzLayer;
            $("input.component-selection, input.component-selection-qgroup").each(function(){
                $sl = $(this);
                $pzLayer = $(".pizza-layer." + $sl.data("layer-name"));
                if($sl.is(':checked')){
                    $pzLayer.show();
                }else{
                    $pzLayer.hide();
                }
            });
        });
        
        
    
    
        
        
    console.log("checking again");
    
        
    })( jQuery );
</script>
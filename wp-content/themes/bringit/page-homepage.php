<?php
/**
Template Name: BringIt Home page
 */
get_header(); ?>

<div class="bi-section">
    <?php the_hp_title(); ?>
    <div class="bi-container clearfix">

        <div class="hp-pane pizza">
            <img src="<?php bloginfo('stylesheet_directory') ?>/assets/hp-make-pizza-title.png" alt="" class="gr-title" />
            <div id="pizzaViewport">
                <img src="<?php bloginfo('stylesheet_directory') ?>/assets/hp-make-pizza.png" alt="" class="gr-title" />
                <button class="make-pizza btn-make-pizza"></button>
            </div>
        </div>
        
        <?php // This block is showing a list of three most recent Bundles. ?>
        <div class="hp-pane specials">
            <img src="<?php bloginfo('stylesheet_directory') ?>/assets/hp-specials-title.png" alt="" class="gr-title" />    
            <?php $bundles = Bundle::getLatestBundles(3); ?>
            <?php foreach($bundles as $bundle): ?>
            <a>
            <?php echo the_loading_animation(); ?>
                <div class="special-box" 
                     bundle-description = "<?php echo $bundle->getDescription(); ?>" 
                     bundle-pizzas = "<?php echo $bundle->getPizzas(); ?>" 
                     bundle-addons = "<?php echo $bundle->getAddons(); ?>" 
                     bundle-drinks = "<?php echo $bundle->getDrinks(); ?>"  
                     bundle-WPID = "<?php echo $bundle->getWPID(); ?>" 
                     bundle-ID = "<?php echo$bundle->getID(); ?>">
                    <div class="content">
                        <div class="price">
                            <span class="nis">&#8362;</span><?php echo $bundle->getLowestPrice(); ?><span class="prefix">החל מ</span>
                        </div>
                        <p class="addson">
                            <?php //        echo $bundle->description; ?>
                            <?php echo $bundle->getPizzas(); ?>
                            פיצות משפחתיות + 
                            <?php echo $bundle->getDrinks(); ?>
                            בקבוקי שתייה + 
                            <?php echo $bundle->getAddons(); ?>
                            תוספות 

                        </p>
                    </div>
                    <img src="<?php echo $bundle->getImage(); ?>" alt="" />
                </div>
            </a>
        <?php endforeach; ?>

        </div>
        <?php // end of Bundles Block ?>
        
    </div>
</div>
    
<div id="addressModalContent" class="mfp-hide">
    <ul class="address-tabs-row clearfix">
        <li class="shipping" data-tab-content="shipping-form">משלוח</li>
        <li class="pick-up" data-tab-content="pick-up-form">איסוף עצמי</li>
        <li class="tail"></li>
    </ul>

    <div id="tabsContent" class="tabs-content"></div>
</div>



<div class="bi-section hp-bottom">
    <?php if (!isset($_GET['pizzeria'])): ?>
    <div class="bi-container enlarged clearfix">
        <div class="bottom-title">
            <img src="<?php bloginfo('stylesheet_directory') ?>/assets/hp-bottom-title.png" alt="" class="title" />
        </div>

        <div class="pizzerias">
            <?php  the_homepage_pizzerias(); ?>
        </div>
    </div>
    <?php endif; ?>
</div>

<?php get_footer(); ?>

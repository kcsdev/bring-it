<?php 
/*
 * Sing in popup
 * When clicking Singin/Signup (on any page) - header
 * */
?>
<div class="sign-in-form-modal-container clearfix">
    <div class="inner">
        <?php the_loading_animation(); ?>
        <div class="login-pane login">
            <p class="title">לקוח קיים</p>
            <div class="form-wrapper">
                <form onsubmit="return validateSignIn(this);">
                    
                    <label class="clearfix label-uname">
                        שם המשתמש:
                        <input type="text" name="uname" />
                    </label>
<!--                    <p class="field-info">כתובת הדוא&quot;ל שלך</p>-->

                    <label class="clearfix label-pwd">
                        סיסמה:
                        <input type="password" name="pwd" />
                    </label>


                    <label class="no-border clearfix">
                        <button type="submit"></button>
                    </label>
                </form>
            </div>

<!--            <a href="javascript:void(0);" class="guest-entry">המשך כאורח</a>-->
        </div>

        <div class="login-pane register">
            <p class="title">לקוח חדש</p>
            <div class="form-wrapper">
                <form onsubmit="return validateSignUp(this);">
                    <label class="clearfix label-uname">
                        שם משתמש:
                        <span class="mandatory-field">*</span>
                        <input type="text" name="uname" />
                    </label>
                    <label class="clearfix label-email">
                        כתובת הדוא&quot;ל:
                        <span class="mandatory-field">*</span>
                        <input type="email" name="email" />
                    </label>
<!--                    <p class="field-info">כתובת הדוא&quot;ל שלך תשמש כשם משתמש לכניסה</p>-->

                    <label class="clearfix label-pwd">
                        סיסמה:
                        <span class="mandatory-field">*</span>
                        <input type="password" name="pwd" />
                    </label>
                    <p class="field-info">שמונה תווים לפחות</p>

                    <label class="clearfix label-pwd-cnf">
                        חזור על סיסמה:
                        <span class="mandatory-field">*</span>
                        <input type="password" name="pwdCnf" />
                    </label>

                    
                    <p class="field-info">במידה ונצטרך ליצור קשר עימך</p>

                    <label class="no-border clearfix">
                        <button type="submit"></button>
                    </label>
                </form>
            </div>
        </div>

        <script type="text/javascript">
            function validateSignIn(theForm){
                jQuery(".login-pane.login label").removeClass("error");
                var formData = {
                    "uname": jQuery.trim(theForm.uname.value + ""),
                    "pwd": jQuery.trim(theForm.pwd.value + "")
                };
                var requestIsValid = true;

                if(formData.uname.length == 0){
                    jQuery(".login-pane.login label.label-uname").addClass("error");
                    requestIsValid = false;
                }
                if(formData.pwd.length == 0){
                    jQuery(".login-pane.login label.label-pwd").addClass("error");
                    requestIsValid = false;
                }

                if(requestIsValid){
                    jQuery.ajax({
                        type: 'POST',
                        url: "/wp-admin/admin-ajax.php",       // *** SHOULD BE CHANGED TO THE REAL URL
                        data: "action=custom_login_action&username="+formData.uname+"&password="+formData.pwd,
                        success: function (data) { 
                            var result = JSON.parse(data);
                            if(result.is_validated == true){
                                location.reload();    
                            }else{
                                jQuery(".login-pane.login label.label-uname").addClass("error");
                                jQuery(".login-pane.login label.label-pwd").addClass("error");
                            }
                            
                        },
                        error: function (xhr, status, error) {
                        },
                        complete: function (xhr, status) { 
                            <?php //if(isset($_GET['target']) && !empty($_GET['target']) && $_GET['target'] == 'delivery') { ?>
                            //jQuery(".sign-in-form-modal-container .inner").load("/pv-hp-shipping-popup/?type=shipping-form");
                            <?php// } else { ?>
                            //jQuery.magnificPopup.close();
                            <?php // } ?>
                        },
                        dataType: 'text',
                        cache: false
                    });
                }
                return false;
            }

            function validateSignUp(theForm){
                jQuery(".login-pane.register label").removeClass("error");
                var formData = {
                    "uname": jQuery.trim(theForm.uname.value + ""),
                    "pwd": jQuery.trim(theForm.pwd.value + ""),
                    "pwdCnf": jQuery.trim(theForm.pwdCnf.value + ""),
                    "email": jQuery.trim(theForm.email.value + "")
                };
                var requestIsValid = true;

                if(formData.uname.length == 0){
                    jQuery(".login-pane.register label.label-uname").addClass("error");
                    requestIsValid = false;
                }
                if(formData.pwd.length == 0){
                    jQuery(".login-pane.register label.label-pwd").addClass("error");
                    requestIsValid = false;
                }

                if(formData.pwdCnf.length == 0){
                    jQuery(".login-pane.register label.label-pwd-cnf").addClass("error");
                    requestIsValid = false;
                }

                if(formData.email.length == 0){
                    jQuery(".login-pane.register label.label-email").addClass("error");
                    requestIsValid = false;
                }

                if(formData.pwd.length > 0 && formData.pwdCnf.length > 0 && formData.pwd != formData.pwdCnf){
                    jQuery(".login-pane.register label.label-pwd-cnf").addClass("error");
                    requestIsValid = false;
                }
                console.log(requestIsValid);
                if(requestIsValid){ 
                    jQuery("#loading-animation").show();
                    jQuery.ajax({
                        type: 'POST',
                        url: "/wp-admin/admin-ajax.php",       // *** SHOULD BE CHANGED TO THE REAL URL
                        data: "action=registration_action&username="+formData.uname+"&password="+formData.pwd+"&re_password="+formData.pwdCnf+"&email="+formData.email,
                        success: function (data, status, xhr) { 
                            console.log(data);
                            console.log("Registration action is now in action! Function initation comes from 'page-pv-sign-in.php'!")
                        },
                        error: function (xhr, status, error) {
                        },
                        complete: function (xhr, status) {
                            <?php //if(isset($_GET['target']) && !empty($_GET['target']) && $_GET['target'] == 'delivery') { ?>
//                            jQuery(".sign-in-form-modal-container .inner").load("/pv-hp-shipping-popup/?type=shipping-form");
                            <?php // } else { ?>
//                            jQuery.magnificPopup.close();
                            <?php //} ?>
                            jQuery("#loading-animation").hide();
                            location.href = "/";
                        },
                        dataType: 'text',
                        cache: false
                    });
                }
                return false;
            }
        </script>
    </div>
</div>

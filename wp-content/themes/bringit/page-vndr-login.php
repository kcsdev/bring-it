<?php
get_header('vendor-login');
//Auth::vendorCheck();
?>

<ul class="h-section login-tabs clearfix">
    <li class="current"><a href="javascript:void(0);">עסקים רשומים</a></li>
    <li><a href="javascript:void(0);">רשום עסק חדש</a></li>
</ul>
<div class="h-section content-pane login down-space clearfix">
    <?php the_loading_animation(); ?>
    <div class="right-pane">
        <form action="/vndr-order">
            <ul class="sign-in-form">
                <li class="field"><input type="text" name="username" id="username" placeholder="שם המשתמש" /></li>
                <li class="field"><input type="password" name="password" id="password" placeholder="סיסמה" /></li>

                <li class="button"><button id = "vndr-login" type="submit">כניסה</button> </li>
            </ul>
        </form>
    </div>

    <div class="left-pane">
        <iframe width="420" height="315" src="https://www.youtube.com/embed/bDv4H4vDAwg" frameborder="0" allowfullscreen></iframe>
        <p>מי אנחנו ולמה כדאי לעבוד אתנו?</p>
    </div>
</div>

    <script type="text/javascript">
        jQuery(document).ready(function($){ 
            $("#vndr-login").bind("click",function(event){
                event.preventDefault();
                
                $("#loading-animation").show();
                
                var login = {};
                login.username = $("input[name=username]").val();
                login.password = $("input[name=password]").val();
                
                $.ajax({
                    type:"post",
                    url:"/wp-admin/admin-ajax.php",
                    data:"action=vndr_login_action&username=" + login.username + "&password=" + login.password,
                    success: function(data){  
                        console.log(data);
                        var login = JSON.parse(data.trim());
                        
                        console.log(login);
                        
                        if(login.status === true){
                            window.location.href = "/vndr-landing";
                        }else{
                            $("#username").css("box-shadow", "0 0 3px 1px red");
                            $("#password").css("box-shadow", "0 0 3px 1px red");
                        }
                    },
                    complete: function(){
                        $("#loading-animation").hide();
                    }
                });
                
            });
            $("#userName").val("");
        });
    </script>
<?php
get_footer('vendor');
?>
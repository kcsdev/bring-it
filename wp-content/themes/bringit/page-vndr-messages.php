<?php
get_header('vendor');
?>
    <div class="h-section down-space">
        <ul class="breadcrumbs">
            <li class="current"><a>הודעות</a></li>
        </ul>
    </div>

    <div class="h-section content-pane down-space">
        <!-- Message -->
        <div id="msgBox2015" class="inner-block down-space msg-new">
            <span class="msgdate">24/12/2015 05:56</span>
            חוקרים בחברת האבטחה Palo Alto networks, שמייסדה הוא הישראלי ניר צוק, הודיעו כי גילו את "גניבת חשבונות אפל הגדולה ביותר מאז ומעולם שנגרמה על ידי תוכנה". תוכנה זדונית שכונתה על ידי החוקרים KeyRaider הצליחה לגנוב פרטי חשבונות אפל של כ-250 אלף משתמשים ב-18 מדינות, ביניהן סין, צרפת, רוס
            <p class="align-left"><label for="chkMsg2015" class="v-align-middle">
                    ראיתי את ההודעה
                    <input type="checkbox" id="chkMsg2015" name="chkMsg2015" data-message-id="2015" class="v-align-middle" />
                </label></p>
        </div>
        <!-- End of Message -->

        <!-- Message -->
        <div id="msgBox2014" class="inner-block down-space msg-new">
            <span class="msgdate">21/02/2015 16:43</span>
            בקרוב אצלכם: הכתבת הודעות בעברית במכשירי אפל
            <p class="align-left"><label for="chkMsg2014" class="v-align-middle">
                    ראיתי את ההודעה
                    <input type="checkbox" id="chkMsg2014" name="chkMsg2014" data-message-id="2014" class="v-align-middle" />
                </label></p>
        </div>
        <!-- End of Message -->

        <!-- Message -->
        <div class="inner-block down-space">
            <span class="msgdate">24/12/2015 05:56</span>
            חוקרים בחברת האבטחה Palo Alto networks, שמייסדה הוא הישראלי ניר צוק, הודיעו כי גילו את "גניבת חשבונות אפל הגדולה ביותר מאז ומעולם שנגרמה על ידי תוכנה". תוכנה זדונית שכונתה על ידי החוקרים KeyRaider הצליחה לגנוב פרטי חשבונות אפל של כ-250 אלף משתמשים ב-18 מדינות, ביניהן סין, צרפת, רוס
        </div>
        <!-- End of Message -->

        <!-- Message -->
        <div class="inner-block down-space">
            <span class="msgdate">21/02/2015 16:43</span>
            בקרוב אצלכם: הכתבת הודעות בעברית במכשירי אפל
        </div>
        <!-- End of Message -->
    </div>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $(".msg-new input[type='checkbox']").bind("click", function(){
            var messageId = $(this).data('message-id');
            $("#msgBox" + messageId + " p").slideUp(600, function(){
                $("#msgBox" + messageId).removeClass("msg-new");
                $("#msgBox" + messageId + " p").remove();
            });
        });
    });
</script>
<?php
get_footer('vendor');
?>
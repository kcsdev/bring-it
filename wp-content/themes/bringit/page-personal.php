<?php
/**
Template Name: Personal Area page
 */
get_header();

if(!Auth::check()){
    header("location:/");
}
echo get_field("phone","user_" . $ID);
?>

    <div class="bi-section">
        <div id="pageContentWrapper" class="bi-container clearfix">
			
			<div class="overlay-mask">
				<div class="iframe-wrapper">
					<iframe id="pzIframe"></iframe>
				</div>
			</div>
			
            <div class="personal-title">
                <div class="pizzeria-name">איזור אישי</div>
            </div>

			<ul class="personal-left-panel">
				<li class="messages clearfix">
					<button type="button">2</button>
					<a>הודעות</a>
                </li>
<!--
				<li class="coupons">
					<p>קופונים שלי</p>
					<ul>
						<li>
							קיבלת קופון ל- לחם שום מ tata pizza  שים לב, הקופון תקף לחודשיים מיום קבלת הקופון.   קוד הקופון:
							<span>xxxxxxx</span>
						</li>
						<li>
							קיבלת קופון ל- לחם שום מ tata pizza  שים לב, הקופון תקף לחודשיים מיום קבלת הקופון.   קוד הקופון:
							<span>xxxxxxx</span>
						</li>
						<li>
							קיבלת קופון ל- לחם שום מ tata pizza  שים לב, הקופון תקף לחודשיים מיום קבלת הקופון.   קוד הקופון:
							<span>xxxxxxx</span>
						</li>
					</ul>
				</li>
-->
			</ul>
			
			<ul class="personal-right-panel">
                <li class="clearfix">
                    <button type="button" id = "personal-area-addr-list" >&gt;&gt;</button>
                    <a id = "personal-area-addr-list-link">רשימת כתובות</a>
                </li>
                <li class="clearfix">
                    <button type="button" id = "personal-area-payment-types" >&gt;&gt;</button>
                    <a>אמצעי תשלום</a>
                </li>
                <li class="clearfix">
                    <button type="button" id = "personal-area-info" >&gt;&gt;</button>
                    <a id = "personal-area-info-link" >פרטים אישיים</a>
                </li>
                <li class="clearfix">
                    <button type="button" id = "personal-area-orders-history" >&gt;&gt;</button>
                    <a id = "personal-area-orders-history-link">היסטוריית הזמנות</a>
                </li>
            </ul>
			
        </div>
    </div>

    <script type="text/javascript">
        jQuery(function ($) {
			
			$(window).bind("resize", function(){
				if($(".overlay-mask").is(":visible")){
					$(".iframe-wrapper").height($(window).height() - 120);
				}
			})
        });
		
		function closeMe(){
			jQuery("#pzIframe").attr("src","about:blank");
			jQuery(".overlay-mask").fadeOut();
		}
    </script>

<?php get_footer(); ?>
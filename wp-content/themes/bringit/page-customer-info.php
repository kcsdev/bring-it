<?php
/**
Template Name: Purchase Address List page
 */
$customer = new Customer(); 
$orders = $customer->getOrders();
get_header('blank'); ?>
<?php //var_dump($customer); ?>
    <div class="bi-section">
        <a class = "close-window" onclick="parent.closeMe();"></a>
        <div class="bi-container addr-list clearfix">

            <p class="pizzeria-name">פרטים אישיים</p>

            <div class="list-title"></div>
            <div class="addr-form-pane">
                <div class="form-wrapper">
                    <form>
                        <label class="clearfix label-fname">
                                    שם פרטי:
                            <input type="text"
                                   value = "<?php echo $customer->getFirstName(); ?>"
                                   name="fname"  /> <?php // first name ?>
                        </label>
                        
                        <label class="clearfix label-lname">
                                    שם משפחה:
                            <input type="text"
                                   value = "<?php echo $customer->getLastName(); ?>"
                                   name="lname"  /> <?php // first name ?>
                        </label>
                        
                        <label class="clearfix label-fname">
                                    כתובת אלקטרונית:
                            <input type="email" 
                                   value = "<?php echo $customer->getEmail(); ?>"
                                   name="email" /> <?php // email ?>
                        </label>
                        
                        <label class="clearfix label-username">
                                    שם משתמש:
                            <input type="text" 
                                   style = "background-color:gray";
                                   value = "<?php echo $customer->getUsername(); ?>"
                                   name="username" readonly  /> <?php // username ?>
                        </label>
                        
                        <label class="clearfix label-phone">
                                    מספר טלפון:
                            <input type="text"
                                   value = "<?php echo $customer->getPhone(); ?>"
                                   name="phone" /> <?php // phone ?>
                        </label>
                        <a class = "save-customer-info">שמור</a>                    
<!--
                        <label class="clearfix label-password">
                                    סיסמא:
                            <input type="text" 
                                   value = "<?php // echo $customer->getPassword(); ?>"
                                   name="password" /> <?php // password ?>
                        </label>
-->
                        
                    </form>
                </div>
            </div>

        </div>
    </div>

<script type="text/javascript">
    jQuery(document).ready(function($){
       $(".save-customer-info").click(function(e){ 
           
           e.preventDefault();
           
           var fname = $("input[name=fname]").val();
           var lname = $("input[name=lname]").val();
           var phone = $("input[name=phone]").val();
           var email = $("input[name=email]").val();
           
           var customer = {
               fname:fname,
               lname:lname,
               phone:phone,
               email:email
           }
           
           var jsonCustomer = JSON.stringify(customer);
           
           $.ajax({
               type:"post",
               url:"/wp-admin/admin-ajax.php",
               data: "action=update_customer_action&customer="+jsonCustomer,
               success:function(data){
                   console.log(data);
               },
               error:function(){
                   
               },
               complete:function(){
                    location.reload();
               }
           });
       });
    });
</script>
<?php get_footer('blank'); ?>
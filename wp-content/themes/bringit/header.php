<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "#main" div.
 *
 * @package BringIt
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php wp_head(); ?>
    <script src = "/wp-content/themes/bringit/js/personal-area/personal-area.js" type = "text/javascript"></script>
    <link rel="stylesheet" href="/wp-content/themes/bringit/css/form-fields.css">
</head>
<?php Cart::Create(); ?>
<?php Order::Create(); // var_dump($_SESSION['Order']); ?>
    
<body <?php body_class(); ?> >
<?php //Order::DEBUG(); ?>
<div class="bi-section page-header">
    <div class="bi-container enlarged">
        <a href="/"><img src="<?php bloginfo('stylesheet_directory') ?>/assets/bringit-top-logo.png" class="top-logo" alt="" /></a>

        <ul class="personal">
            <?php if ( is_user_logged_in() ): ?>
            <?php $customer = new Customer(); ?>
            <li>
                שלום,
                <?php echo $customer->getFirstName(); ?>
                <?php echo $customer->getLastName(); ?>
            </li>
            <li>/</li>
            <li><a href="/personal-area/">איזור אישי</a></li>
            <li>/</li>
            <li><a href="<?php echo wp_logout_url('/'); ?>">התנתק</a></li>
            <?php else: ?>
            <li><a href="/pv-sign-in/" class="jq-show-login">כניסה</a></li>
            <li>/</li>
            <li><a href="/pv-sign-in/" class="jq-show-login">הרשמה</a></li>
            <?php endif; ?>
        </ul>
        
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function($){
        $('.jq-show-login').magnificPopup({
            type: 'ajax',
            closeOnBgClick: false,
            closeBtnInside: true
        });
    });
</script>

<div id="main">

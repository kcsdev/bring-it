<?php
/**
Template Name: Delivery page
 */
get_header(); ?>
<?php $vendor = new Vendor($_SESSION["vendor"]); ?>
<div class="bi-section">
    <div class="bi-container delivery">
        <img src="<?php bloginfo('stylesheet_directory') ?>/assets/delivery-page-title.png" alt="" class="delivery-title" />

        <ul class="map clearfix">
            <li class="photo">
                <img src="<?php bloginfo('stylesheet_directory') ?>/assets/temp/pz-photo.png" class="pz-photo" alt="" />
            </li>

            <li class="pizza-name">
                <p class="name">
                    <?php echo $vendor->getName(); ?>
                </p>
                <p class="phone">
                    <?php echo $vendor->getPhone(); ?>
                </p>
            </li>

            <li class="map-cell">
                <img src="<?php bloginfo('stylesheet_directory') ?>/assets/temp/waze-map.png" class="waze-map" alt="" />
            </li>
        </ul>
    </div>
<?php // Classes: ?>
<?php   // passed ?>
<?php   // current ?>
    <div class="bi-container">
        <ul class="tracker clearfix">
            <li class="current">הזמנתך התקבלה</li>
            <li>הפיצה בהכנה</li>
            <li>הפיצה בתנור</li>
            <li>בדרך אליך</li>
        </ul>
		
		<!-- <img src="<?php bloginfo('stylesheet_directory') ?>/assets/temp/tracking-numbers.png" class="tracking-nums" /> -->
		<div class="coupon">
			<a href="javascript:void(0);"><img src="<?php bloginfo('stylesheet_directory') ?>/assets/temp/coupon.png" class="coupon" alt="" /></a>
		</div>
		
    </div>
</div>

<?php get_footer(); ?>
<?php
get_header('vendor');
$bundles = Bundle::getBundles();
?>
    <div class="h-section down-space">
        <ul class="breadcrumbs">
            <li><a>מבצעים</a></li>
            <li class="current"><a>מבצעים שלי</a></li>
        </ul>
    </div>

    <div class="h-section content-pane down-space">
        <form onsubmit="return false;">
            <div class="inner-block down-space">
                לחץ על תוספות שאינך מעוניין לכלול במבצע. תוספות שלא יכללו במבצע, מחיר התוספת יתווסף לעלות המבצע.
            </div>
            
            <?php foreach($bundles as $bundle): ?>
            <?php if($bundle->inStock()): ?>
                <?php $checked = "checked"; ?>
                <?php $active = "active"; ?>
            <?php else: ?>
                <?php $active = "";?>
                <?php $checked = ""; ?>
            <?php endif; ?>
            <?php $price = $bundle->getVendorsPrice(); ?>
            <div class="inner-block down-space clearfix">
                <a class="special-block <?php echo $active; ?> " id="<?php echo $bundle->getWPID(); ?>-bundle" data-special-id="<?php echo $bundle->getWPID(); ?>">
                    <?php echo $bundle->getDescription(); ?>
                </a>
                <div class="special-data-block" id="<?php echo $bundle->getWPID(); ?>">
                    <ul class="info-row">
                        <li>
                            <label class="v-align-middle">
                                <input type="checkbox" 
                                       data-special-title="<?php echo $bundle->getWPID(); ?>-bundle" 
                                       class="v-align-middle bundle-in-stock" 
                                       id = "<?php echo $bundle->getWPID(); ?>-in-stock"
                                       <?php echo $checked; ?> />
                                אני משתתף במבצע
                            </label>
                        </li>
                        <li>
                            <label class="v-align-middle">
                                קבע מחיר להזמנה:
                                <input type="text" 
                                       bundle-WPID = "<?php echo $bundle->getWPID(); ?>"
                                       id = "<?php echo $bundle->getWPID(); ?>-price"
                                       value = "<?php echo $price; ?>"
                                       class="v-align-middle bundle-price" />
                            </label>
                        </li>
                    </ul>
                </div>
            </div>

            <?php endforeach; ?>

            <div class="clearfix">
                <button type="submit" class="list-save" id = "vndr-specials-mine-save">שמירה</button>
            </div>
        </form>
    </div>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $(".special-block").bind("click", function(){
            var $self = $(this);
            $("#" +  $self.data("special-id")).toggle(400);
        });

        $('.special-data-block .adds-on span').bind('click', function(){
            $(this).toggleClass('selected');
        })

        $('.special-data-block input[type="checkbox"]').bind('click', function(){
            var $self = $(this);
            console.log($self.data('special-title'));
            console.log($self.prop('checked'));
            var $obj = $('#' + $self.data('special-title'));
            console.log($obj);
            if($self.prop('checked')){
                $obj.addClass('active');
            }else{
                $obj.removeClass('active');
            }
        })
    });
</script>
<?php
get_footer('vendor');
?>
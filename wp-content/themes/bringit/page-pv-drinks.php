<?php $drinks = Drink::getAll(); ?>
<?php foreach($drinks as $drink): ?>
<div class="item">
    <img src="<?php echo $drink->getImage(); ?>" alt="" />
    <p class="name"><?php echo $drink->getTitle(); ?></p>
    <p class="descr"><?php echo $drink->getDescription(); ?></p>
    <div class="bottom">
        <button onclick = "addDrinkToCart(event)" type="button" class="btn-add-drink" drink-id = "<?php echo $drink->getID(); ?>" drink-WPID = "<?php echo $drink->getWPID(); ?>" drink-name = "<?php echo $drink->getName(); ?>">הוסף</button>
    </div>
</div>
<?php endforeach; ?>
<?php 
/*

Template Name: Addons page
 */
get_header(); ?>

    <div class="bi-section">
        <div class="bi-container clearfix drinks">
            <div class="overlay-mask">
                <div class="iframe-wrapper">
                    <img src="<?php bloginfo('stylesheet_directory') ?>/assets/pop-up-close.png" alt="" class="close-iframe" />
                    <iframe id="pzIframe"></iframe>
                </div>
            </div>
            <div class="mask">
                
            </div>
            
            <div class="drinks-pane clearfix">
                <div class="title">
                    <img src="<?php bloginfo('stylesheet_directory') ?>/assets/title-addson-pop-up.png" alt="" />
                </div>
<!--                <iframe id="pzIframe"></iframe>-->
                <div id="drinkTiles" class="drink-tiles"></div>
            </div>

            <div class="drinks-left-sidebar">
                <div class="toggle-button">&lt;&lt;</div>
                <img src="<?php bloginfo('stylesheet_directory') ?>/assets/bike.png" alt="" class="bike" />
                <div class="shopping-cart">
                    <div id="shoppingCartContent"></div>
                    
                </div>
                <button type="button" class="checkout"></button>
            </div>

        </div>


    </div>
<?php 
    $link = "purchase-sign-in";
if(!Auth::check()){
    $link = "purchase-sign-in";
}else{
    $link = "pv-order-submition";
}
?>
    <script type="text/javascript">
        jQuery(function ($) {
           
//
            $(".drinks-left-sidebar .toggle-button").bind("click", function(){
                $self = $(this);
                var objMenu = $(".drinks-left-sidebar");
                if($self.hasClass("open")) {
                    objMenu.animate({"left":"-310px"}, 200, function(){
                        $(".bi-section .bi-container.drinks .mask").fadeOut(200, function(){
                            $self.removeClass("open").text('<<');
                            objMenu.css({"left":""});
                        });
                    })
                } else {
                    $(".bi-section .bi-container.drinks .mask").fadeIn(200, function(){
                        objMenu.animate({"left":"0px"}, 200, function(){
                            $self.addClass("open").text('>>');
                        });
                    });
                }
            });

            $(document).ready(function(){
                $("#drinkTiles").load("/pv-addons/");
                $("#shoppingCartContent").load("/pv-shopping-cart-content/");
            });
            
            
            $(".checkout").bind("click", function(){
                $(".iframe-wrapper").height($(window).height() - 90);
                $("#pzIframe").attr("src",'<?php echo $link; ?>');
                $(".overlay-mask").fadeIn();
            });
            $("img.close-iframe").bind("click", function(){
                $("#pzIframe").attr("src","");
                $(".overlay-mask").fadeOut();
            });
        });
        
        
        
        
    </script>
<?php get_footer(); ?>
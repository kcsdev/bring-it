jQuery(document).ready(function($){
    
    var pizzerias = [];
    
    // here will be the code that gets all pizzerias info (prices and delivery time);
    
    $("#order-by-price").click(function(event){
        
        orderByPrice(pizzerias);
        
    });
    
    $("#order-by-distance").click(function(){
       
        orderByDistance(pizzerias);
        
    });
    
});

function orderByPrice(pizzerias){
    
}

function orderByDistance(pizzerias){
    
}

function pizzeriasCheckout(e){ 
    
    e.preventDefault();
    
    var target = e.currentTarget;
    
    var vendor = {};
    vendor.id = jQuery(target).attr("vendor-id");
    
    jQuery("#loading-animation").show();
    jQuery.ajax({
        type:"post",
        url:"/wp-admin/admin-ajax.php",
        data:"action=choose_vendor_action&vendor_ID="+vendor.id,
        success:function(data){
            console.log(data);
        },
        complete: function(){
            jQuery("#loading-animation").hide();
        }
    });
    
    location.href = "/addons";
        
}
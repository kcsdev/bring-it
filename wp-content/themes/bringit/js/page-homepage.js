  //*********************************************************
    //Pizza's smoke
(function($){
    if(!$.browser.msie){
        var a=0;for(;a<15;a+=1){setTimeout(function b(){var a=Math.random()*1e3+5e3,c=$("<div />",{"class":"smoke",css:{opacity:0,left:Math.random()*200+80}});$(c).appendTo("#pizzaViewport");$.when($(c).animate({opacity:1},{duration:a/4,easing:"linear",queue:false,complete:function(){$(c).animate({opacity:0},{duration:a/3,easing:"linear",queue:false})}}),$(c).animate({bottom:$("#pizzaViewport").height()},{duration:a,easing:"linear",queue:false})).then(function(){$(c).remove();b()})},Math.random()*3e3)}
        }else{
            "use strict";var a=0;for(;a<15;a+=1){setTimeout(function b(){var a=Math.random()*1e3+5e3,c=$("<div />",{"class":"smoke",css:{left:Math.random()*200+80}});$(c).appendTo("#pizzaViewport");$.when($(c).animate({},{duration:a/4,easing:"linear",queue:false,complete:function(){$(c).animate({},{duration:a/3,easing:"linear",queue:false})}}),$(c).animate({bottom:$("#pizzaViewport").height()},{duration:a,easing:"linear",queue:false})).then(function(){$(c).remove();b()})},Math.random()*3e3)}}
    })(jQuery);
    //*******************************************************************************

jQuery(document).ready(function($){
     jQuery(function ($) { 
        // When clicking the Make Pizza Button OR when choosing one of the bundles
        // appears the Shipping Popup
        $('.make-pizza.btn-make-pizza').magnificPopup({
            items: {
                src: '#addressModalContent',
                type: 'inline',
                closeOnBgClick: false,
                closeBtnInside: true
            },

            callbacks: {
                open: function() {
                    $(".address-tabs-row .shipping").click();
                }
            }
        });
        
         $(".special-box").bind("click",function(event){
            var bundle = {};
             
            bundle.WPID = $(this).attr("bundle-WPID");
            bundle.ID = $(this).attr("bundle-ID");
            
            var jsonBundle = JSON.stringify(bundle);
             
            console.log(bundle);
            
            $.ajax({
                type:"POST",
                url: "/wp-admin/admin-ajax.php",
                data:"action=start_new_bundle_action&bundle="+jsonBundle,
                success:function(data){
                    console.log(data);
                },
                complete:function(){

                }
            });
             
         });
         
         $('.special-box').magnificPopup({
            items: {
                src: '#addressModalContent',
                type: 'inline',
                closeOnBgClick: false,
                closeBtnInside: true
            },

            callbacks: {
                open: function() {
                    $(".address-tabs-row .shipping").click();
                }
            }
        });

        //**************************************************************************//
        $('.address-tabs-row > li').bind('click', function(){
            var $self = $(this);
            if(!$self.hasClass('tail') && !$self.hasClass('current')){
                $("#tabsContent").load("/pv-hp-shipping-popup/?type=" + $self.data('tab-content'), function(){
                    $('.address-tabs-row > li').removeClass('current');
                    $self.addClass('current');
                });
            }
        })
    });
});
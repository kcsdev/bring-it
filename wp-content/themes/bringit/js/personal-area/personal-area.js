jQuery(document).ready(function($){ 
    
    $("#personal-area-addr-list, #personal-area-addr-list-link").bind("click", function(){
        $(".iframe-wrapper").height($(window).height() - 90);
        $("#pzIframe").attr("src","/customer-addr-list/");
        $(".overlay-mask").fadeIn();
    });
    
    $("#personal-area-info, #personal-area-info-link").bind("click", function(){
        $(".iframe-wrapper").height($(window).height() - 90);
        $("#pzIframe").attr("src","/customer-info/");
        $(".overlay-mask").fadeIn();
    });
    
    $("#personal-area-orders-history, #personal-area-orders-history-link").bind("click", function(){
        $(".iframe-wrapper").height($(window).height() - 90);
        $("#pzIframe").attr("src","/customer-order-list/");
        $(".overlay-mask").fadeIn();
    });
    
});
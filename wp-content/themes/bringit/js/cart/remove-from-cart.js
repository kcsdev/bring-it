function emptyTheCart(){ 
            jQuery("#loading-animation").show();
            jQuery.ajax({
                type: 'POST',
                url: "/wp-admin/admin-ajax.php",       // *** SHOULD BE CHANGED TO THE REAL URL //The url is always the same in WP 
                data:'action=empty_the_cart_action',
                success: function (data, status, xhr) { 
                    console.log("Cart is empty");
                },
                error: function (xhr, status, error) { 
                    console.log("Oops, something went wrong!");
                },
                complete: function (xhr, status) { 
//                    jQuery("#makePizzaWorkpad").load("/pv-make-pizza/?r=" + (new Date()).getTime());
                    jQuery("#shoppingCartContent").load("/pv-shopping-cart-content/");
                    jQuery("#loading-animation").hide();
                },
//                dataType: 'text',
                cache: false
            });
        } 
 function deleteFromCart(id,productType){ 
        jQuery("#loading-animation").show();
        jQuery.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",       // *** SHOULD BE CHANGED TO THE REAL URL //The url is always the same in WP 
            data:'action=delete_from_cart_action&id='+id+"&product_type="+productType,
            success: function (data, status, xhr) { 
                console.log("Product removed from cart!");
                console.log(data);
            },
            error: function (xhr, status, error) { 
                console.log("Oops, something went wrong!");
            },
            complete: function (xhr, status) { 
                jQuery("#shoppingCartContent").load("/pv-shopping-cart-content/");
//                    jQuery("#makePizzaWorkpad").load("/pv-make-pizza/?r=" + (new Date()).getTime());
                jQuery("#loading-animation").hide();
            },
//                dataType: 'text',
            cache: false
        });
    }
function removePizza(id){ 
    jQuery("#loading-animation").show();
    jQuery.ajax({
        type: 'POST',
        url: "/wp-admin/admin-ajax.php",       // *** SHOULD BE CHANGED TO THE REAL URL //The url is always the same in WP 
        data:'action=remove_pizza_from_cart_action&id='+id,
        success: function (data, status, xhr) { 
            console.log("Product removed from cart!");
            console.log(data);
        },
        error: function (xhr, status, error) { 
            console.log("Oops, something went wrong!");
        },
        complete: function (xhr, status) { 
            jQuery("#shoppingCartContent").load("/pv-shopping-cart-content/");
//                    jQuery("#makePizzaWorkpad").load("/pv-make-pizza/?r=" + (new Date()).getTime());
            jQuery("#loading-animation").hide();
        },
//                dataType: 'text',
        cache: false
    });
}
function removeDrink(WPID){ 
    jQuery("#loading-animation").show();
    jQuery.ajax({
        type: 'POST',
        url: "/wp-admin/admin-ajax.php",      
        data:'action=remove_drink_from_cart_action&WPID='+WPID,
        success: function (data, status, xhr) { 
            console.log("Product removed from cart!");
            console.log(data);
        },
        error: function (xhr, status, error) { 
            console.log("Oops, something went wrong!");
        },
        complete: function (xhr, status) { 
            jQuery("#shoppingCartContent").load("/pv-shopping-cart-content/");
//                    jQuery("#makePizzaWorkpad").load("/pv-make-pizza/?r=" + (new Date()).getTime());
            jQuery("#loading-animation").hide();
        },
//                dataType: 'text',
        cache: false
    });
}
function removeAdditionalProduct(id){ 
    jQuery("#loading-animation").show();
    jQuery.ajax({
        type: 'POST',
        url: "/wp-admin/admin-ajax.php",
        data:'action=remove_additional_product_from_cart_action&id='+id,
        success: function (data, status, xhr) { 
            console.log("Product removed from cart!");
            console.log(data);
        },
        error: function (xhr, status, error) { 
            console.log("Oops, something went wrong!");
        },
        complete: function (xhr, status) { 
            jQuery("#shoppingCartContent").load("/pv-shopping-cart-content/");
//                    jQuery("#makePizzaWorkpad").load("/pv-make-pizza/?r=" + (new Date()).getTime());
            jQuery("#loading-animation").hide();
        },
//                dataType: 'text',
        cache: false
    });
}

function removeAddon(e){ 
    var target = e.currentTarget;
    var addonId = jQuery(target).attr("cart-addon-id");
    var pizzaId = jQuery(target).attr("cart-pizza-id");
    
    jQuery("#loading-animation").show();
    jQuery.ajax({
        type:"POST",
        url:"/wp-admin/admin-ajax.php",
        data:"action=remove_addon_from_current_pizza_action&id="+addonId+"&pizza_id="+pizzaId,
        success: function (data){
            console.log(data);
        },
        error:function(){
            console.log("Oops... Something went Wrong!");
        },
        complete:function(){ 
            jQuery("#shoppingCartContent").load("/pv-shopping-cart-content/");
            jQuery("#loading-animation").hide();
        }
    });
}
function removeBundle(id){ 
    
    var bundle = {};
    bundle.ID = id;
    console.log(bundle);
    jsonBundle = JSON.stringify(bundle);
    
    jQuery("#loading-animation").show();
    jQuery.ajax({
        type:"POST",
        url:"/wp-admin/admin-ajax.php",
        data:"action=remove_bundle_from_cart_action&bundle="+jsonBundle,
        success:function(data){
            console.log(data);
            
        },
        complete:function(){
            jQuery("#shoppingCartContent").load("/pv-shopping-cart-content/");
            jQuery("#loading-animation").hide();
        }
    });
}

function removePizzaFromBundle(bundleID,pizzaID){
    
    jQuery("#loading-animation").show();
    jQuery.ajax({
        type:"POST",
        url:"/wp-admin/admin-ajax.php",
        data:"action=remove_pizza_from_bundle_action&bundle_ID="+bundleID+"&pizza_ID="+pizzaID,
        success:function(data){
            console.log(data);
        },
        complete:function(){
            jQuery("#loading-animation").hide();
            jQuery("#shoppingCartContent").load("/pv-shopping-cart-content/");
        }
    });
    
    
}

function removeDrinkFromBundle(bundleID,drinkWPID){
    
    jQuery("#loading-animation").show();
    
    jQuery.ajax({
        type:"POST",
        url:"/wp-admin/admin-ajax.php",
        data:"action=remove_drink_from_bundle_action&bundle_ID=" + bundleID + "&drink_WPID=" + drinkWPID,
        success: function(data){
            console.log(data);
        },
        complete:function(){
            jQuery("#loading-animation").hide();
            jQuery("#shoppingCartContent").load("/pv-shopping-cart-content/");
        }
    });
    
}
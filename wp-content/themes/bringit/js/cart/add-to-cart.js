var addons = [];


jQuery(document).ready(function(){
    

    
});


function addPizza(event){
   


} 

function addAdditionalProductToCart(e){
    
    e.preventDefault();
    
    var target = e.currentTarget;
    
    var additionalProduct = {};
    additionalProduct.id = jQuery(target).attr("additional-product-id");
    
    jQuery("#loading-animation").show();
    
    jQuery.ajax({
        type:"post",
        url:"/wp-admin/admin-ajax.php",
        data: "action=add_additional_product_to_cart_action&additionalProductId=" +additionalProduct.id,
        success: function(data){
            console.log(data);
            jQuery("#shoppingCartContent").load("/pv-shopping-cart-content/");
        },
        complete: function(){
            jQuery("#loading-animation").hide();
        }
    });
    
}

//function update_pb_cart_and_redirect(event){
//    event.preventDefault();
//    jQuery("#loading-animation").show();
//    var jsonAddons = JSON.stringify(addons);
//    jQuery.ajax({ 
//
//        url:"/wp-admin/admin-ajax.php",
//        type:'POST',
//        data:'action=add_pizza_to_cart_action&addons='+jsonAddons, 
//        success:function(result){ 
//            console.log("Add pizza and redirect to drinks");
//            console.log(result);
////                                jQuery("#makePizzaWorkpad").load("/pv-make-pizza/");
//            jQuery("#shoppingCartContent").load("/pv-shopping-cart-content/");
//            location='/drinks/';
//        },
//        complete:function(){
//            jQuery("#loading-animation").hide();
//        }
//
//    });
//
//
//}

function addDrinkToCart(e){
    
    var target = e.currentTarget;

    var WPID    = jQuery(target).attr("drink-WPID");
    var name    = jQuery(target).attr("drink-name");
    var id      = jQuery(target).attr("drink-id");
    jQuery("#loading-animation").show();
    jQuery.ajax({
        type: 'POST',
        url: "/wp-admin/admin-ajax.php",       // *** SHOULD BE CHANGED TO THE REAL URL //The url is always the same in WP 
        data:'action=add_drink_to_cart_action&id='+id+'&name='+name+'&WPID='+WPID,
        success: function (data, status, xhr) { 
            console.log("Everything went OK. If the result is nagative than the problem is in the PHP");
            console.log("Data: " + data);
            console.log("Status: " + status);

        },
        error: function (xhr, status, error) { 
            console.log("Oops, something went wrong!");
        },
        complete: function (xhr, status) { 
            console.log("Add a drink");
            jQuery("#loading-animation").hide();
            jQuery("#makePizzaWorkpad").load("/pv-make-pizza/?r=" + (new Date()).getTime());
            jQuery("#shoppingCartContent").load("/pv-shopping-cart-content/");
        },

//                dataType: 'text',
        cache: false
    });

}
function addDrinkToBundle(e){
    
    var target = e.currentTarget;

    var WPID    = jQuery(target).attr("drink-WPID");
    var name    = jQuery(target).attr("drink-name");
    var id      = jQuery(target).attr("drink-id");
    
    var drink = {
        WPID:WPID,
        name:name,
        id:id
    }
    
    var jsonDrink = JSON.stringify(drink);
    
    jQuery("#loading-animation").show();
    jQuery.ajax({
        type: 'POST',
        url: "/wp-admin/admin-ajax.php",       // *** SHOULD BE CHANGED TO THE REAL URL //The url is always the same in WP 
        data:'action=add_drink_to_bundle_action&drink='+jsonDrink,
        success: function (data) { 
            console.log(data);
            var result = JSON.parse(data.trim());
            
            if(result.ready){
                jQuery("#save-bundle-to-cart").fadeIn("slow");
            }

        },
        error: function (xhr, status, error) { 
            console.log("Oops, something went wrong!");
        },
        complete: function (xhr, status) { 
            console.log("Add a drink");
            jQuery("#loading-animation").hide();
            jQuery("#makePizzaWorkpad").load("/pv-make-pizza/?r=" + (new Date()).getTime());
            jQuery("#shoppingCartContent").load("/pv-shopping-cart-content/");
        },

//                dataType: 'text',
        cache: false
    });

}

jQuery(document).ready(function($){ 
    
    $(".dayStatus").change(function(){
        var day = {};
        day.ID = $(this).attr("day");
        
        if($(this).is(":checked")){
            day.status = 1;
        }else{
            day.status = 0;
        }
        
        var jsonDay = JSON.stringify(day);
        
        $.ajax({
            type:"post",
            url:"/wp-admin/admin-ajax.php",
            data:"action=update_vendors_day_status_action&day="+jsonDay,
            success:function(data){
                console.log(data);
            },
            complete:function(){
                
            }
        });
        
    });
    
    $(".closeHour").change(function(){
        var day = {};
        day.ID = $(this).attr("day");
        day.closingTime = $(this).val();
        
        var jsonDay = JSON.stringify(day);
        
        $.ajax({
            type:"post",
            url:"/wp-admin/admin-ajax.php",
            data:"action=update_vendors_closing_time_action&day="+jsonDay,
            success:function(data){
                console.log(data);
            },
            complete:function(){
                
            }
        });
    });
});
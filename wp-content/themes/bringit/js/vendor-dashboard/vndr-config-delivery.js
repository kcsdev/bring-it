jQuery(document).ready(function($){
    $(".area-block").bind("click", function(){        
        var $self = $(this);
            $("#" +  $self.data("area-data-id")).toggle(400);
    });

    $(".locality").bind("change", function(){ 
        $("#show-area-animation").fadeIn("slow");
        $("#localityNewData").load("/pv-area-data/?ref=newArea&locality=" + $(this).val(), function(){ 
            $("#show-area-animation").fadeOut("slow");
            $("#new-area-save").bind("click",function(){ 
                $("#add-area-animation").fadeIn("slow");
                var city = {};
                
                city.name = $("#new-area-table").attr("city-name");
                city.regions = [];
                $(".region-row").each(function(){ 
                    var region = {};
                    region.name = $(this).attr("region-name");
                    region.shippingPrice = $("#shipping-price_"+region.name).val();
                    region.shippingTime = $("#shipping-time_"+region.name).val();
                    region.minimumOrderPrice = $("#minimum-order-price_"+region.name).val();
                    region.minimumOrderPriceForFreeShipping = $("#minimum-order-price-for-free-shipping_"+region.name).val();
                    if($("#is_shipping_"+region.name).is(":checked")){
                        region.is_shipping = 1;
                    }else{
                        region.is_shipping = 0;
                    }
                    
                    city.regions.push(region);
                });
                var jsonCity = JSON.stringify(city);

                $.ajax({
                    type:"post",
                    url:"/wp-admin/admin-ajax.php",
                    data:"action=add_new_delivery_area_action&city="+jsonCity,
                    success:function(data){
                        console.log("Adding new area...");
                        console.log(data);
                        $('#localityNewData').html('');
                        $('option#'+city.name).remove();
                        
                        $("#temp-area-data").load("/pv-temp-area-data/?locality=" + city.name,function(){
                            $(".area-block").bind("click", function(){        
                                var $self = $(this);
                                    $("#" +  $self.data("area-data-id")).toggle(400);
                            });
                            areaDataButtons();
                        });
                    },
                    complete:function(){
                        $("#add-area-animation").fadeOut("slow");
                    }
                });
            });
        });
    });

    // ------- static tooltips
    $("body").on("mouseover", ".static-tooltip", function (e) {
        var trg = $(e.currentTarget);
        if (trg.attr("data-tooltip-class") != undefined) {
            var ttObj = $("<div></div>");
            ttObj.addClass("vndr-tooltip " + trg.attr("data-tooltip-class"));
            ttObj.html(trg.attr("data-tooltip-text"));
            $("body").append(ttObj);
            ttObj.css("left", (trg.offset().left + trg.outerWidth() / 2) - ttObj.outerWidth() / 2);
            ttObj.css("top", trg.offset().top - ttObj.outerHeight() - 11);
            ttObj.fadeIn();
        }
    });

    $("body").on("mouseout", ".static-tooltip", function (e) {
        var trg = $(e.currentTarget);
        if (trg.attr("data-tooltip-class") != undefined) {
            var ttObj = $(".vndr-tooltip." + trg.attr("data-tooltip-class"));
            ttObj.remove();
        }
    });
        // ------- /static tooltips
    
    var areaDataButtons = function(){
        // Save Button On the Editting pane
        $("#edit-area-save").bind("click",function(){ 
            $("#save-area-animation").fadeIn("slow");
            var city = {};
            city.name = $(this).attr("city-name"); // Getting city name;
            city.regions = [];
            $("#region-row_"+city.name).each(function(){ // going in a loop throught all regions in current city
                var region = {};
                region.name = $(this).attr("region-name"); // Getting region name
                if($("#is_shipping_"+city.name+"_"+region.name).is(":checked")){
                    region.is_shipping = 1;
                }else{
                    region.is_shipping = 0;
                }
                region.minimumOrderPrice = $("#minimum-order-price_"+city.name+"_"+region.name).val();
                region.minimumOrderPriceForFreeShipping = $("#minimum-order-price-for-free-shipping_"+city.name+"_"+region.name).val();
                region.shippingPrice = $("#shipping-price_"+city.name+"_"+region.name).val();
                region.shippingTime = $("#shipping-time_"+city.name+"_"+region.name).val();

                city.regions.push(region);
            });
            console.log(city);
            var jsonCity = JSON.stringify(city);

            $.ajax({
                type:"post",
                url:"/wp-admin/admin-ajax.php",
                data:"action=update_delivery_area_action&city="+jsonCity,
                success:function(data){
                    console.log(data);
                },
                complete:function(){
                    $(".save-area-animation").fadeOut();
                }
            });

        });
        // Removing the chosen City.
        $("#edit-area-remove").bind("click",function(){
            $("#remove-area-animation").fadeIn("slow");
            var city = {};
            city.name = $(this).attr("city-name");

            jsonCity = JSON.stringify(city);

            $.ajax({
                type:"post",
                url:"/wp-admin/admin-ajax.php",
                data:"action=remove_delivery_area_action&city="+jsonCity,
                success:function(){
                    $(".area-block[data-area-data-id='area-"+city.name+"']").parent().remove();
                    $("#locality-new").append($("<option>",{
                        value:city.name,
                        text:city.name,
                        id:city.name
                    }));
                },
                complete:function(){
                    $("#remove-area-animation").fadeOut("slow");
                }
            });
        });

        //Resetting preivious data in current City
        $("#edit-area-rest").bind("click",function(){
            $("#reset-area-animation").fadeIn("fast");
            var city = {};
            city.name = $(this).attr("city-name");

            $("#region-row_"+city.name).each(function(){ // going in a loop throught all regions in current city
                var region = {};
                region.name = $(this).attr("region-name"); // Getting region name

                $("#minimum-order-price_"+city.name+"_"+region.name).val($("#minimum-order-price_"+city.name+"_"+region.name).attr("origin-data"));
                $("#minimum-order-price-for-free-shipping_"+city.name+"_"+region.name).val($("#minimum-order-price-for-free-shipping_"+city.name+"_"+region.name).attr("origin-data"));
                $("#shipping-price_"+city.name+"_"+region.name).val($("#shipping-price_"+city.name+"_"+region.name).attr("origin-data"));
                $("#shipping-time_"+city.name+"_"+region.name).val($("#shipping-time_"+city.name+"_"+region.name).attr("origin-data"));
                if($("#shipping-time_"+city.name+"_"+region.name).attr("origin-data") == ""){
                    $("#shipping-time_"+city.name+"_"+region.name).prop("checked",false);
                }else{
                    $("#shipping-time_"+city.name+"_"+region.name).prop("checked",true);
                }

            });
            $("#reset-area-animation").fadeOut("fast");
        });
    };
    areaDataButtons();
});
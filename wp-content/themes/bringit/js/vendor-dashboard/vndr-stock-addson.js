/*
* 
* This JS file is responsible for vndr-stock-drinks.php file
* 
*/

jQuery(document).ready(function($){ 
    
    $(".vndr-addon-price").keyup(function(){ // When entering price to price field

        var WPID = $(this).attr("addon-WPID"); // Get WPID of the drink by field attribute
        var price = $("input[name="+WPID+"-price]").val(); // get price by field value
        
        if(price > 0 && price !== "" && price !== undefined && price !== NaN){  // If price is ok
            
            $("input[name = " + WPID + "-stock-status]").prop("disabled",false); // Enable checkbox
            $("input[name = " + WPID + "-in-bundle]").prop("disabled",false); // Enable checkbox
            
        }else{  // else
            
            $("input[name = " + WPID + "-stock-status]").prop("disabled",true); // Disable checkbox
            $("input[name = " + WPID + "-in-bundle]").prop("disabled",true); // Disable checkbox
            
        }
        
    });
    
    $(".vndr-addon-price").each(function(){ // A foreach that runs after page is loaded to check which rows should be enabled
        var WPID = $(this).attr("addon-WPID");
        var priceValue = $("input[name="+WPID+"-price]").val(); // get price by field value
        if(priceValue > 0 && priceValue !== "" && priceValue !== undefined && priceValue !== NaN){ // If price is OK
            $("input[name = " + WPID + "-stock-status]").prop("disabled",false); // Enable checkbox
            $("input[name = " + WPID + "-in-bundle]").prop("disabled",false); // Enable checkbox
        }else{
            $("input[name = " + WPID + "-stock-status]").prop("disabled",true); // Disable checkbox
            $("input[name = " + WPID + "-in-bundle]").prop("disabled",true); // Disable checkbox
        }
    });
    
    $("#vndr-stock-addson-save").bind("click",function(){
        var addson = [];
        $("#update-addson-button").fadeIn();
        $(".vndr-addon-price").each(function(){ 
            var addon = {};
            addon.WPID = $(this).attr("addon-WPID");
            addon.price = $(this).val();
            if( $("input[name = " + addon.WPID + "-stock-status]").is(":checked") ){
                addon.in_stock = 1;
            }else{
                addon.in_stock = 0;
            }
            
            if( $("input[name = "+ addon.WPID +"-in-bundle]").is(":checked") ){
                addon.in_bundle = 1;
            }else{
                addon.in_bundle = 0;
            }
            
            
            if($("input[name = " + addon.WPID + "-stock-status]").prop("disabled") == false){
                addson.push(addon);   
            }            
        });
        console.log(addson);
        var jsonAddson = JSON.stringify(addson);
        $.ajax({
            type:"post",
            url:"/wp-admin/admin-ajax.php",
            data:"action=update_vendors_addson_stock_action&addson=" + jsonAddson,
            success: function(data){
                console.log(data);
            },
            complete:function(){
                console.log("VNDR_STOCK_ADDSON save Completed");
                $("#update-addson-button").fadeOut();
            }
        });
    });
    
});
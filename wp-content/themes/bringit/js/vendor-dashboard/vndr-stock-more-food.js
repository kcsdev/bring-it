

jQuery(document).ready(function($){
    jQuery(".dropzone").dropzone({ url: "/ajax-upload-image" });
    $("#add-additional-product-button").click(function(e){
        e.preventDefault();
        
        $("#add-new-additional-product-loading-animation").fadeIn("slow");
        
        var additionalProduct = {};
        additionalProduct.name = $("#newAdditionalProductName").val();
        additionalProduct.desc = $("#newAdditionalProductDescription").val();
        additionalProduct.fileName = "/wp-content/themes/bringit/assets/images/additional-products/" + $(".dz-filename").text();
        var jsonAdditionalProduct = JSON.stringify(additionalProduct);
        
        $.ajax({
            type:"post",
            url:"/wp-admin/admin-ajax.php",
            data:"action=add_new_additional_product_action&additional_product="+jsonAdditionalProduct,
            success:function(data){
                console.log(data);
                var id = data.trim();
                $.get("/pv-new-additional-product-row?id="+id,function(data){
                    $(".stock-list").prepend(data);
                });
            },
            complete:function(){
                $("#add-new-additional-product-loading-animation").fadeOut("slow");
                $("from").html("");
                $("form").html('<form class = "dropzone" action="/ajax-upload-image"></form>')
            }
        });
    });
    
    $("#list-save-additional-product").click(function(e){
        e.preventDefault();
        var additionalProducts = [];
        
        $("#update-additional-product-button").fadeIn("slow");
        
        $(".additional-product-row").each(function(){
            var additionalProduct = {};
            additionalProduct.id = $(this).attr("additional-product-id");
            if( $("#in_stock_"+additionalProduct.id).is(":checked") ){
                additionalProduct.in_stock = 1;
            }else{
                additionalProduct.in_stock = 0;
            }
            
            additionalProduct.price = $("#price_"+additionalProduct.id).val();
            
            additionalProducts.push(additionalProduct);
            
        });
        console.log(additionalProducts);
        var jsonAdditionalProducts = JSON.stringify(additionalProducts);
        
        $.ajax({
            type:"post",
            url:"/wp-admin/admin-ajax.php",
            data:"action=update_additional_products_action&additional_products="+jsonAdditionalProducts,
            success: function(data){
                console.log(data);
            },
            complete:function(){
                $("#update-additional-product-button").fadeOut("slow");
            }
        });
    });
    
});
 jQuery(document).ready(function($){ 
     
     $("input.status").click(function(event){
         var currentCheckboxID = $(this).attr("region-id");
         
         var checkStatus = $("input#status_"+currentCheckboxID).attr("status");
         
         if(checkStatus == "checked"){ 
            
            var cityID = $(this).attr("city-id");
            var regionID = $(this).attr("region-id");
             
            $("#shipping_price_"+currentCheckboxID).prop("disabled",true);
            $("#shipping_time_"+currentCheckboxID).prop("disabled",true);
            $("#pizza_price_"+currentCheckboxID).prop("disabled",true);
            $("#minimum_order_price_for_free_shipping_"+currentCheckboxID).prop("disabled",true);
            $("#vendor-dashboard-city-region-row-save-button-"+currentCheckboxID).prop("disabled",true);
             
            $("input#status_"+currentCheckboxID).attr("status","unchecked");
             
            $.ajax({
                type:"POST",
                url:"/wp-admin/admin-ajax.php",
                data:"action=remove_city_region_from_vendor_action&city_ID="+cityID+"&region_ID="+regionID,
                success:function(data){ 
                    console.log(data);
                    console.log("Deleting region from Vendor...");
                },
                complete:function(){
                    console.log("Deleting region from Vendor Complete!");
                }
            });
            
         }else{ 
            
            var cityID = $(this).attr("city-id");
            var regionID = $(this).attr("region-id");
            
            $("#shipping_price_"+currentCheckboxID).prop("disabled",false);
            $("#shipping_price_"+currentCheckboxID).val(0);
             
            $("#shipping_time_"+currentCheckboxID).prop("disabled",false);
            $("#shipping_time_"+currentCheckboxID).val(0);
             
            $("#pizza_price_"+currentCheckboxID).prop("disabled",false);
            $("#pizza_price_"+currentCheckboxID).val(0);
            
            $("#minimum_order_price_for_free_shipping_"+currentCheckboxID).prop("disabled",false);
            $("#minimum_order_price_for_free_shipping_"+currentCheckboxID).val(0);
            
            $("#vendor-dashboard-city-region-row-save-button-"+currentCheckboxID).prop("disabled",false);
            $("#vendor-dashboard-city-region-row-save-button-"+currentCheckboxID).val(0);
             
            $.ajax({
                type:"POST",
                url:"/wp-admin/admin-ajax.php",
                data:"action=add_city_region_from_vendor_action&city_ID="+cityID+"&region_ID="+regionID,
                success:function(data){
                    console.log(data);
                    
                },
                complete:function(){
                    
                }
            });
            
            $("input#status_"+currentCheckboxID).attr("status","checked");
             
        }
         
         
     });
     
     $("#vendor-dashboard-choose-city-button, #vendor-dashboard-choose-city-select").prop("disabled",false);
     $("#vendor-dashboard-choose-city-button").click(function(event){
         event.preventDefault();
         var chosenCity = $("#vendor-dashboard-choose-city-select").val();
         console.log("Value saved when clicked: " + chosenCity);
         if(chosenCity == "none"){
             $("#vendor-dashboard-choose-city-select").css("box-shadow","0 0 1px 1px red");
         }else{
             addCity(chosenCity);
         }
     });
     
     
    $(".save-region").bind("click",function(event){ // Save current city regions
        event.preventDefault();
        var ID = $(this).attr("region-id");
        var region = { 
            ID: $(this).attr("region-id"),
            cityID: $(this).attr("city-ID"),
            pizzaPrice:$("input[name=pizza_price_"+ID+"]").val(),
            shippingPrice:$("input[name=shipping_price_"+ID+"]").val(),
            minimumOrderPriceForFreeShipping:$("input[name=minimum_order_price_for_free_shipping_"+ID+"]").val(),
            shippingTime:$("input[name=shipping_time_"+ID+"]").val()
        }
        var jsonRegion = JSON.stringify(region);
        console.log(jsonRegion);
        var errors = 0;
        if(region.pizzaPrice == ""){ 
            $("#pizza_price_"+ID).css("box-shadow","0 0 2px 1px red");
            errors++;
        }
        if(region.shippingPrice == ""){ 
            $("#shipping_price_"+ID).css("box-shadow","0 0 2px 1px red");
            errors++;
        }
        if(region.minimumOrderPriceForFreeShipping == ""){ 
            $("#minimum_order_price_for_free_shipping_"+ID).css("box-shadow","0 0 2px 1px red");
            errors++;
        }
        if(region.shippingTime == ""){ 
            $("#shipping_time_"+ID).css("box-shadow","0 0 2px 1px red");
            errors++;
        }
        if(errors > 0){
            return false;
        }else{
            
            $.ajax({
                type:"post",
                url:"/wp-admin/admin-ajax.php",
                data:"action=vendor_dashboard_save_region_action&region="+jsonRegion,
                success:function(data){
                    console.log(data);
                },
                complete:function(data){

                }
            });
            
        }
         
    });
     
     
     
     

    // set effect from select menu value
//    $( "#vendor-dashboard-vendoring-city-row-edit-button" ).click(function() {
//        runEffect();
//    });
     
     
     
     function addCity(chosenCity){
        $("#loading-animation").show();
        $.ajax({
            type: "POST",
            url: "/wp-admin/admin-ajax.php",
            data:"action=add_city_to_vendor_action&city="+chosenCity,
            success:function(data){
                console.log("Passed city value to 'addCity' function: "+ chosenCity);
                
                
                
                console.log("Data recieved after ajax has finished: ");
                console.log(data);
            },
            complete:function(){ 
                $("#loading-animation").hide();
                location.reload(); 
            }
        });

    }
     
     
     
 });

function removeCity(chosenCity){
    if(confirm("Are you sure you want to remove this city from your list?")){
        
        jQuery("#loading-animation").show();
        jQuery.ajax({

             type:"POST",
             url:"/wp-admin/admin-ajax.php",
             data:"action=remove_city_from_vendor_action&city="+chosenCity,
             success:function(data){
                 console.log("Passed city value to 'removeCity' function: "+ chosenCity);



                console.log("Data recieved after ajax has finished: ");
                console.log(data);
             },
             complete:function(){ 
                 jQuery("#loading-animation").hide();
                 location.reload();
             }

        });
    }
 }
function runEffect(id) {
          // get effect type from
          var selectedEffect = "blind";

          // most effect types need no options passed by default
          var options = {};
          // some effects have required parameters
          if ( selectedEffect === "scale" ) {
            options = { percent: 0 };
          } else if ( selectedEffect === "size" ) {
            options = { to: { width: 200, height: 60 } };
          }

          // run the effect
          jQuery( "#vendor-dashboard-edit-regions-"+id ).toggle( selectedEffect, options, 500 );
     };
/*
* 
* This JS file is responsible for vndr-stock-drinks.php file
* 
*/

jQuery(document).ready(function($){ 
    
    $(".vndr-drink-price").keyup(function(){ // When entering price to price field

        var WPID = $(this).attr("drink-WPID"); // Get WPID of the drink by field attribute
        var price = $("input[name="+WPID+"-price]").val(); // get price by field value
        
        if(price > 0 && price !== "" && price !== undefined && price !== NaN){  // If price is ok
            
            $("input[name = " + WPID + "-stock-status]").prop("disabled",false); // Enable checkbox
            
        }else{  // else
            
            $("input[name = " + WPID + "-stock-status]").prop("disabled",true); // Disable checkbox
            
        }
        
    });
    
    $(".vndr-drink-price").each(function(){ // A foreach that runs after page is loaded to check which rows should be enabled
        var WPID = $(this).attr("drink-WPID");
        var priceValue = $("input[name="+WPID+"-price]").val(); // get price by field value
        if(priceValue > 0 && priceValue !== "" && priceValue !== undefined && priceValue !== NaN){ // If price is OK
            $("input[name = " + WPID + "-stock-status]").prop("disabled",false); // Enable checkbox
        }else{
            $("input[name = " + WPID + "-stock-status]").prop("disabled",true); // Disable checkbox
        }
    });
    
    $("#vndr-stock-drinks-save").bind("click",function(){ 
        $("#update-drinks-button").fadeIn("slow");
        var drinks = [];
        $(".vndr-drink-price").each(function(){ 
            var drink = {};
            drink.WPID = $(this).attr("drink-WPID");
            drink.price = $(this).val();
            if( $("input[name = " + drink.WPID + "-stock-status]").is(":checked") ){
                drink.in_stock = 1;
            }else{
                drink.in_stock = 0;
            }
            
            
            if($("input[name = " + drink.WPID + "-stock-status]").prop("disabled") == false){
                drinks.push(drink);   
            }            
        });
        console.log(drinks);
        var jsonDrinks = JSON.stringify(drinks);
        $.ajax({
            type:"post",
            url:"/wp-admin/admin-ajax.php",
            data:"action=update_vendors_drinks_stock_action&drinks=" + jsonDrinks,
            success: function(data){
                console.log(data);
            },
            complete:function(){ 
                $("#update-drinks-button").fadeOut("slow");
                console.log("VNDR_STOCK_DRINK save Completed");
            }
        });
    });
    
});
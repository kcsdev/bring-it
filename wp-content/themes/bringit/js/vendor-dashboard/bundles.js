function bundleRunEffect(id) {
      // get effect type from
      var selectedEffect = "blind";

      // most effect types need no options passed by default
      var options = {};
      // some effects have required parameters
      if ( selectedEffect === "scale" ) {
        options = { percent: 0 };
      } else if ( selectedEffect === "size" ) {
        options = { to: { width: 200, height: 60 } };
      }

      // run the effect
      jQuery( "#vendor-dashboard-bundle-box-hidden-part-"+id ).toggle( selectedEffect, options, 500 );
 };

jQuery(document).ready(function($){

    // Hidden Part of a bundle *****************************************//
    
    $(".vendor-dashboard-bundle-box").each(function(){ 
        var WPID = $(this).attr("bundle-WPID");
        var is_checked = $(".vendor-dashboard-bundle-box #vendor-dashboard-bundle-activation-checkbox-"+WPID).attr("status");
        
        console.log(is_checked);
        
        if(is_checked == "checked"){ 
            var effect = "blind";
            var options = {};
            $("#vendor-dashboard-bundle-box-hidden-part-"+WPID).toggle( effect, options, 500);
        }
        
        
    });
    
    //*****************************************************//
    
    $(".vendor-dashboard-bundle-save-button").click(function(event){ 
        
        var bundle = {};
        bundle.WPID = $(this).attr("bundle-id");
        bundle.price = $("#vendor-dashboard-bundle-price-input-"+bundle.WPID).val();
        
        var tempAddons = []; // Temporary array to gather all the selected addtional products
        $("#vendor-dashboard-bundle-box-hidden-part-"+bundle.WPID+"  input[name=addon-to-include]").each(function(){ 
            if($(this).is(":checked"))
                tempAddons.push( parseInt($(this).val()) ); // Putting from each select input its value and name to an array
        });
        var addons = JSON.stringify(tempAddons); //Stringifying that array so we could send it to the php function

        bundle.addons = addons;
            
        JsonBundle = JSON.stringify(bundle);
        $.ajax({
            type:"POST",
            url:"/wp-admin/admin-ajax.php",
            data: "action=update_bundle_of_a_vendor_action&bundle="+JsonBundle,
            success:function(data){
                console.log(data);
            },
            complete: function(){
                
            }
        });
    });
    
    $(".vendor-dashboard-activation-checkbox").change(function(){ 
        
        var bundle = {};
        bundle.WPID = $(this).attr("bundle-WPID"); //getting the WPID(post ID defined by wordpress) of the bundle.
        
        $("#loading-gif-"+bundle.WPID).show();
        $("#vendor-dashboard-activation-checkbox-"+bundle.WPID).hide();
        
        var status = $(this).attr("status");
        if(status == "unchecked"){ // if you are ativating the bundle to current vendor - perform an Add bundle to vendor method.
            
            
            
            var tempAddons = []; // Temporary array to gather all the selected addtional products
            $("#vendor-dashboard-bundle-box-hidden-part-"+bundle.WPID+"  input[name=addon-to-include]").each(function(){
                tempAddons.push( parseInt($(this).val()) ); // Putting from each select input its value and name to an array
            });
            var addons = JSON.stringify(tempAddons); //Stringifying that array so we could send it to the php function
            
            bundle.addons = addons;
            bundle.price = $("#vendor-dashboard-bundle-price-input-"+bundle.WPID).val();
            
            JsonBundle = JSON.stringify(bundle);
            
            $.ajax({ //ajax call
                url:"/wp-admin/admin-ajax.php", 
                type:"POST",
                data:"action=add_bundle_to_vendor_action&bundle="+JsonBundle,
                success:function(data){
                    console.log(data);
                },
                complete:function(){
                    $("#loading-gif-"+bundle.WPID).hide();
                    $("#vendor-dashboard-activation-checkbox-"+bundle.WPID).show();
                }
            });    
            
            $(this).attr("status","checked");
            
        }else if(status == "checked"){ // if unchecked delete that row from the DB
            
            $(this).attr("status","unchecked");
            
            $.ajax({
                url:"/wp-admin/admin-ajax.php",
                type:"POST",
                data:"action=remove_bundle_from_vendor_action&bundle-id="+bundle.WPID,
                success: function(data){
                    console.log(data);
                },
                complete:function(){
                    
                }
            });
        }
        
    });
});
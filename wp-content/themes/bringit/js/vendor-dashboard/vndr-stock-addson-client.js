jQuery(document).ready(function(){
    $("#newClientAddOnButton").click(function(e){ 
        e.preventDefault();
        $("#add-special-addon-button").fadeIn("slow");
        console.log("check");
        var name = $("#newClientAddOn").val();
        
        $.ajax({
            type:"post",
            url:"/wp-admin/admin-ajax.php",
            data:"action=add_new_addon_client_action&name="+name,
            success:function(data){
                console.log(data);
                var id = data.trim();
                $.get("/pv-new-special-addon?id="+id,function(data){
                    $(".stock-list").prepend(data);
                });
            },
            complete:function(){
                $("#add-special-addon-button").fadeOut("slow");
            }
        });
    });
    
    $("#list-save-special-addson").click(function(e){
        e.preventDefault();
        var specialAddson = [];
        
        $("#update-special-addon-button").fadeIn("slow");
        
        $(".special-addon-row").each(function(){
            var specialAddon = {};
            specialAddon.id = $(this).attr("special-addon-id");
            if( $("#in_stock_"+specialAddon.id).is(":checked") ){
                specialAddon.in_stock = 1;
            }else{
                specialAddon.in_stock = 0;
            }
            
            if( $("#in_bundles_"+specialAddon.id).is(":checked") ){
                specialAddon.in_bundles = 1;
            }else{
                specialAddon.in_bundles = 0;
            }
            
            specialAddon.price = $("#price_"+specialAddon.id).val();
            
            specialAddson.push(specialAddon);
            console.log(specialAddon);
        });
        console.log(specialAddson);
        var jsonSpecialAddson = JSON.stringify(specialAddson);
        
        $.ajax({
            type:"post",
            url:"/wp-admin/admin-ajax.php",
            data:"action=update_addson_client_action&special-addson="+jsonSpecialAddson,
            success: function(data){
                console.log(data);
            },
            complete:function(){
                $("#update-special-addon-button").fadeOut("slow");
            }
        });
    });
});
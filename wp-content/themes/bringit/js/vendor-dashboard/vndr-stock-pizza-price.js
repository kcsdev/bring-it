jQuery(document).ready(function($){ 
    
    $("#pizza-price").keyup(function(){ 
        $(".no-changes-were-made").hide();
        $(".changes-svaed-successfully").hide();
        $(".saving").fadeIn();
        var price = $(this).val();
        
        $.ajax({
            type:"post",
            url:"/wp-admin/admin-ajax.php",
            data:"action=update_vendors_pizza_price_action&price="+price,
            success:function(data){
                console.log(data);
            },
            complete:function(){
                $(".saving").fadeOut("fast");
                $(".changes-svaed-successfully").fadeIn("slow");
            }
        });
    });
});
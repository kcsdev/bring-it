jQuery(document).ready(function($){
    
    $("#vndr-specials-mine-save").click(function(){
        
        var bundles = [];

        $(".bundle-price").each(function(){
            var bundle = {};
            bundle.WPID = $(this).attr("bundle-WPID");
            bundle.price = $(this).val();
            if( $("#" + bundle.WPID + "-in-stock").is(":checked") ){ 
                bundle.in_stock = 1;
            }else{
                bundle.in_stock = 0;
            }

            bundles.push(bundle);
        });
        
        var jsonBundles = JSON.stringify(bundles);
        
        $.ajax({
            type:"post",
            url:"/wp-admin/admin-ajax.php",
            data:"action=update_vendors_bundles_stock_action&bundles="+jsonBundles,
            success: function(data){
                console.log(data);
            },
            complete:function(){
                
            }
        });
        
    });
    
});
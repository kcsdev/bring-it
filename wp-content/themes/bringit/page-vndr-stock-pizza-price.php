<?php
get_header('vendor');
?>
    <div class="h-section down-space">
        <ul class="breadcrumbs">
            <li><a>מלאי</a></li>
            <li class="current"><a>מחיר פיצה</a></li>
        </ul>
    </div>

    <div class="h-section content-pane down-space" id = "vndr-stock-pizza-price-container">
        <div class = "vndr-pizza-price" >
            <p id = "pizza-price-comment" class = "comment">
                <span class = "notice">*</span>
                מחיר פיצה בודדת לפני תוספות.
            </p>
            <label class="v-align-middle">
                קבע מחיר לפיצה:
                <input type="text" id= "pizza-price" class="v-align-middle pizza-price" value = "<?php echo $vendor->getPizzaPrice(); ?>">
            </label>
            <p class = "warning" id = "pizza-price-warning">
                <span id = "" class = "no-changes-were-made" >טרם נעשה שינוי</span>
                <span id = "" class = "saving hidden" >שומר</span>
                <span id = "" class = "dot-one hidden" >.</span>
                <span id = "" class = "dot-two hidden" >.</span>
                <span id = "" class = "dot-three hidden" >.</span>
                <span id = "" class = "changes-svaed-successfully hidden" >שינויים נשמרו בהצלחה</span>
                <span id = "" class = "changes-were-not-saved hidden" >שינויים לא נשמרו</span>
            </p>
        </div>  
    </div>
<script type="text/javascript">
    jQuery(document).ready(function($){
    });
</script>
<?php
get_footer('vendor');
?>
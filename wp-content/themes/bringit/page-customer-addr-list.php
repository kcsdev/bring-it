<?php
/**
Template Name: Purchase Address List page
 */
$customer = new Customer(); 
get_header('blank'); ?>
<?php //var_dump($customer); ?>
    <div class="bi-section">
        <a class = "close-window" onclick="parent.closeMe();"></a>
        <div class="bi-container addr-list clearfix">

            <p class="pizzeria-name">רשימת כתובות</p>

            <div class="list-title"></div>

            <ul>
                
                <?php if( $customer ): ?>
                    <?php $addresses = $customer->getShippingAddressList(); ?>
                    <?php foreach( $addresses as $address ): ?>
                <li>
                    <button type="button">&gt;&gt;</button>
                    <a  href="/purchase-addr-form/"> רח' <?php echo $address->street; ?> <?php echo $address->building_number; ?>, דירה <?php echo $address->apartment_number; ?>, <?php echo $address->city; ?>
                    
                    </a>
                </li>
                
                    <?php endforeach; ?>
                <?php endif; ?>
                <li class="new-addr">
                    <a href="/purchase-addr-form/">הוספת כתובת חדשה</a>
                </li>
            </ul>

        </div>
    </div>

<script type="text/javascript">
    jQuery(document).ready(function($){
       
    });
</script>
<?php get_footer('blank'); ?>
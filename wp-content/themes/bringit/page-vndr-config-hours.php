<?php
get_header('vendor');
function get_hours(){
    //date('YmdH', time() - (BACKWARD_PERIOD * 3600));
    $start_time = mktime(0, 0, 0, 1, 1, 2015);
    for($i = 0; $i < 48; $i++ ){
        echo '<option value="';
        echo date('H:i', mktime(0, 0, 0, 1, 1, 2015) + (1800 * $i));
        echo '">' . date('H:i', mktime(0, 0, 0, 1, 1, 2015) + (1800 * $i)) . '</option>';
    }
}
//$vendors_days = $vendor->getDays();
?>
    <div class="h-section down-space">
        <ul class="breadcrumbs">
            <li><a>הגדרות</a></li>
            <li class="current"><a>הגדרת שעות פעילות</a></li>
        </ul>
    </div>

    <div class="h-section content-pane down-space clearfix">
        <form onsubmit="return false;">
            <table class="hours">
                <colgroup>
                    <col style="width:16%;" />
                    <col style="width:12%;" />
                    <col style="width:12%;" />
                    <col style="width:12%;" />
                    <col style="width:12%;" />
                    <col style="width:12%;" />
                    <col style="width:12%;" />
                    <col style="width:12%;" />
                </colgroup>
                <tr>
                    <td class="row">ימי עבודה:</td>
                    <td class="day">
                        <span>א'</span><br />
                        <input type="checkbox" 
                               name="daySu"
                               day = "1"
                               class = "dayStatus"
                               id="daySu" 
                               data-hours-list="daySuHours" <?php if($vendor->getDayStatus(1)): echo "checked"; else: echo ""; endif; ?> />
                    </td>
                    <td class="day">
                        <span>ב'</span><br />
                        <input type="checkbox" 
                               name="dayMo"
                               day = "2"
                               class = "dayStatus"
                               id="dayMo" 
                               data-hours-list="dayMoHours" <?php if($vendor->getDayStatus(2)): echo "checked"; else: echo ""; endif; ?> />
                    </td>
                    <td class="day">
                        <span>ג'</span><br />
                        <input type="checkbox" 
                               name="dayTu"
                               day = "3"
                               class = "dayStatus"
                               id="dayTu" 
                               data-hours-list="dayTuHours" <?php if($vendor->getDayStatus(3)): echo "checked"; else: echo ""; endif; ?> />
                    </td>
                    <td class="day">
                        <span>ד'</span><br />
                        <input type="checkbox" 
                               name="dayWe"
                               day = "4"
                               class = "dayStatus"
                               id="dayWe" 
                               data-hours-list="dayWeHours" <?php if($vendor->getDayStatus(4)): echo "checked"; else: echo ""; endif; ?> />
                    </td>
                    <td class="day">
                        <span>ה'</span><br />
                        <input type="checkbox" 
                               name="dayTh" 
                               id="dayTh"
                               day = "5"
                               class = "dayStatus"
                               data-hours-list="dayThHours" <?php if($vendor->getDayStatus(5)): echo "checked"; else: echo ""; endif; ?> />
                    </td>
                    <td class="day">
                        <span>ו'</span><br />
                        <input type="checkbox" 
                               name="dayFr" 
                               day = "6"
                               class = "dayStatus"
                               id="dayFr" 
                               data-hours-list="dayFrHours" <?php if($vendor->getDayStatus(6)): echo "checked"; else: echo ""; endif; ?> />
                    </td>
                    <td class="day">
                        <span>שבת</span><br />
                        <input type="checkbox" 
                               name="daySa" 
                               day = "7"
                               class = "dayStatus"
                               id="daySa" 
                               data-hours-list="daySaHours" <?php if($vendor->getDayStatus(7)): echo "checked"; else: echo ""; endif; ?> />
                    </td>
                </tr>
                <tr>
                    <td class="row">שעת סגירה:</td>
                    <td class="day">
                        <select class="closeHour" 
                                name="daySuHours"
                                day = "1"
                                <?php if($vendor->getDayStatus(1)): echo ""; else: echo "disabled"; endif; ?>
                                id="daySuHours">
                            <option value = "<?php echo $vendor->getClosingTime(1); ?>" selected >
                                <?php echo $vendor->getClosingTime(1); ?>
                            </option>
                            <?php get_hours(); ?>
                        </select>
                    </td>
                    <td class="day">
                        <select class="closeHour" 
                                name="dayMoHours" 
                                day = "2"
                                <?php if($vendor->getDayStatus(2)): echo ""; else: echo "disabled"; endif; ?>
                                id="dayMoHours">
                            <option value = "<?php echo $vendor->getClosingTime(2); ?>" selected >
                                <?php echo $vendor->getClosingTime(2); ?>
                            </option>
                            <?php get_hours(); ?>
                        </select>
                    </td>
                    <td class="day">
                        <select class="closeHour" 
                                name="dayTuHours" 
                                day = "3"
                                <?php if($vendor->getDayStatus(3)): echo ""; else: echo "disabled"; endif; ?>
                                id="dayTuHours">
                            <option value = "<?php echo $vendor->getClosingTime(3); ?>" selected >
                                <?php echo $vendor->getClosingTime(3); ?>
                            </option>
                            <?php get_hours(); ?>
                        </select>
                    </td>
                    <td class="day">
                        <select class="closeHour" 
                                name="dayWeHours" 
                                day = "4"
                                <?php if($vendor->getDayStatus(4)): echo ""; else: echo "disabled"; endif; ?>
                                id="dayWeHours">
                            <option value = "<?php echo $vendor->getClosingTime(4); ?>" selected >
                                <?php echo $vendor->getClosingTime(4); ?>
                            </option>
                            <?php get_hours(); ?>
                        </select>
                    </td>
                    <td class="day">
                        <select class="closeHour" 
                                name="dayThHours" 
                                day = "5"
                                <?php if($vendor->getDayStatus(5)): echo ""; else: echo "disabled"; endif; ?>
                                id="dayThHours">
                            <option value = "<?php echo $vendor->getClosingTime(5); ?>" selected >
                                <?php echo $vendor->getClosingTime(5); ?>
                            </option>
                            <?php get_hours(); ?>
                        </select>
                    </td>
                    <td class="day">
                        <select class="closeHour" 
                                name="dayFrHours" 
                                day = "6"
                                <?php if($vendor->getDayStatus(6)): echo ""; else: echo "disabled"; endif; ?>
                                id="dayFrHours">
                            <option value = "<?php echo $vendor->getClosingTime(6); ?>" selected >
                                <?php echo $vendor->getClosingTime(6); ?>
                            </option>
                            <?php get_hours(); ?>
                        </select>
                    </td>
                    <td class="day">
                        <select class="closeHour" 
                                name="daySaHours" 
                                day = "7"
                                <?php if($vendor->getDayStatus(7)): echo ""; else: echo "disabled"; endif; ?>
                                id="daySaHours">
                            <option value = "<?php echo $vendor->getClosingTime(7); ?>" selected >
                                <?php echo $vendor->getClosingTime(7); ?>
                            </option>
                            <?php get_hours(); ?>
                        </select>
                    </td>
                </tr>
            </table>
            <p class = "comment">
                שים לב כי פתיחת העסק במערכת מתבצעת באופן ידני כל יום, והסגירה מתבצעת אוטומטית בשעה שהוזנה בשעת הסגירה.
            </p>
            <p class = "comment">
                אם העסק פתוח מעבר לשעות הפעילות יש לפתוח אותו ידנית ולסגור בסוף הפעילות.
            </p>
        </form>
    </div>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $('.day input[type="checkbox"]').bind('change', function(){
            var $self = $(this);
            var theListId = $self.data('hours-list');
            $('#' + theListId).prop( "disabled", !($self.prop('checked')));
        })
    });
</script>
<?php
get_footer('vendor');
?>
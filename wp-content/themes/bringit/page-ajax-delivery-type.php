<?php
/**
 * This page returns value which defines if the delivery type pop-up should or should not be displayed
 *
 * For now it returns "0" as a plain text
 */

header('Content-Type: text/plain');
echo '0';

?>
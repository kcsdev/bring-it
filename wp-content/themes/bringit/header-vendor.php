<?php
Auth::vendorCheck();
$vendor = new Vendor(); // Gets current logged in vendor
?><!DOCTYPE html>
<html>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0" />

    <title>BringIt - Management System</title>

    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/vendor/vendor-styles.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/css/libs/dropzone.css" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/wp-content/themes/bringit/js/vendor-dashboard/vndr-stock-drinks.js"></script>
    <script src="/wp-content/themes/bringit/js/vendor-dashboard/vndr-stock-addson.js"></script>
    <script src="/wp-content/themes/bringit/js/vendor-dashboard/vndr-specials-mine.js"></script>
    <script src="/wp-content/themes/bringit/js/vendor-dashboard/vndr-config-delivery.js"></script>
    <script src="/wp-content/themes/bringit/js/vendor-dashboard/vndr-config-hours.js"></script>
    <script src="/wp-content/themes/bringit/js/vendor-dashboard/vndr-stock-pizza-price.js"></script>
    <script src="/wp-content/themes/bringit/js/vendor-dashboard/vndr-config-logo.js"></script>
    <script src="/wp-content/themes/bringit/js/vendor-dashboard/vndr-stock-addson-client.js"></script>
    <script src="/wp-content/themes/bringit/js/vendor-dashboard/vndr-stock-more-food.js"></script>
    <script src="/wp-content/themes/bringit/js/libs/dropzone.js"></script>
    
    
    <script type="text/javascript">
        jQuery(document).ready(function($){
            $(".toggle-btn").on("click", function(){
                if($('.ctrl-panel').hasClass('is-open')){
                    $('.ctrl-panel').css({'position':'fixed'});
                    $(".ctrl-panel").animate({'left':'-225px'}, 400, function(){
                        $('.ctrl-panel').removeClass('is-open');
                        $('.ctrl-panel').css({'left':'','position':''});
                    });
                    $('.ctrl-panel').removeClass('is-open');
                }else{
                    $(".ctrl-panel").animate({'left':0}, 400, function(){
                        $('.ctrl-panel').addClass('is-open');
                        $('.ctrl-panel').css({'left':'','position':''});
                    });
                }
            });
            
            
        });
        
        
    </script>
</head>

<body <?php body_class(); ?>>

<div class="ctrl-panel clearfix">
    <div id="toggleBtn" class="toggle-btn"><span>3</span></div>

    <ul class="ctrl-block">

        <!-- Availability -->
        <li class="availability clearfix">
            <span class="pz-status-switch">
                <input type="checkbox" id="pzStatus" />
                <label for="pzStatus"></label>
            </span>
            <span id="pzCurStatus">אני עובד</span>
        </li>
        <!-- /Availability -->

        <!-- Delivery Time -->
        <li>
            <ul class="delivery-time clearfix">
                <li class="clock-icon"></li>
                <li class="input up"></li>
                <li class="input time"><input type="text" id="dlvrTime" name="dlvrTime" readonly value="40" /></li>
                <li class="input down disabled"></li>
                <li class="input reset-time"></li>
            </ul>
        </li>
        <!-- /Delivery Time -->

        <!-- Orders -->
        <li class="orders">
            <ul id="ordersList">
                
                <?php $orders = $vendor->getOrders(); ?>
                <?php //var_dump($orders); ?>
                <?php $count = 0; ?>
                <?php foreach($orders as $order): ?>
                    <?php // classes to use:  ?>
                    <?php // current ?>
                    <?php // new ?>
                    <?php // in-proc ?>
                    <?php // en-route ?>
                <li class="order new" data-order-id="<?php echo $order->getID(); ?>">
                    <?php echo $order->getShippingAddress(); ?>
                </li>
                    <?php $count++; ?>
                <?php endforeach; ?>
            </ul>
        </li>

        <!-- /Orders -->

    </ul>
    <script type="text/javascript">
        jQuery(document).ready(function($){
            $("#ordersList").on("click", ".order", function(){
                var $self = $(this);
                if(!$self.hasClass("current")) {
                    location = '/vndr-order/?order_id=' + $self.data("order-id");
                }
            })
        });
    </script>
</div>

<div class="page-wrapper clearfix">
    <header class="page-header h-section down-space clearfix">
        <!--<a class="logout-btn"></a>-->
        <img alt="Pizza-Pizza" src="<?php echo $vendor->getLogo(); ?>" class="top-logo-img" />
        <ul class="topnav clearfix">
            <!-- ******* Item ******* -->
            <li>
                <a href="javascript:void(0);" class="icon stock">מלאי</a>
                <ul class="sublist">
                    <li><a href="/vndr-stock-drinks">שתיה</a></li>
                    <li><a href="/vndr-stock-addson">תוספות</a></li>
                    <li><a href="/vndr-stock-addson-client">תוספות אישיות</a></li>
                    <li><a href="/vndr-stock-more-food">אוכל נוסף</a></li>
                    <li><a href="/vndr-stock-pizza-price">מחיר פיצה</a></li>
                </ul>
            </li>

            <!-- ******* Item ******* -->
            <li>
                <a href="javascript:void(0);" class="icon specials">מבצעים</a>
                <ul class="sublist">
                    <li><a href="/vndr-specials-mine">מבצעים שלי</a></li>
                    <li><a href="/vndr-specials-coupons">מערכת קופונחם</a></li>
                </ul>
            </li>

            <!-- ******* Item ******* -->
            <li>
                <a href="javascript:void(0);" class="icon settings">הגדרות</a>
                <ul class="sublist">
                    <li><a href="/vndr-config-logo">לוגו אישי</a></li>
                    <li><a href="/vndr-config-delivery">הגדרות משלוח</a></li>
                    <li><a href="/vndr-config-profile">שינוי פרטים אישיים</a></li>
                    <li><a href="/vndr-config-site">הגדרות אתר אישי</a></li>
                    <li><a href="/vndr-config-hours">הגדרת שעות פעילות</a></li>
                </ul>
            </li>

            <!-- ******* Item ******* -->
            <li class="wide">
                <a href="javascript:void(0);" class="icon stats">סטטיסטיקה</a>
                <ul class="sublist">
                    <li><a href="/vndr-stat-orders">הזמנות שלי</a></li>
                    <li><a href="/vndr-stat-topsales">מה הכי נמכר באתר</a></li>
                    <li><a href="/vndr-stat-profits">הרווחים שלי</a></li>
                </ul>
            </li>

            <!-- ******* Item ******* -->
            <li>
                <a href="/vndr-messages" class="icon messages">
                    הודעות
                    <span class="msginfo">2</span>
                </a>
            </li>

            <!-- ******* Item ******* -->
            <li><a href = "<?php echo wp_logout_url('/vndr-login'); ?>" class="icon logout" id = "vndr-logout">יציאה</a></li>
        </ul>
    </header>



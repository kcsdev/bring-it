<?php
/**
Template Name: Specials Pop up page
 */
get_header('blank'); ?>

    <div class="bi-section">
        <div class="bi-container">
            <div class="hp-pane specials" style="margin: 0 auto 20px; float: none;">
                <img src="<?php bloginfo('stylesheet_directory') ?>/assets/hp-specials-title.png" alt="" class="gr-title" />

                <?php the_homepage_specials(); ?>

            </div>

            <!--<button class="close-me" onclick="parent.closeMe();"></button>-->
        </div>
    </div>

<?php get_footer('blank'); ?>
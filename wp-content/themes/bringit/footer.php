<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "#main" div and all content after.
 *
 * @package BringIt
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
</div><!-- #main -->

<div class="bi-section page-footer">
    <div class="bi-container enlarged">
        <p>
        כל הזכויות שמורות
        </p>
    </div>
</div>

<?php wp_footer(); ?>

<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/js/jquery.simplemodal.js"></script>
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/js/jquery.mfp.js"></script>

</body>
</html>

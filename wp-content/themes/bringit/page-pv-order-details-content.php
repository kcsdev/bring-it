<?php 
$order = new Order($_REQUEST["order_id"]);
$vendor = new Vendor($order->getSellerID());

//Collecting all parts of the order together
$pizzas = $order->getPizzas();
$additional_products = $order->getAdditionalProducts();
$drinks = $order->getDrinks();
$bundles = $order->getBundles();

?>

<?php /** ******************************************************************************************* **/ ?>
<?php /** Pizzas **/ ?>
<?php /** ******************************************************************************************* **/ ?>
<?php if(!empty($pizzas)): ?>
<div class = "title-row">
    <p>פיצות</p>
</div>
    <?php  foreach($pizzas as $pizza): ?> 
    <?php $addons = $pizza->getAddons(); ?>
    <?php if(empty($addons)){ $plus = ""; }else{ $plus = "+"; } ?>
        <div class="order-row">
            <span class="col-1" id = "pizza-item-col"><img src="<?php  bloginfo('stylesheet_directory') ?>/assets/icon-order-pizza.png" alt="" /></span>
            <span class="col-2">
                פיצה 
                <?php echo $plus; ?>
                <?php $i = 1; ?>
                <?php if(!empty($addons)): ?>
                    <?php foreach($addons as $addon): ?> <?php  /************************* Addons  *********************************/ ?>
                        <?php if($i == count($addons)){ $plus = ''; }else{$plus = "&#43;";} ?>
                <span class = "addon-col" id = "addon-col">
                    <?php  echo  $addon->getTitle() . "" . $plus; ?>
                </span>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </span>
            <span class = "col-3">
                <?php echo $vendor->getPizzaPrice(); ?>
                &#8362;
            </span>         
        </div>
    <?php  endforeach; ?>
<?php endif; ?>

<?php /** ******************************************************************************************* **/ ?>
<?php /** Additional Products **/ ?>
<?php /** ******************************************************************************************* **/ ?>
<div class="title-row">    
    <?php if(!empty($additional_products)): //checks if the array of additional products is not empty ?>
    <p>תוספות</p>
        <?php foreach( $additional_products as $additional_product ): ?>
            <div class="order-row">
                <span class="col-1"><img src="<?php bloginfo('stylesheet_directory') ?>/assets/icon-order-pizza.png" alt="" /></span>
                <span class="col-2"><?php echo $additional_product->getName(); //displays the name of the additional product ?></span>
                <span class="col-3"><?php echo $additional_product->getPrice(); ?>₪</span>
                <span class="col-4" onclick="removeAdditionalProduct( '<?php echo $additional_product->getID(); ?>')" ><img src="<?php bloginfo('stylesheet_directory') ?>/assets/icon-order-close.png" alt="" /></span>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>


<?php /** ******************************************************************************************* **/ ?>
<?php /** Bundles **/ ?>
<?php /** ******************************************************************************************* **/ ?>

<div class="title-row">
<?php if(!empty($bundles)):?>
    <p>מבצעים</p>
<?php endif; ?>
<?php foreach($bundles as $bundle): ?>    
    <div class="order-row">
        <span class="col-1" ><img src="<?php bloginfo('stylesheet_directory') ?>/assets/icon-order-pizza.png" alt="" /></span>
        <span class="col-2"><?php echo $bundle->getDescription(); ?></span>
        <span class = "col-3"> <?php echo $vendor->getBundlePrice($bundle->getWPID()); ?> </span>
        <span class="col-4" onclick="removeBundle('<?php echo $bundle->getID() ?>')" ><img src="<?php  bloginfo('stylesheet_directory') ?>/assets/icon-order-close.png" alt="" /></span>
    </div>
    
    <?php $drinks = $bundle->getIncludedDrinks(); // Get Drinks that are in current Bundle ?>
    <?php $additionalProducts = $bundle->getIncludedAdditionalProducts(); // Get Additional Products that are in current Bundle ?>
    <?php $pizzas = $bundle->getIncludedPizzas(); // Get pizzas that are in current Bundle ?> 
    
    <?php  foreach($pizzas as $pizza): ?> 
    <?php $addons = $pizza->getAddons(); ?>
    <?php if(empty($addons)){ $plus = ""; }else{ $plus = "+"; } ?>
        <div class="order-row">
            <span class="col-1" id = "pizza-item-col"><img src="<?php  bloginfo('stylesheet_directory') ?>/assets/icon-order-pizza.png" alt="" /></span>
            <span class="col-2">
                פיצה 
                <?php echo $plus; ?>
                <?php $i = 1; ?>
                <?php if(!empty($addons)): ?>
                    <?php foreach($addons as $addon): ?> <?php  /************************* Addons  *********************************/ ?>
                        <?php if($i == count($addons)){ $plus = ''; }else{$plus = "&#43;";} ?>
                <span class = "addon-col" id = "addon-col">
                    <?php  echo  $addon->getTitle() . "" . $plus; ?>
                </span>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </span>
                     
        </div>
    <?php  endforeach; ?>
    <?php $pizzas = array(); ?>
    <?php if(!empty($drinks)): ?>
        <div class="title-row">
            <?php $temp_stacks = array(); ?>
            <?php foreach($drinks as $drink): ?>
                <?php $temp_stack = $bundle->getDrinkStackByWPID($drink->getWPID()); ?>
                <?php $temp_stacks[] = $temp_stack;?>
            <?php endforeach; ?>
            <?php $stacks = $order->filterStacks($temp_stacks); ?>

            <?php if(!empty($stacks)): ?>
                <?php foreach($stacks as $stack): ?>
                <?php if($stack->drink->getType() == "bottled_drink"){$img = "icon-order-bottle.png";}else{$img = "icon-order-can.png";} ?>
                    <?php  ?>
                <div class="order-row">
                    <span class="col-1">
                        <img src="<?php  bloginfo('stylesheet_directory') ?>/assets/<?php echo $img; ?>" alt="" />
                    </span>
                    <span class="col-2">
                        <?php echo $stack->drink->getTitle() . " (".$stack->quantity.")"; ?>
                    </span> 
                    
                </div>
                <?php endforeach; ?>
            <?php endif; ?>
            </div>
    <?php endif; ?>
<?php endforeach; ?>
</div>



<?php /** ******************************************************************************************* **/ ?>
<?php /** Drinks **/ ?>
<?php /** ******************************************************************************************* **/ ?>
<div class="title-row"> 
<?php $temp_stacks = array(); ?>
<?php foreach($drinks as $drink): ?>
    <?php $temp_stack = $order->getDrinkStackByWPID($drink->getWPID()); ?>
    <?php $temp_stacks[] = $temp_stack;?>
<?php endforeach; ?>
<?php $stacks = $order->filterStacks($temp_stacks); ?>

<?php if(!empty($stacks)): ?>
    <p>שתייה</p>
    <?php foreach($stacks as $stack): ?>
    <?php if($stack->drink->getType() == "bottled_drink"){$img = "icon-order-bottle.png";}else{$img = "icon-order-can.png";} ?>
        <?php  ?>
    <div class="order-row">
        <span class="col-1">
            <img src="<?php  bloginfo('stylesheet_directory') ?>/assets/<?php echo $img; ?>" alt="" />
        </span>
        <span class="col-2">
            <?php echo $stack->drink->getTitle() . " (".$stack->quantity.")"; ?>
        </span> 
    </div>
    <?php endforeach; ?>
<?php endif; ?>
</div>
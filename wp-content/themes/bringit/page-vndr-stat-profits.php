<?php
get_header('vendor');
?>
    <div class="h-section down-space">
        <ul class="breadcrumbs">
            <li><a>סטטיסטיקה</a></li>
            <li class="current"><a>הרווחים שלי</a></li>
        </ul>
    </div>

    <div class="h-section content-pane down-space clearfix">
        <div class="profit-right">
            <h1 class="content">העברות קודמות</h1>
            <ul class="clearfix">
                <li class="head">תאריך</li>
                <li class="head">רווחים לתקופה</li>

                <li>15/9/15</li>
                <li><a>1000</a></li>

                <li>15/10/15</li>
                <li><a>1200</a></li>

                <li>15/11/15</li>
                <li><a>1700</a></li>
            </ul>
        </div><!-- /profit-right -->

        <div class="profit-left">
            <h1 class="content">מ-8/10/15 עד 8/11/15</h1>
            <table class="profit-details">
                <colgroup>
                    <col style="width: 40%;" />
                    <col style="width: 15%;" />
                    <col style="width: 15%;" />
                    <col style="width: 15%;" />
                    <col style="width: 15%;" />
                </colgroup>

                <tr>
                    <td class="bg-grey"></td>
                    <td class="bg-grey">מזומן</td>
                    <td class="bg-grey">אשראי</td>
                    <td class="bg-grey">PayPal</td>
                    <td class="bold bg-steel">סה&quot;כ</td>
                </tr>

                <tr>
                    <td class="bg-grey">מס' עסקאות</td>
                    <td>2</td>
                    <td>3</td>
                    <td>4</td>
                    <td class="bold bg-light-steel">9</td>
                </tr>

                <tr>
                    <td class="bg-grey">סכום עסקאות</td>
                    <td>100 &#8362;</td>
                    <td>300 &#8362;</td>
                    <td>400 &#8362;</td>
                    <td class="bg-light-steel bold">800 &#8362;</td>
                </tr>

                <tr>
                    <td class="bg-grey">קופונים של BringIt</td>
                    <td colspan="3"></td>
                    <td class="bg-light-steel bold">20 &#8362;</td>
                </tr>

                <tr>
                    <td class="bg-grey">עמלת אתר</td>
                    <td class="negative"><bdi>-7</bdi> &#8362;</td>
                    <td class="negative"><bdi>-21</bdi> &#8362;</td>
                    <td class="negative"><bdi>-28</bdi> &#8362;</td>
                    <td class="bg-light-steel bold negative"><bdi>-56</bdi> &#8362;</td>
                </tr>

                <tr>
                    <td class="bg-grey">עמלת סליקה</td>
                    <td class="negative"></td>
                    <td class="negative"><bdi>-5</bdi> &#8362;</td>
                    <td class="negative"><bdi>-4</bdi> &#8362;</td>
                    <td class="bg-light-steel bold negative"><bdi>-9</bdi> &#8362;</td>
                </tr>

                <tr>
                    <td class="bg-grey">דמי מנוי</td>
                    <td colspan="3"></td>
                    <td class="bg-light-steel bold negative"><bdi>-10</bdi> &#8362;</td>
                </tr>

                <tr>
                    <td class="bg-grey">עמלת העברה</td>
                    <td colspan="3"></td>
                    <td class="bg-light-steel bold negative"><bdi>-5</bdi> &#8362;</td>
                </tr>

                <tr>
                    <td class="bg-grey">קופונים</td>
                    <td colspan="3"></td>
                    <td class="bg-light-steel bold negative"><bdi>-20</bdi> &#8362;</td>
                </tr>

                <tr>
                    <td class="bg-grey">שורה פתוחה</td>
                    <td colspan="3"></td>
                    <td class="bg-light-steel bold negative"><bdi>-5</bdi> &#8362;</td>
                </tr>

                <tr>
                    <td class="bg-grey">רווחים לתקופה</td>
                    <td colspan="3"></td>
                    <td class="bg-steel bold"><bdi>715</bdi> &#8362;</td>
                </tr>
            </table>
            <h1 class="content">פירוט העברה</h1>
            <table class="transfer-details">
                <tr>
                    <td>רווחים לתקופה</td>
                    <td class="bg-light-steel bold">715 &#8362;</td>
                </tr>
                <tr>
                    <td>נלקח במזומן על ידי בית העסק</td>
                    <td class="bg-light-steel bold">100 &#8362;</td>
                </tr>
                <tr>
                    <td>להעברה 10/03/15</td>
                    <td class="bg-light-steel bold">615 &#8362;</td>
                </tr>
            </table>
        </div>
    </div>
<script type="text/javascript">
    jQuery(document).ready(function($){
    });
</script>
<?php
get_footer('vendor');
?>
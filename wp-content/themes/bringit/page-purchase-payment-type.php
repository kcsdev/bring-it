<?php
/**
Template Name: Purchase Payment Type page
 */
get_header('blank'); ?>
<?php $vendor = new Vendor($_SESSION['vendor']); ?>
<?php //Order::DEBUG(); ?>
    <div class="bi-section">
        <div class="bi-container payment-types clearfix">

            <p class="pizzeria-name">
                <?php echo $vendor->getName(); ?>
            </p>

            <div class="list-title"></div>

            <ul>
                <li class="payment-type credircard clearfix" payment-type = "creditcard">
                    <a>תשלום באשראי</a>
                </li>
                <li class="payment-type cash clearfix" payment-type = "cash">
                    <a>תשלום במזומן</a>
                </li>
                <li class="payment-type paypal clearfix" payment-type = "paypal">
                    <a>תשלום באמצעות PayPal</a>
                </li>
            </ul>

        </div>
    </div>

<script type="text/javascript">
    function redirectTo(href){
        window.top.location.href = href;
    }
    
</script>
<?php get_footer('blank'); ?>
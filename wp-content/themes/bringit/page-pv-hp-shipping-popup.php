<?php $cities = Address::getCities(); ?>
<?php $streets = Address::getStreets(); ?>
<?php if(isset($_GET['type']) && !empty($_GET['type']) && $_GET['type'] == 'shipping-form') { ?>

<div class="shipping-form">
    <form onsubmit="return validateShippingForm(this);">
        
        <?php the_loading_animation(); ?>    
        <input type = 'hidden' value = 'shipping' name = 'theMethod' />
        
        <label class="clearfix label-the-city">
            עיר

            <span class="mandatory-field">*</span>
<!--
            <input type="text" 
                   name = "theCity" 
                   class="autocomplete-field" 
                   id="cities" />
-->
            <select id="city-combobox" name = "theCity" class = "cities">
            <?php foreach($cities as $city): ?>
                <option></option>
                <option value = "<?php echo trim($city->name); ?>"><?php echo $city->name; ?></option>
            <?php endforeach; ?>
            </select>
        </label>
        
        
    
        <label class="clearfix label-the-street">
            רחוב
            <span class="mandatory-field">*</span>
<!--            <input type = "text" 
                   name="theStreet" 
                   class="autocomplete-field disabled" 
                   id="streets" disabled > -->
            <select id = "street-comboxbox" name = "theStreet" class = "streets"  >
                <option></option>
            </select>
        </label>
        
       
       <label class="clearfix label-the-number">
           מס' בית
           <span class="mandatory-field">*</span>
           <input type="text" 
                  name="theNumber"  />
       </label>
        
        <label class="no-border">
            <input type="radio" name="filter" value="all" checked />
            הצג הכל
        </label>

        <label class="no-border">
            <input type="radio" name="filter" value="kosher" />
            כשר
        </label>

        <label class="no-border">
            <input type="radio" name="filter" value="glatt-kosher" />
            כשר בד&quot;ץ
        </label>
        
        
        <label class="no-border clearfix">
            <button type="submit"></button>
        </label>
        
    </form>
</div>

<?php } elseif(isset($_GET['type']) && !empty($_GET['type']) && $_GET['type'] == 'pick-up-form') { ?>
    <div class="shipping-form">
        <form onsubmit="return validateShippingForm(this);">
            <?php the_loading_animation(); ?>
            <input type = 'hidden' value = 'takeaway' name = 'theMethod' />
            
            <label class="clearfix label-the-city">
                עיר

                <span class="mandatory-field">*</span>
    <!--
                <input type="text" 
                       name = "theCity" 
                       class="autocomplete-field" 
                       id="cities" />
    -->
                <select id="city-combobox" name = "theCity" class = "cities">
                <?php foreach($cities as $city): ?>
                    <option></option>
                    <option value = "<?php echo trim($city->name); ?>"><?php echo $city->name; ?></option>
                <?php endforeach; ?>
                </select>
            </label>
        
        
    
            <label class="clearfix label-the-street">
                רחוב
                <span class="mandatory-field">*</span>
    <!--            <input type = "text" 
                       name="theStreet" 
                       class="autocomplete-field disabled" 
                       id="streets" disabled > -->
                <select id = "street-comboxbox" name = "theStreet" class = "streets"  >
                    <option></option>
                </select>
            </label>

            <label class="no-border">
            <input type="radio" name="filter" value="all" checked />
                הצג הכל
            </label>

            <label class="no-border">
                <input type="radio" name="filter" value="kosher" />
                כשר
            </label>

            <label class="no-border">
                <input type="radio" name="filter" value="glatt-kosher" />
                כשר בד&quot;ץ
            </label>

            <label class="no-border clearfix">
                <button type="submit"></button>
            </label>
        </form>
    </div>
<?php } ?>

<script type="text/javascript">
    (function($){
        
        $("#city-combobox").combobox();
        $(".streets").combobox();
        $(".label-the-city .custom-combobox").on("blur","input",function(){ 
            //alert("check");
            var city = $(this).val();

            $.ajax({
                url: "/wp-admin/admin-ajax.php",
                type: "post",
                data: "action=autocomplete_action&city="+city,
                success:function(result){         
//                    console.log(result);
                    var parsedResult = JSON.parse(result);
//                    console.log(parsedResult);
                    if(parsedResult.success){ 

                        $(".streets").removeClass("disabled");
                        $(".streets").prop("disabled",false);
                        
                        for(var i = 0; i < parsedResult.streets.length; i++){
                            $(".streets").append("<option value = '"+ parsedResult.streets[i].name +"'>"+ parsedResult.streets[i].name +"</option>");
                        }

                        
                    }else{
                        $(".streets").addClass("disabled");
                        $(".street").prop("disabled",true);
                        
                        $(".streets > option").remove();
                    }
                    
                },
                complete(){

                }
            });
        });

    })(jQuery);
    
    function validateShippingForm(theForm){
            jQuery(".shipping-form label").removeClass("error");
            var formData = {
                "theCity": jQuery.trim(theForm.theCity.value + ""),
                "theStreet": jQuery.trim(theForm.theStreet.value + ""),
                "theNumber": (theForm.theNumber) ? jQuery.trim(theForm.theNumber.value + "") : null,
                "theMethod": jQuery.trim(theForm.theMethod.value + "")
            };
            var requestIsValid = true;

            if(formData.theCity.length == 0){
                jQuery(".shipping-form label.label-the-city").addClass("error");
                requestIsValid = false;
            }
            if(formData.theStreet.length == 0){
                jQuery(".shipping-form label.label-the-street").addClass("error");
                requestIsValid = false;
            }

            if(theForm.theNumber && formData.theNumber.length == 0){
                jQuery(".shipping-form label.label-the-number").addClass("error");
                requestIsValid = false;
            }

            if(requestIsValid){ 
                jQuery("#loading-animation").show();
                jQuery.ajax({
                    type: 'POST',
                    url: "wp-admin/admin-ajax.php",       // *** SHOULD BE CHANGED TO THE REAL URL //The url is always the same in WP 
                    data:'action=add_shipping_details_action&the_city='+formData.theCity+'&the_street='+formData.theStreet+'&the_number='+formData.theNumber+'&the_method='+formData.theMethod,
                    success: function (data) { 
                        console.log(data);
                        var res = JSON.parse(data.trim());
                        //console.log(response);
                        if( data.trim() == "false" ){                    
                            jQuery("input[name=theCity]").css("box-shadow", "0 0 5px 2px red");
                            jQuery("input[name=theStreet]").css("box-shadow", "0 0 5px 2px red");
                        }else{ 
                            if(res.is_bundle){
                                location = '/make-bundle-pizza/';    
                            }else{
                                location = '/make-pizza/';
                            }

                        }
                    },
                    error: function (xhr, status, error) { 
                        console.log("Oops, something went wrong!");
                    },
                    complete: function (xhr, status) { 
                        jQuery("#loading-animation").hide();
                    },
    //                dataType: 'text',
                    cache: false
                });
            }
            return false;
        }
</script>

<?php
$special_addon = new AddonClient($_GET["id"]);
?>
<?php if(!$special_addon->isApproved()): ?>
                        <?php $disabled = "disabled"; ?>
                    <?php endif; ?>
                <tr class = '<?php echo $disabled; ?>' >
                    <td class="product-name">
                        <?php echo $special_addon->getName(); ?>
                    </td>
                    <td class="align-center">
                        <?php if($special_addon->inStock()): ?>
                            <?php $checked = "checked"; ?>
                        <?php else: ?>
                            <?php $checked = ""; ?>
                        <?php endif; ?>
                        <input type="checkbox" name="hotPeppers" id="hotPeppers" <?php echo $checked; ?> />
                    </td>
                    <td class="align-center">
                        <?php if($special_addon->inBundles()): ?>
                            <?php $checked = "checked"; ?>
                        <?php else: ?>
                            <?php $checked = ""; ?>
                        <?php endif; ?>
                        <input type="checkbox" name="hotPeppers2" id="hotPeppers2" <?php echo $checked; ?> />
                    </td>
                    <td class="align-center">
                        <input type="text" name="hot_peppers" id="hot_peppers" maxlength="5" value = "<?php echo $special_addon->getPrice(); ?>" />
                        &#8362;
                    </td>
                    <td class="align-center">
                    <?php if($special_addon->isApproved() == 1): ?>
                        <?php echo "ממתין לאישור"; ?>
                    <?php elseif($special_addon->isApproved() == 2): ?>
                        <?php echo "אושר"; ?>
                    <?php else: ?>
                        <?php echo "לא אושר"; ?>
                    <?php endif; ?>
                    </td>
                </tr>
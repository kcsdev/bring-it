<?php

/*
 * Returns the home page specials DIVs.
 * THIS IS THE PROTOTYPE FUNCTIONS
 */


function get_rate_stars($green, $grey){
    $result = '';
    for($i = 0; $i < $green; $i++ ){
        $result .= '<img src="' . get_stylesheet_directory_uri() . '/assets/star.png" alt="" />';
    }
    for($i = 0; $i < $grey; $i++ ){
        $result .= '<img src="' . get_stylesheet_directory_uri() . '/assets/star-grey.png" alt="" />';
    }
    return $result;
}


?>
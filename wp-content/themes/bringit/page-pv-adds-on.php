<img src="<?php bloginfo('stylesheet_directory') ?>/assets/title-addson-pop-up.png" alt="" class="title-addson-pop-up" />

<ul class="adds-on-tiles">
    <li>
        <img src="<?php bloginfo('stylesheet_directory') ?>/assets/temp/adds-on-1.jpg" alt="" />
        <p class="title">לחם שום</p>
        <p class="descr">חביתית או חמיטה, הנקראת גם בלועזית פנקייק או קרפ</p>
        <div class="bottom">
            <span>11.90 &#8362;</span>
            <button type="button" onclick="location='/sign-in/';">הוסף</button>
        </div>
    </li>

    <li>
        <img src="<?php bloginfo('stylesheet_directory') ?>/assets/temp/adds-on-2.jpg" alt="" />
        <p class="title">מיני פנקייקס</p>
        <p class="descr">חביתית או חמיטה, הנקראת גם בלועזית פנקייק או קרפ</p>
        <div class="bottom">
            <span>11.90 &#8362;</span>
            <button type="button" onclick="location='/sign-in/';">הוסף</button>
        </div>
    </li>
</ul>

<button class="no-adds-on" onclick="location='/sign-in/';"></button>
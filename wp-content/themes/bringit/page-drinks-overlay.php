<?php
/**
Template Name: Drinks Overlay page
 */
get_header('blank'); 
$cart = Cart::OpenCart();
$drinks = Drink::getAll();
$cart_drinks = $cart->getAllStacks();
$vendor = new Vendor($_REQUEST["vendor-id"]);
$offered_drinks = $vendor->getDrinks();

//var_dump($cart_drinks);

?>

    <div class="bi-section">
        <?php the_loading_animation(); ?>
        <div class="bi-container clearfix drinks">

            <div id="drinkTiles" class="drink-tiles">
                <?php if(!empty($cart_drinks)): ?>
                    <?php foreach($cart_drinks as $stack): ?>
                <div class="item drink-from-cart"  drink-WPID = "<?php echo $stack->drink->getWPID(); ?>" drink-quantity = "<?php echo $stack->quantity; ?>">
                    <img src="<?php echo $stack->drink->getImage();?>" alt="" />
                    <p class="name"><?php echo $stack->drink->getTitle(); ?></p>
<!--                        <p class="descr">בקבוק קוקה קולה משפחתי</p>-->
                    <div class="bottom">
                        <span><?php echo $stack->drink->getVendorsPrice($vendor->getID()); ?> &#8362;</span>
                    </div>
                    <ul class="controls">
                        <li class="plus"></li>
                        <li class="total" drink-WPID = "<?php echo $stack->drink->getWPID(); ?>"><?php echo $stack->quantity; ?></li>
                        <li class="minus"></li>
                    </ul>
                </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
            
                <hr style = "clear:right">

            <div id="drinkTiles" class="drink-tiles">
                <?php foreach($offered_drinks as $drink): ?>
                <div class="item drink-from-vendor" drink-WPID = "<?php echo $drink->getWPID(); ?>" drink-quantity = "0" >
                    <img src="<?php echo $drink->getImage();?>" alt="" />
                    <p class="name"><?php echo $drink->getTitle(); ?></p>
<!--                        <p class="descr">בקבוק קוקה קולה משפחתי</p>-->
                    <div class="bottom">
                        <span><?php echo $drink->getVendorsPrice($vendor->getID()); ?> &#8362;</span>
                    </div>
                    <ul class="controls">
                        <li class="plus"></li>
                        <li class="total" drink-WPID = "<?php echo $drink->getWPID(); ?>">0</li>
                        <li class="minus"></li>
                    </ul>
                </div>
                <?php endforeach; ?>

            </div>
            <button class="close-me" id = "update-cart-drinks"></button>
        </div>

        <script type="text/javascript">
            jQuery(document).ready(function($){  
                
                $("#update-cart-drinks").click(function(){ 
                    
                    var drinks = [];
                    $("#loading-animation").fadeIn("slow");
                    $(".drink-from-cart").each(function(){ // gathering the drinks that are left in the cart;
                        var drink = {};
                        drink.quantity = $(this).attr("drink-quantity");
                        drink.WPID = $(this).attr("drink-WPID");
                        
                        drinks.push(drink);
                    });
                    $(".drink-from-vendor").each(function(){ // gathering the drinks that are added to the cart
                        var drink = {};
                        drink.quantity = $(this).attr("drink-quantity");
                        drink.WPID = $(this).attr("drink-WPID");
                        
                        drinks.push(drink);
                    });
                    
                    var jsonDrinks = JSON.stringify(drinks);
                    
                    $.ajax({
                        type:"post",
                        url:"/wp-admin/admin-ajax.php",
                        data:"action=cart_change_drinks_action&drinks="+jsonDrinks,
                        success:function(data){
                            console.log(data);
                        },
                        complete:function(){
                            $("#loading-animation").fadeOut("slow");
                            parent.closeMe();
                            window.top.location.reload();
                        }
                    });
                    
                });
                
               $(".drink-tiles .drink-from-cart .controls .plus").bind("click", function(){ //PLUS drink from cart
                   $self = $(this);
                   $self.next().text(parseInt($self.next().text()) + 1);
                   
                   var WPID = $self.next().attr("drink-WPID");
                   var quantity =   parseInt($(".drink-from-cart[drink-WPID="+WPID+"]").attr("drink-quantity"));
                   quantity = quantity + 1;
                   
                   console.log(quantity);
                   $(".drink-from-cart[drink-WPID="+WPID+"]").attr("drink-quantity", quantity);
                   if(parseInt($self.next().text()) > 0 && $self.siblings(".minus").hasClass("zero")){
                       $self.siblings(".minus").removeClass("zero")
                   }
               })

                $(".drink-tiles .drink-from-cart .controls .minus").bind("click", function(){ // Minus drink from cart
                    $self = $(this);
                    var currentNum = parseInt($self.prev().text());
                    
                    var WPID = $self.prev().attr("drink-WPID");
                    var quantity = parseInt($(".drink-from-cart[drink-WPID="+WPID+"]").attr("drink-quantity"));
                    
                    if(currentNum > 0){ 
                        
                        quantity = quantity - 1;
                        $(".drink-from-cart[drink-WPID="+WPID+"]").attr("drink-quantity", quantity);
                        
                        $self.prev().text(currentNum - 1);
                        if(parseInt($self.prev().text()) <= 0){
                            $self.prev().text("0");
                            $self.addClass("zero");
                            
                        }
                    }
                });
                
                
                $(".drink-tiles .drink-from-vendor .controls .plus").bind("click", function(){ // PLUS drink from vendor
                   $self = $(this);
                   $self.next().text(parseInt($self.next().text()) + 1);
                   
                   var WPID = $self.next().attr("drink-WPID");
                   var quantity =   parseInt($(".drink-from-vendor[drink-WPID="+WPID+"]").attr("drink-quantity"));
                   quantity = quantity + 1;
                   
                   console.log(quantity);
                   $(".drink-from-vendor[drink-WPID="+WPID+"]").attr("drink-quantity", quantity);
                   if(parseInt($self.next().text()) > 0 && $self.siblings(".minus").hasClass("zero")){
                       $self.siblings(".minus").removeClass("zero")
                   }
               })

                $(".drink-tiles .drink-from-vendor .controls .minus").bind("click", function(){ // MINUS drink from vendor
                    $self = $(this);
                    var currentNum = parseInt($self.prev().text());
                    
                    var WPID = $self.prev().attr("drink-WPID");
                    var quantity =   parseInt($(".drink-from-vendor[drink-WPID="+WPID+"]").attr("drink-quantity"));
                    
                    if(currentNum > 0){ 
                        
                        quantity = quantity - 1;
                        $(".drink-from-vendor[drink-WPID="+WPID+"]").attr("drink-quantity", quantity);
                        
                        $self.prev().text(currentNum - 1);
                        if(parseInt($self.prev().text()) <= 0){
                            $self.prev().text("0");
                            $self.addClass("zero");
                            
                        }
                    }
                });
            });
        </script>
    </div>

<?php get_footer('blank'); ?>
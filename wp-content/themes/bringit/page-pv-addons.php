<?php $order = Order::OpenOrder(); ?>
<?php $vendor = new Vendor($order->getSellerID()); ?>
<?php $additional_products = $vendor->getAdditionalProductsToSell(); ?>
<?php // var_dump($additional_products); ?>
<?php foreach($additional_products as $additional_product): ?>
<div class="item">
    <img id = "addon-image" src="<?php echo $additional_product->getImage(); ?>" alt="" />
    <p class="price" id = "addon-price"><?php echo $additional_product->getPrice(); ?>&#8362;</p>
    <p class="name" id = "addon-name"><?php echo $additional_product->getName(); ?></p>
    <p class="descr" id = "addon-descr"><?php echo $additional_product->getDescription(); ?></p>
    <div class="bottom" id = "addon-bottom">
        <button onclick = "addAdditionalProductToCart(event)" 
                type="button" 
                class="btn-add-drink" 
                additional-product-id = "<?php echo $additional_product->getID(); ?>">
            הוסף
        </button>
    </div>
</div>
<?php endforeach; ?>
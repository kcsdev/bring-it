<?php

//------------------------------------------------------------------------------------


//Delete product from cart - Ajax

function delete_from_cart(){ 
    if(isset($_POST["id"]) && isset($_POST["product_type"])){ 

        $id = $_POST["id"];
        $product_type = $_POST["product_type"];
        
        if($product_type == "pizza"){ 
            
            $pizzas = $_SESSION['pizzas'];
            
             $_SESSION['pizzas'] = array_filter( 
               $pizzas,
                function($e){ 
                   return $e->id != $_POST["id"];
                }
            );
            
        }elseif($product_type == "drink"){ 
            
            unset($_SESSION["drinks"][$id]);
            
        }elseif($product_type == "bundle"){
            
            $bundles = $_SESSION['bundles'];
            
             $_SESSION['bundles'] = array_filter( 
               $bundles,
                function($e){ 
                   return $e->id != $_POST["id"];
                }
            );
            
        }elseif($product_type == "additional_product"){
            
            $additional_products = $_SESSION['additional_products'];
            
             $_SESSION['additional_products'] = array_filter( 
               $additional_products,
                function($e){ 
                   return $e->id != $_POST["id"];
                }
            );
            
        }
    }else if( isset($_POST['id']) && !isset($_POST['product_type']) ){ 
        
        echo "Error: You have to tell me what product type am I deleting...";
            
    }else if( !isset($_POST['id']) ){
        
        echo "Error: You have to tell me what product am I deleting by giving me its Identifier!!!";
        
    }
}


//------------------------------------------------------------------------------------

function remove_addon_from_current_pizza(){
    if(isset($_POST['id']) && isset($_POST['pizza_id'])){ 
        
        $id = $_POST['id'];
        $pizza_id = $_POST['pizza_id'];
        
        echo $id ." / ". $pizza_id;
        
        $pizzas = $_SESSION['pizzas'];
        foreach($_SESSION['pizzas'] as $pizza){ 
            echo "...".$pizza->id;
            if($pizza->id == $pizza_id){
                foreach($pizza->addons as $key => $addon){ 
                    if($addon['id'] == $id){  
                        unset($pizza->addons[$key]);
                    }
                }
            }else continue;
        }
        
    }else{
        echo "No Addon ID has been recieved... try again!";
    }
}
add_action("wp_ajax_nopriv_remove_addon_from_current_pizza_action","remove_addon_from_current_pizza");
add_action("wp_ajax_remove_addon_from_current_pizza_action","remove_addon_from_current_pizza");
?>
<?php

$vendor = new Vendor();  


// Add new delivery area to vendor 
add_action("wp_ajax_add_new_delivery_area_action",array($vendor,"addNewArea"));
add_action("wp_ajax_nopriv_add_new_delivery_area_action",array($vendor,"addNewArea"));

// Update existing area of a vendor
add_action("wp_ajax_update_delivery_area_action",array($vendor,"updateArea"));
add_action("wp_ajax_nopriv_update_delivery_area_action",array($vendor,"updateArea"));

// Remove existing area of a vendor
add_action("wp_ajax_remove_delivery_area_action",array($vendor,"removeArea"));
add_action("wp_ajax_nopriv_remove_delivery_area_action",array($vendor,"removeArea"));

// Enter Vendor Mode
add_action("wp_ajax_choose_vendor_action","Vendor::chooseVendor");
add_action("wp_ajax_nopriv_choose_vendor_action","Vendor::chooseVendor");

// Update Vendors Drinks Stock
add_action("wp_ajax_update_vendors_drinks_stock_action",array($vendor,"updateDrinksStock"));
add_action("wp_ajax_nopriv_update_vendors_drinks_stock_action",array($vendor,"updateDrinksStock"));

// Update Vendors Addson Stock
add_action("wp_ajax_update_vendors_addson_stock_action",array($vendor,"updateAddsonStock"));
add_action("wp_ajax_nopriv_update_vendors_addson_stock_action",array($vendor,"updateAddsonStock"));

// Update Vendors Bundles Stock
add_action("wp_ajax_update_vendors_bundles_stock_action",array($vendor,"updateBundlesStock"));
add_action("wp_ajax_nopriv_update_vendors_bundles_stock_action",array($vendor,"updateBundlesStock"));

// Update Vendors Day status (open/closed)
add_action("wp_ajax_update_vendors_day_status_action",array($vendor,"updateDayStatus"));
add_action("wp_ajax_nopriv_update_vendors_day_status_action",array($vendor,"updateDayStatus"));

// Update Vendors Closting Time
add_action("wp_ajax_update_vendors_closing_time_action",array($vendor,"updateClosingTime"));
add_action("wp_ajax_nopriv_update_vendors_closing_time_action",array($vendor,"updateClosingTime"));

// Update Vendors Pizza Price
add_action("wp_ajax_update_vendors_pizza_price_action",array($vendor,"updatePizzaPrice"));
add_action("wp_ajax_nopriv_update_vendors_pizza_pizza_action",array($vendor,"updatePizzaPrice"));

// Update Vendors Logo
add_action("wp_ajax_update_vendors_logo_action",array($vendor,"setLogo"));
add_action("wp_ajax_nopriv_update_vendors_logo_action",array($vendor,"setLogo"));

// Add Addon Client
add_action("wp_ajax_add_new_addon_client_action",array($vendor,'addAddonClient'));
add_action("wp_ajax_nopriv_add_new_addon_client_action",array($vendor,'addAddonClient'));

// Update Addson Client
add_action("wp_ajax_update_addson_client_action",array($vendor,"updateAddsonClient"));
add_action("wp_ajax_nopriv_update_addson_client_action",array($vendor,"updateAddsonClient"));

// Add Additional Product
add_action("wp_ajax_add_new_additional_product_action",array($vendor,"addAdditionalProduct"));
add_action("wp_ajax_nopriv_add_new_additional_product_action",array($vendor,"addAdditionalProduct"));

// Update Additional Products
add_action("wp_ajax_update_additional_products_action", array($vendor,"updateAdditionalProducts"));
add_action("wp_ajax_nopriv_update_additional_products_action",array($vendor,"updateAdditionalProducts"));
?>
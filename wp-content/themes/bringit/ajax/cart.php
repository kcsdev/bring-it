<?php
if(isset($_SESSION['Cart'])){
    $cart = unserialize($_SESSION['Cart']);
    
    add_action("wp_ajax_empty_the_cart_action",array($cart,"emptyCart")); // NOTE Empty the Cart
    add_action("wp_ajax_nopriv_empty_the_cart_action",array($cart,"emptyCart"));

    //pizzas
    add_action('wp_ajax_add_pizza_to_cart_action', array($cart,"addPizza")); // NOTE Add Pizza to Cart
    add_action('wp_ajax_nopriv_add_pizza_to_cart_action', array($cart,"addPizza"));

    add_action("wp_ajax_remove_pizza_from_cart_action",array($cart,"removePizza")); // NOTE Remove Pizza from Cart
    add_action("wp_ajax_nopriv_remove_pizza_from_cart_action",array($cart,"removePizza"));

    //drinks
    add_action('wp_ajax_add_drink_to_cart_action', array($cart,"addDrink")); // NOTE Add Drink to Cart
    add_action('wp_ajax_nopriv_add_drink_to_cart_action', array($cart,"addDrink"));

    add_action("wp_ajax_remove_drink_from_cart_action",array($cart,"removeDrink")); // NOTE Remove Drink from Cart
    add_action("wp_ajax_nopriv_remove_drink_from_cart_action",array($cart,"removeDrink"));
    
    // Additinal Products
    add_action('wp_ajax_add_additional_product_to_cart_action', array($cart,"addAdditionalProduct")); // NOTE Add Additional Product to Cart
    add_action('wp_ajax_nopriv_add_additional_product_to_cart_action', array($cart,"addAdditionalProduct"));
    
    add_action("wp_ajax_remove_additional_product_from_cart_action",array($cart,"removeAdditionalProduct")); // NOTE Remove Additional Product from Cart
    add_action("wp_ajax_nopriv_remove_additional_product_from_cart_action",array($cart,"removeAdditionalProduct"));
    
    // Pizza Addons
    add_action("wp_ajax_remove_addon_from_current_pizza_action",array($cart,"removeAddon")); // NOTE Remove Addon from Pizza that in the Cart
    add_action("wp_ajax_nopriv_remove_addon_from_current_pizza_action",array($cart,"removeAddon"));
    
    // Bundles
    add_action("wp_ajax_add_bundle_to_cart_action",array($cart,"addBundle")); // NOTE Add Bundle to Cart
    add_action("wp_ajax_nopriv_add_bundle_to_cart_action",array($cart,"addBundle"));
    
    add_action("wp_ajax_remove_bundle_from_cart_action",array($cart,"removeBundle")); // NOTE Remove Bundle to Cart
    add_action("wp_ajax_nopriv_remove_bundle_from_cart_action", array($cart,"removeBundle"));
    
    add_action("wp_ajax_remove_pizza_from_bundle_action",array($cart, "removePizzaFromBundle")); // NOTE Remove Pizza from Bundle
    add_action("wp_ajax_nopriv_remove_pizza_from_bundle_action",array($cart, "removePizzaFromBundle"));
    
    add_action("wp_ajax_remove_drink_from_bundle_action",array($cart,"removeDrinkFromBundle")); // NOTE Remove Drink from Bundle
    add_action("wp_ajax_nopriv_remove_drink_from_bundle_action", array($cart,"removeDrinkFromBundle"));
    
    add_action("wp_ajax_make_pizza_check_if_bundles_ready_action",array($cart,"checkBundlesForPizzas")); // NOTE Check If bundle are ready to go to the next page 
    add_action("wp_ajax_nopriv_make_pizza_check_if_bundles_ready_action",array($cart,"checkBundlesForPizzas"));
    
    
    
    // Save Cart to Order
    
    add_action("wp_ajax_save_payment_type_to_order_action",array($cart,"savePaymentTypeToOrder")); // NOTE Save Cart to Order
    add_action("wp_ajax_nopriv_save_payment_type_to_order_action",array($cart,"savePaymentTypeToOrder"));
    
    add_action("wp_ajax_save_cart_to_order_action",array($cart,"saveCartToOrder")); // NOTE Save Cart to Order
    add_action("wp_ajax_nopriv_save_cart_to_order_action",array($cart,"saveCartToOrder"));
    
    // Change
    
    add_action("wp_ajax_cart_change_drinks_action",array($cart,"changeDrinks"));
    add_action("wp_ajax_nopriv_cart_change_drinks_action",array($cart,"changeDrinks"));
    
}
?>
<?php 
// Customer Login
add_action("wp_ajax_custom_login_action","Auth::login");
add_action("wp_ajax_nopriv_custom_login_action","Auth::login");

//Customer Registration
add_action("wp_ajax_registration_action","Auth::register");
add_action("wp_ajax_nopriv_registration_action","Auth::register");

// Vendor Login
add_action("wp_ajax_vndr_login_action","Auth::vendorLogin");
add_action("wp_ajax_nopriv_vndr_login_action","Auth::vendorLogin");
?>
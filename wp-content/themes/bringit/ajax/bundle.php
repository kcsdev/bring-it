<?php 

if($bundle = Bundle::Open()){
    add_action("wp_ajax_add_pizza_to_bundle_action",array($bundle,"addPizza")); // NOTE add a pizza to a bundle in the cart
    add_action("wp_ajax_nopriv_add_pizza_to_bundle_action",array($bundle,"addPizza"));
    
    add_action("wp_ajax_add_drink_to_bundle_action",array($bundle,"addDrink")); // Note add a drink to a bundle in the cart
    add_action("wp_ajax_nopriv_add_drink_to_bundle_action",array($bundle,"addDrink"));
    
    
}
add_action("wp_ajax_start_new_bundle_action","Bundle::AjaxCreate");
add_action("wp_ajax_nopriv_start_new_bundle_action","Bundle::AjaxCreate");

?>
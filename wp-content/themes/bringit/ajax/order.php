<?php
if(isset($_SESSION['Order'])){
    $order = unserialize($_SESSION['Order']);
    
    add_action('wp_ajax_add_shipping_details_action', array($order,'addShippingInformation')); 
    add_action('wp_ajax_nopriv_add_shipping_details_action', array($order,'addShippingInformation'));
    
    add_action('wp_ajax_add_shipping_address_to_order_action',array($order,'addFullShippingInformation'));
    add_action('wp_ajax_nopriv_add_shipping_address_to_order_action',array($order,'addFullShippingInformation'));
}

?>
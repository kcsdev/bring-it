<?php
/**
Template Name: Vendor Dashboard
 */
wp_head();
acf_form_head();
//get_header();

$vendor = new Vendor();
$cities = Address::getCities();
$vendor_cities = $vendor->getCities();
var_dump( $vendor->getCities() );
$add_city_block_title = "איזורי משלוח";
//$cities = $vendor->getCities();
//var_dump($vendor);
foreach($vendor_cities as $key => $vendor_city){ 
    
    foreach($cities as $k => $city){
        if($city->name == $vendor_city->name){
            unset($cities[$k]);
        }
        break;
    }
    
}
$options = array( 
    'id' => 743,
    'post_id' => "user_" . $vendor->getID(),
    'field_groups' => array(743),
    'form' => true, 
    'return' => add_query_arg( 'updated', 'true', get_permalink() ), 
    'html_before_fields' => '',
    'html_after_fields' => '',
    'submit_value' => 'Update' 
);
?>
<hr>
<div id = "add-city-block">
    <h3><?php echo $add_city_block_title; ?></h3>
    
    <select id = "vendor-dashboard-choose-city-select" disabled> <?php // First you choose the city. ?>
        <option value="none">-Choose City-</option>
        <?php foreach($cities as $city): ?>
        <option value = "<?php echo $city->name; ?>"><?php echo $city->name; ?></option>
        <?php endforeach; ?>
    </select>
    
    <button id = "vendor-dashboard-choose-city-button" disabled>Add City</button> <?php // ?>
    
    <div id = "vendor-dashboard-vendoring-cities" class = "vendor-dashboard-block">
        <?php the_loading_animation(); ?>
        <?php foreach($vendor_cities as $vendor_city): ?>
            <?php $city_regions = Address::getRegionsByCity($vendor_city); ?>
            <?php $vendor_regions = $vendor->getRegionsByCity($vendor_city->ID); ?>
        
            <?php foreach($city_regions as $key_c => $city_region): ?>
                <?php foreach($vendor_regions as $key_v => $vendor_region): ?>
        
                    <?php if($city_region->name == $vendor_region->name): ?>
                        <?php unset($city_regions[$key_c]); ?>
                    <?php endif; ?>
        
                <?php endforeach; ?>
            <?php endforeach; ?>
        
        <div class = "vendor-dashboard-vendoring-city vendor-dashboard-row" id = "city-<?php echo $vendor_city->ID; ?>"> <?php // City block starts here ?>
            
            <div class = "vendor-dashboard-city-detail">
                <span class = "vendor-dashboard-row-name"><?php echo $vendor_city->name; ?></span>
                <button class = "button button-warning" id = "vendor-dashboard-vendoring-city-row-remove-button" onclick = "removeCity(<?php echo $vendor_city->ID; ?>)" >Remove</button>
                <button class = "button" id = "vendor-dashboard-vendoring-city-row-edit-button-<?php echo $vendor_city->ID; ?>" onclick = "runEffect(<?php echo $vendor_city->ID; ?>)">Edit</button>
                <div class = "clearfix"></div>
            </div>
            
            <div id = "vendor-dashboard-edit-regions-<?php echo $vendor_city->ID; ?>" class = "vendor-dashboard-edit-regions">
                <?php foreach($vendor_regions as $vendor_region): ?>
                <formrow>
                    <label class = "region-label"><?php echo $vendor_region->name; ?></label>
                    <input 
                           type= "number" 
                           placeholder = "Shipping Price" 
                           value = "<?php echo $vendor_region->shippingPrice; ?>" 
                           name = "shipping_price_<?php echo $vendor_region->name; ?>" 
                           id = "shipping_price_<?php echo $vendor_region->name; ?>" 
                           class = "" /> <?php // shipping price ?>
                    <input 
                           type= "number" 
                           placeholder = "Shipping Time" 
                           value = "<?php echo $vendor_region->shippingTime; ?>" 
                           name = "shipping_time_<?php echo $vendor_region->name; ?>" 
                           id = "shipping_time_<?php echo $vendor_region->name; ?>" 
                           class = "" /> <?php // shipping time ?>
                    <input 
                           style = "width:280px;" 
                           type= "number" 
                           value = "<?php echo $vendor_region->minimumOrderPriceForFreeShipping; ?>" 
                           placeholder = "Minimum Price For Free Shipping" 
                           name = "minimum_order_price_for_free_shipping_<?php echo $vendor_region->name; ?>" 
                           id = "minimum_order_price_for_free_shipping_<?php echo $vendor_region->name; ?>" 
                           class = "" /> <?php // minimum order price for free shipping ?>
                    <input 
                           type= "number" 
                           placeholder = "Pizza Price" 
                           value = "<?php echo $vendor_region->pizzaPrice; ?>"
                           name = "pizza_price_<?php echo $vendor_region->name; ?>" 
                           id = "pizza_price_<?php echo $vendor_region->name; ?>" 
                           class = "" /> <?php // pizza price ?>
                    <input 
                           status = "checked" 
                           type = "checkbox" 
                           name = "status_<?php echo $vendor_region->name; ?>" 
                           id = "status_<?php echo $vendor_region->name; ?>" 
                           class = "status" 
                           region-id = "<?php echo $vendor_region->name; ?>"
                           city-id = "<?php echo $vendor_city->ID?>"
                           checked /> <?php // check ?>
                    
                    <button 
                            class = "button button-accept save-region" 
                            city-id="<?php echo $vendor_city->ID; ?>" 
                            region-id = "<?php echo $vendor_region->name ?>" 
                            id = "vendor-dashboard-city-region-row-save-button-<?php echo $vendor_region->name; ?>">
                        Save
                    </button>
                </formrow>                
                <?php endforeach; ?>
                
                <?php foreach($city_regions as $city_region): ?>
                <formrow>
                    <label class = "region-label"><?php echo $city_region->name; ?></label>
                    <input 
                           type= "number" 
                           placeholder = "Shipping Price" 
                           name = "shipping_price_<?php echo $city_region->name; ?>" 
                           id = "shipping_price_<?php echo $city_region->name; ?>" 
                           class = "" disabled /> <?php // shipping price ?>
                    <input 
                           type= "number" 
                           placeholder = "Shipping Time" 
                           name = "shipping_time_<?php echo $city_region->name; ?>" 
                           id = "shipping_time_<?php echo $city_region->name; ?>" 
                           class = "" disabled /> <?php // shipping time ?>
                    <input 
                           style = "width:280px;" 
                           type= "number" 
                           placeholder = "Minimum Price For Free Shipping" 
                           name = "minimum_order_price_for_free_shipping_<?php echo $city_region->name; ?>" 
                           id = "minimum_order_price_for_free_shipping_<?php echo $city_region->name; ?>" 
                           class = "" disabled /> <?php // minimum order price for free shipping ?>
                    <input 
                           type= "number" 
                           placeholder = "Pizza Price" 
                           name = "pizza_price_<?php echo $city_region->name; ?>" 
                           id = "pizza_price_<?php echo $city_region->name; ?>" 
                           class = "" disabled /> <?php // pizza price ?>
                    <input 
                           status = "unchecked" 
                           type = "checkbox" 
                           name = "status_<?php echo $city_region->name; ?>" 
                           id = "status_<?php echo $city_region->name; ?>" 
                           class = "status" 
                           city-id = "<?php echo $vendor_city->ID?>"
                           region-id = "<?php echo $city_region->name; ?>" /> <?php // check ?>
                    
                    <button 
                            class = "button button-accept save-region" 
                            city-id="<?php echo $vendor_city->ID; ?>" 
                            region-id = "<?php echo $city_region->name ?>" 
                            id = "vendor-dashboard-city-region-row-save-button-<?php echo $city_region->name; ?>" 
                            disabled >
                        Save
                    </button>
                </formrow>
                <?php endforeach; ?>
            </div>
            
        </div> <?php // City block ends here ?>
        <?php endforeach; ?>
        
    </div>
</div>
<hr>
<?php /********************************************************************************************************************************************/ ?>
<?php $all_bundles = Bundle::getBundles(); ?>
<?php $vendor_bundles = $vendor->getBundles(); ?>
<?php $bundles = $vendor->getAvailableBundles(); ?>
<div id = "vendor-dashboard-define-bundles-block" class = "vendor-dashboard-block">
    <h3>Choose your Bundles</h3>
    
    <?php foreach($all_bundles as $bundle): ?>
        <?php $status = ""; // If the there is an error the checkbox will be disabled ?>
        <?php $attr_status = "unchecked"; // This goes into attribute "status" in the activation checkbox ?>
        <?php $checked = ""; // Goes into the checkbox for it to be checked or not ?>
        <?php if($bundle->isActivated()): //checkes if a bundle is activated for current vendor ?>
            <?php $checked = "checked"; ?>
            <?php $attr_status = "checked"; ?>
        <?php endif; ?>
    
    <?php $addons = $bundle->getAddons(); // gets the amount of addons that the bundle offers ?>
    <?php $drinks = $bundle->getDrinks(); // gets the amount of drinks that the bundle offers ?>
    <?php $additional_products = $bundle->getAdditionalProducts(); // gets the amount of additional products that the bundle offers ?>
    <?php $pAddons = $vendor->getAddons(); //gets the list of addons of a vendor ?>
    <?php $pDrinks = $vendor->getDrinks(); // gets the list of drinks of a vendor ?>
    <?php $pAdditionalProducts = $vendor->getAdditionalProducts(); //gets the list of Additional Products of a vendor ?>
    <?php 
    $errors = array(); // array that will contain error during the loop
    if(count($pAddons) < 1 && $addons > 1){
        $status = "disabled";
        $errors["addons"] = "You don't have addons to offer.";
    } 
    if(count($pDrinks) < 1 && $drinks > 1){
        $status = "disabled";
        $errors["drinks"] = "You don't have drinks to offer.";
    }
    if(count($pAdditionalProducts) < 1 && $additional_products > 1){
        $status = "disabled";
        $errors["additional_products"] = "You don't have Additional Products to offer";
    }
    ?>
    <div class = "vendor-dashboard-bundle-box" id = "vendor-dashboard-bundle-box-<?php echo $bundle->getWPID(); ?>" bundle-WPID = "<?php echo $bundle->getWPID(); ?>">
        <img src = "<?php echo $bundle->getImage(); ?>" class = "vendor-dashboard-bundle-box-image" />
        <h4>
            <?php echo $bundle->getTitle(); ?>
        </h4>
        <p>
            <?php echo $bundle->getDescription();?>
        </p>
        <lable>
            <span class = " <?php echo $status; ?>">Activate/Deactivate</span>
            <input 
                   onchange = "bundleRunEffect('<?php echo $bundle->getWPID(); ?>')"
                   type = "checkbox" 
                   bundle-WPID = "<?php echo $bundle->getWPID(); ?>"
                   class = "vendor-dashboard-activation-checkbox" 
                   name = "vendor-dashboard-bundle-activation-checkbox"
                   status = "<?php echo $attr_status; ?>"
                   id = "vendor-dashboard-bundle-activation-checkbox-<?php  echo $bundle->getWPID(); ?>"
                   <?php echo $checked; ?>
                   <?php echo $status; ?> />
            <img class = "loading-gif" id = "loading-gif-<?php echo $bundle->getWPID() ?>" src = "<?php echo LOADING_GIF; ?>" />
            <?php foreach( $errors as $error): ?>
                <span class = "warning-sign"><?php echo $error; ?></span>
            <?php endforeach; ?>
        </lable>
        <div class = "vendor-dashboard-bundle-box-hidden-part" id = "vendor-dashboard-bundle-box-hidden-part-<?php echo $bundle->getWPID(); ?>">
            <h5>General</h5>
            <label>
                Bundle Price
                <?php if($bundle->isActivated()): ?>
                    <?php $price = $bundle->getVendorsPrice($bundle->getWPID()); ?>
                <?php else: ?>
                    <?php $price = $bundle->getPrice(); ?>
                <?php endif; ?>
                <input 
                       type="number" 
                       value = "<?php echo $price ?>" 
                       class = "vendor-dashboard-bundle-price-input" 
                       id = "vendor-dashboard-bundle-price-input-<?php echo $bundle->getWPID(); ?>" />
            </label>
            
            <h5>Addons</h5>
            <?php if($addons < 1): ?>
            <span class = "warning-sign">No addons are required for this bundle.</span>
            <?php else: ?>
                <p>Addons offered in this bundle: <?php echo $addons; ?></p>
                <?php foreach( $pAddons as $pAddon ): ?>
                    <?php $status = ""; ?> 
                    <?php if($bundle->isActivated()): ?>
            
                        <?php $bAddons = $bundle->getAddonsByVendor(); ?>
                        <?php foreach($bAddons as $bAddon): ?>
            
                            <?php if($bAddon->getWPID() == $pAddon->getWPID()): ?>
                                <?php $status = "checked"; ?>
                            <?php endif; ?>
            
                        <?php endforeach; ?>
            
                    <?php endif; ?>
            
                    
                <label class = "addon-to-include-label">
                    <?php echo $pAddon->getTitle(); ?>
                    
                    <input 
                           <?php echo $status; ?>
                           value = "<?php echo $pAddon->getWPID(); ?>"
                           type = "checkbox" 
                           name = "addon-to-include" 
                           id = "addon-to-include-<?php echo $pAddon->getWPID(); ?>" 
                           class = "addon-to-include" />
                </label>
                <?php endforeach; ?>
            <?php endif; ?>
            
            <h5>Drinks</h5>
            <?php if($drinks < 1): ?>
            <span class = "warning-sign">No drinks are required for this bundle.</span>
            <?php else: ?>
            <p>Drinks offered in this bundle: <?php echo $drinks; ?></p>
                
            <?php endif; ?>
            
            
            
            <?php if($additional_products < 1): ?>
            <span class = "warning-sign">No Additional products are required for this bundle.</span>
            <?php elseif(count($pAdditionalProducts) > 1): ?>
            <h5>Additional Products</h5>
            
            <p>Additional Products offered in this bundle <?php echo $additional_products; ?></p>
                
            <?php endif; ?>
            
            <button 
                    class = "vendor-dashboard-bundle-save-button"
                    id = "vendor-dashboard-bundle-save-button"
                    bundle-id = "<?php echo $bundle->getWPID() ?>">
                Save Bundle</button>
            
        </div>
    </div>
    <?php endforeach; ?>
    
    
    <div class = "clearfix"></div>
</div>
<?php
acf_form($options);

get_footer();
?>
<?php
class Secure{
    
    public static function Hash($toHash = false){
        
        $date = DateTime::createFromFormat('d/m/Y', '23/05/2013');
        
        if(!$toHash){
            $toHash = $date->format("d(D)/m(F)/Y - h:i:s:u");
            $hash = md5($toHash);
        }else{
            $toHash .= $date->format("d(D)/m(F)/Y - h:i:s:u");
            $hash = md5($toHash);
        }
        return $hash;
        
    }
    
    public static function JsonDecode($RAWjson){
        $jsonready = preg_replace('/\\\"/',"\"",$RAWjson);
        $json = stripslashes($jsonready);
        $return = json_decode($json);
        return $return;
    }
    
}


?>

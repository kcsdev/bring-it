<?php
class AdditionalsProduct{
    private $WPID = null;
    private $ID = null;
    private $name = null;
    private $title = null;
    private $description = null;
    private $price = null;
    private $vendor_ID = null;
    private $image = null;
    
    public function __construct($WPID,$vendor_ID){
            
        $this->WPID = $WPID;
        $this->ID = Secure::Hash($WPID);
        $this->vendor_ID = $vendor_ID;
        $this->name = get_the_title($WPID);
        $this->title = get_the_title($WPID);
        $this->price          = get_field("price", $WPID);
        $this->image          = get_field("image", $WPID);
        $this->description    = get_field("description", $WPID);
    }
    public function getWPID(){
        return $this->WPID;
    }
    public function getID(){
        return $this->ID;
    }
    public function getName(){
        return $this->name;
    }
    public function getTitle(){
        return $this->title;
    }
    public function getDescription(){
        return $this->description;
    }
    public function getPrice(){
        return $this->price;
    }
    public function getImage(){
        return $this->image;
    }
    public static function getAllByVendor($vendor_ID, $num = 3){ //gets all additional products by vendor id.
        
        if(!isset($vendor_ID)){
            return false;
        }
        
        $args = array(
            "number"    => $num,
            "post_type" => "additional_product",
            "status"    => "publish",
            "author"    => $vendor_ID
        );

        $temp_additional_products = get_posts($args);

        $additional_products = array();

        foreach($temp_additional_products as $temp_additional_product){
            $additional_product = new AdditionalProduct($temp_additional_product->ID,$vendor_ID);
            $additional_products[] = $additional_product;
        }

        return $additional_products;    
    }
    
}


function get_additional_product($id = null){
    
    if($id == null){
        return "Error";
    }
    
    $temp_additional_product = get_post($id);
    
    $additional_product = new stdClass();
    
    $additional_product->id             = $temp_additional_product->ID;
    $additional_product->title          = $temp_additional_product->post_title;
    $additional_product->price          = get_field("price",$additional_product->id);
    $additional_product->image          = get_field("image",$additional_product->id);
    $additional_product->description    = get_field("description", $additional_product->id);
    
    return $additional_product;
    
}

function save_chosen_vendor_to_session(){ 
    
    if(isset($_POST["vendor_id"])){ 
        
        $_SESSION['vendor'] = $_POST['vendor_id'];
        echo $_SESSION['vendor'];
        
    }else{
        
        echo "Error!";
        
    }
    
}
add_action("wp_ajax_save_chosen_vendor_to_session_action","save_chosen_vendor_to_session");
add_action("wp_ajax_nopriv_save_chosen_vendor_to_session_action","save_chosen_vendor_to_session");



?>
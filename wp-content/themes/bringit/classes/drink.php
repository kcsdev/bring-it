<?php

function get_drinks_by_vendor($vendor_id){
    
//    $args = array(
//        "post_type" => "addon",
//        "post_per_page" => -1,
//        "status" => "publish",
//    );
//    
    $drinks = array();
    
    
    if( have_rows('drinks', 'user_' . $vendor_id) ){
        
        while(have_rows('drinks', 'user_' . $vendor_id)){ 
            
            the_row();
            
            if( get_sub_field('drinks_in_stock') ){
                
                $temp_drinks = get_sub_field('drinks_in_stock');
                $drinks = array_merge($drinks,$temp_drinks);
                //var_dump($drinks);
                
            }
            
        }
        
    }
    
    if( !empty($drinks) )
        return $drinks;
    
    return false;
    
}
class Drink{
    
    private $table = VENDOR_DRINKS_TABLE;
    
    private $ID = null;
    private $WPID = null;
    private $image = null;
    private $name = null;
    private $title = null;
    private $description = null;
    private $type = null;
    private $price = null;
    
    public function __construct($WPID = null, $image = null, $name = null, $title = null, $description = null){ 
        if($WPID !== null){ 
            
            $temp_drink = get_post($WPID);
            
            $this->ID = Secure::Hash($WPID);
            $this->WPID = $WPID;
            $this->type = $temp_drink->post_type;
            $this->title = $temp_drink->post_title;
            $this->name = $temp_drink->post_name;
            
        }else{
            $this->ID = Secure::Hash();   
        }
        if($image !== null){
            $this->image = $image;
        }else{
            $this->image = get_field("image",$WPID);
        }
        
        if($name !== null){
            $this->name = $name;
        }else{
            $this->name = $temp_drink->post_name;
        }
        if($title !== null){
            $this->title = $title;
        }else{
            $this->title = $temp_drink->post_title;
        }
        if($description !== null){
            $this->description = $description;
        }
        
    }
    //Getters
    public function getID(){
        return $this->ID;
    }
    public function getWPID(){
        return $this->WPID;
    }
    public function getImage(){
        return $this->image;
    }
    public function getTitle(){
        return $this->title;
    }
    public function getName(){
        return $this->name;
    }
    public function getDescription(){
        return $this->description;
    }
    public function getType(){
        return $this->type;
    }
    public function getPrice(){ 
        return $this->price;
    }
    public function getPriceByID($ID){ 
        return false;
    }
    
    public static function getAll(){
        $drinks = array();
        $args = array( 
            "status" => "publish",
            "post_type" => array("canned_drink","bottled_drink"),
            "posts_per_page" => -1
        );
        $temp_drinks = get_posts($args);
        
        foreach($temp_drinks as $temp_drink){
            $drink = new Drink($temp_drink->ID);
            $drink->setImage(get_field("image",$drink->getWPID())); 
            $drink->setName($temp_drink->post_name);
            $drink->setTitle($temp_drink->post_title);
            $drink->setDescription(get_field("description",$drink->getWPID()));
            
            $drinks[] = $drink;
        }
        return $drinks;
    }
    
    public static function getAllBottled(){ 
        $drinks = array();
        $args = array( 
            "post_type" => "bottled_drink",
            "status" => "publish",
            "posts_per_page" => -1
        );
        $temp_drinks = get_posts($args);
        foreach($temp_drinks as $temp_drink){
            $drink = new Drink($temp_drink->ID);
            $drink->setImage(get_field("image",$drink->getWPID())); 
            $drink->setName($temp_drink->post_name);
            $drink->setTitle($temp_drink->post_title);
            $drink->setDescription(get_field("description",$drink->getWPID()));
            
            $drinks[] = $drink;
        }
        return $drinks;
    }
    
    public static function getAllCanned(){
        $drinks = array($temp_drink->ID);
        $args = array( 
            "post_type" => "canned_drink",
            "status" => "publish",
            "posts_per_page" => -1
        );
        $temp_drinks = get_posts($args);
        foreach($temp_drinks as $temp_drink){
            $drink = new Drink();
            $drink->setImage(get_field("image",$drink->getWPID())); 
            $drink->setName($temp_drink->post_name);
            $drink->setTitle($temp_drink->post_title);
            $drink->setDescription(get_field("description",$drink->getWPID()));
            
            $drinks[] = $drink;
        }
        return $drinks;
    }
    public function setWPID($WPID){
        $this->WPID = $WPID;
    }
    public function setImage($image){
        $this->image = $image;
    }
    public function setName($name){
        $this->name = $name;
    }
    public function setTitle($title){
        $this->title = $title;
    }
    public function setDescription($description){
        $this->description = $description;
    }
    public function setPrice($price){
        $this->price = $price;
    }

    public function inStock(){ 
        
        global $wpdb;
        
        $vendor = new Vendor();
        
        $sql  = "SELECT * ";
        $sql .= "FROM " . VENDOR_DRINKS_TABLE . " "; 
        $sql .= "WHERE vendor_ID = " . $vendor->getID() . " ";
        $sql .= "AND drink_WPID = " . $this->WPID;
        
        $results = $wpdb->get_results($sql);
        
        if(empty($results)){
            return false;
        }else{ 
            return $results[0]->in_stock;
        }
        
    }
    
    public function getVendorsPrice($vendor_ID = -1){ 
        
        if($vendor_ID < 0){
            $vendor = new Vendor();    
        }else{
            $vendor = new Vendor($vendor_ID);
        }
        
        
        $sql  = "SELECT * ";
        $sql .= "FROM " . $this->table . " ";
        $sql .= "WHERE vendor_ID = " . $vendor->getID() . " ";
        $sql .= "AND drink_WPID = " . $this->WPID;
        
        $results = DB::Select($sql);
        
        if(empty($results)){
            return "";
        }else{
            return $results[0]->vendor_price;
        }
        
    }
    
}

?>
<?php
class Pizza{
    private $ID = null;
    private $addons = array();
    
    public function __construct(){
        $this->ID = Secure::Hash();
    }
    public function getID(){
        return $this->ID;   
    }
    public function getAddons(){
        return $this->addons;
    }
    public function addAddon($addon){ 
        $addons = $this->addons;
        $addons[] = $addon;
        $this->addons = $addons;
    }
    
    public function removeAddon($ID = "not set"){
        if($ID == "not set"){
            return false;
        }
        $addons = $this->addons;
        foreach($addons as $key => $addon){
            if($ID == $addon->getID()){
                unset($addons[$key]);
            }
        }
        $this->addons = $addons;
        
    }
}
?>
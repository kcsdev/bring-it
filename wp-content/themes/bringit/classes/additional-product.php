<?php

class AdditionalProduct{
    private $ID = null;
    private $name = null;
    private $image = null;
    private $vendor_ID = null;
    private $price = null;
    private $decription = null;
    private $in_stock = null;
    private $table = VENDORS_ADDITIONAL_PRODUCTS_TABLE;
    
    public function __construct($ID=""){ 

        $table = VENDOR_ADDITIONAL_PRODUCTS_TABLE;
        if($ID !== ""){

            $sql  = "SELECT * ";
            $sql .= "FROM " . $table . " ";
            $sql .= "WHERE id = " . $ID . " ";
            
            $results = DB::Select($sql);

            if(empty($results)){
                return "No additional product with such an ID";
            }

            $this->ID = $ID;
            $this->vendor_ID = $results[0]->vendor_ID;
            $this->name = $results[0]->name;
            $this->price = $results[0]->price;
            $this->in_stock = $results[0]->in_stock;
            $this->image = $results[0]->img_path;
            $this->description = $results[0]->description;

        }else{ 
            
            global $wpdb;
            
            $vendor = new Vendor();
            $this->ID = $ID;
            $this->vendor_ID = $vendor->getID();
            
            $data = array();
            $data["vendor_ID"] = $this->vendor_ID;
            if($wpdb->insert($table,$data)){
                $this->ID = $wpdb->insert_id;
                $this->image = "/wp-content/themes/bringit/assets/images/default/additional-products/default.png";
            }
        }
        
    }
    //****************************************************************************************
    // Booleans
    
    public function inStock(){
        return $this->in_stock;
    }
    //****************************************************************************************
    // Setters
    public function setName($name){
        $this->name = $name;
    }
    
    public function setPrice($price){
        $this->price = $price;
    }
    
    public function setStock($in_stock){
        $this->in_stock = $in_stock;
    }
    
    public function setDescription($description){
        $this->description = $description;
    }
    public function setImage($image){
        $this->image = $image;
    }
    //****************************************************************************************
    // Getters
    public function getName(){
        return $this->name;
    }
    
    public function getPrice(){
        return $this->price;
    }
    
    public function getID(){
        return $this->ID;
    }
    
    public function getImage(){
        return $this->image;
    }
    
    public function getDescription(){
        return $this->description;
    }
    //****************************************************************************************
    // Common
    public function Save(){
        global $wpdb;
        
        $table = VENDOR_ADDITIONAL_PRODUCTS_TABLE;
        
        $data = array();
        $data["id"] = $this->ID;
        $data["vendor_ID"] = $this->vendor_ID;
        $data["name"] = $this->name;
        $data["price"] = $this->price;
        $data["in_stock"] = $this->in_stock;
        $data["description"] = $this->description;
        $data["img_path"] = $this->image;
        $where = array();
        $where["id"] = $this->ID;
        
        $wpdb->update($table,$data,$where);
        
    }
    
    public function Delete(){
        global $wpdb;
        
        $table = VENDOR_ADDITIONAL_PRODUCTS_TABLE;
        
        $where = array();
        $where["id"] = $this->ID;
        
        $wpdb->delete($table,$where);
        
    }
    //****************************************************************************************
    // Static functions
    
    public static function getAll(){
        
        $additional_products = array();
        
        $sql = "SELECT * ";
        $sql .= "FROM " . $this->table . " ";
        $sql .= "Where in_stock = 1";
        
        $results = DB::Select($sql);
        
        foreach($results as $result){
            $additional_products[] = new AdditionalProduct($result->id);
        }
        
    }
    
    
}






?>
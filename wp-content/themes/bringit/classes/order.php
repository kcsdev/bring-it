<?php 
class Order{
    
    private $ID;
    private $customer_ID;
    private $seller_ID;
    private $shipping_address;
    private $order_type;
    private $city;
    private $street;
    private $building_number;
    private $apartment_number;
    private $level;
    private $entrance;
    private $order_date;
    private $payment_method;
    private $order_cost;
    private $shipping_cost;
    private $discount;
    private $order_status;
    private $kashrut;
    private $pizzas = array();
    private $additional_products = array();
    private $drinks = array();
    private $bundles = array();
    private $fname;
    private $lname;
    private $phone;
    private $courier_comment;
    private $order_comment;
    
    private $table = BRINGIT_ORDERS;
    
    public function __construct($order = null){ 
        
        if($order){ 
            
            $sql = "SELECT * ";
            $sql .= "FROM " . $this->table . " ";
            $sql .= "WHERE id = " . $order;
            
            $results = DB::Select($sql);
            
            $this->ID = $order;
            
            if(!empty($results)){
                foreach($results as $result){
                    $this->ID = $result->id;
                    $this->customer_ID = $result->customer_ID;
                    $this->seller_ID =  $result->seller_ID;
                    $this->shipping_address =  $result->shipping_address;
//                    $this->order_type =  $result->payment_method; // TODO
                    $this->city =  $result->city;
                    $this->street =  $result->street;
                    $this->building_number =  $result->building_number;
                    $this->apartment_number =  $result->apartment_number;
                    $this->level =  $result->level;
                    $this->entrance =  $result->entrance;
                    $this->order_date =  $result->order_date;
                    $this->payment_method =  $result->payment_method;
                    
                    $this->order_cost =  $result->order_cost;
                    $this->shipping_cost =  $result->shipping_cost;
//                    $this->discount =  $result->discount; //TODO
                    $this->order_status =  $result->order_status;
                    $this->kashrut =  $result->kashrut;
                    $this->pizzas =  unserialize($result->pizzas);
                    $this->additional_products = unserialize($result->additional_products);
                    $this->drinks =  unserialize($result->drinks);
                    $this->bundles =  unserialize($result->bundles);
                    $this->fname =  $result->fname;
                    $this->lname =  $result->lname;
                    $this->phone =  $result->phone;
                    $this->courier_comment =  $result->courier_comment;
                    $this->order_comment =  $result->order_comment;
                }
            }
        }
        
    }
    
    
    
    public function addShippingInformation(){ 
        $response = array();
        $errors = array();
            
        if(isset($_POST['the_city']) && isset($_POST['the_street']) && isset($_POST['the_number']) && isset($_POST["the_method"])){ 
            //checks if the needed fields are in place
            if( trim($_POST["the_city"]) != "" && trim($_POST["the_street"]) != "" && trim($_POST["the_number"]) != ""){ // check if the user filled the needed fields
                
//                if(Address::ValidateAddress($_POST["the_city"],$_POST["the_street"]) == false){
//                    var_dump (Address::ValidateAddress($_POST["the_city"],$_POST["the_street"])); die;
//                }
                
                if($_POST["the_method"] == "shipping"){ //if user chose "Shipping" method
                    $this->city = $_POST['the_city'];
                    $this->street = $_POST['the_street'];
                    $this->building_number = $_POST['the_number'];
                }else if($_POST["the_method"] == "takeaway"){ //if user chose "Takeaway" method
                    $this->city = $_POST['the_city'];
                    $this->street = $_POST['the_street'];
                } 
                if(isset($_POST["the_method"])){
                    $this->order_type = $_POST['the_method'];
                } 
                if(isset($_POST["the_filter"])){
                    $this->kashrut = $_POST["the_filter"];   
                }
            }else{
                $errors[] = new Error('Order','addShippingInformation','One or more of the $_POSTs passed is an empty string');
            }

        }else{
            $errors[] = new Error('Order','addShippingInformation','One or more of the $_POSTs is not set');
        }
        
        $this->Save();
        
        $response['errors'] = $errors;
        if( isset($_SESSION["Bundle"]) ){
            $response['is_bundle'] = true;    
        }else{
            $response['is_bundle'] = false;
        }
        
        $jsonResponse = json_encode($response);
        
        echo $jsonResponse;
        
        die;
    }
    
    
    public function getOrder($ID = null){
        if($ID === null){
            $order = new Order();
        }else{
            global $wpdb;
            
            $sql = "SELECT * ";
            $sql .= "FROM " . BRINGIT_ORDERS . " bo ";
            $sql .= "WHERE id = " .$ID;
            
            $results = $wpdb->get_results($sql);
            //var_dump($results[0]);
            $order = new Order($results[0]);
        }
        
        return $order;
    }
    public function getID(){
        return $this->ID; 
    }
    public function getCustomerID(){
        return $this->customer_ID;
    }
    public function getSellerID(){
        return $this->seller_ID;
    }
    public function getShippingAddress(){
        return $this->shipping_address;
    }
    public function getCity(){
        return $this->city;
    }
    public function getStreet(){
        return $this->street;
    }
    public function getBuildingNumber(){
        return $this->building_number;
    }
    public function getApartmentNumber(){
        return $this->apartment_number;
    }
    public function getEntrance(){
        return $this->entrance;
    }
    public function getOrderDate(){
        return $this->order_date;
    }
    public function getPaymentMethod(){
        if($this->payment_method == 1){
            return "אשראי";
        }elseif($this->payment_method == 2){
            return "מזומן";
        }else{
            return "PayPal";
        }
        
    }
    public function getShippingCost(){
        return $this->shipping_cost;
    }
    public function getDiscount(){
        return $this->discount;
    }
    public function getOrderStatus(){
        return $this->order_status;
    }
    public function getOrderType(){
        return $this->order_type;
    }
    public function getOrderCost(){
        return $this->order_cost;
    } 
    public function getKashrut(){
        return $this->kashrut;
    }
    public function getPizzas(){
        return $this->pizzas;
    }
    public function getAdditionalProducts(){
        return $this->additional_products;
    }
    public function getDrinks(){
        return $this->drinks;
    }
    public function getBundles(){
        return $this->bundles;
    }
    public function getCustomerText(){
        return $this->customer_text;
    }
    public function getFirstName(){
        return $this->fname;
    }
    public function getLastName(){
        return $this->lname;
    }
    public function getLevel(){
        return $this->level;
    }
    public function getTime(){
        $timestamp = $this->order_date;
        $arr = explode(" ",$timestamp);
        $time = $arr[1];
        return $time;
    }
    public function getCourierComment(){
        return $this->courier_comment;
    }
    public function getOrderComment(){
        return $this->order_comment;
    }
    public function getPhone(){
        return $this->phone;
    }
    public static function getAllOrders(){ //TODO
        
    }
    public static function getLatestOrders($number = 0){ //TODO
        
    }
    public static function getPaypalOrders($number = 0){ //TODO
        
    }
    public static function getCashOrders($number = 0){ //TODO
        
    }
    public static function getCreditCardOrders($number = 0){ //TODO
        
    }
    public static function getCancelledOrders($number = 0){ //TODO
        
    }
    public static function getCompletedOrders($number = 0){ //TODO
        
    }
    public static function getUncompletedOrders($number = 0){ //TODO
        
    }
//***********************************************************************************************
// NOTE Setters
    
    
    public function setName($fname){
        $this->fname = $fname;
    }
    
    public function setLastName($lname){
        $this->lname = $lname;
    }
    
    public function setApartmentNumber($apartment_number){
        $this->apartment_number = $apartment_number;
    }

    public function setLevel($level){
        $this->level = $level;
    }
    
    public function setCustomerID($customer_ID){
        $this->customer_ID = $customer_ID;
    }
    
    public function setEntrance($entrance){
        $this->entrance = $entrance;
    }
    
    public function setPhone($phone){
        $this->phone = $phone;
    }
    public function setSeller($vendor_ID){
        $this->seller_ID = $vendor_ID;
    }
    public function setPizzasFromCart(){ 
        
        $cart = Cart::OpenCart(); // Getting the Cart From Session
        
        $pizzas = $cart->getPizzas(); // Getting all the Pizzas from Cart
        
        $this->pizzas = $pizzas; // Setting Pizzas to Order.
        
        $this->Save(); // Saving the Order To Session.
    }
    public function setDrinksFromCart(){
        
        $cart = Cart::OpenCart(); // Getting the Cart From Session
        
        $drinks = $cart->getDrinks(); // Getting all the Drinks from Cart
        
        $this->drinks = $drinks; // Setting Drinks to Order
        
        $this->Save(); // Saving the Order to Session.
    }
    public function setAdditionalProductsFromCart(){
        
        $cart = Cart::OpenCart(); // Getting the Cart from Session
        
        $additional_products = $cart->getAdditionalProducts(); // Getting all Additional Products from Cart.
        
        $this->additional_products = $additional_products; // Setting Additional Products to Order
        
        $this->Save(); // Saving the Order to Session.
    }
    public function setBundlesFromCart(){
        
        $cart = Cart::OpenCart(); // Getting the Cart From Session.
        
        $bundles = $cart->getBundles(); // Getting all Bundles from the Cart.
        
        $this->bundles = $bundles; // Setting Bundles To Order.
        
        $this->Save(); // Saving the Order to Session.
    }
    public function setOrderPriceFromCart(){
        
        $cart = Cart::OpenCart(); // Getting the Cart from Session
        
        $price = $cart->getTotalPrice(); // Getting the Total Price from the Cart.
        
        $this->order_price = $price; // Setting Global Price to Order.
        
    }
    
    public function setOrderDate(){ 
        
        $this->order_date = time();
        
        $this->Save();
    }
    public function setOrderStatus($orderStatus = 1){
        $this->order_status = $orderStatus;
        $this->Save();
    }
    public function setPaymentType($payment_type = -1){
        if($payment_type !== -1){
            if($payment_type === "creditcard"){
                $payment_type = 1; // 1 = creditcard.
            }elseif($payment_type === "cash"){
                $payment_type = 2; // 2 = cash.
            }elseif($payment_type === "paypal"){
                $payment_type = 3; // 3 = paypal.
            }else{
                return false;
            }
        }
        $this->payment_method = $payment_type;
        $this->Save(); 
    }
    
    public function setCourierComment($courier_comment){
        $this->courier_comment = $courier_comment;
    }
    
    public function setOrderComment($order_comment){
        $this->order_comment = $order_comment;
    }
    public function setAddress($address){
        $this->shipping_address = $address;
    }
    
//***********************************************************************************************
    //  Generators
    public function generateAddress($city,$street,$bn/*Building Number*/){
        $return = "$city, $street $bn";
        return $return;
    }
//***********************************************************************************************
    // Add Info to Order
    
    public function addFullShippingInformation(){ 
        $shippingInfo = Secure::JsonDecode($_POST["formData"]);
        
        var_dump($shippingInfo);
        
        $this->setName($shippingInfo->fname);
        $this->setLastName($shippingInfo->lname);
        $this->setApartmentNumber($shippingInfo->apartmentNumber);
        $this->setLevel($shippingInfo->level);
        $this->setEntrance($shippingInfo->entrance);
        $this->setPhone($shippingInfo->phone);
        $this->setCourierComment($shippingInfo->courierComment);
        $this->setOrderComment($shippingInfo->orderComment);
        $this->setOrderPriceFromCart();
        
        $address = $this->generateAddress($this->city,$this->street,$this->building_number);
        
        $this->setAddress($address);
        
        $this->Save();
        
        
        
        die;
    }// AJAX function
//***********************************************************************************************
    
    public static function OpenOrder(){
        $order = unserialize($_SESSION["Order"]);
        return $order;
    }
    public function Save(){
        $_SESSION["Order"] = serialize($this);
    }
    public static function Create(){
        if( !isset($_SESSION['Order']) ){
            $order = new Order();
            $order->Save();
        }else{
            $order = Order::OpenOrder();
            return $order;
        }
    }
    public function DBSAVE(){
        $order = $this;
        global $wpdb;
        
        $table = BRINGIT_ORDERS;

        $data = array();
//        $data["id"] = $order->ID;
        $data["customer_id"] = $order->customer_ID;
        $data["seller_id"] = $order->seller_ID;
        $data["shipping_address"] = $order->shipping_address;
        $data["city"] = $order->city;
        $data["street"] = $order->street;
        $data["building_number"] = $order->building_number;
        $data["apartment_number"] = $order->apartment_number;
        $data["entrance"] = $order->entrance;
        $data["payment_method"] = $order->payment_method;
        $data["order_cost"] = $order->order_cost;
        $data["shipping_cost"] = $order->shipping_cost;
        $data["discount"] = $order->discount;
        $data["order_status"] = $order->order_status;
        $data["kashrut"] = $order->kashrut;
        $data["courier_comment"] = $order->courier_comment;
        $data["order_comment"] = $order->order_comment;
        $data["pizzas"] = serialize($order->pizzas);
        $data["drinks"] = serialize($order->drinks);
        $data["bundles"] = serialize($order->bundles);
        $data["fname"] = $order->fname;
        $data["lname"] = $order->lname;
        $data["phone"] = $order->phone;
        echo "\n We are one row from Success!!!";
        var_dump($data);
        $wpdb->insert($table,$data);
        
    }
    public static function DEBUG(){
        $order = Order::OpenOrder();
        
        echo "<br> ID: " . $order->getID();
        echo "<br> Customer ID: " . $order->getCustomerID();
        echo "<br> First Name: " . $order->getFirstName();
        echo "<br> Last Name: " . $order->getLastName();
        echo "<br> Vendor ID: " . $order->getSellerID();
        echo "<br> Shipping Address: " . $order->getShippingAddress();
        echo "<br> Order Type: " . $order->getOrderType();
        echo "<br> City: " . $order->getCity();
        echo "<br> Street: " . $order->getStreet();
        echo "<br> Building Number: " . $order->getBuildingNumber();
        echo "<br> Apartment Number: " . $order->getApartmentNumber();
        echo "<br> Entrance: " . $order->getEntrance();
        echo "<br> Order Date: " . $order->getOrderDate();
        echo "<br> Payment Method: " . $order->getPaymentMethod();
        echo "<br> Order Cost: " . $order->getOrderCost();
        echo "<br> Shipping Cost: " . $order->getShippingCost();
        echo "<br> Discount: " . $order->getDiscount();
        echo "<br> Order Status: " . $order->getOrderStatus();
        echo "<br> Kashrut: " . $order->getKashrut();
        echo "<br> Pizzas: " . $order->getPizzas();
        echo "<br> Additional Products: " . $order->getAdditionalProducts();
        echo "<br> Drinks: " . $order->getDrinks();
        
    }
    public function filterStacks($stacks){        
        $unique_stacks = array_map("unserialize", array_unique(array_map("serialize", $stacks)));        
        return $unique_stacks;
    }
    public function getDrinkStackByWPID($WPID){
        $stack = new stdClass();
        $stack->drink = new Drink($WPID);
        $stack->quantity = 0;
        
        foreach($this->drinks as $drink){
            if($drink->getWPID() == $WPID)
                $stack->quantity++;
        }
        
        return $stack;
    }
}



?>
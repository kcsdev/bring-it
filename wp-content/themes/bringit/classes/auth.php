<?php
class Auth{
    
    public static function login(){
        
        $result = array();
        
        if(isset($_POST['username']) && isset($_POST['password'])){ 
            $creds = array();
            $creds['user_login'] = $_POST['username'];
            $creds['user_password'] = $_POST['password'];
            $creds['remember'] = true;
            $user = wp_signon( $creds, false );
    //        var_dump($user);
//            var_dump( is_wp_error($user) );
            if ( is_wp_error($user) ){
                $result["is_validated"] = false;
//                $result["errors"][] = new Error("Auth","login",$user->get_error_message());
            }else{
                $result["is_validated"] = true;
            }

        }else{
            $result["errors"][] = new Error("Auth","login","1 or more parameters have not been passed by POST");
        }
        
        $jsonResult = json_encode($result);
        
        echo $jsonResult;
        
        die; // “I'm the one that's got to die when it's time for me to die, so let me live my life the way I want to.” 
        
    }
    public static function check(){
        
        if(is_user_logged_in()){
            return true;
        }
        return false;
        
    }
    public static function getUserID(){
        $ID = get_current_user_id();
        return $ID;
    }
    public static function register(){ 
        
        if ( isset($_POST['username'], $_POST['password'], $_POST['re_password'], $_POST['email'] ) ) { 
            global $reg_errors;
            Auth::validateRegistrationForm( $_POST['username'], $_POST['password'], $_POST['re_password'], $_POST['email'] );
    //        global $username, $password, $email;
            $username   =   $_POST['username']; //sanitize_user( $_POST['username'] );
            $password   =   $_POST['password'];//esc_attr( $_POST['password'] );
            $email      =   $_POST['email'];//sanitize_email( $_POST['email'] );
            // call @function complete_registration to create the user
            // only when no WP_error is found
            echo $username . " " . $password . " " . $email;
            Auth::completeRegistration($username, $password, $email);
            //$reg_errors, 

        }
        
    }
    
    private function validateRegistrationForm( $username, $password, $re_password, $email ){
        
         global $reg_errors;
        $reg_errors = new WP_Error;
        if(empty($username) || empty($email) || empty($password) || empty($re_password)){
            $reg_errors->add("field","Required form field is missing!");
            echo "Required form field is missing!";
        }
        elseif( 4 > strlen($username)){
            $reg_errors->add("username_length","Username too short, at least 4 characters are required");
            echo "Username too short, at least 4 characters are required";
        }
        elseif(username_exists($username)){
            $reg_errors->add("user_name","Sorry, this username is already taken.");
            echo "Sorry, this username is already taken.";
        }
        elseif($password != $re_password){
            $reg_errors->add("password","The 2 password should match");
            echo "The 2 password should match";
        }
        elseif ( ! validate_username( $username ) ){
            $reg_errors->add( 'username_invalid', 'Sorry, the username you entered is not valid' );
            echo 'Sorry, the username you entered is not valid';
        }
        elseif ( 5 > strlen( $password ) ){
            $reg_errors->add( 'password', 'Password length must be greater than 5' );
            echo 'Password length must be greater than 5';
        }
        elseif ( !is_email( $email ) ){
            $reg_errors->add( 'email_invalid', 'Email is not valid' );
            echo 'Email is not valid';
        }
        elseif ( email_exists( $email ) ){
            $reg_errors->add( 'email', 'Email Already in use' );
            echo 'Email Already in use';
        } else{
            echo "Validtion Successful";
        }
        
        Auth::completeRegistration($username,$password,$email);
        
    }
    private function completeRegistration($username,$password,$email){
         //$reg_errors, $username, $password, $email
        echo "-----------------------";
        global $reg_errors;
        //var_dump($reg_errors);
        echo $username;
        echo $password;
        echo $email;
        if ( 1 > count( $reg_errors->get_error_messages() ) ){
            $userdata = array(
                'user_login'    =>   $username,
                'user_email'    =>   $email,
                'user_pass'     =>   $password,
                'role'          => "customer"
            );
            $user =   wp_insert_user( $userdata );
    //        $user = wp_create_user ($username,$password,$email);
            echo 'Registration complete. Goto <a href="' . get_site_url() . '/wp-login.php">login page</a>.';   
            echo "this the new users ID: ".$user->ID;
        }else{
            echo "There was an error in the last step of the registration (function: complete_registration)";
        }
    }
    
    
    public static function vendorCheck(){
        
        if(!is_user_logged_in()){ 
            if($_SERVER["REQUEST_URI"] != "/vndr-login/" && $_SERVER["REQUEST_URI"] != "/vndr-login"){ 
                
                header("location: /vndr-login");
                return;    
            }
            
        }else{
            $user = wp_get_current_user();
            if($user->roles[0] != "vendor"){ 
                if($_SERVER["REQUEST_URI"] == "/vndr-login/" || $_SERVER["REQUEST_URI"] == "/vndr-login"){
                    return;
                }else{
                    header("location: /vndr-login");
                    return;    
                }
                
            }elseif($_SERVER["REQUEST_URI"] == "/vndr-login/" || $_SERVER["REQUEST_URI"] == "/vndr-login"){
                header("location: /vndr-order/");
                return;
            }
        }
        
    }
    
    public static function vendorLogin(){ 
        
        $login = new stdClass();
        $login->status = false;
        if(isset($_POST['username']) && isset($_POST['password'])){ 
            // Check if all credentials are passed.
            $creds = array(); // an array of credentials for wp_signon().
            $creds['user_login'] = $_POST['username'];
            $creds['user_password'] = $_POST['password'];
            $creds['remember'] = true; //remember the vendor by default.
            
            $user = wp_signon( $creds, false ); // Signing in 
            $login->role = $user->roles[0];
            // user->roles[0] is to get the current user's User Role (VENDOR/ADMIN/CUSTOMER .. etc.)
            if ( is_wp_error($user) ){
                $login->status = false;
            }else{ 
                if($login->role == "vendor"){
                    $login->status = true;
                }else{
                    $login->status = false;
                }
            }

        }  
        $json_login = json_encode($login);
        
        echo $json_login;
        
        die;
        
    }
    
    
    
    
}


?>
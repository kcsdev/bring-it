<?php

class AddonClient{
    private $ID = null;
    private $vendor_ID = null;
    private $name = null;
    private $price = null;
    private $in_bundles = null;
    private $in_stock = null;
    private $is_approved = null;
    private $layers = array();
    
    public function __construct($ID=""){ 
        global $wpdb;
        $table = VENDOR_SPECIAL_ADDSON_TABLE;
        if($ID !== ""){

            $sql  = "SELECT * ";
            $sql .= "FROM " . $table . " ";
            $sql .= "WHERE id = " . $ID . " ";
            
            $results = $wpdb->get_results($sql);

            if(empty($results)){
                return "No addon with such an ID";
            }

            $this->ID = $ID;
            $this->vendor_ID = $results[0]->vendor_ID;
            $this->name = $results[0]->name;
            $this->price = $results[0]->price;
            $this->in_bundles = $results[0]->in_bundles;
            $this->in_stock = $results[0]->in_stock;
            $this->is_approved = $results[0]->is_approved;
            $layers = $this->layers;
            $layers["sw"] = $results[0]->sw_layer;
            $layers["se"] = $results[0]->se_layer;
            $layers["nw"] = $results[0]->nw_layer;
            $layers["ne"] = $results[0]->ne_layer;
            $layers["l-half"] = $results[0]->lhalf_layer;
            $layers["r-half"] = $results[0]->rhalf_layer;
            $layers["full"] = $results[0]->full_layer;
        }else{ 
            $vendor = new Vendor();
            $this->ID = $ID;
            $this->vendor_ID = $vendor->getID();
            
            $data = array();
            $data["vendor_ID"] = $this->vendor_ID;
            $data["is_approved"] = 1;
            if($wpdb->insert($table,$data)){
                $this->ID = $wpdb->insert_id;
                $this->is_approved = 1;
            }
        }
        
    }
    //****************************************************************************************
    // Booleans
    public function isApproved(){
        return $this->is_approved;
    }
    
    public function inBundles(){
        return $this->in_bundles;
    }
    
    public function inStock(){
        return $this->in_stock;
    }
    //****************************************************************************************
    // Setters
    public function setName($name){
        $this->name = $name;
    }
    
    public function setPrice($price){
        $this->price = $price;
    }
    
    public function setStock($in_stock){
        $this->in_stock = $in_stock;
    }
    
    public function setBundles($in_bundles){
        $this->in_bundles = $in_bundles;
    }
    //****************************************************************************************
    // Getters
    public function getName(){
        return $this->name;
    }
    
    public function getPrice(){
        return $this->price;
    }
    
    public function getID(){
        return $this->ID;
    }
    //****************************************************************************************
    // Common
    public function Save(){
        global $wpdb;
        
        $table = VENDOR_SPECIAL_ADDSON_TABLE;
        
        $data = array();
        $data["id"] = $this->ID;
        $data["vendor_ID"] = $this->vendor_ID;
        $data["name"] = $this->name;
        $data["price"] = $this->price;
        $data["in_bundles"] = $this->in_bundles;
        $data["in_stock"] = $this->in_stock;
        $data["is_approved"] = $this->is_approved;
        if(!empty($this->layers)){
            $data["sw_layer"] = $this->layers["sw"];
            $data["se_layer"] = $this->layers["se"];
            $data["nw_layer"] = $this->layers["nw"];
            $data["ne_layer"] = $this->layers["ne"];
            $data["rhalf_layer"] = $this->layers["r-half"];
            $data["lhalf_layer"] = $this->layers["l-half"];
            $data["full_layer"] = $this->layers["full"];
        }
        
        $where = array();
        $where["id"] = $this->ID;
        
        $wpdb->update($table,$data,$where);
        
    }
    
    public function Delete(){
        global $wpdb;
        
        $table = VENDOR_SPECIAL_ADDSON_TABLE;
        
        $where = array();
        $where["id"] = $this->ID;
        
        $wpdb->delete($table,$where);
        
    }
    
    
}

?>
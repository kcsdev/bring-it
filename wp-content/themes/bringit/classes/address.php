<?php 
class Address{
    private $city = null;
    private $district = null;
    private $street = null;
    private $building_number = null;
    private $aprtment_number = null;
    private $entrance = null;
    
    public function __construct($city = "not set",$district = "not set",$street = "not set",$building_number = "not set",$apartment_number = "not set",$entrance = "not set"){
        $this->city = $city;
        $this->district = $district;
        $this->street = $street;
        $this->building_number = $building_number;
        $this->apartment_number = $apartment_number;
        $this->entrance = $entrance;
    }
    
    public static function getCities(){ // Return a list of cities that are in the Database
        global $wpdb;
        $cities = array();
        
        $sql = "SELECT DISTINCT bi_c.city ";
        $sql .= "FROM bi_cities bi_c";
        
        $temp_cities = $wpdb->get_results($sql);
        
        foreach($temp_cities as $temp_city){
            
            $city = new stdClass();
            $city->name = $temp_city->city;

            $cities[] = $city;
            
        }
        return $cities;
    }
    
    public static function getStreets($city = "not set",$district = "not set"){ // Return the list of streets either by city or all of them that are in the Database
        
        $streets = array();
        if($city == "not set"){ //city is an essential part variable to look for streets
            $sql = "SELECT bi_c.street as name, bi_c.id ";
            $sql .="FROM bi_cities bi_c ";
            
            $temp_streets = DB::Select($sql);
            
        }elseif($district == "not set"){ // district is not an essential variable to look for streets
            
            $sql = "SELECT bi_c.street as name, bi_c.id ";
            $sql .="FROM bi_cities bi_c ";
            $sql .="WHERE bi_c.city = '". $city ."' ";
            
            $temp_streets = DB::Select($sql);
            
        }else{
            $sql = "SELECT bi_c.street as name, bi_c.id ";
            $sql .="FROM bi_cities bi_c ";
            $sql .="WHERE bi_c.city = '". $city ."' ";
            $sql .="AND bi_c.region = '". $district ."' "; 
            
            $temp_streets = DB::Select($sql);
            
        }
        if(!empty($temp_streets)){ 
            
            foreach($temp_streets as $temp_street){ 
                
                $street = new stdClass();

                $street->id = $temp_street->id;
                $street->name = $temp_street->name;
                
                $streets[] = $street;
            }
            
            return $streets;
            
        }else{
            return false;   
        }
        
    }
    
    public static function getRegion($city = "not set" , $street = "not set"){
        
        global $wpdb;
        
        if($city == "not set" || $street == "not set"){
            return false;
        }
        
        $sql =  "SELECT DISTINCT bi_c.region ";
        $sql .= "FROM " . BRINGIT_CITIES . " bi_c ";
        $sql .= "WHERE bi_c.city = '" . $city . "' ";
        $sql .= "AND bi_c.street = '" . $street . "' ";
        
        $obj_region = $wpdb->get_results($sql);
        
        $region = $obj_region[0]->region;
        
        return $region;
        
    }
    
    public static function getRegionsByCity($city = "not set"){ 
        global $wpdb;
        
        if($city == "not set"){
            return false;
        }
        $regions = array();
        
        $sql  = "SELECT DISTINCT bi_c.region ";
        $sql .= "FROM bi_cities bi_c ";
        $sql .= "WHERE city = '". $city ."'"; 
        
        $temp_regions = $wpdb->get_results($sql);
        
        
        foreach($temp_regions as $temp_region){
            
            $region = new stdClass();
            $region->name = $temp_region->region;
            
            $regions[] = $region;
            
        }
        return $regions;
    }
    
    public static function ValidateAddress($city,$steet){
        global $wpdb;
        
        $validation = true;
        
        $sql  = "SELECT DISTINCT vc.city ";
        $sql .= "FROM " . BRINGIT_CITIES . " vc ";
        $sql .= "WHERE vc.city = '" . $city . "' ";
        
        $temp_cities = $wpdb->get_results($sql);
        
        if(!$temp_cities)
            $validation = false;
        
        $sql = "SELECT DISTINCT vc.street ";
        $sql .= "FROM " . BRINGIT_CITIES . " vc ";
        $sql .= "WHERE vc.city = '" . $city . "' ";
        $sql .= "AND vc.street = '" . $street . "' ";
        
        $temp_streets = $wpdb->get_results($sql);
    
        if(!$temp_streets)
            $validation = false;
        
        return $validation;
    }
    
    public static function Autocomplete(){ 
        
        $result = new stdClass();
        $result->errors = array();        
        $result->params = array();
    
        $city = $_POST["city"];
        
        $result->params["city"] = $city;
        
        $result->streets = Address::getStreets(trim($city));
        
        if(empty($result->streets)){
            $result->success = false;
        }else{
            $result->success = true;    
        }
        
        $return = json_encode($result,JSON_UNESCAPED_UNICODE);
        
        echo $return;
        
        die;// Death is a gift.
        
    } //AJAX
    
}


?>
<?php
class Bundle{ 
    
    private $ID = null;
    private $WPID = null;
    private $title = null;
    private $price = null;
    private $description = null;
    private $image = null;
    
    private $addons = null;
    private $drinks = null;
    private $pizzas = null;
    private $additional_products = null;
    
    private $includedAddons = array();
    private $included_drinks = array();
    private $included_pizzas = array();
    private $included_additional_products = array();
    
    private $addons_to_pay = array();
    
    private $vendorActivated = null;
    private $is_completed = false;
    
    private $table = VENDOR_BUNDLES_TABLE;
    
    public function __construct($WPID = "not set", $ID = "not set"){
        
        if($WPID == "not set"){
            return false;
        }
        
        $temp_bundle = get_post($WPID);
        
        if($ID == "not set"){
            $this->ID = Secure::Hash($WPID);
        }else{
            $this->ID = $ID;
        }
        
        $this->WPID =         $temp_bundle->ID;
        $this->title =        get_field("name",$this->WPID);
        $this->description =  get_field("description",$this->WPID);
        $this->image =        get_field("image",$this->WPID);
        $this->addons =       get_field("addons",$this->WPID);
        $this->drinks =       get_field("drinks",$this->WPID);
        $this->pizzas =       get_field("pizzas",$this->WPID);
        
    }
    
    //*************************************************************************************************
    // Simple Getters
    
    public function getID(){
        return $this->ID;
    }
    public function getWPID(){
        return $this->WPID;
    }
    public function getTitle(){
        return $this->title;
    }
    public function getDescription(){
        return $this->description;   
    }
    public function getPrice(){
        return $this->price;
    }
    public function getImage(){
        return $this->image;
    }
    public function getAddons(){
        return $this->addons;
    }
    public function getDrinks(){
        return $this->drinks;
    }
    public function getPizzas(){
        return $this->pizzas;
    }
    public function getAdditionalProducts(){
        return $this->additional_products;
    }
    public function getIncludedPizzas(){
        return $this->included_pizzas;
    }
    public function getIncludedDrinks(){
        return $this->included_drinks;
    }
    public function getIncludedAdditionalProducts(){
        return $this->included_additional_products;
    }
    public function getIncludedAddson(){
        return $this->includedAddons;
    }
    
    //********************************************************************************************
    // Getters
    
    // Gets the latest bundles added by admin. The default number of bundles returned is 3.
    public static function getLatestBundles($number=3){

        $bundles = array(); // Creating an empty array to hold the bundles
        
        $args = array( 
            "post_type"         => "bundle",
            "posts_per_page"    => $number,
            "post_status"       => "publish",
            "orderby"           => "date",
            "order"             => "DESC"
        ); // These are the paramaters that I use to get the right bundles (WP)
        
        $temp_bundles = get_posts($args); // returns an array of stdClasses of bundles
        
        foreach($temp_bundles as $temp_bundle){ // looping through
            
            $bundle = new Bundle($temp_bundle->ID); 
            $bundles[] = $bundle;
            // Creating an array with values of type Bundle
        }

        return $bundles; 
        
    }
    
    // Return the count of Addson included in the bundle
    public function getIncludedAddons(){ 
        $count = 0;
        $pizzas = $this->included_pizzas;
        foreach($pizzas as $pizza){
            $addons = $pizza->getAddons();
            foreach($addons as $addon){ 
                $count++;
            }
        }
        return $count; // The number of Addson that is in the bundle
    }
    
    public static function getBundles(){
        
        $bundles = array();
        
        $args = array(
            "post_type" => "bundle",
            "posts_per_page" => -1,
            "status" => "publish"
        );
        
        $temp_bundles = get_posts($args);
        
        foreach($temp_bundles as $temp_bundle){
            $bundle = new Bundle($temp_bundle->ID);
            $bundles[] = $bundle;
        }
        return $bundles;
    }
    
    public function getAddonsByVendor($vendor_ID = -1){
        global $wpdb;
        
        if($vendor_ID == -1){
            $vendor = new Vendor();
        }elseif($vendor_ID > 0){
            $vendor = new Vendor($vendor_ID);
        }else{
            return false;
        }
        
        $bAddons = array();
        
        $sql =  "SELECT vb.addons ";
        $sql .= "FROM " . VENDOR_BUNDLES_TABLE . " vb ";
        $sql .= "WHERE vb.vendor_ID = " . $vendor->getID() . " ";
        $sql .= "AND vb.bundle_ID = " . $this->WPID;
        
        $temp_bundles = $wpdb->get_results($sql);
    
        foreach($temp_bundles as $temp_bundles){ 
            $temp_bAddons = unserialize($temp_bundles->addons);
            $temp_Addons = json_decode($temp_bAddons);
                
            foreach($temp_Addons as $Addon){
                $bAddon = new Addon($Addon);
                $bAddons[] = $bAddon;   
            }
    
        }
        
        return $bAddons;
        
    }
    
    public function getVendorsPrice($ID = -1){
        global $wpdb;
        
        if($ID == -1)
            $vendor = new Vendor();
        else
            $vendor = new Vendor($ID);
        
        $sql = "SELECT * ";
        $sql .= "FROM " . VENDOR_BUNDLES_TABLE . " ";
        $sql .= "WHERE vendor_ID = " . $vendor->getID() . " ";
        $sql .= "AND bundle_WPID = " . $this->WPID;
        
        $results = $wpdb->get_results($sql);
        
        if(empty($results)){
            $price = 0;
        }else{
            $price = $results[0]->vendor_price;
        }
        
        return $price;
        
    }
    
    public static function getActiveBundles($limit = -1){
        global $wpdb;
        $activeBundles = array();
        
        $sql = "SELECT DISTINCT * ";
        $sql .= "FROM " . VENDOR_BUNDLES_TABLE . " vb ";
        if($limit >= 0)
            $sql .= "LIMIT = " .$number . " ";
        
        $tempActiveBundles = $wpdb->get_results($sql);
        
        foreach($tempActiveBundles as $tempActiveBundle){ 
            
            $activeBundle = new Bundle($tempActiveBundle->bundle_ID);
            
            $activeBundle->setVendorActivated($tempActivatedBundle->vendor_ID);
            $activeBundle->setPrice($tempActivatedBundle->price);
            
            $serializedIncludeAddons = $tempActiveBundle->addons;
            $jsonIncludedAddons = unserialize($serializedIncludedAddons);
            $tempIncludedAddons = json_decode($jsonIncludedAddons);
            
            $includedAddons = array();
            
            foreach($tempIncludedAddons as $tempIncludedAddon){
                
                $includedAddon = new Addon($tempIncludedAddon);
                
                $includedAddons[] = $includedAddon;
                
            }
            
            $activeBundle->setIncludedAddods($includedAddons);
            
        }
        
        return $activeBundles;
        
    }
    
    public function getAddonsAdded(){ // Return the count of addons that are already in the bundle
        $count = 0;
        if(empty($this->included_pizzas)){
            return $count;
        }else{
            foreach($this->included_pizzas as $included_pizza){
                $addons = $included_pizza->getAddons();
                $count += count($addons);
            }
        }
        
        return $count;
    }
    public function getAddedPizzas(){ // Return the count of Pizzas that are already in the bundle
        $count = 0;
        if(empty($this->included_pizzas)){
            return $count;
        }else{
            $count += count($this->included_pizzas);
        }
        
        return $count;
    }
    public function getAddedDrinks(){ // Return the count of Drinks that are already in the bundle
        $count = 0;
        if(empty($this->included_drinks)){
            return $count;
        }else{
            $count += count($this->included_drinks);
        }
        echo $count;
        return $count;
    }
    public function getAddedAdditionalProducts(){ // Return the count of Additional Products that are aleady in the bundle
        $count = 0;
        if(empty($this->included_additional_products)){
            return $count;
        }else{
            $count += count($this->included_additional_products);
        }
        
        return $count;
    }
    
    public function getRemainingAddons(){
        $remaining_slots = $this->addons - $this->getAddonsAdded();
        if($remaining_slots < 0){
            $remaining_slots = 0;
        }
        return $remaining_slots;
    } // Get Remaining slots for Addons
    public function getRemainingPizzas(){
        $remaining_slots = $this->pizzas - $this->getPizzasAdded();
        if($remaining_slots < 0){
            $remaining_slots = 0;
        }
        return $remaining_slots;
    } // Get Remaining slots for Pizzas
    public function getRemainingDrinks(){
        $remaining_slots = $this->drinks - $this->getDrinksAdded();
        if($remaining_slots < 0){
            $remaining_slots = 0;
        }
        return $remaining_slots;
    } // Get Remaining slots for Drinks
    public function getRemainingAdditionalProducts(){
        $remaining_slots = $this->additional_products - $this->getAdditionalProductsAdded();
        if($remaining_slots < 0){
            $remaining_slots = 0;
        }
        return $remaining_slots;
    } // Get Remaining slots for Additional Products
    public function getLowestPrice(){
        
        // Constructing a SELECT query.
        $sql  = "SELECT min(vendor_price) as vendor_price ";
        $sql .= "FROM " . $this->table . " ";
        $sql .= "WHERE bundle_WPID = " . $this->WPID . " ";
        $sql .= "AND in_stock = 1 ";
        $sql .= "LIMIT 1";
    
        //SELECTing the lowest price of current bundle from all available prices.
        $results = DB::Select($sql);
        
        return $results[0]->vendor_price;
        
    }
    //************************************************************************************************************
    // Setters
    
    public function setVendorActivated($vendorActivated){
        $this->vendorActivated = $vendorActivated;
    }
    public function setPrice($price){
        $this->price = $price;
    }
    
    //************************************************************************************************************
    // Booleans
    
    public function isCompleted(){
        return $is_completed;    
    }
    
    public function isActivated($vendor_ID = -1){  
        if($vednor_ID = -1)
            $vendor = new Vendor();
        else
            $vendor = new Vendor($vendor_ID);
        $vendor_bundles = $vendor->getBundles();
        $is_activated = false;
        foreach($vendor_bundles as $vendor_bundle){
            if($this->WPID == $vendor_bundle->getWPID()){
                $is_activated = true;
                break;
            }
        }
        
        return $is_activated;
    }

    public function addAddonsToPay($addons_to_pay){
        $this->addons_to_pay = array_merge($addons_to_pay,$this->addons_to_pay);
    }
    
    public function isEnoughSpaceForAddons(){
        if($this->addons <= $this->getAddonsAdded()){
            return false;
        }else{
            return true;
        }
    }  // Checks if there are slots left for more Addons
    public function isEnoughSpaceForPizzas(){
        if($this->pizzas <= $this->getAddedPizzas()){
            return false;
        }else{
            return true;
        }
    } // Checks if there are slots left for more Pizzas
    public function isEnoughSpaceForDrinks(){ 
        
        if($this->drinks <= $this->getAddedDrinks()){
            return false;
        }else{
            return true;
        }
    } // Checks if there are slots left for more Drinks
    public function isEnoughSpaceForAdditionalProducts(){
        if($this->additional_products <= $this->getAddedAdditionalProducts){
            return false;
        }else{
            return true;
        }
    } // Checks if there are slots left for more Additional Products
    
    public function inStock(){
        global $wpdb;
        
        $vendor = new Vendor();
        
        $sql  = "SELECT * ";
        $sql .= "FROM " . VENDOR_BUNDLES_TABLE . " ";
        $sql .= "WHERE vendor_ID = " . $vendor->getID() . " ";
        $sql .= "AND bundle_WPID = " . $this->WPID;
        
        $results = $wpdb->get_results($sql);
        
        if(empty($results)){
            return false;
        }elseif($results[0]->in_stock == 0){
            return 0;
        }else{
            return 1;
        }
    } // Checks if current Vendor that bundle in stock;
    
    //********************************************************************************/
    // Add to bundle
    
    public function addPizza($pizza){ 
        $result = array();
        
        $temp_addson = Secure::JsonDecode($_POST["addson"]); // Decoding all info about addson that was passed
        $pizza = new Pizza(); // Creating new Pizza
        $addson = array(); // Empty array to hold addson
        
        foreach($temp_addson as $temp_addon){ // looping through
            
            $addon = new Addon($temp_addon->WPID); 
            $addon->setLocation($temp_addon->location);
            
            $pizza->addAddon($addon); // Adding the Addon to pizza
            
        }
        
        if(!$this->isEnoughSpaceForAddons()){ 
            
            if( !($this->getRemainingAddons() <= count($temp_addson)) ){ 
                
                $result["errors"][] = new Error("Bundle","addPizza","Not Enough Space for a Addson");
                echo json_encode($result);
                die; // Life has no meaning
            }
            
        }
        
        if(!$this->isEnoughSpaceForPizzas()){ 
            
            $result["errors"][] = new Error("Bundle","addPizza","Not Enough Space for a pizza");
            echo json_encode($result);
            die; // Death is Heaven
        }
        
        $included_pizzas = $this->included_pizzas;
        $included_pizzas[] = $pizza; // Adding pizza to bundle
        $this->included_pizzas = $included_pizzas;
        
        $this->Save(); // Saving the bundle
        
        if($this->checkPizzas()){ 
            $result["ready"] = true;
        }else{
            $result["ready"] = false;
        }
        
        echo json_encode($result);
        
        die; // To Live is to Die.
    } // Ajax
    
    // Adds a drink to current bundle
    public function addDrink(){ 
        // Adding a drink to a bundle (Ajax)
        // Returning an Array with errors and indication if the bundle is full
        $result = array();
        
        $temp_drink = Secure::JsonDecode($_POST["drink"]);
        
        $drink = new Drink($temp_drink->WPID);
        
        if($this->checkDrinks()){
            $result["errors"][] = new Error("Bundle","addDrink","Not Enough Space for a Drink");
            echo json_encode($result);
            die; // Death is sacred
        }
        
        $included_drinks = $this->included_drinks;
        $included_drinks[] = $drink;
        $this->included_drinks = $included_drinks;
        
        $this->Save();
        if($this->checkDrinks()){
            $result["ready"] = true;
        }else{
            $result["ready"] = false;
        }
        echo json_encode($result);
        die; // To be or not to be, that is the question
        
    } // Ajax
    
    public function addAdditionalProduct(){} // TODO addAdditionalProduct
    
    //********************************************************************************/
    // Remove from bundle
    
    public function removePizza($ID){
        $pizzas = $this->included_pizzas;
        //var_dump($pizzas);
        foreach($pizzas as $key => $pizza){
            if($pizza->getID() == $ID){ 
                echo "removeing..";
                unset($pizzas[$key]);
                break;
            }
        }
        $this->included_pizzas = $pizzas;
    }
    
    public function removeDrink($WPID){ 
        $drinks = $this->included_drinks;
        
        foreach($drinks as $key => $drink){
            if($drink->getWPID() == $WPID){
                echo "Removing ... ";
                unset($drinks[$key]);
                break;
            }
        }
        $this->included_drinks = $drinks;
    }
    
    //********************************************************************************/
    
    //********************************************************************************/
    public function getDrinkStackByWPID($WPID){
        $stack = new stdClass();    
        $stack->drink = new Drink($WPID);
        $stack->quantity = 0;
        
        foreach($this->included_drinks as $drink){
            if($drink->getWPID() == $WPID)
                $stack->quantity++;
        }
        
        return $stack;
    }
    
    
    
    //********************************************************************************/
    
    public function checkPizzas(){ 
        
        if( $this->pizzas == count($this->included_pizzas) ){ 
            
            if($this->addons == $this->getIncludedAddons()){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
        return true;
    }
    
    public function checkDrinks(){
        
        if( $this->drinks == count($this->included_drinks) ){
            return true;
        }else{
           return false; 
        }
        
    }
    //********************************************************************************/
    
    public static function AjaxCreate(){ 
        $temp_bundle = Secure::JsonDecode($_POST['bundle']);
        $bundle = new Bundle($temp_bundle->WPID,$temp_bundle->ID);
        $_SESSION["Bundle"] = serialize($bundle);
    }
    
    public function Create(){
        $_SESSION["Bundle"] = serialize($this);
    }
    
    public function Save(){
        $_SESSION["Bundle"] = serialize($this);
    }
    public static function Open(){ 
        if(isset($_SESSION["Bundle"])){
            $bundle = unserialize($_SESSION["Bundle"]);    
        }else{
            return false;
        }
        
        return $bundle;
    }
    public function Delete(){
        unset($_SESSION["Bundle"]);
    }
}






?>
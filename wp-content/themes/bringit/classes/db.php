<?php

class DB{
    
    public static function isSetRow($table,$where){
        global $wpdb;
        
        $sql = "SELECT * ";
        $sql .= "FROM " . $table . " ";
        $i = 0;
        foreach($where as $key => $value){
            if($i==0){
                $sql .= "WHERE " . $key . " = " . $value . " ";
            }else{
                $sql .= "AND " . $key . " = " . $value . " ";
            }
            $i++;
        }
        
        var_dump($sql);
        
        $results = $wpdb->get_results($sql);
        
        if(empty($results)){
            return false;
        }else{
            return true;
        }
    }
    
    
    public static function Select($sql){ // Select from db using WPDB class
        global $wpdb;
        
        $results = $wpdb->get_results($sql);
        
        return $results;
    }
    
}

?>
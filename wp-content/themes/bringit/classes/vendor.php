<?php

class Vendor{
    
    const table = VENDOR_DRINKS_TABLE;
    
    private $ID = null;
    private $name = null;
    private $phone = null;
    private $image = null;
    private $shipping_time = null;
    private $shipping_price = null;
    private $free_shipping_minimum_price = null;
    private $pizza_cost = null;
    private $shipping_rating = null;
    private $taste_rating = null;
    private $kashrut = null;
    private $state = null;
    private $addons = array();
    private $drinks = array();
    private $bundles = array();
    private $additional_products = array();
    private $shipping_regions = array();
    private $shipping_cities = array();
    
    public function __construct($vendor_ID = "not set"){ 
        
        global $wpdb;
        
        if($vendor_ID == "not set"){
            $ID = get_current_user_id();    
        }else{
            $ID = $vendor_ID;
        }
        
    
        if( !$ID ){ 
            return false;  
        }

        $this->name                           = get_field("name","user_" . $ID);
        $this->phone                           = get_field("phone","user_" . $ID);
        $this->ID                             = $ID;
        $this->image                          = get_field("image","user_" . $ID);
        $this->shipping_price                 = get_field("shipping_price","user_" . $ID);
        $this->free_shipping_minimum_price    = get_field("free_shipping_minimum_price", "user_" . $ID);
        $this->pizza_cost                     = get_field("pizza_price","user_" . $ID);
        $this->shipping_time                  = get_field("shipping_time","user_" . $ID);
        $this->shipping_rating              = get_field("shipping_rating","user_" . $ID);
        $this->taste_rating                 = get_field("taste_rating","user_" . $ID);
        $this->kashrut                      = get_field("kashrut","user_" . $ID);
        $this->state                        = get_field("open-closed","user_" . $ID);
        
        $this->drinks = $this->getDrinksByID($ID);
        $this->addons = $this->getAddonsByID($ID);
        $this->additional_products = $this->getAdditionalProductsByID($ID);
        
        $sql =  "SELECT DISTINCT vc.id, vc.name ";
        $sql .= "FROM vendor_cities vc ";
        $sql .= "WHERE vc.vendor_ID=".$ID;
            
        $temp_shipping_cities = $wpdb->get_results($sql);
        $shipping_cities = array();
        foreach($temp_shipping_cities as $temp_shipping_city){ 
            
            $shipping_city = new stdClass();
            $shipping_city->name = $temp_shipping_city->name;
            $shipping_city->ID = $temp_shipping_city->id;
            $shipping_city->regions = array();
            
            $shipping_cities[] = $shipping_city;
        }
        $this->shipping_cities = $shipping_cities;

    }
    
    // Getters
    public function getID(){
        return $this->ID;
    }
    public function getName(){
        return $this->name;
    }
    public function getPhone(){
        return $this->phone;
    }
    public function getImage(){
        return $this->image;
    }
    public function getShippingTime(){
        return $this->shipping_time;
    }
    public function getShippingPrice(){
        return $this->shipping_price;
    }
    public function getFreeShippingMinimumPrice(){
        return $this->free_shipping_minimum_price;
    }
    public function getPizzaCost(){
        return $this->pizza_cost;
    }
    public function getShippingRating(){
        return $this->shipping_rating;
    }
    public function getTasteRating(){
        return $this->taste_rating;
    }
    public function getKashrut(){
        return $this->kashrut;
    }
    public function getState(){
        return $this->state;
    }
    public function getAddons(){
        return $this->addons;
    }
    public function getDrinks(){
        return $this->drinks;
    }
    public function getPizzaPrice(){
        global $wpdb;
        
        $sql  = "SELECT * ";
        $sql .= "FROM " . VENDOR_PIZZA_PRICE_TABLE . " ";
        $sql .= "WHERE vendor_ID=" . $this->ID;
        
        $results = $wpdb->get_results($sql);
        
        $price = $results[0]->pizza_price;
        
        return $price;
    }
    public function getBundles($number = -1){
        
        global $wpdb;
        
        $bundles = array();
        
        $sql  = "SELECT DISTINCT * ";
        $sql .= "FROM " . VENDOR_BUNDLES_TABLE . " ";
        $sql .= "WHERE vendor_ID = " .$this->ID;
        
        $temp_bundles = $wpdb->get_results($sql);
        
        foreach($temp_bundles as $temp_bundle){
            $bundle = new Bundle($temp_bundle->bundle_WPID);
            $bundles[] = $bundle;
        }
        
        return $bundles;
        
    }
    
    public function getBundlePrice($WPID){
        
        $sql  = "SELECT vendor_price as price ";
        $sql .= "FROM " . VENDOR_BUNDLES_TABLE . " ";
        $sql .= "WHERE bundle_WPID = " . $WPID . " ";
        $sql .= "AND vendor_ID = " . $this->ID;
    
        $results = DB::Select($sql);
        
        if(empty($results)){
            return 0;
        }else{
            return $results[0]->price;
        }
        
    }
    
    public function getCities(){
        return $this->shipping_cities;
    }
    public function getUnregisteredCities(){ 
        
        $cities = Address::getCities();
        $vendor_cities = $this->getCities();
        foreach($vendor_cities as $key => $vendor_city){ 
    
            foreach($cities as $k => $city){
                if($city->name == $vendor_city->name){
                    unset($cities[$k]);
                }
                break;
            }

        }
        
        return $cities;
        
    } // gets list of cities that the vendor does not deliver to
    public function getOrders(){
        global $wpdb;
        
        $sql =  "SELECT * ";
        $sql .= "FROM " . BRINGIT_ORDERS . " bo ";
        $sql .= "WHERE seller_id = " .$this->ID;
        
        $results = $wpdb->get_results($sql);
        $orders = array();
        foreach($results as $result){
            $orders[] = new Order($result->id);
        }
        return $orders;
    }
    //Setters
    public function setID($ID){
        $this->ID = $ID;
    }
    public function setName($name){
        $this->name = $name;
    }
    public function setImage($image){
        $this->image = $image;
    }
    public function setShippingTime($shipping_time){
        $this->shipping_time = $shipping_time;
    }
    public function setShippingPrice($shipping_price){
        $this->shipping_price = $shipping_price;
    }
    public function setFreeShippingMinimumPrice($shipping_minimum_price){
        $this->free_shipping_minimum_price = $free_shipping_minimum_price;
    }
    public function setPizzaCost($pizza_cost){
        $this->pizza_cost = $pizza_cost;
    }
    public function setShippingRating($shipping_rating){
        $this->shipping_rating = $shipping_rating;
    }
    public function setTasteRating($taste_rating){
        $this->taste_rating = $taste_rating;
    }
    public function setKashrut($kashrut){
        $this->kashrut = $kashrut;
    }
    public function setState($state){
        $this->state = $state;
    }
    
    public static function getVendors($number = 5){ 
        
        $args = array(
            "role"      => "vendor",
            "number"    => $number,
        );

        $temp_vendors = get_users($args);

        $vendors = array();

        foreach($temp_vendors as $temp_vendor){

            $id = $temp_vendor->ID;

            $vendor = new Vendor($id);

            $vendors[] = $vendor;

        }

        return $vendors;
    }
    public static function getVendorsByCity($city = "not set", $number = 5){
        global $wpdb;
        
        $vendors = array();
        
        if($city == "not set"){
            return false;
        }
        
        $sql =  "SELECT DISTINCT vr.vendor_ID ";
        $sql .= "FROM " . VENDOR_REGIONS_TABLE . " vr, " . VENDOR_CITIES_TABLE . " vc ";
        $sql .= "WHERE vr.city_name = vc.name ";
        $sql .= "AND vc.name = '" . $city ."' ";
        $sql .= "LIMIT " . $number;
        
        
        $temp_vendors = $wpdb->get_results($sql);
        
        foreach($temp_vendors as $temp_vendor){
            
            $vendor = new Vendor($temp_vendor->vendor_ID);
            
            $vendors[] = $vendor;
            
        }
        
        return $vendors;
    }
    
    public static function getVendorsByRegion($city = "not set", $region = "not set", $number = 5){
        global $wpdb;
        
        $vendors = array();
        
        if($city == "not set" || $region == "not set"){
            return false;
        }
        
        $sql =  "SELECT vr.vendor_ID, vr.shipping_time, vr.shipping_price, vr.pizza_price, vr.minimum_order_price_for_free_shipping ";
        $sql .= "FROM " . VENDOR_REGIONS_TABLE . " vr, " . VENDOR_CITIES_TABLE . " vc ";
        $sql .= "WHERE vr.city_ID = vc.id ";
        $sql .= "AND vc.name = '" . $city ."' ";
        $sql .= "AND vr.name = '" . $region ."' ";
        $sql .= "LIMIT " . $number;
        
        $temp_vendors = $wpdb->get_results($sql);
        
        foreach($temp_vendors as $temp_vendor){
            
            $vendor = new Vendor($temp_vendor->vendor_ID);
            
            $vendors[] = $vendor;
            
        }
        
        return $vendors;
        
    }

    public static function getDrinksByID($ID){
        
        $drinks = array();
        
        $sql  = "SELECT * ";
        $sql .= "FROM " . Vendor::table . " ";
        $sql .= "WHERE vendor_ID = " . $ID;
        
        $results = DB::Select($sql);
    
        foreach($results as $result){
            $drink = new Drink($result->drink_WPID);
            $drink->setPrice($result->vendor_price);
            
            $drinks[] = $drink;
        }
        
        return $drinks;
    }

    
    public static function getAddonsByID($ID){
        global $wpdb;
        
        $addons = array(); // The array that will contain all addons of the vendor
        
        $sql  = "SELECT * ";
        $sql .= "FROM " . VENDOR_ADDONS_TABLE . " ";
        $sql .= "WHERE vendor_ID = " . $ID;
        
        $results = $wpdb->get_results($sql);
        
        foreach($results as $result){
            $addon = new Addon($result->addon_WPID);
            $addon->setPrice($result->vendor_price);
            $addons[] = $addon;
        }
        return $addons;
    }
    
    public static function getAdditionalProductsByID($ID = "not set"){ 
        $additional_products = array();
        
        $args = array(
            "post_type" => "additional_product",
            "status" => "publish",
            "author" => $ID,
            "number" => -1
        );
        
        $temp_additional_products = get_posts($args);
        
        foreach($temp_additional_products as $temp_additional_product){ 
            
            $additional_product = new AdditionalProduct($temp_additional_product->ID,$ID);
            $additional_products[] = $additional_product;
            
        }
        return $additional_products;
    }
    public function getShippingTimeByRegion($city,$region){
        
        $region_obj = $this->getRegionData($city,$region);
        
        if($region_obj != false){
            return $region_obj->shipping_time;    
        }else{ 
            $html  = "<p class = 'no-delivery'>";
            $html .= "לא מבצעים משלוחים";
            $html .= "</p>";
            return  -1;
        }
        
    }
    
    // Prices
    public function getRegionData($city,$region){
        global $wpdb;
        
        $sql  = "SELECT vr.shipping_price, vr.shipping_time, vr.minimum_order_price, vr.minimum_order_price_for_free_shipping ";
        $sql .= "FROM " . VENDOR_REGIONS_TABLE . " vr, " . VENDOR_CITIES_TABLE . " vc ";
        $sql .= "WHERE vr.name = '" . $region . "' ";
        $sql .= "AND vc.name = vr.city_name ";
        $sql .= "AND vc.name = '" . $city . "' ";
        $sql .= "AND vr.vendor_ID = " . $this->ID;
        
        $temp_regions = $wpdb->get_results($sql);
    
        if(empty($temp_regions)){
            return false;
        }else{
            $region = $temp_regions[0];
        }
        
        return $region;
    }
    public function getPizzaPriceByRegion($city,$region){ 
        global $wpdb;
        
        $sql  = "SELECT pizza_price ";
        $sql .= "FROM " . VENDOR_PIZZA_PRICE_TABLE . " ";
        $sql .= "WHERE vendor_ID = " . $this->ID;
        
        $results = $wpdb->get_results($sql);
        
        if(empty($results)){
            return 0;
        }else{
            return $results[0]->pizza_price;
        }
        
    }    
    public function getDrinksPrice(){
        $total_drinks_price = 0;
        $drinks = $this->getDrinksByID($this->ID); // Gets all drinks available from current Vendor
        foreach($drinks as $drink){ 
            if($drink->inStock())
                $total_drinks_price += $drink->getPrice();
        }
        return $total_drinks_price;
    } // Returns the Drinks' price from the cart by vendor
    
    public function getAddonsPrice(){
        $total_addson_price - 0;
        $addson = $this->getAddonsByID($this->ID); // Gets all addson available from current Vendor
        foreach($addson as $addon){
            if($addon->inStock())
                $total_addson_price += $addon->getPrice();
        }
        return $total_addson_price;
    }
    
    public function getAdditionalProductsPrice(){
        return 0;
    } // TODO getAdditionalProductsPrice
    public function getBundlesPrice(){
        global $wpdb;
        $cart = Cart::OpenCart();
        
        $offered_bundles = $this->getOfferedBundles(); // gets the list of Bundles that are offered by current Vendor From Customer Cart
       
        $bundles_price = 0;
        
        foreach($offered_bundles as $offered_bundle){
            $bundles_price += $offered_bundle->getPrice();
        }
//        return 0;
        return $bundles_price;
        
    }
    public function getShippingPriceByRegion($city,$region){ 
        
        $region_obj = $this->getRegionData($city,$region);
        
        return $region_obj->shipping_price;
        
    }
    public function getMinimumOrderPriceForFreeShippingByRegion($city,$region){
        
        $region_obj = $this->getRegionData($city,$region);
        
        return $region_obj->minimum_order_price_for_free_shipping;
        
    }
    //
    public function getAvailableAddons(){ 
        
        $cart = unserialize($_SESSION['Cart']);
    
        $addons = array();
        $pizzas = $cart->getPizzas();
        
        foreach($pizzas as $pizza){

            $current_addons = $pizza->getAddons();
            
            if( empty($current_addons) )
                continue; // If there are no addons on current pizzas then skip current loop.

            
            $addons = array_merge($addons,$current_addons);

        }

        $available_addons = array(); // An array that will contain the missing Addons for current Vendor
        
        $missing_addons = $this->getMissingAddons();
        
        $vendors_addons = $this->addons; // get all the addons that the vendor sells;
    
        foreach($addons as $addon){ // making the array with unique values
        
            $miss = false;
            $repeat = false;
            if(!empty($missing_addons)){
                foreach($missing_addons as $missing_addon){
                    if($missing_addon->getWPID() == $addon->getWPID()){
                        $miss = true;
                    }
                }
            }
            
            if($miss)
                continue;
            
            foreach( $available_addons as $available_addon ){

                if( $available_addon->getWPID() == $addon->getWPID() ){
                    $repeat = true;
                    break;
                }

            }
            
            if($repeat)
                continue;
            
            if(!empty($vendors_addons)){
                foreach($vendors_addons as $vendors_addon){
                    
                    if( $vendors_addon->getWPID() == $addon->getWPID() ){
                        $available_addons[] = $addon;
                        break;
                    }

                }
            }
            

        }
        
        

        if( !empty($available_addons) ){
            return $available_addons;
        }


        return false;
    }
    public function getAvailableDrinks($vendor_ID = 'not set'){
        
        $cart = unserialize($_SESSION['Cart']); // Gets the cart from the session to work with
        
        $available_drinks = array(); // An array that will contain the missing Drinks for current Vendor

        $drinks = $cart->getDrinks(); // get all the drink from the cart
        
        $vendors_drinks = $this->drinks; // get all the drinks that the vendor sells;
        
        foreach($drinks as $drink){ // making the array with unique values
        
            $miss = false;
            $repeat = false;
            if(!empty($missing_drinks)){
                foreach($missing_drinks as $missing_drink){
                    if($missing_drink->getWPID() == $drink->getWPID()){
                        $miss = true;
                    }
                }
            }
            
            if($miss)
                continue;
            
            foreach( $available_drinks as $available_drink ){

                if( $available_drink->getWPID() == $drink->getWPID() ){
                    $repeat = true;
                    break;
                }

            }
            
            if($repeat)
                continue;
            
            if(!empty($vendors_drinks)){
                foreach($vendors_drinks as $vendors_drink){
                    
                    if( $vendors_drink->getWPID() == $drink->getWPID() ){
                        $available_drinks[] = $drink;
                        break;
                    }

                }
            }
            

        }
        
        

        if( !empty($available_drinks) ){
            return $available_drinks;
        }


        return false;
        
    }
    public function getOfferedBundles(){
        $cart = Cart::OpenCart();
        //echo "Vendor ID: " . $this->ID;
        $cart_bundles = $cart->getBundles();
        $vendor_bundles = $this->getBundles();
        
        $offered_bundles = array();
        
        foreach($cart_bundles as $key => $cart_bundle){ // Comparing between what's in the cart and what the vendor is offering
            $is_offered = false;
            foreach($vendor_bundles as $vendor_bundle){
                
                if($vendor_bundle->getWPID() == $cart_bundle->getWPID()){
                    $is_offered = true;
                }
                
            }
            if(!$is_offered)
                unset($cart_bundles[$key]); // unsetting bundles that are not offered by the Vendor
        }
        foreach($cart_bundles as $cart_bundle){ // After Filtering the cart Bundles I save them in a different array.
            $offered_bundles[] = $cart_bundle;
        }
        
        return $offered_bundles; 
        
    } // This function is like the "getAvailable..." functions that are used to get products from cart that are available from current Vendor.
    //
    public function getMissingAddons(){ //returns missing Addons for current vendor according to current cart
        
        $cart = Cart::OpenCart(); // Getting the contents of current cart
        
        $addons = array(); // This is array will hold the addson
        $pizzas = $cart->getPizzas();

         // Now I will get every all addson from the bundles that are in the cart
        $bundles = $cart->getBundles();
        foreach($bundles as $bundle){ // Looping through the bundles
            $temp_addson = $bundle->getIncludedAddson(); // Getting Addons that are in the bundle
            $addons = array_merge($addons,$temp_addson); // Adding those addons to the array of addons
        }
        
        foreach($pizzas as $pizza){ // Looping through the pizzas
            
            $current_addons = $pizza->getAddons(); // collection the addons from current pizza
            
            if( empty($current_addons) )
                continue; // If there are no addons on current pizzas then skip current loop.

            $addons = array_merge($addons,$current_addons); // adding the collected addons from  current pizza to the array that holds all addons from current cart

        }

        $missing_addons = array(); // An array that will contain the missing Addons for current Vendor

        //$addons = $cart->getAddons(); // get all the Addons from the cart
        
        $vendors_addons = $this->addons; // get all the addons that the vendor sells;

        foreach($addons as $addon){ // making the array with unique values

            $repeat = false;

            foreach( $missing_addons as $missing_addon ){

                if( $missing_addon->getWPID() == $addon->getWPID() ){
                    $repeat = true;
                    break;
                }

            }
            if($vendors_addons){
                foreach($vendors_addons as $vendors_addon){

                    if( $vendors_addon->getWPID() == $addon->getWPID() ){
                        $repeat = true;
                        break;
                    }

                }
            }

            if($repeat)
                continue;

            $missing_addons[] = $addon;

        }


        if( !empty($missing_addons) ){
            return $missing_addons;
        }


        return false;

    }
    
    public function getMissingDrinks(){
        $cart = Cart::OpenCart();
        $vendor = $this;
        
        $drinks = $cart->getAllDrinks();
        $offered_drinks = $vendor->getDrinks();
//        var_dump($offered_drinks);
        $missing_drinks = array();
        
        foreach($drinks as $drink){ // gathers missing drinks (unique array)
            $is_available = false;
            
            foreach($offered_drinks as $offered_drink){ 
//                echo $offered_drink->getWPID();
                if($drink->getWPID() == $offered_drink->getWPID()){
                    $is_available = true; 
                }
            }
            
            if(!$is_available){ 
                $is_unique = true;
                foreach($missing_drinks as $missing_drink){
                    if($drink->getWPID() == $missing_drink->getWPID())
                        $is_unique = false;
                }
                if($is_unique)
                    $missing_drinks[] = $drink;
            }
            
        }
        
        // Make the array unique.
        
        
        return $missing_drinks;
        
    }
    
    
    public function getAdditionalProductsToSell(){
        $table = VENDOR_ADDITIONAL_PRODUCTS_TABLE;
        
        $sql = "SELECT id ";
        $sql .= "FROM " . $table . " ";
        $sql .= "WHERE vendor_ID = " . $this->ID . " ";
        $sql .= "AND in_stock = 1";
        
        $results = DB::Select($sql);
        
        $additional_products = array();
        
        if(!empty($results)){
            foreach($results as $result){
                $additional_product = new AdditionalProduct($result->id);
                
                $additional_products[] = $additional_product;
            }
        }
        return $additional_products;
    }
    // *************************************************************************************************************** //
    // Booleans
    public function isShipping($city,$region){ 
        if($this->getShippingTimeByRegion($city,$region) === -1 || $this->getShippingTimeByRegion($city,$region) == 0 ){
            return false;
        }
        return true;
    }
    
    // Vendors Dashboard methods (Starts); ******************************************************************************


    public function addNewArea(){
        global $wpdb;
        if(!isset($_POST["city"])){ // IF the city parameter somehow was not passed the function will die here...
            echo "City is not defined"; 
            die;
        }
        
        $RAWjsonCITY = $_POST["city"]; // Saving the "city" parameter that was passed by POST into a normal variable.
        $jsonCity = preg_replace('/\\\"/',"\"",$RAWjsonCITY); // Removing the slashes from the JSON string that was passed.
        $city = json_decode($jsonCity); // Decoding the JSON that was passed and cleaned from slashes.
        
        $table = VENDOR_CITIES_TABLE; // The table that will be used.
    
        $data = array(); // New empty array that will hold the columns to insert into the row. (vendor_cities)
        // I am saving the city that the vendor chose to work in.
        
        $data["name"] = $city->name; // The name of the city.
        $data["vendor_ID"] = $this->ID; // The ID of the Vendor.
        
        $wpdb->insert($table,$data); // Inserting the row into the table.
        echo "\n City was Inserted Successfully \n"; // Debugging Message.
        
        $table = VENDOR_REGIONS_TABLE; // The table that will be used.
        // Now here I am saving the data about the regions chosen to another table with a reference to the city and vendor ID.
        
        foreach($city->regions as $region){ // Looping through the regions in current city
            $data = array(); // Creating new array of data that will be inserted further
            $data["name"] = $region->name; // Region Name.
            $data["shipping_time"] = $region->shippingTime; // Shipping Time
            $data["shipping_price"] = $region->shippingPrice; // Shipping Price
            $data["minimum_order_price"] = $region->minimumOrderPrice; // minimum order price
            $data["minimum_order_price_for_free_shipping"] = $region->minimumOrderPriceForFreeShipping; // Minimum Order Price For Free Shipping
            $data["vendor_ID"] = $this->ID; // Vendor ID 
            $data["city_name"] = $city->name; // City Name that the regions refer to.
            $data["is_shipping"] = $region->is_shipping; // An Indication if the vendor is shipping to current region or not.
            
            $wpdb->insert($table,$data); // inserting the new row to the table.
        }
        echo "\n Regions were Inserted Successfully \n"; // Debugging Message.
        die;
    } // AJAX
    
    public function updateArea(){ // An ajax function that update the information in shipping areas of current vendor
        global $wpdb;
        
        $city = Secure::JsonDecode($_POST["city"]); // Decoding the information that was passed
        
        $table = VENDOR_REGIONS_TABLE;
        
        $where = array();
        $where["vendor_ID"] = $this->ID;
        $where["city_name"] = $city->name;
        
        $wpdb->delete($table,$where);
        
        echo "Deleted all previous data about this city for current Vendor \n"; // notification in console that all previous info was deleted.
        
        foreach($city->regions as $region){ // looping through the array of regions that was passed
            // saving the info into array $data that will be used to INSERT into DB
            $data = array();
            $data["vendor_ID"] = $this->ID;
            $data["name"] = $region->name;
            $data["shipping_time"] = $region->shippingTime;
            $data["shipping_price"] = $region->shippingPrice;
            $data["minimum_order_price"] = $region->minimumOrderPrice;
            $data["minimum_order_price_for_free_shipping"] = $region->minimumOrderPriceForFreeShipping;
            $data["city_name"] = $city->name;
            $data["is_shipping"] = $region->is_shipping;
            
            $wpdb->insert($table,$data);
        }
        echo "Inserted all new data about this city for current Vendor \n"; // notification in console that all new Data was INSERTed.
        die; // DIE DIE DIE my darling!
    } // AJAX
    
    public function removeArea(){ // Remove shipping area of current Vendor (AJAX)
        global $wpdb;
        
        $city = Secure::JsonDecode($_POST["city"]); // Decoding passed Data
        
        $table = VENDOR_REGIONS_TABLE;
        
        $where = array(); // Which rows to remove.
        $where["vendor_ID"] = $this->ID;
        $where["city_name"] = $city->name;
        
        $wpdb->delete($table,$where);
        
        $table = VENDOR_CITIES_TABLE;
        
        $where = array();
        $where["vendor_ID"] = $this->ID;
        $where["name"] = $city->name;
        
        $wpdb->delete($table,$where);
        echo "Deleted all data about this city for current Vendor \n";
        die;
    } // AJAX

    public function getRegionsByCity($city_name = "not set"){ 
        
        global $wpdb;
        
        $regions = array();
        
        if($city_ID == "not set"){
            return false;
        }
        
        $sql  = "SELECT * ";
        $sql .= "FROM " . VENDOR_REGIONS_TABLE . " vr ";
        $sql .= "WHERE city_name = '". $city_name . "' ";
        $sql .= "AND vendor_ID = ". $this->ID . " ";
        
        $temp_regions = $wpdb->get_results($sql);
        
        foreach($temp_regions as $temp_region){
            
            $region = new stdClass();
            
            $region->ID = $temp_region->id;
            $region->vendorID = $temp_region->vendor_ID;
            $region->city_name = $temp_region->city_name;
            $region->name = $temp_region->name;
            $region->shippingTime = $temp_region->shipping_time;
            $region->shippingPrice = $temp_region->shipping_price;
            $region->minimumOrderPrice = $temp_region->minimum_order_price;
            $region->minimumOrderPriceForFreeShipping = $temp_region->minimum_order_price_for_free_shipping;
            
            $regions[] = $region;
            
        }
        
        return $regions;
        
    }
    public function is_shipping($city_name,$region_name){
        global $wpdb;
        
        $sql  = "SELECT * ";
        $sql .= "FROM " . VENDOR_REGIONS_TABLE . " ";
        $sql .= "WHERE city_name = '" . $city_name . "' ";
        $sql .= "AND name = '" . $region_name . "' ";
        $sql .= "AND vendor_ID = " . $this->ID;
        
        $results = $wpdb->get_results($sql);

        if(empty($results)){
            $is_shipping = false;
        }elseif($results[0]->is_shipping == 1){
            $is_shipping = true;
        }else{
            $is_shipping = false;
        }
        
        return $is_shipping;
    }

    
    // Stock
    
    public function updateDrinksStock(){
        global $wpdb;
        echo "Vendor->updateDrinksStock is Updating the Vendors Drinks Stock";
        
        // Step 1: I am deleting existing rows of drinks that are in stock for current vendor
        $table = VENDOR_DRINKS_TABLE; // Getting the name of the table from theme-config.php
        $where = array(
            "vendor_ID" => $this->ID // Defining WHERE to delete rows - by current vendor_ID.
        );
        $wpdb->delete($table,$where); // Deleting...
        
        // Step 2: I am saving the new list of drinks that are in stock to an Array.
        $RAWjsonDRINKS = $_POST["drinks"]; // Saving the JSON to normal variable.
        $drinks = array(); // Creating a new empty array where I will store the new Rows that I should INSERT into the DB
        
        $jsonDrinks = preg_replace('/\\\"/',"\"",$RAWjsonDRINKS); // Here I am removing all slashes from the json string that was passed by POST
        $drinks = json_decode($jsonDrinks); // Array of Drinks to save is ready
        // Step 3: I am Inserting this Array to the "vedor_drinks" table
        foreach($drinks as $drink){
            $data = array();
            $data["vendor_ID"] = $this->ID;
            $data["drink_WPID"] = $drink->WPID;
            $data["vendor_price"] = $drink->price;
            $data["in_stock"] = $drink->in_stock;
            
            if($wpdb->insert($table,$data)){
                echo "everything is ok here";
            }
        }
        
        echo "Vendor->updateDrinksStock has finished without any error.";
        die;
        
    } // Ajax function - Updates in DB the drinks that current Vendor has in stock
    public function updateBundlesStock(){
        global $wpdb;
        echo "Vendor->updateBundlesStock is Updating the Vendors Bundles Stock";
        
        // Step 1: I am Deleting existing rows of drinks that are in stock for current vendor.
        $table = VENDOR_BUNDLES_TABLE; // Getting the name of the table from theme-config.php
        $where = array( 
            "vendor_ID" => $this->ID // Defining WHERE to delete rows -  by current Vendor_ID
        );
        $wpdb->delete($table,$where); // Deleting...
        
        // Step 2: I am saving the new list of drinks that are in stock to an Array.
        $RAWjsonBUNDLES = $_POST["bundles"]; // Saving the JSON to normal variable.
        $bundles = array(); // Creating a new emoty array where I will store the new Rows that I should INSERT into the DB.
        
        $jsonBundles = preg_replace('/\\\"/',"\"",$RAWjsonBUNDLES); // Here I am removing all slashes drom the json string that was passed by POST
        $bundles = json_decode($jsonBundles); // Array of Bundles to save is ready
        // Step 3: I am Inserting this array to the "vendor_bundles" table.
        foreach($bundles as $bundle){
            $data = array();
            $data["vendor_ID"] = $this->ID;
            $data["bundle_WPID"] = $bundle->WPID;
            $data["vendor_price"] = $bundle->price;
            $data["in_stock"] = $bundle->in_stock;
            
            if($wpdb->insert($table,$data)){
                echo "Everything is OK here!";
            }else{
                die(" \n DB ERROR \n ");
            }
        }
        
        echo "Vendor->updateBundlesStock has finished without any error.";
        die;
    } // Ajax function - Updates un DB the bundles that current Vendor has in Stock.
    
    public function updateAddsonStock(){
        global $wpdb;
        echo "Vendor->updateAddsonStock is Updating the Vendors Addson Stock";
        
        // Step 1: I am deleting existing rows of addson that are in stock for current vendor
        $table = VENDOR_ADDONS_TABLE; // Getting the name of the table frin theme-config.php
        $where = array( 
            "vendor_ID" => $this->ID
        );
        
        $wpdb->delete($table,$where); // Deleting...
        
        // Step 2: I am Saving the new list of Addson that are in stock to an Array.
        $RAWjsonADDSON = $_POST["addson"]; // Saving the JSON to a normal Variable.
        $addson = array(); // Creating a new empty array where I will store the new Rows that I should INSERT into the DB.
            
        $jsonAddson = preg_replace('/\\\"/',"\"",$RAWjsonADDSON); // Here I am removing all slashes from the json string that was passed by POST.
        $addson = json_decode($jsonAddson); //Array of Addson to save is ready
        // Step 3: I am inserting this Array to the "vendor_addons" table.
        foreach($addson as $addon){
            $data = array();
            $data["vendor_ID"] = $this->ID;
            $data["addon_WPID"] = $addon->WPID;
            $data["vendor_price"] = $addon->price;
            $data["in_stock"] = $addon->in_stock;
            $data["in_bundles"] = $addon->in_bundle;
            
            if($wpdb->insert($table,$data)){
                echo "Everything went OK here!";
            }
        }
        echo "Vendor->updateAddsonStock has finished without any error.";
        die;
        
    }
    
    // Vendors Dashboard methods (Ends); ********************************************************************************

    public function getAvailableBundles(){ 
        
        $vendor_bundles = $this->getBundles();
        $bundles = array();
        $all_bundles = Bundle::getBundles();
        
        foreach($all_bundles as $key => $single_bundle){
            foreach($vendor_bundles as $vendor_bundle){
                if($single_bundle->getWPID == $vendor_bundle->getWPID)
                    unset($all_bundles[$key]);
                
            }
        }
        $bundles = $all_bundles;
        
        return $bundles;
        
    } // This function is not like the other "getAvailable...". This one gets the bundles that the vendor can activate in his dashboard
    
    //
    public static function chooseVendor(){
        $vendor_ID = $_POST['vendor_ID'];
        //echo "Saving Vendor_ID(".$_POST['vendor_ID'].") to a variable vendor_ID \n ";
        //echo "And now the variable vendor_ID holds the value: " . $vendor_ID . "\n";
        
        $order = Order::OpenOrder();
        //echo "Opening current Order: \n";
        //var_dump($order);
        
        $order->setSeller($vendor_ID);
        //echo "\n Saving the vendor_ID(". $vendor_ID . ") to Order. \n";
        $order->Save();
        //echo "Saving the Order itself; \n";
        //var_dump($order);
        Order::DEBUG();
        
        $cart = Cart::OpenCart();
        $cart->setVendor($vendor_ID);
        $cart->save();
        die;
    }
    
    //********************************************************************************************
    // VNDR-CONFIG-HOURS
    public function updateDayStatus(){
        global $wpdb; 
        $errors = array(); // Creating an empty array to hold errors.
        $day = Secure::jsonDecode($_POST["day"]); // Decoding the JSON that was passed from JS.
        
        $table = VENDOR_HOURS_TABLE; // defining the table we work on here.
        $where = array(); // creating an empty where array to hold WHERE statements.
        $where["vendor_ID"] = $this->ID;
        $where["day"] = $day->ID;
        
        $data = array(); // Creating data array to hold the data to UPDATE.
        $data["day_status"] = $day->status;
        if(DB::isSetRow($table,$where)){ 
            if(!$wpdb->update($table,$data,$where)){ // If the update not successful...
                $errors["update"] = "UPDATE failed!"; // THEN creatr an error in the errors array.
            }    
        }else{ 
            $data["vendor_ID"] = $this->ID;
            $data["day"] = $day->ID;
            if(!$wpdb->insert($table,$data)){
                $error["insert"] = "INSERT failed";
            }
        }
        
        
        if(!empty($errors)){ // IF there are errors THEN send it back to js.
            $jsonErrors = json_encode($errors); // Encoding to a json string the errors.
            echo $jsonErrors;
        }
        
        die; //  end of ajax function.
    }
    public function updateClosingTime(){ 
        global $wpdb;
        $errors = array();
        $day = Secure::jsonDecode($_POST["day"]);
        
        $table = VENDOR_HOURS_TABLE;
        $where = array();
        $where["vendor_ID"] = $this->ID;
        $where["day"] = $day->ID;
        
        $data = array();
        $data["closing_time"] = $day->closingTime;
        
        if(DB::isSetRow($table,$where)){
            if(!$wpdb->update($table,$data,$where)){ // If the update not successful...
                $errors["update"] = "UPDATE failed!"; // THEN creatr an error in the errors array.
            }    
        }else{  
            $data["vendor_ID"] = $this->ID;
            $data["day"] = $day->ID;
            if(!$wpdb->insert($table,$data)){
                $error["insert"] = "INSERT failed";
            }
        }
        
        if(!empty($errors)){ // IF there are errors THEN send it back to js.
            $jsonErrors = json_encode($errors); // Encoding to a json string the errors.
            echo $jsonErrors;
        }
        
        die;
    } // AJAX
    
    public function getDayStatus($day){
        
        global $wpdb; // A WP class that holds all function to manipulate the DB
        
        $sql  = "SELECT * ";
        $sql .= "FROM " . VENDOR_HOURS_TABLE . " ";
        $sql .= "WHERE day = " .$day . " ";
        $sql .= "AND vendor_ID = " . $this->ID;
        
        $results = $wpdb->get_results($sql);
        
        if(empty($results)){
            return 0;
        }elseif($results[0]->day_status == 0){
            return 0;
        }else{
            return 1;
        }
        
    } 
    
    public function getClosingTime($day){
        global $wpdb; // A WP class that holds all function to manipulate the DB
        
        $sql  = "SELECT TIME_FORMAT(closing_time, '%H:%i') as closing_time ";
        $sql .= "FROM " . VENDOR_HOURS_TABLE . " ";
        $sql .= "WHERE day = " .$day . " ";
        $sql .= "AND vendor_ID = " . $this->ID;
        
        $results = $wpdb->get_results($sql);
        
        if(empty($results)){
            return "11:00";
        }else{
            return $results[0]->closing_time;
        }
        
    }
    //********************************************************************************************
    // VNDR-STOCK-PIZZA-PRICE
    public function updatePizzaPrice(){
        global $wpdb; // A WP class that holds all function to manipulate the DB
        
        $price = $_POST["price"];
        $table = VENDOR_PIZZA_PRICE_TABLE;
        $where = array();
        $where["vendor_ID"] = $this->ID;
        
        if(!DB::isSetRow($table,$where)){ 
            $data = array();
            $data["vendor_ID"] = $this->ID;
            $data["pizza_price"] = $price;
            
            $wpdb->insert($table,$data);
        }else{ 
            $data = array();
            $data["pizza_price"] = $price;
            $wpdb->update($table,$data,$where);
        }
        die;
        
    }
    public function getDays(){
        global $wpdb;
        
        $sql = "SELECT * ";
        $sql .= "FROM " . VENDOR_HOURS_TABLE . " ";
        $sql .= "WHERE vendor_ID = " .$this->ID;
        
        $results = $wpdb->get_select($sql);
        
        return $results;
    }
    
    //********************************************************************************************
    // VNDR-CONFIG-LOGO
    public function getLogo(){
        global $wpdb;
        
        $sql  = "SELECT * ";
        $sql .= "FROM " . VENDOR_LOGOS_TABLE . " ";
        $sql .= "WHERE vendor_ID = " . $this->ID;
        
        $results = $wpdb->get_results($sql);
        
        if(empty($results)){
            return DEFAULT_VENDOR_LOGO_PATH;
        }else{
            return $results[0]->path;
        }
    }
    public function setLogo(){ 
        global $wpdb;
        if(!$_FILES["error"]){
        
            $name = $_FILES["upload-img"]["name"];
            if(move_uploaded_file( $_FILES["upload-img"]["tmp_name"], "wp-content/themes/bringit/uploads/" . $_FILES['upload-img']['name'])){
                echo "<h2>Successfully Uploaded Images</h2>";
                $where = array();
                $where["vendor_ID"] = $this->ID;
                
                $data = array();
                $data["path"] = "/wp-content/themes/bringit/uploads/" . $_FILES['upload-img']['name'];
                $data["vendor_ID"] = $this->ID;
                
                $table = VENDOR_LOGOS_TABLE;
                $wpdb->delete($table,$where);
                $wpdb->insert($table,$data);
                
                header("location: /vndr-config-logo");
                return;
            }
            
        }
    }
    
    //********************************************************************************************
    // VNDR-STOCK-ADDSON-CLIENT
    
    public function addAddonClient(){ 
        
        $special_addon = new AddonClient();
        
        $special_addon->setName($_POST["name"]);
        
        $special_addon->Save();

        echo $special_addon->getID();
        
        die;
    } // AJAX

    public function updateAddsonClient(){
        
        $special_addson = Secure::JsonDecode($_POST["special-addson"]);
        var_dump($special_addson);
        foreach($special_addson as $temp_special_addon){
            $special_addon = new AddonClient($temp_special_addon->id);
            
            $special_addon->setPrice($temp_special_addon->price);
            $special_addon->setStock($temp_special_addon->in_stock);
            $special_addon->setBundles($temp_special_addon->in_bundles);
            
            $special_addon->Save();
        }
        
        die;
    }
    
    public function getSpecialAddson(){
        global $wpdb;
        
        $table = VENDOR_SPECIAL_ADDSON_TABLE;
        
        $sql  = "SELECT id ";
        $sql .= "FROM " . $table . " ";
        $sql .= "WHERE vendor_ID = " . $this->ID;
        
        $results = $wpdb->get_results($sql);
        
        $special_addson = array();
        
        if(!empty($results)){
            foreach($results as $result){
                $special_addon = new AddonClient($result->id);
                $special_addson[] = $special_addon;
            }
        }
        
        return $special_addson;
    }
    //********************************************************************************************
    // VNDR-STOCK-MORE-FOOD
    public function getAdditionalProducts(){
        $table = VENDOR_ADDITIONAL_PRODUCTS_TABLE;
        
        $sql = "SELECT id ";
        $sql .= "FROM " . $table . " ";
        $sql .= "WHERE vendor_ID = " . $this->ID . " ";
        
        $results = DB::Select($sql);
        
        $additional_products = array();
        
        if(!empty($results)){
            foreach($results as $result){
                $additional_product = new AdditionalProduct($result->id);
                
                $additional_products[] = $additional_product;
            }
        }
        return $additional_products;
    }
    public function addAdditionalProduct(){
        global $wpdb;
        
        $temp_additional_product = Secure::JsonDecode($_POST["additional_product"]);
        
        $additional_product = new AdditionalProduct();
        
        $additional_product->setName($temp_additional_product->name);
        $additional_product->setDescription($temp_additional_product->desc);
        $additional_product->setImage($temp_additional_product->fileName);
        
        $additional_product->Save();
        
        echo $additional_product->getID();
        
        die;
    } // AJAX
    public function updateAdditionalProducts(){
        global $wpdb;
        
        $temp_additional_products = Secure::JsonDecode($_POST["additional_products"]);
        
        
        foreach($temp_additional_products as $tap){
            $additional_product = new AdditionalProduct($tap->id);
            
            $additional_product->setStock($tap->in_stock);
            $additional_product->setPrice($tap->price);
            
            $additional_product->Save();
        }
        
        die;
    } // AJAX
    
}



?>
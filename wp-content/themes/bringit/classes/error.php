<?php
class Error{
    public $class_name;
    public $method_name;
    public $description;
    
    public function __construct($class_name,$method_name,$description){
        $this->class_name = $class_name;
        $this->method_name = $method_name;
        $this->description = $description;
    }
    
    
    //***************************************************************************/
    // Getters
    public function getClassName(){
        return $this->class_name;
    }
    
    public function getMethodName(){
        return $this->method_name;
    }
    
    public function getDescription(){
        return $this->description;
    }
    
    //***************************************************************************/
    // Setters
    
    public function setClassName($class_name){
        $this->class_name = $class_name;
    }
    
    public function setMethodName($method_name){
        $this->method_name = $method_name;
    }
    
    public function setDescription($description){
        $this->description = $description;
    }
}


?>
<?php
class Customer{
    
    private $ID = null;
    private $shipping_address_list = array();
    private $username = null;
    private $password = null;
    private $fname;
    private $lname;
    private $email;
    private $phone;
    private $orders = array();
    
    public function __construct($ID = "not set"){
        
        if($ID = "not set"){
            $temp_customer = wp_get_current_user();
            $ID = $temp_customer->ID;
            $this->ID = $ID;
        }
        $user_meta = get_user_meta($ID);
//        var_dump($user_meta);
        $this->username = $temp_customer->user_login;
        $this->password = $temp_customer->user_pass;
        $this->email = $temp_customer->user_email;
        $this->fname = $user_meta["first_name"][0];
        $this->lname = $user_meta["last_name"][0];
        $this->phone = get_field("phone","user_" . $ID);
        
        
        if( have_rows('shipping_addresses', 'user_' . $ID ) ){
            
            
            while(have_rows( 'shipping_addresses', 'user_' . $ID )){
                
                $address = new stdClass();
                
                the_row();
                
                if( get_sub_field('city') ){
                    $address->city = trim(get_sub_field('city'));    
                }
                if( get_sub_field('street') ){
                    $address->street = trim(get_sub_field('street'));    
                }
                if( get_sub_field('building_number') ){
                    $address->building_number = trim(get_sub_field('building_number'));    
                }
                if( get_sub_field('apartment_number') ){
                    $address->apartment_number = trim (get_sub_field('apartment_number'));    
                }
                
                $shipping_address_list[] = $address;
                
            }
            
            $this->shipping_address_list = $shipping_address_list;
            
            // Getting Order list from DB
            
            $sql = "SELECT * ";
            $sql .= "FROM " . BRINGIT_ORDERS . " ";
            $sql .= "WHERE customer_ID = " . $this->ID;
            
            $results = DB::Select($sql);
            
            $orders = array();
            
            if(!empty($results)){
                foreach($results as $result){
                    $orders[] = new Order($result->id);
                }
            }
            
            $this->orders = $orders;
            
            //***********************************************************************************
            //$this->username = User::getUsernameByID($ID); //TODO
            //$this->password = User::getPasswordByID($ID); //TODO
            //$this->orders = Order::getOrderListByID($ID); //TODO
            
        }
        
    }
    // Getters
    public function getID(){
        return $this->ID;
    }
    public function getShippingAddressList(){
        return $this->shipping_address_list;
    }
    public function getFirstName(){
        return $this->fname;
    }
    public function getLastName(){
        return $this->lname;
    }
    public function getUsername(){
        return $this->username;
    }
    public function getPassword(){
        return $this->password;
    }
    public function getEmail(){
        return $this->email;
    }
    public function getOrders(){
        return $this->orders;
    }
    public function getPhone(){
        return $this->phone;
    }
    // Setters
    public function setUsername($username){ 
        $this->username = $username;
    }
    public function setPassword($password){
        $this->password = $password;
    }
    
    public static function getCurrent(){ // get currently logged in Customer
        
        $temp_customer = wp_get_current_user();
        
        $customer = new Customer($temp_customer->ID);

        return $customer;
        
    }
    
    public static function getUserAddressList($customer_ID){ 
        $addresses = array();
        while(have_rows( 'shipping_addresses', 'user_' . $customer_ID )){
                
                $address = new stdClass();
                
                the_row();
                
                if( get_sub_field('city') ){
                    $address->city = trim(get_sub_field('city'));    
                }
                if( get_sub_field('street') ){
                    $address->street = trim(get_sub_field('street'));    
                }
                if( get_sub_field('building_number') ){
                    $address->building_number = trim(get_sub_field('building_number'));    
                }
                if( get_sub_field('apartment_number') ){
                    $address->apartment_number = trim (get_sub_field('apartment_number'));    
                }
                
                $addresses[] = $address;
                
            }
        return $addresses;
    }
    public function addAddress(){
           
        $address = new stdClass();
        $value = array();
        $field_key = "field_5588fb2ec0df7";
        echo "check";
        if(isset($_POST['id'])){
            $customer_id = $_POST['id'];   
        }else{ 
            echo 'Customer ID is not set.';
            return;
        }

        $post_id = "user_" . $customer_id;

        $value = get_field($field_key, "user_" . $customer_id);

        if( isset($_POST['fname']) 
           && isset($_POST['lname']) 
           && isset($_POST['city']) 
           && isset($_POST['street']) 
           && isset($_POST['apartment_number']) 
           && isset($_POST['building_number']) 
           && isset($_POST['level']) 
           && isset($_POST['entrance']) 
           && isset($_POST['phone']) 
           && isset($_POST['terms'])){

            $address->fname                 = $_POST['fname'];
            $address->lname                 = $_POST['lname'];
            $address->city                  = $_POST['city'];
            $address->street                = $_POST['street'];
            $address->apartment_number      = $_POST['apartment_number'];
            $address->building_number       = $_POST['building_number'];
            $address->level                 = $_POST['level'];
            $address->entrance              = $_POST['entrance'];
            $address->phone                 = $_POST['phone'];
            $address->terms                 = $_POST['terms'];

            if( !$address->terms ){
                echo 'Not all the required fields are set';
                die;
            }


            if( $address->fname && $address->lname && $address->city && $address->street && $address->building_number && $address->phone ){

                $value[] = array(
                    "first_name" => $address->fname,
                    "last_name" => $address->lname,
                    "city" => $address->city,
                    "street" => $address->street,
                    "apartment_number" => $address->apartment_number,
                    "building_number" => $address->building_number,
                    "phone" => $address->phone,
                    "level" => $address->level,
                    "entrance" => $address->entrance,               
                );

            }

            update_field( $field_key, $value, $post_id );
        }
        
    }
    public static function formAddShippingAddress($user_id){
        
        $args = array(
            "post_id" => 'user_' . $user_id,
            'fields' => array('field_5588fb2ec0df7'),
        );
        
        acf_form($args);
        
        return '';
        
    }
    
    //****************************************************************************//
    public function Update(){
        
        $customer = Secure::JsonDecode($_POST["customer"]);
        
        update_metadata("user",$this->ID,"first_name",$customer->fname); // First Name
        update_metadata("user",$this->ID,"last_name",$customer->lname); // Last Name
//        update_metadata("user",$this->ID,"user_email",$customer->email); // email
        wp_update_user( array( 'ID' => $this->ID, 'user_email' => $customer->email ) ); // email
        update_field( "field_5643456ba5534", $customer->phone, "user_".$this->ID ); // Phone number
        
        die;
    }
    
    
}

?>
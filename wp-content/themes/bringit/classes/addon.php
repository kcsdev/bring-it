<?php
include("addon-categories.php");

class Addon{ 
    
    private $ID = null;
    private $WPID = null;
    private $name = null;
    private $title = null;
    private $category_image = null;
    private $category = null;
    private $vendors = array();
    private $price = null;
    private $location = null;
    private $additional_payment_required = true;
    
    public function __construct($WPID = "not set"){ 
        if($WPID == "not set"){
            return false;
        }
        $this->WPID = $WPID;
        $this->ID = Secure::Hash($WPID);
        $temp_addon = get_post($WPID);
        if($temp_addon == NULL){
            return false;
        }
//        var_dump($temp_addon);
//        echo "<br><br><br><br><br><br><br><br>";
//        echo $WPID;
//        echo "<br><br><br><br><br><br><br><br>";
        $this->name = $temp_addon->post_name;
        $this->title = $temp_addon->post_title;
        
    }
    
    public static function getAll(){
    
        $args = array( 
            "post_type" => "addon",
            "status" => "publish",
            "posts_per_page" => -1
        );
        $temp_addons = get_posts($args);
        $addons = array();
        foreach($temp_addons as $temp_addon){
            $addon = new Addon($temp_addon->ID);
            $addons[] = $addon;
        }
        
        return $addons;
    }
    public static function getAddonsByCategory($category = 'not set'){
        if($category == 'not set'){
            return false;
        }
        
        $args = array(
            "post_type"         =>"addon",
            "status"            => "publish",
            "addon_cat"         => $category->slug,
            "posts_per_page"    => -1,
            "number"            => -1
        );

        $temp_addons = get_posts($args); 
        
        foreach($temp_addons as $temp_addon){ 
            
            $addon = new Addon($temp_addon->ID);
            $addon->setName($temp_addon->post_name);
            $addon->setTitle($temp_addon->post_title);
           // $addon->setCategoryImage( get_field("image","term_" . $category->term_id) );
            //$addon->setCategory($category->slug);
            
            $addons[] = $addon;
        }
        
        return $addons;
        
    }
    public static function getCategories(){
        $args = array();
        $categories = get_terms("addon_cat",$args);
        return $categories;
    }
    public function getLayer($part = "whole"){
        return false;
    }
    
    public function getPrice($ID = "not set"){ 
        return false;
    }
    public function getID(){
        return $this->ID;
    }
    public function getWPID(){
        return $this->WPID;
    }
    public function getName(){
        return $this->name;
    }
    public function getTitle(){
        return $this->title;    
    }
    public function getCategoryImage(){
        return $this->category_image;
    }
    public function getCategory(){
        return $this->category;
    }
    public function getVendors(){
        return false; // TODO
    }
    
    //Setters
    public function setName($name){
        $this->name = $name;
    }
    public function setTitle($title){
        $this->title = $title;
    }
    public function setCategoryImage($category_image){
        $this->category_image = $category_image;
    }
    public function setCategory($category){
        $this->category = $category;
    }
    public function setPrice($price){
        $this->price = $price;
    }
    public function setLocation($location){
        $this->location = $location;
    }
    public function setAdditionalPaymentRequired($boolean){
        $this->additional_payment_required = $boolean;
    }
    
    public function inStock(){
        global $wpdb;
        
        $vendor = new Vendor();
        
        $sql  = "SELECT * ";
        $sql .= "FROM " . VENDOR_ADDONS_TABLE . " "; 
        $sql .= "WHERE vendor_ID = " . $vendor->getID() . " ";
        $sql .= "AND addon_WPID = " . $this->WPID;
        
        $results = $wpdb->get_results($sql);
        
        if(empty($results)){
            return false;
        }else{ 
            return $results[0]->in_stock;
        }
    }
    
    public function inBundles(){
        global $wpdb;
        
        $vendor = new Vendor();
        
        $sql  = "SELECT * ";
        $sql .= "FROM " . VENDOR_ADDONS_TABLE . " ";
        $sql .= "WHERE vendor_ID = " . $vendor->getID() . " ";
        $sql .= "AND addon_WPID = " . $this->WPID;
        
        $results = $wpdb->get_results($sql);
        
        if(empty($results)){
            return false;
        }else{
            return $results[0]->in_bundles;
        }
    }
    
    public function getVendorsPrice(){ 
        
        global $wpdb;
        
        $vendor = new Vendor();
        
        $sql  = "SELECT * ";
        $sql .= "FROM " . VENDOR_ADDONS_TABLE . " ";
        $sql .= "WHERE vendor_ID = " . $vendor->getID() . " ";
        $sql .= "AND addon_WPID = " . $this->WPID;
        
        $results = $wpdb->get_results($sql);
        
        if(empty($results)){
            return "";
        }else{
            return $results[0]->vendor_price;
        }
        
    }
}
?>
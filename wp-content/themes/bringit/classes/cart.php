<?php 


function ajax_pb_addons_form_script() {
//    wp_enqueue_script( 'pb-addons-form', "/wp-content/themes/bringit/js/pb-addons-form.js", array('jquery'), '1.0', true );
    wp_enqueue_script( 'remove-from-cart', "/wp-content/themes/bringit/js/cart/remove-from-cart.js", array('jquery'), '1.0', true );
    wp_enqueue_script( 'add-to-cart', "/wp-content/themes/bringit/js/cart/add-to-cart.js", array('jquery'), '1.0', true );
} 
add_action( 'wp_enqueue_scripts', 'ajax_pb_addons_form_script' );

class Cart{
    
    private $bundles = array();
    private $pizzas = array();
    private $drinks = array();
    private $additional_products = array();
    private $addons = array();
    private $vendor;
    private $price;
    private $shipping_price;
    private $total_price;
    
    public function __construct(){
        
    }
//***********************************************************************************************************
    public function defineVendor($vendor_ID = null){
        if($vendor_ID == null){
            return false;
        }
        
        $vendor = new Vendor($vendor_ID);
        
//        $this->price = $this->getPrice($vendor_ID);;
//        $this->shipping_price = $this->getShippingPrice($vendor_ID);;
//        $this->total_price = $this->price + $this->shipping_price;
    }
    public function getTotalPrice(){ 
        return $this->total_price;
    }
    public function getShippingPrice(){
        return $this->shipping_price;
    }
    public function getTotalPriceByRegion($vendor,$city,$region,$shipping = false){ 
        $vendors_pizza_price = $vendor->getPizzaPriceByRegion($city,$region);
        $pizzas_cart_count = count($this->getPizzas());
        
        $pizzas_price = $vendors_pizza_price * $pizzas_cart_count;
        
        $addons_price = $vendor->getAddonsPrice();
        $drinks_price = $vendor->getDrinksPrice();
        $additional_products_price = $vendor->getAdditionalProductsPrice();
        $bundles_price = $vendor->getBundlesPrice();
        $shipping_price = 0;
        if($shipping){
            $shipping_price = $vendor->getShippingPriceByRegion($city,$region);
        }
        
        $total_price = $pizzas_price + $addons_price + $drinks_price + $additional_products_price + $bundles_price + $shipping_price;
        
        return $total_price;
    }

//***********************************************************************************************************
    public function getBundles(){
        return $this->bundles;
    }
    public function getPizzas(){
        return $this->pizzas;
    }
    public function getDrinks(){
        return $this->drinks;
    }
    public function getAddons(){
        return $this->addons;
    }
    public function getAdditionalProducts(){
        return $this->additional_products;
    }
    public function getVendor(){
        return $this->vendor;
    }
    public function getAllDrinks(){
        $drinks = array();
        $unbundled_drinks = $this->getDrinks();
        $bundled_drinks = array();
        $bundles = $this->getBundles();
        
        foreach($bundles as $bundle){
            $temp_drinks = $bundle->getDrinks();
            $bundled_drinks = array_merge($bundled_drinks,$temp_drinks);
        }
        
        $drinks = array_merge($drinks,$unbundled_drinks);
        $drinks = array_merge($drinks,$bundled_drinks);
        
        return $drinks;
    }
//***********************************************************************************************************
    public function setVendor($vendor_ID){
        $this->vendor = $vendor_ID;
    }
    public function setTotalPrice($vendor,$city,$region,$shipping = false){
        $this->total_price = $this->getTotalPriceByRegion($vendor,$city,$region,$shipping);
    }
//***********************************************************************************************************
// Add Products to Cart
    
    public function addPizza(){  // used as an ajax callback function
        
        $errors = array();
        
        // This function adds a pizza to cart
        $addons = array(); // an array for holding Addons
        $count_addons_to_include = 0;
        $pizza = new Pizza(); // Creation of a new Pizza to add to the cart if the it passes the validation
        
        if ( isset($_POST["addson"]) ){ // A simple check if there were Addons added to the pizza
            
            $temp_addons = Secure::JsonDecode($_POST['addson']); // Decoding the json String passed by the js
        
            foreach( $temp_addons as $temp_addon ){ // Going through the Addons we recieved
                $addon = new Addon($temp_addon->WPID); // Creating a new addon based on the information passed
                $addon->setLocation($temp_addon->location); // Setting the location of an addon on the pizza
                $addons[] = $addon; // adding the addon to the array
            }
            
            $count_addons_to_include = count($addons);
            
            foreach($addons as $addon){ // going through the addons
                $pizza->addAddon($addon); // adding the addon to the pizza
            }
            
        }
        
        $pizzas = $this->pizzas; // getting the pizzas that user want to purchase
        $pizzas[] = $pizza; //  adding the pizza to the list
        $this->pizzas = $pizzas; // saving the new list to the cart

        $this->save(); 
        die();
            
    }
    
    // Add a Pizza to a Bundle in the Cart
    public function addPizzaToBundle(){
        $bundle = Bundle::Open();
        
        $pizza = new Pizza();
        $addson = array();
        
        $temp_addson = Secure::JsonDecode($_POST["addson"]);
        
        foreach( $temp_addson as $temp_addon ){  // Going through rge Addson we recieved.
            $addon = new Addon($temp_addon->WPID); // Creating a new Addon based on the WPID passed.
            $addon->setLocation($temp_addon->location); // Setting the location of an addon on the pizza.
            
            $addson[] = $addon; // Addin the addon to the array.
        }
        
        // NOTE Stopped Here... Check how much addson are added and how much are already in the bundle
        
        
        
        $bundle->addPizza();
    }
    
    // Add Drink to Cart
    public function addDrink(){ // used as an ajax callback function
        
        if(!isset($_POST['id']) || !isset($_POST['name']) || !isset($_POST['WPID'])){
            return false;
        }
        $drink = new Drink($_POST['WPID'],null,$_POST['name']);
        $isAddedToBundle = true;
        $bundles = $this->getBundles();
        if(empty($bundles)){
            $this->drinks[] = $drink;
        }else{
            foreach($bundles as $bundle){
                if($bundle->isEnoughSpaceForDrinks()){
                    $bundle->addDrink($drink);
                    echo "enough space";
                    break;
                }else if(!$bundle->isEnoughSpaceForDrinks()){
                    $isAddedToBundle = false;
                }
            }
            if(!$isAddedToBundle){
                $this->drinks[] = $drink;
            }
        }
        $this->save();
        die;
    }
    
    // Add Additional Product to Cart
    public function addAdditionalProduct(){ //used as an ajax callback function
        
        $this->additional_products[] = new AdditionalProduct($_POST['additionalProductId']);
        $this->save();
        die; // Death is just a semicolon in your life;
    }
    
    //Add Bundle to Cart
    public function addBundle(){ 
        if(isset($_SESSION['Bundle'])){
            $bundle = unserialize($_SESSION["Bundle"]);
            unset($_SESSION['Bundle']);
        }else{
            $tempBundle = Secure::JsonDecode($_POST['bundle']);
            $bundle = new Bundle($tempBundle->WPID,$tempBundle->ID);
        }
        
        $bundles = $this->bundles;
        
        $bundles[] = $bundle;
        $this->bundles = $bundles;
        
        $this->save();
        
        // This line of code Starts a flow of a bundle so the user won't have the option to add more products that the bundle offers before he is done building his bundle
        
        die; // The fear of death follows from the fear of life. A man who lives fully is prepared to die at any time
    }
    
//***********************************************************************************************************
    // Removing Products from the Cart
    
    public function removeBundle(){ 
        
        $RAWjsonBundle = $_POST["bundle"];
        $jsonBundle = preg_replace('/\\\"/',"\"",$RAWjsonBundle);
        $tempBundle = json_decode($jsonBundle);
        var_dump($jsonBundle);
        
        $bundleToRemove = $tempBundle->ID;
        //var_dump($bundleToRemove);
        $bundles = $this->bundles;
        $removed = false;
        foreach($bundles as $key => $bundle){
            if( $bundle->getID() == $bundleToRemove ){
                unset($this->bundles[$key]);
                $removed = true;
            }
        }
        if(!$removed){
            Bundle::Delete();
        }
        $this->save();
        die;
    }
    public function removePizza($id = "not set"){ 
        
        if($id == "not set")
            return false;
        if(isset($_POST['id']))
            $id = $_POST['id'];
        foreach($this->pizzas as $key => $pizza){
            if( $pizza->getID() == $id ){
                unset($this->pizzas[$key]);
            }
        }
        echo $_POST['id'];
        $this->save();
    }
    public function removeDrink($WPID = "not set"){ 
        
        if($WPID == "not set"){
            echo "WPID is not set.";die;
        }
        if(isset($_POST['WPID'])){ 
            echo "WPID is set: " . $_POST["WPID"];
            $WPID = trim($_POST['WPID']); // Saving the passed variable to a normal variable.
        }else{
            echo "false"; die;
        }
        
        foreach($this->drinks as $key => $drink){
            if( $drink->getWPID() == trim($WPID) ){ 
                echo "true";
                unset($this->drinks[$key]);
                break;
            }else{
                echo "false"; die;
            }
        }
        $this->save();
        die;
    }
    public function removeAdditionalProduct($id){ 
        
        if($id == "not set")
            return false;
        if(isset($_POST['id'])){
            $id = trim($_POST['id']);
        }else{
            return false;
        }
        
        
        foreach($this->additional_products as $key => $additional_product){
            if( $additional_product->getID() == $id ){
                unset($this->additional_products[$key]);
            }
        }
        $this->save();
        die;
    }
    public function removeAddon($pizza_ID = "not set", $addon_ID = "not set"){ 
        if(!isset($_POST["id"]) || !isset($_POST["pizza_id"])){
            return false;
        }
        
        $pizzas = $this->pizzas;
        
        foreach($pizzas as $pizza){
            if($_POST['pizza_id'] == $pizza->getID()){
                $pizza->removeAddon($_POST['id']);
            }
        }
        $this->save();
        die;
    } // used as a callback function
    
//*********************Bundle Related************************************************************************
    public function removePizzaFromBundle(){
        if(!isset($_POST['bundle_ID']) || !isset($_POST['pizza_ID'])){
            die("Lack of data");
        }
        
        $bundles = $this->bundles;
        foreach($bundles as $bundle){
            if($bundle->getID() == $_POST['bundle_ID']){
                $pizzas = $bundle->getIncludedPizzas();
                
                foreach($pizzas as $key => $pizza){ 
                    if($pizza->getID() == $_POST['pizza_ID']){
                        $bundle->removePizza($pizza->getID());
                        echo "found the pizza to remove";
                    }else{
                        echo "no pizza found";
                    }
                }
            }
        }
        
        $this->save();
        die;
    } // used as a callback function  
    public function removeDrinkFromBundle(){
        if( !isset($_POST["bundle_ID"]) || !isset($_POST['drink_WPID']) ){
            die("Missing Parameter");
        }
        
        $bundles = $this->bundles;
        
        foreach($bundles as $bundle){
            
            if($bundle->getID() == $_POST["bundle_ID"]){
                
                $drinks = $bundle->getIncludedDrinks();
                
                foreach( $drinks as $key => $drink ){
                    if( $drink->getWPID() == $_POST["drink_WPID"]){
                        $bundle->removeDrink($drink->getWPID());
                        echo "Found the Drink to remove";
                        break;
                    }else{
                        echo "No Drink Found";
                    }
                }
                
            }
            
        }
        $this->save();
        die;
    }
//***********************************************************************************************************
    public function getDrinkStackByWPID($WPID){
        $stack = new stdClass();
        $stack->drink = new Drink($WPID);
        $stack->quantity = 0;
        
        foreach($this->drinks as $drink){
            if($drink->getWPID() == $WPID)
                $stack->quantity++;
        }
        
        return $stack;
    }
    public function filterStacks($stacks){        
        $unique_stacks = array_map("unserialize", array_unique(array_map("serialize", $stacks)));        
        return $unique_stacks;
    }
    public function getAllStacks(){
        $drinks = $this->getDrinks();  // Get Drinks from cart;
        $temp_stacks = array();
        foreach($drinks as $drink){
            $temp_stack = $this->getDrinkStackByWPID($drink->getWPID());
            $temp_stacks[] = $temp_stack;
        }
        $stacks = $this->filterStacks($temp_stacks);
        
        return $stacks;
    }
//***********************************************************************************************************
       
    public function checkBundlesForPizzas(){
        $bundles = $this->getBundles();
        
        $result = new stdClass(); // This stdClass will be returned;
        $result->errors = array(); // This is an array of errors that might occure
        $result->bundleReadyToGo = true; // This a boolean that determines if the user can continue to drinks or not
        
        foreach($bundles as $bundle){
            if(count($bundle->getIncludedPizzas()) < $bundle->getPizzas()){
                $result->bundleReadyToGo = false;
            }elseif($bundle->getIncludedAddons() < $bundle->getAddons()){
                $result->bundleReadyToGo = false;
                $result->error['addons'] = true;
                $result->addons = $bundle->getAddons();
                $result->includedAddons = $bundle->getIncludedAddons();
            }    
        }
        
        $json_result = json_encode($result);
        echo $json_result;
        die;
    }
    
    public function changeDrinks(){
        
        $this->drinks = array();
        $temp_drinks = Secure::JsonDecode($_POST["drinks"]);
        
        $drinks = array();
        var_dump($temp_drinks);
        foreach($temp_drinks as $temp_drink){
            for($i=0; $i<$temp_drink->quantity; $i++){ 
                echo $i;
                $drink = new Drink($temp_drink->WPID);
                $drinks[] = $drink;
            }
        }
        $this->drinks = $drinks;
        $this->save();
        die;
    }
    
//***********************************************************************************************************
    public function emptyCart(){
        $this->bundles = array();
        $this->pizzas = array();
        $this->drinks = array();
        $this->additional_products = array();
        $this->save();
    }
    public static function OpenCart(){
        $cart = unserialize($_SESSION["Cart"]);
        return $cart;
    }
    public function savePaymentTypeToOrder(){ 
        $payment_type = $_POST["payment_type"];
        
        $order = Order::OpenOrder();
        
        $order->setPaymentType($payment_type);
        $order->Save();
        die;
    } // Works like it sounds.
    public function saveCartToOrder(){ 
        echo "Saving Cart to Order...";
        
        $order = Order::OpenOrder();
        $customer = new Customer();
        
        $order->setPizzasFromCart();
        $order->setDrinksFromCart();
        $order->setAdditionalProductsFromCart();
        $order->setBundlesFromCart();
        $order->setOrderPriceFromCart();
        $order->setOrderDate();
        $order->setOrderStatus("1");
        $order->setCustomerID($customer->getID());
        
        $order->Save();
        
        $order->DBSAVE();
        
        die;
    }
    public function save(){
        $_SESSION['Cart'] = serialize($this);
    }
    public static function Create(){
        if( !isset($_SESSION['Cart']) ){
            $cart = new Cart();
            $cart->save();
        }else{
            $cart = Cart::OpenCart();
            return $cart;
        }
    }
}


?>
<?php
/**
Template Name: Purchase Address List page
 */
$customer = new Customer(); 
$order = new Order($_REQUEST["order_id"]);
get_header('blank'); ?>
<?php //var_dump($customer); ?>
    <div class="bi-section">
        <a class = "close-window" onclick="parent.closeMe();"></a>
        <div class="bi-container addr-list clearfix">

            <p class="pizzeria-name">הזמנה מס' <?php echo $order->getID(); ?></p>

            <div class="list-title"></div>

            <table class = "order-list-table">
                <tr>
                    <th>מספר הזמנה</th>
                    <th>תאריך</th>
                    <th>שם</th>
                    <th>כתובת</th>
                    <th>סכום</th>
                </tr>
                
                <tr>
                    <td><?php echo $order->getID(); ?></td>
                    <td><?php echo $order->getOrderDate(); ?></td>
                    <td><?php echo $order->getFirstName() . " " . $order->getLastName(); ?></td>
                    <td><?php echo $order->getShippingAddress(); ?></td>
                    <td><?php echo $order->getOrderCost(); ?></td>
                    
                </tr>
                
            </table>

            
            <div class="shopping-cart order-details-div">
                <div id="shoppingCartContent"></div>
            </div>
        </div>
    </div>

<script type="text/javascript">
    jQuery(document).ready(function($){
        $("#shoppingCartContent").load("/pv-order-details-content?order_id=<?php echo $order->getID(); ?>",function(){});
    });
</script>
<?php get_footer('blank'); ?>
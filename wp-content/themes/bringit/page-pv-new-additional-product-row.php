<?php
$temp_additional_product = new AdditionalProduct($_GET["id"]);
?>
<tr class = "additional-product-row" additional-product-id = "<?php echo $temp_additional_product->getID(); ?>" >
    <td class="product-name image-holder">
        <?php echo $temp_additional_product->getName(); ?>
        <img src="<?php echo $temp_additional_product->getImage(); ?>" alt="" />
    </td>
    <td class="align-center">
        <?php if($temp_additional_product->inStock()): ?>
            <?php $checked = "checked"; ?>
        <?php else: ?>
            <?php $checked = ""; ?>
        <?php endif; ?>
        <input type="checkbox" 
               name="hotPeppers" 
               id="in_stock_<?php echo $temp_additional_product->getID(); ?>" 
               <?php echo $checked; ?> />
    </td>
    <td class="align-center">
        <input type="text" 
               name="hot_peppers" 
               id="price_<?php echo $temp_additional_product->getID(); ?>" 
               maxlength="5" 
               value = "<?php echo $temp_additional_product->getPrice(); ?>" />
        &#8362;
    </td>
</tr>

<?php
/**
Template Name: Purchase Address Form page
 */
get_header('blank'); ?>
<?php $customer = Customer::getCurrent(); ?>
<div class="bi-section">
    <a class = "close-window" onclick="parent.closeMe();"></a>
    <div class="bi-container clearfix">
        
        <p class="pizzeria-name">הוספת כתובת</p>

        <div class="addr-form-pane">
            <div class="form-title"></div>

            <div class="form-wrapper">
                <form onsubmit="validateAddress(event);">
                    <?php the_loading_animation(); ?>
                    <input type="hidden" value = "<?php echo $customer->getID(); ?>" name="id" /> <?php // id ?>
                    <label class="clearfix label-fname">
                        שם פרטי
                        <span class="mandatory-field">*</span>
                        <input type="text" name="fname" /> <?php // fname ?>
                    </label>

                    <label class="clearfix label-lname">
                        שם משפחה
                        <span class="mandatory-field">*</span>
                        <input type="text" name="lname" /> <?php // lname ?>
                    </label>

                    <label class="clearfix label-address">
                        עיר
                        <span class="mandatory-field">*</span>
                        <input type="text" name="city" /> <?php // city ?>
                    </label>
                    
                    <label class="clearfix label-address">
                        רחוב
                        <span class="mandatory-field">*</span>
                        <input type="text" name="street" /> <?php // street ?>
                    </label>
                    
                    <label class="clearfix small label-entry">
                        מס' בית
                        <input type="text" name="buildingNumber" /> <?php // buildingNumber ?>
                    </label>
                    
                    <label class="clearfix small label-entry">
                        דירה
                        <input type="text" name="apartmentNumber" /> <?php // apartmentNumber ?>
                    </label>
                    <label class="clearfix small label-store">
                        קומה
                        <input type="text" name="level" /> <?php // level ?>
                    </label>

                    <label class="clearfix small last label-appartment">
                        כניסה
                        <input type="text" name="entrance" /> <?php // entrance ?>
                    </label>

                    <label class="clearfix label-phone">
                        טלפון
                        <span class="mandatory-field">*</span>
                        <input type="text" name="phone"  /> <?php // phone ?>
                    </label>

                    <label class="clearfix no-border label-pwd">
                        <input type="checkbox" name="terms" /> <?php // terms ?>
                        <a href="javascript:void(0);" class="show-terms">מסכים לתנאי השימוש</a>
                    </label>
                    <div class="terms">
                        <p>המילה pizza מקורה באיטלקית, ומשמעה "עוגה" או "פשטידה". תיעוד למילה pizza ניתן למצוא עוד בלטינית מאוחרת, ובה משמעה "לחם שטוח". לא ברור כיצד הגיעה המילה ללטינית; על פי ארנסט קליין, המילה נגזרה מהמילה היוונית pitta, שמו של מאפה יווני שמקורו במילה היוונית העתיקה שמשמעה "פיח"‏[1] (גם המילה העברית "פיתה" הגיעה מאותו המקור). סברה אחרת גורסת כי מקור המילה גרמאני, וכי היא באה מהמילה bezzo שמשמעה "נגיסה"; על פי סברה זו המילה היוונית אולי הגיעה דווקא מהמילה האיטלקית ולא להפך.‏[2] על פי המילון האטימולוגי הוותיק של פיאניג'אני, מקור המילה הוא בצורה של המילה pinza, מילת דיאלקט שהיא צורת בינוני-פעול של הפועל "ללחוץ" או "לרמוס".‏[3] במשמעותה המודרנית תועדה המילה באנגלית לראשונה בשנת 1825.</p>
                        <p>הפיצה האיטלקית המסורתית מבוססת על בצק שמרים, על בסיס קמח דורום, שמרים ומים בלבד. את הבצק מרדדים ביד (ללא עזרת מערוך) לשכבת בצק עגולה ודקה, דמוית פיתה עיראקית או דרוזית, מוסיפים את הרטבים והתוספות השונות ומכניסים לתנור אפייה בחום גבוה מאוד (לפחות 400°C) לפרק זמן של כ-2 דקות, כך שמתקבל בסיס דק ופריך.</p>
                        <p>פיצה מרגריטה היא הפיצה האיטלקית המוכרת ביותר. פיצה זו מבוססת על רוטב מעגבניות מבושלות מרוסקות ומסוננות הנקרא באיטלקית pasata. מעל הרוטב מונחת גבינת מוצרלה וכן מונחים עלי בזיליקום. בשולי הפיצה נמשח שמן זית איכותי.</p>
                    </div>

                    <button type="submit"></button>
                </form>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
    
    //jQuery(document).ready(function(){
        
//        var name = jQuery("input[name=fname]").val();
//        var id = jQuery("input[name=id]").val();
//        console.log(name);    
//        console.log(id);
    
    
        function validateAddress(e){ 

            e.preventDefault();
            //jQuery(".login-pane.login label").removeClass("error");
            jQuery("#the_loading_animation").show();
            var formData = { 
                "id":               jQuery("input[name=id]").val(),
                "fname":            jQuery("input[name=fname]").val(),
                "lname":            jQuery("input[name=lname]").val(),
                "city":             jQuery("input[name=city]").val(),
                "street":           jQuery("input[name=street]").val(),
                "apartmentNumber":  jQuery("input[name=apartmentNumber]").val(),
                "buildingNumber":   jQuery("input[name=buildingNumber]").val(),
                "level":            jQuery("input[name=level]").val(),
                "entrance":         jQuery("input[name=entrance]").val(),
                "phone":            jQuery("input[name=phone]").val(),
                "terms":            jQuery("input[name=terms]").val(),
//                "id": jQuery("iframe").contents().find("input[name=id]").val(),
//                "fname": jQuery("iframe").contents().find("input[name=fname]").val(),
//                "lname": jQuery("iframe").contents().find("input[name=lname]").val(),
//                "city": jQuery("iframe").contents().find("input[name=city]").val(),
//                "street": jQuery("iframe").contents().find("input[name=street]").val(),
//                "apartmentNumber": jQuery("iframe").contents().find("input[name=apartmentNumber]").val(),
//                "buildingNumber": jQuery("iframe").contents().find("input[name=buildingNumber]").val(),
//                "level": jQuery("iframe").contents().find("input[name=level]").val(),
//                "entrance": jQuery("iframe").contents().find("input[name=entrance]").val(),
//                "phone": jQuery("iframe").contents().find("input[name=phone]").val(),
//                "terms": jQuery("iframe").contents().find("input[name=terms]").val()
            };
            console.log(formData);
            jQuery.ajax({ 
                type: 'POST',
                url: "/wp-admin/admin-ajax.php/",
                data: "action=add_shipping_address_row_to_customer_action&fname=" +formData.fname+ "&lname=" +formData.lname+ "&city=" +formData.city+ "&street=" +formData.street+ "&apartment_number=" +formData.apartmentNumber+ "&building_number=" +formData.buildingNumber+ "&level=" +formData.level+"&entrance=" +formData.entrance+ "&phone=" +formData.phone+ "&terms=" +formData.terms+ "&id=" +formData.id,
                success: function (data, status, xhr) { 
                    window.location.href = "/purchase-addr-list";
                    console.log('AJAX request succeeded on page-purchase-addr-form');
                    console.log(data);
                },
                error: function (xhr, status, error) { 
                    console.log('Connection error')
                },
                complete: function (xhr, status) { 
                    jQuery("#the_loading_animation").show();
                    console.log('AJAX request complete on page-purchase-addr-form');
                    location = '/purchase-addr-list/';
                },
                dataType: 'text',
                cache: false
            });
            return false;
        }
//    });
    jQuery(document).ready(function($){
       $(".show-terms").bind("click", function(){
           $(".terms").toggle();
       })
    });
</script>

<?php get_footer('blank'); ?>
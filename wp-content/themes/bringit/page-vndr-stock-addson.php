<?php
get_header('vendor');
?>
    <div class="h-section down-space">
        <ul class="breadcrumbs">
            <li><a>מלאי</a></li>
            <li class="current"><a>תוספות</a></li>
        </ul>
    </div>

    <div class="h-section content-pane down-space clearfix">
        <form onsubmit="return false;">
            <table class="stock-list down-space">
                <colgroup>
                    <col style="width:40%;" />
                    <col style="width:20%;" />
                    <col style="width:20%;" />
                    <col style="width:20%;" />
                </colgroup>
                <thead>
                    <tr>
                        <th>
                            שם
                        </th>
                        <th>
                            האם זמין לרכישה באתר?
                        </th>
                        <th>
                            האם נכלל במבצעים?
                        </th>
                        <th>
                            מחיר
                        </th>
                    </tr>
                </thead>
                <?php $addons = Addon::getAll(); ?>
                <?php foreach($addons as $addon): ?>
                <?php if($addon->inStock()): ?>
                    <?php $disabled = ""; ?>
                    <?php $in_stock_checked = "checked"; ?>
                <?php else: ?>
                    <?php $disabled = "disabled"; ?>
                    <?php $in_stock_checked = ""; ?>
                <?php endif; ?>
                <?php if($addon->inBundles()): ?>
                    <?php $in_bundles_checked = "checked"; ?>
                <?php else: ?>
                    <?php $in_bundles_checked = ""; ?>
                <?php endif; ?>
                <?php $price = $addon->getVendorsPrice(); ?>
                <tr>
                    <td class="product-name">
                        <?php echo $addon->getTitle(); ?>
                    </td>
                    <td class="align-center">
                        <input type="checkbox" 
                               name="<?php echo $addon->getWPID(); ?>-stock-status" 
                               id="<?php echo $addon->getWPID(); ?>-stock-status" 
                               class = "vndr-stock-addson-status"
                               <?php echo $disabled . " "; ?> 
                               <?php echo $in_stock_checked; ?> />
                    </td>
                    <td class="align-center">
                        <input type="checkbox" 
                               name="<?php echo $addon->getWPID(); ?>-in-bundle" 
                               id="<?php echo $addon->getWPID(); ?>-in-bundle" 
                               class = "vndr-stock-addson-in-bundle"
                               <?php echo $disabled . " "; ?> 
                               <?php echo $in_bundles_checked; ?> />
                    </td>
                    <td class="align-center">
                        <input type="text" 
                               addon-WPID = "<?php echo $addon->getWPID(); ?>"
                               value = '<?php echo $price; ?>'
                               name="<?php echo $addon->getWPID(); ?>-price" 
                               id="<?php echo $addon->getWPID(); ?>-price" 
                               class = "vndr-addon-price"
                               maxlength="5" />
                        &#8362;
                    </td>
                </tr>
                <?php endforeach; ?>
                
            </table>
            <div class="clearfix">
                <button type="submit" id = "vndr-stock-addson-save" class="list-save">שמירה</button>
                <img src = "<?php echo LOADING_GIF; ?>" class = "save-area-animation-absolute" id = "update-addson-button" />
            </div>
        </form>
    </div>

<script type="text/javascript">
    jQuery(document).ready(function($){

    });
</script>
<?php
get_footer('vendor');
?>
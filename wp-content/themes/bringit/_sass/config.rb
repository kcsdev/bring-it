# Require any additional compass plugins here.

#Folder settings
relative_assets = true      #because we're not working from the root
css_dir = "../css"          #where the CSS will saved
sass_dir = "sources"           #where our .scss files are
images_dir = "./images"    #the folder with your images

# output_style = :expanded or :nested or :compact or :compressed
output_style = :compact
#output_style = :expanded
#output_style = :compressed

# To disable debugging comments that display the original location of your selectors. Uncomment:
line_comments = false

# Obviously
preferred_syntax = :scss
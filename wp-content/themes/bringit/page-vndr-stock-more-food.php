<?php
get_header('vendor');
$additional_products = $vendor->getAdditionalProducts();
?>
    <div class="h-section down-space">
        <ul class="breadcrumbs">
            <li><a>מלאי</a></li>
            <li class="current"><a>אוכל נוסף</a></li>
        </ul>
    </div>

    <div class="h-section content-pane down-space">
<!--        <form method="post" onsubmit="return false;">-->
            <div class="inner-block down-space">
                <p>הוספת פריט</p>
                <div>
                    <input type="text" name="newClientAddOn" id="newAdditionalProductName" class="top-form-field" style="width:150px;" placeholder="שם פריט" />
                    &nbsp;
                    <input type="text" name="newClientAddOnDescr" id="newAdditionalProductDescription" class="top-form-field" style="width:150px;" placeholder="תתיאור פריט" />
                    &nbsp;
                    
                    &nbsp;
                    <!--<input type="file" name="newClientAddOn" id="newClientAddOn" />-->
                    <button type="submit" id = "add-additional-product-button" class="top-form-button">הוסף</button>
                    <img src = "<?php echo LOADING_GIF; ?>" id = "add-new-additional-product-loading-animation" class = "save-area-animation" />
                    <button type="reset" class="top-form-button">אפס</button>
                </div>
                
            </div>
        <form class = "dropzone" action="/ajax-upload-image"></form>
            <table class="stock-list down-space">
                <colgroup>
                    <col style="width:50%;" />
                    <col style="width:25%;" />
                    <col style="width:25%;" />
                </colgroup>
                <thead>
                <tr>
                    <th>
                        שם
                    </th>
                    <th>
                        האם זמין לרכישה באתר?
                    </th>
                    <th>
                        מחיר
                    </th>
                </tr>
                </thead>
                <?php foreach($additional_products as $additional_product): ?>
                <tr class = "additional-product-row" additional-product-id = "<?php echo $additional_product->getID(); ?>">
                    <td class="product-name image-holder">
                        <?php echo $additional_product->getName(); ?>
                        <img src="<?php echo $additional_product->getImage(); ?>" alt="" />
                    </td>
                    <td class="align-center">
                        <?php if($additional_product->inStock()): ?>
                            <?php $checked = "checked"; ?>
                        <?php else: ?>
                            <?php $checked = ""; ?>
                        <?php endif; ?>
                        <input type="checkbox" name="hotPeppers" id="in_stock_<?php echo $additional_product->getID(); ?>" <?php echo $checked; ?> />
                    </td>
                    <td class="align-center">
                        <input type="text" name="hot_peppers" id="price_<?php echo $additional_product->getID(); ?>" maxlength="5" value = "<?php echo $additional_product->getPrice(); ?>" />
                        &#8362;
                    </td>
                </tr>
                
                <?php endforeach; ?>
            </table>
            <div class="clearfix">
                <button type="submit" id = "list-save-additional-product" class="list-save">שמירה</button>
                <img src = "<?php echo LOADING_GIF ?>" id = "update-additional-product-button" class = "save-area-animation-absolute" />
            </div>
<!--        </form>-->
    </div>
<script type="text/javascript">
    jQuery(document).ready(function($){
    });
</script>
<?php
get_footer('vendor');
?>
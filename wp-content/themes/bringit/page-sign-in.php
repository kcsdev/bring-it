<?php
/**
Template Name: Sign In page
 */
get_header(); ?>

<div class="bi-section">
    <div class="bi-container clearfix">

        <div class="login-pane login">
            <p class="title">לקוח קיים</p>
            <div class="form-wrapper">
                <form onsubmit="return validateSignIn(this);">
                    <label class="clearfix label-uname">
                        שם המשתמש:
                        <input type="text" name="uname" />
                    </label>
                    <p class="field-info">כתובת הדוא&quot;ל שלך</p>

                    <label class="clearfix label-pwd">
                        סיסמה:
                        <input type="password" name="pwd" />
                    </label>


                    <label class="no-border clearfix">
                        <button type="submit"></button>
                    </label>
                </form>
            </div>
        </div>

        <div class="login-pane register">
            <p class="title">לקוח חדש</p>
            <div class="form-wrapper">
                <form onsubmit="return validateSignUp(this);">
                    <label class="clearfix label-uname">
                        כתובת הדוא&quot;ל:
                        <span class="mandatory-field">*</span>
                        <input type="email" name="uname" />
                    </label>
                    <p class="field-info">כתובת הדוא&quot;ל שלך תשמש כשם משתמש לכניסה</p>

                    <label class="clearfix label-pwd">
                        סיסמה:
                        <span class="mandatory-field">*</span>
                        <input type="password" name="pwd" />
                    </label>
                    <p class="field-info">שמונה תווים לפחות</p>

                    <label class="clearfix label-pwd-cnf">
                        חזור על סיסמה:
                        <span class="mandatory-field">*</span>
                        <input type="password" name="pwdCnf" />
                    </label>

                    <label class="clearfix label-phone">
                        מס' הטלפון:
                        <span class="mandatory-field">*</span>
                        <input type="text" name="phone" />
                    </label>
                    <p class="field-info">במידה ונצטרך ליצור קשר עימך</p>

                    <label class="no-border clearfix">
                        <button type="submit"></button>
                    </label>
                </form>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
    function validateSignIn(theForm){
        jQuery(".login-pane.login label").removeClass("error");
        var formData = {
            "uname": jQuery.trim(theForm.uname.value + ""),
            "pwd": jQuery.trim(theForm.pwd.value + "")
        };
        var requestIsValid = true;

        if(formData.uname.length == 0){
            jQuery(".login-pane.login label.label-uname").addClass("error");
            requestIsValid = false;
        }
        if(formData.pwd.length == 0){
            jQuery(".login-pane.login label.label-pwd").addClass("error");
            requestIsValid = false;
        }

        if(requestIsValid){
            jQuery.ajax({
                type: 'POST',
                url: "/fake-request/",       // *** SHOULD BE CHANGED TO THE REAL URL
                data: formData,
                success: function (data, status, xhr) {
                },
                error: function (xhr, status, error) {
                },
                complete: function (xhr, status) {
                    location = '/delivery/';
                },
                dataType: 'text',
                cache: false
            });
        }
        return false;
    }

    function validateSignUp(theForm){
        jQuery(".login-pane.register label").removeClass("error");
        var formData = {
            "uname": jQuery.trim(theForm.uname.value + ""),
            "pwd": jQuery.trim(theForm.pwd.value + ""),
            "pwdCnf": jQuery.trim(theForm.pwdCnf.value + ""),
            "phone": jQuery.trim(theForm.phone.value + "")
        };
        var requestIsValid = true;

        if(formData.uname.length == 0){
            jQuery(".login-pane.register label.label-uname").addClass("error");
            requestIsValid = false;
        }
        if(formData.pwd.length == 0){
            jQuery(".login-pane.register label.label-pwd").addClass("error");
            requestIsValid = false;
        }

        if(formData.pwdCnf.length == 0){
            jQuery(".login-pane.register label.label-pwd-cnf").addClass("error");
            requestIsValid = false;
        }

        if(formData.phone.length == 0){
            jQuery(".login-pane.register label.label-phone").addClass("error");
            requestIsValid = false;
        }

        if(formData.pwd.length > 0 && formData.pwdCnf.length > 0 && formData.pwd != formData.pwdCnf){
            jQuery(".login-pane.register label.label-pwd-cnf").addClass("error");
            requestIsValid = false;
        }

        if(requestIsValid){
            jQuery.ajax({
                type: 'POST',
                url: "/form-submit-url/",       // *** SHOULD BE CHANGED TO THE REAL URL
                data: formData,
                success: function (data, status, xhr) {
                },
                error: function (xhr, status, error) {
                },
                complete: function (xhr, status) {
                    location = '/delivery/';
                },
                dataType: 'text',
                cache: false
            });
        }
        return false;
    }
</script>

<?php get_footer(); ?>
<?php
get_header('vendor');
$orders = $vendor->getOrders();
?>
<script type="application/javascript" src="<?php bloginfo('stylesheet_directory') ?>/js/jquery.tablesorter.min.js"></script>
    <div class="h-section down-space">
        <ul class="breadcrumbs">
            <li><a>סטטיסטיקה</a></li>
            <li class="current"><a>הזמנות שלי</a></li>
        </ul>
    </div>

    <div class="h-section content-pane down-space">
        <form method="post" onsubmit="return false;">
            <div class="inner-block down-space">
                <p>סינון הזמנות</p>
                <div class="filter-form">
                    <label class="font-sz-small">
                        מתאריך:
                        <input type="text" name="fromDate" id="fromDate" readonly class="font-sz-small" />
                    </label>
                    &nbsp;
                    <label class="font-sz-small">
עד תאריך:
                        <input type="text" name="toDate" id="toDate" readonly class="font-sz-small" />
                    </label>
                    &nbsp;
                    <label class="font-sz-small">
שם מזמין:
                        <input type="text" name="customerName" id="customerName" class="name-field font-sz-small" />
                    </label>
                    &nbsp;
                    <button class="font-sz-small">הצג</button>
                </div>
            </div>
        </form>

        <table class="tab-data" id="tabOrders">
            <colgroup>
                <col style="width:14%;" />
                <col style="width:29%;" />
                <col style="width:29%;" />
                <col style="width:14%;" />
                <col style="width:14%;" />
            </colgroup>
            <thead>
            <tr>
                <th id="firstColumn">תאריך</th>
                <th>שם מזמין</th>
                <th>כתובת</th>
                <th>חשבון</th>
                <th>אמצעי תשלום</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($orders as $order): ?>
            <tr class="data-row" data-order-id="12345">
                <td>
                    <?php echo $order->getOrderDate(); ?>
                </td>
                <td>
                    <?php echo $order->getFirstName() . " " . $order->getLastName(); ?>
                </td>
                <td>
                    <?php echo $order->getShippingAddress(); ?>
                </td>
                <td><?php echo $order->getOrderCost(); ?> &#8362;</td>
                <td><?php echo $order->getPaymentMethod(); ?></td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>

<script type="text/javascript">
    jQuery(document).ready(function($){
        $("#tabOrders").tablesorter();

        $('.data-row').bind('click', function(){
            $self = $(this);
            if($self.data('order-id')){
                location = '/vndr-order/?orderId=' + $self.data('order-id') + '&ref=stat';
            }
        })

        $("#firstColumn").click();
    });
</script>
<?php
get_footer('vendor');
?>
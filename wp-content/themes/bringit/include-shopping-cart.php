<div class="shopping-cart">
    <div id="shoppingCartContent"></div>

    <button type="button" class="checkout"></button>

</div>

<div id="addson-modal-content" class="clearfix"></div>

<script type="text/javascript">
    jQuery(function ($) {
        
        $('#shoppingCartContent').load('/pv-shopping-cart-content/');

        $('.shopping-cart').on('click', 'button.checkout', function (e) {
            $('#addson-modal-content').load('/pv-adds-on/', function(){
                $('#addson-modal-content').modal({
                    minHeight:540,
                    minWidth: 960
                });
                return false;
            });
        });
        
    });
</script>
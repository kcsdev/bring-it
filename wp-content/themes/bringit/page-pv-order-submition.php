<?php
/**
Template Name: Purchase Address Form page
 */
get_header('blank'); ?>
<?php $customer = Customer::getCurrent(); ?>
<?php $order = Order::OpenOrder(); ?>
<?php $vendor = new Vendor($order->getSellerID()); ?>

<div class="bi-section">
    <div class="bi-container clearfix">

        <p class="pizzeria-name"><?php echo $vendor->getName(); ?></p>

        <div class="addr-form-pane">
            <div class="form-title"></div>

            <div class="form-wrapper">
                <form onsubmit="validateAddress(event);">
                    <?php the_loading_animation(); ?>
                    <input type="hidden" value = "<?php echo $customer->getID(); ?>" name="id" /> <?php // id ?>
                    <label class="clearfix label-fname">
                        שם פרטי
                        <span class="mandatory-field">*</span>
                        <input type="text" 
                               name="fname" 
                               value = "<?php if( $order->getFirstName() ){echo $order->getFirstName();} ?>" /> <?php // fname ?>
                    </label>

                    <label class="clearfix label-lname">
                        שם משפחה
                        <span class="mandatory-field">*</span>
                        <input type="text" 
                               value = "<?php if( $order->getLastName() ){echo $order->getLastName();} ?>"
                               name="lname" /> <?php // lname ?>
                    </label>

                    <label class="clearfix label-address">
                        עיר
                        <span class="mandatory-field">*</span>
                        <input type="text" value = "<?php echo $order->getCity(); ?>" name="city" readonly /> <?php // city ?>
                    </label>
                    
                    <label class="clearfix label-address">
                        רחוב
                        <span class="mandatory-field">*</span>
                        <input type="text" value = "<?php echo $order->getStreet(); ?>" name="street" readonly /> <?php // street ?>
                    </label>
                    
                    <label class="clearfix small label-entry">
                        מס' בית
                        <input type="text" value = "<?php echo $order->getBuildingNumber(); ?>" name="buildingNumber" readonly /> <?php // buildingNumber ?>
                    </label>
                    
                    <label class="clearfix small label-entry">
                        דירה
                        <input type="text"
                               value = "<?php if( $order->getapArtmentNumber() ){echo $order->getapArtmentNumber();} ?>"
                               name="apartmentNumber" /> <?php // apartmentNumber ?>
                    </label>
                    <label class="clearfix small label-store">
                        קומה
                        <input type="text" 
                               value = "<?php if( $order->getLevel() ){echo $order->getLevel();} ?>"
                               name="level" /> <?php // level ?>
                    </label>

                    <label class="clearfix small last label-appartment">
                        כניסה
                        <input type="text" 
                               value = "<?php if( $order->getEntrance() ){echo $order->getEntrance();} ?>"
                               name="entrance" /> <?php // entrance ?>
                    </label>

                    <label class="clearfix label-phone">
                        טלפון
                        <span class="mandatory-field">*</span>
                        <input type="text" 
                               value = "<?php if( $order->getPhone() ){echo $order->getPhone();} ?>"
                               name="phone"  /> <?php // phone ?>
                    </label>
                    
                    <label class = "clearfix label-order-comment">
                        הערות להזמנה
                        <input type="text" 
                               value = "<?php if( $order->getOrderComment() ){echo $order->getOrderComment();} ?>"
                               name = "orderComment" maxlength = "150" />
                    </label>
                    
                    <label class = "clearfix label-courier-comment">
                        הערות לשליח
                        <input type="text" 
                               value = "<?php if( $order->getCourierComment() ){echo $order->getCourierComment();} ?>"
                               name = "courierComment" maxlength = "150" />
                    </label>

                    <label class="clearfix no-border label-pwd">
                        <input type="checkbox" name="terms" /> <?php // terms ?>
                        <a href="javascript:void(0);" class="show-terms">מסכים לתנאי השימוש</a>
                    </label>
                    <div class="terms">
                        <p>המילה pizza מקורה באיטלקית, ומשמעה "עוגה" או "פשטידה". תיעוד למילה pizza ניתן למצוא עוד בלטינית מאוחרת, ובה משמעה "לחם שטוח". לא ברור כיצד הגיעה המילה ללטינית; על פי ארנסט קליין, המילה נגזרה מהמילה היוונית pitta, שמו של מאפה יווני שמקורו במילה היוונית העתיקה שמשמעה "פיח"‏[1] (גם המילה העברית "פיתה" הגיעה מאותו המקור). סברה אחרת גורסת כי מקור המילה גרמאני, וכי היא באה מהמילה bezzo שמשמעה "נגיסה"; על פי סברה זו המילה היוונית אולי הגיעה דווקא מהמילה האיטלקית ולא להפך.‏[2] על פי המילון האטימולוגי הוותיק של פיאניג'אני, מקור המילה הוא בצורה של המילה pinza, מילת דיאלקט שהיא צורת בינוני-פעול של הפועל "ללחוץ" או "לרמוס".‏[3] במשמעותה המודרנית תועדה המילה באנגלית לראשונה בשנת 1825.</p>
                        <p>הפיצה האיטלקית המסורתית מבוססת על בצק שמרים, על בסיס קמח דורום, שמרים ומים בלבד. את הבצק מרדדים ביד (ללא עזרת מערוך) לשכבת בצק עגולה ודקה, דמוית פיתה עיראקית או דרוזית, מוסיפים את הרטבים והתוספות השונות ומכניסים לתנור אפייה בחום גבוה מאוד (לפחות 400°C) לפרק זמן של כ-2 דקות, כך שמתקבל בסיס דק ופריך.</p>
                        <p>פיצה מרגריטה היא הפיצה האיטלקית המוכרת ביותר. פיצה זו מבוססת על רוטב מעגבניות מבושלות מרוסקות ומסוננות הנקרא באיטלקית pasata. מעל הרוטב מונחת גבינת מוצרלה וכן מונחים עלי בזיליקום. בשולי הפיצה נמשח שמן זית איכותי.</p>
                    </div>

                    <button type="submit"></button>
                </form>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
    
    //jQuery(document).ready(function(){
        
//        var name = jQuery("input[name=fname]").val();
//        var id = jQuery("input[name=id]").val();
//        console.log(name);    
//        console.log(id);
    
    
        function validateAddress(e){ 

            e.preventDefault();
            //jQuery(".login-pane.login label").removeClass("error");
            jQuery("#the_loading_animation").show();
            var formData = { 
                "id":               jQuery("input[name=id]").val(),
                "fname":            jQuery("input[name=fname]").val(),
                "lname":            jQuery("input[name=lname]").val(),
                "city":             jQuery("input[name=city]").val(),
                "street":           jQuery("input[name=street]").val(),
                "apartmentNumber":  jQuery("input[name=apartmentNumber]").val(),
                "buildingNumber":   jQuery("input[name=buildingNumber]").val(),
                "level":            jQuery("input[name=level]").val(),
                "entrance":         jQuery("input[name=entrance]").val(),
                "phone":            jQuery("input[name=phone]").val(),
                "terms":            jQuery("input[name=terms]").val(),
                "orderComment":     jQuery("input[name=orderComment]").val(),
                courierComment:            jQuery("input[name=courierComment]").val(),
//                "id": jQuery("iframe").contents().find("input[name=id]").val(),
//                "fname": jQuery("iframe").contents().find("input[name=fname]").val(),
//                "lname": jQuery("iframe").contents().find("input[name=lname]").val(),
//                "city": jQuery("iframe").contents().find("input[name=city]").val(),
//                "street": jQuery("iframe").contents().find("input[name=street]").val(),
//                "apartmentNumber": jQuery("iframe").contents().find("input[name=apartmentNumber]").val(),
//                "buildingNumber": jQuery("iframe").contents().find("input[name=buildingNumber]").val(),
//                "level": jQuery("iframe").contents().find("input[name=level]").val(),
//                "entrance": jQuery("iframe").contents().find("input[name=entrance]").val(),
//                "phone": jQuery("iframe").contents().find("input[name=phone]").val(),
//                "terms": jQuery("iframe").contents().find("input[name=terms]").val()
            };
//            console.log(formData);
            var jsonFormData = JSON.stringify(formData);
//            console.log("check");
            jQuery.ajax({ 
                type: 'POST',
                url: "/wp-admin/admin-ajax.php/",
                data: "action=add_shipping_address_to_order_action&formData="+jsonFormData,
                success: function (data, status, xhr) { 
//                    window.location.href = "/purchase-addr-list";
                    console.log('AJAX request succeeded on page-purchase-addr-form');
                    console.log(data);
                    
                },
                error: function (xhr, status, error) { 
                    console.log('Connection error')
                },
                complete: function (xhr, status) { 
                    jQuery("#the_loading_animation").show();
                    console.log('AJAX request complete on page-purchase-addr-form');
                    location.href = "purchase-payment-type";
                },
                dataType: 'text',
                cache: false
            });
            return false;
        }
//    });
    jQuery(document).ready(function($){
       $(".show-terms").bind("click", function(){
           $(".terms").toggle();
       })
    });
</script>

<?php get_footer('blank'); ?>
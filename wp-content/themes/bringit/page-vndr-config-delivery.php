<?php
get_header('vendor');
// In header we get the $vendor with the information about current logged in vendor

$vendor_cities = $vendor->getCities();
$cities = $vendor->getUnregisteredCities();
?>
    <div class="h-section down-space">
        <ul class="breadcrumbs">
            <li><a>הגדרות</a></li>
            <li class="current"><a>הגדרות משלוח</a></li>
        </ul>
    </div>

    <div class="h-section content-pane down-space">
        
        <div class="inner-block down-space clearfix">
            <a class="area-block" data-area-data-id="newArea">
+               הוסף איזור חלוקה
            </a>
            <div class="area-data-block" id="newArea">
                <form onsubmit="return false;">
                    <label>בחר ישוב:</label>
                    <select class="locality" name="locality-new" id="locality-new" > <?php // First you choose the city. ?>
                        <option value="none">-Choose City-</option>
                        <?php foreach($cities as $city): ?>
                        <option id = "<?php echo $city->name; ?>" value = "<?php echo $city->name; ?>"><?php echo $city->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <img class = "save-area-animation" id = "show-area-animation" src = "<?php echo LOADING_GIF ?>" />
                    <div id="localityNewData"></div>
                </form>
            </div>
        </div>
        
        <div id = "temp-area-data">
        </div>
<?php foreach($vendor_cities as $city): ?>
    <?php $regions = $vendor->getRegionsByCity($city->name); ?>
        <div class="inner-block down-space clearfix">
            <a class="area-block" data-area-data-id="area-<?php echo $city->name; ?>">
                <?php echo $city->name; ?>
            </a>
            <div class="area-data-block" id="area-<?php echo $city->name; ?>">
                <form onsubmit="return false;">
                    <div>
                        <table class="tab-data">
                            <colgroup>
                                <col style="width: 10%;" />
                                <col style="width: 18%;" />
                                <col style="width: 18%;" />
                                <col style="width: 18%;" />
                                <col style="width: 18%;" />
                                <col style="width: 18%;" />
                            </colgroup>
                            <thead>
                            <tr>
                                <th>אזור
                                    <span class="icon-tooltip static-tooltip" data-tooltip-class="blue" data-tooltip-text="טבלה עם אפשרויות סינון- לפי תאריכים. על מנת שאוכל להגיע לכל הזמנה קודמת שהייתה לי שנה אחורה.">?</span></th>
                                <th>מבצע משלוח לאיזור זה
                                    <span class="icon-tooltip static-tooltip" data-tooltip-class="blue" data-tooltip-text="טבלה עם אפשרויות סינון- לפי תאריכים. על מנת שאוכל להגיע לכל הזמנה קודמת שהייתה לי שנה אחורה.">?</span></th>
                                <th>מינימום הזמנה באתר (&#8362;)
                                    <span class="icon-tooltip static-tooltip" data-tooltip-class="blue" data-tooltip-text="טבלה עם אפשרויות סינון- לפי תאריכים. על מנת שאוכל להגיע לכל הזמנה קודמת שהייתה לי שנה אחורה.">?</span></th>
                                <th>עלות משלוח (&#8362;)
                                    <span class="icon-tooltip static-tooltip" data-tooltip-class="blue" data-tooltip-text="טבלה עם אפשרויות סינון- לפי תאריכים. על מנת שאוכל להגיע לכל הזמנה קודמת שהייתה לי שנה אחורה.">?</span></th>
                                <th>משלוח חינם החל מ- (&#8362;)
                                    <span class="icon-tooltip static-tooltip" data-tooltip-class="blue" data-tooltip-text="טבלה עם אפשרויות סינון- לפי תאריכים. על מנת שאוכל להגיע לכל הזמנה קודמת שהייתה לי שנה אחורה.">?</span></th>
                                <th>זמן משלוח (דקות)
                                    <span class="icon-tooltip static-tooltip" data-tooltip-class="blue" data-tooltip-text="טבלה עם אפשרויות סינון- לפי תאריכים. על מנת שאוכל להגיע לכל הזמנה קודמת שהייתה לי שנה אחורה.">?</span></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($regions as $region): ?>
                            <tr class = "region-row" id = "region-row_<?php echo $city->name; ?>" region-name = "<?php echo $region->name; ?>">
                                <td>
                                    <label>
                                    <?php echo $region->name; ?>
                                    </label>
                                </td>
                                <?php if($vendor->is_shipping($city->name,$region->name)): ?>
                                    <?php $checked = "checked"; ?>
                                <?php else: ?>
                                    <?php $checked = ""; ?>
                                <?php endif; ?>
                                <td class="align-center">
                                    <input type="checkbox" 
                                           class="v-align-middle" 
                                           id = "is_shipping_<?php echo $city->name; ?>_<?php echo $region->name; ?>" 
                                           name = "is_shipping_<?php echo $city->name; ?>_<?php echo $region->name; ?>" 
                                           <?php echo $checked; ?> 
                                           origin-data = "<?php echo $checked; ?>"/>
                                </td>
                                <td>
                                    <input type="text" 
                                           class="tab-data-field" 
                                           id = "minimum-order-price_<?php echo $city->name; ?>_<?php echo $region->name; ?>" 
                                           name = "minimum-order-price_<?php echo $city->name; ?>_<?php echo $region->name; ?>"
                                           value = "<?php echo $region->minimumOrderPrice; ?>" 
                                           orgin-data = "<?php echo $region->minimumOrderPrice; ?>" 
                                           />
                                </td>
                                <td>
                                    <input type="text" 
                                           class="tab-data-field" 
                                           id = "shipping-price_<?php echo $city->name; ?>_<?php echo $region->name; ?>" 
                                           name = "shipping-price_<?php echo $city->name; ?>_<?php echo $region->name; ?>"
                                           value = "<?php echo $region->shippingPrice; ?>"
                                            orgin-data = "<?php echo $region->shippingPrice; ?>" />
                                </td>
                                <td>
                                    <input type="text" 
                                           class="tab-data-field" 
                                           id = "minimum-order-price-for-free-shipping_<?php echo $city->name; ?>_<?php echo $region->name; ?>" 
                                           name = "minimum-order-price-for-free-shipping_<?php echo $city->name; ?>_<?php echo $region->name; ?>"
                                           value = "<?php echo $region->minimumOrderPriceForFreeShipping; ?>"
                                           orgin-data = "<?php echo $region->minimumOrderPriceForFreeShipping; ?>"
                                           />
                                </td>
                                <td>
                                    <input type="text" 
                                           class="tab-data-field" 
                                           id = "shipping-time_<?php echo $city->name; ?>_<?php echo $region->name; ?>" 
                                           name = "shipping-time_<?php echo $city->name; ?>_<?php echo $region->name; ?>"
                                           value = "<?php echo $region->shippingTime; ?>" 
                                           orgin-data = "<?php echo $region->shippingTime; ?>" 
                                           />
                                </td>
                            </tr>
                            <?php endforeach; ?>
                                
                            </tbody>
                        </table>
                        <ul class="locality-buttons">
                            <li>
                                <button type="submit" 
                                        id = "edit-area-save"
                                        city-name = "<?php echo $city->name; ?>" >שמור</button>
                                <img class = "save-area-animation" id = "save-area-animation" src = "<?php echo LOADING_GIF ?>" />
                            </li>
                            <li>
                                <button type="reset" 
                                        city-name = "<?php echo $city-name; ?>"
                                        id = "edit-area-reset">
                                    בטל
                                </button>
                                <img class = "save-area-animation" id = "reset-area-animation" src = "<?php echo LOADING_GIF ?>" />
                            </li>
                            <li class="remove-area">
                                <button type="button"
                                        id = "edit-area-remove"
                                        city-name = "<?php echo $city->name; ?>">מחק אזור</button>
                                <img class = "save-area-animation" id = "remove-area-animation" src = "<?php echo LOADING_GIF ?>" />
                            </li>
                        </ul>
                    </div>
                </form>
            </div>
        </div>
<?php endforeach; ?>
    

    </div>

<?php
get_footer('vendor');
?>
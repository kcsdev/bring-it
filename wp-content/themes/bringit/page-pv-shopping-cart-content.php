<?php
/*
 * Template Name: PV Shopping Cart Content
 */
the_loading_animation(); 
$cart = unserialize($_SESSION['Cart']); 
if($cart->getVendor()){
    $vendor = new Vendor($cart->getVendor());
}
?>

<?php  /************************* Pizzas Part of the cart  *********************************/ ?>
<?php $pizzas = $cart->getPizzas(); ?>
<?php if(!empty($pizzas)): ?>
    <?php  foreach($pizzas as $pizza): ?> 
    <?php $addons = $pizza->getAddons(); ?>
    <?php if(empty($addons)){ $plus = ""; }else{ $plus = "+"; } ?>
        <div class="order-row">
            <span class="col-1" id = "pizza-item-col"><img src="<?php  bloginfo('stylesheet_directory') ?>/assets/icon-order-pizza.png" alt="" /></span>
            <span class="col-2">
                פיצה 
                <?php echo $plus; ?>
                <?php $i = 1; ?>
                <?php if(!empty($addons)): ?>
                    <?php foreach($addons as $addon): ?> <?php  /************************* Addons Part of the cart  *********************************/ ?>
                        <?php if($i == count($addons)){ $plus = ''; }else{$plus = "&#43;";} ?>
                <span cart-pizza-id= "<?php echo $pizza->getID(); ?>" cart-addon-id="<?php echo $addon->getID(); ?>" onclick="removeAddon(event)" class = "addon-col" id = "addon-col">
                    <?php  echo  $addon->getTitle() . "" . $plus; ?>
                </span>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </span>
            <?php if($cart->getVendor()): ?>
            <span class = "col-3">
                <?php echo $vendor->getPizzaPrice(); ?>
                &#8362;
            </span>
            <?php endif; ?>
            <span class="col-4" onclick="removePizza('<?php echo $pizza->getID(); ?>')">
                <img src="<?php  bloginfo('stylesheet_directory') ?>/assets/icon-order-close.png" alt="" />
            </span>           
        </div>
    <?php  endforeach; ?>
<?php endif; ?>


<?php  /************************* Additional Products Part of the cart  *********************************/ ?>
<div class="title-row">    
    <?php $additional_products = $cart->getAdditionalProducts(); // Gets the array of Additional products that are in the cart; ?> 
    <?php if(!empty($additional_products)): //checks if the array of additional products is not empty ?>
    <p>תוספות</p>
        <?php foreach( $additional_products as $additional_product ): ?>
            <div class="order-row">
                <span class="col-1"><img src="<?php bloginfo('stylesheet_directory') ?>/assets/icon-order-pizza.png" alt="" /></span>
                <span class="col-2"><?php echo $additional_product->getName(); //displays the name of the additional product ?></span>
                <span class="col-3"><?php echo $additional_product->getPrice(); ?>₪</span>
                <span class="col-4" onclick="removeAdditionalProduct( '<?php echo $additional_product->getID(); ?>')" ><img src="<?php bloginfo('stylesheet_directory') ?>/assets/icon-order-close.png" alt="" /></span>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>


<?php  /************************* Drinks Part of the cart  *********************************/ ?>
<div class="title-row">
<?php $drinks = $cart->getDrinks();  // Get Drinks from cart; ?>
<?php $temp_stacks = array(); ?>
<?php foreach($drinks as $drink): ?>
    <?php $temp_stack = $cart->getDrinkStackByWPID($drink->getWPID()); ?>
    <?php $temp_stacks[] = $temp_stack;?>
<?php endforeach; ?>
<?php $stacks = $cart->filterStacks($temp_stacks); ?>

<?php if(!empty($stacks)): ?>
    <p>שתייה</p>
    <?php foreach($stacks as $stack): ?>
    <?php if($stack->drink->getType() == "bottled_drink"){$img = "icon-order-bottle.png";}else{$img = "icon-order-can.png";} ?>
        <?php  ?>
    <div class="order-row">
        <span class="col-1">
            <img src="<?php  bloginfo('stylesheet_directory') ?>/assets/<?php echo $img; ?>" alt="" />
        </span>
        <span class="col-2">
            <?php echo $stack->drink->getTitle() . " (".$stack->quantity.")"; ?>
        </span> 
        <?php if($cart->getVendor()): ?>
            <span class = "col-3">
                <?php if($vendor->getDrinksprice()): ?>
                    <?php echo $vendor->getDrinksPrice(); ?>&#8362;
                <?php else: ?>
                    בפיצריה לא נמכר מוצר זה.
                <?php endif; ?>
            </span>
        <?php endif; ?>
        <span class="col-4" onclick="removeDrink('<?php echo $stack->drink->getWPID(); ?>')">
            <img src="<?php  bloginfo('stylesheet_directory') ?>/assets/icon-order-close.png" alt="" />
        </span>
    </div>
    <?php endforeach; ?>
<?php endif; ?>
</div>

<?php  /************************* Bundles Part of the cart  *********************************/ ?>
<div class="title-row">
<?php $bundles = $cart->getBundles(); ?>
<?php if(Bundle::Open()): ?>
    <?php $bundles[] = Bundle::Open(); ?>
<?php endif; ?>
<?php if(!empty($bundles)):?>
    <p>מבצעים</p>
<?php endif; ?>
<?php foreach($bundles as $bundle): ?>    
    <div class="order-row">
        <span class="col-1" ><img src="<?php bloginfo('stylesheet_directory') ?>/assets/icon-order-pizza.png" alt="" /></span>
        <span class="col-2"><?php echo $bundle->getDescription(); ?></span>
        <?php if( $cart->getVendor() ): ?>
        <span class = "col-3"> <?php echo $vendor->getBundlePrice($bundle->getWPID()); ?> </span>
        <?php endif; ?>
        <span class="col-4" onclick="removeBundle('<?php echo $bundle->getID() ?>')" ><img src="<?php  bloginfo('stylesheet_directory') ?>/assets/icon-order-close.png" alt="" /></span>
    </div>
    
    <?php $drinks = $bundle->getIncludedDrinks(); // Get Drinks that are in current Bundle ?>
    <?php $additionalProducts = $bundle->getIncludedAdditionalProducts(); // Get Additional Products that are in current Bundle ?>
    <?php $pizzas = $bundle->getIncludedPizzas(); // Get pizzas that are in current Bundle ?> 
    
    <?php  foreach($pizzas as $pizza): ?> 
    <?php $addons = $pizza->getAddons(); ?>
    <?php if(empty($addons)){ $plus = ""; }else{ $plus = "+"; } ?>
        <div class="order-row">
            <span class="col-1" id = "pizza-item-col"><img src="<?php  bloginfo('stylesheet_directory') ?>/assets/icon-order-pizza.png" alt="" /></span>
            <span class="col-2">
                פיצה 
                <?php echo $plus; ?>
                <?php $i = 1; ?>
                <?php if(!empty($addons)): ?>
                    <?php foreach($addons as $addon): ?> <?php  /************************* Addons Part of the cart  *********************************/ ?>
                        <?php if($i == count($addons)){ $plus = ''; }else{$plus = "&#43;";} ?>
                <span cart-pizza-id= "<?php echo $pizza->getID(); ?>" cart-addon-id="<?php echo $addon->getID(); ?>" onclick="removeAddon(event)" class = "addon-col" id = "addon-col">
                    <?php  echo  $addon->getTitle() . "" . $plus; ?>
                </span>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </span>
                     
        </div>
    <?php  endforeach; ?>
    <?php $pizzas = array(); ?>
    <?php if(!empty($drinks)): ?>
        <div class="title-row">
            <?php $temp_stacks = array(); ?>
            <?php foreach($drinks as $drink): ?>
                <?php $temp_stack = $bundle->getDrinkStackByWPID($drink->getWPID()); ?>
                <?php $temp_stacks[] = $temp_stack;?>
            <?php endforeach; ?>
            <?php $stacks = $cart->filterStacks($temp_stacks); ?>

            <?php if(!empty($stacks)): ?>
                <?php foreach($stacks as $stack): ?>
                <?php if($stack->drink->getType() == "bottled_drink"){$img = "icon-order-bottle.png";}else{$img = "icon-order-can.png";} ?>
                    <?php  ?>
                <div class="order-row">
                    <span class="col-1">
                        <img src="<?php  bloginfo('stylesheet_directory') ?>/assets/<?php echo $img; ?>" alt="" />
                    </span>
                    <span class="col-2">
                        <?php echo $stack->drink->getTitle() . " (".$stack->quantity.")"; ?>
                    </span> 
                    
                </div>
                <?php endforeach; ?>
            <?php endif; ?>
            </div>
    <?php endif; ?>
<?php endforeach; ?>
</div>


<div class="order-last-row">
    <span class="col-4" onclick="emptyTheCart()"><img src="<?php bloginfo('stylesheet_directory') ?>/assets/icon-order-close.png" alt="" /></span>
    <span class="last">מחק את כל הפריטים</span>
</div>
<script>

   
</script>
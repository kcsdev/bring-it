<?php
/**
Template Name: View Order page
 */
get_header(); ?>

    <div class="bi-section">
        <div class="bi-container clearfix">
            <div class="overlay-mask">
                <div class="iframe-close-button" onclick="closeMe();"></div>
				<div class="iframe-wrapper">
					<iframe id="pzIframe"></iframe>
				</div>
			</div>
            <div class="view-order-title"></div>
                <div class = "view-order-buttons">
                    <ul class="vo-options">
                        <li class="popup-more-deal clearfix">
                            <button type="button">&gt;&gt;</button>
                            <a>להוסיף עוד מבצע</a>
                        </li>
                        <li class="popup-more-pizza clearfix">
                            <button type="button">&gt;&gt;</button>
                            <a>להוסיף עוד פיצה</a>
                        </li>
                        <li class="popup-more-drink clearfix">
                            <button type="button">&gt;&gt;</button>
                            <a>להוסיף עוד שתיה</a>
                        </li>
                    </ul>
                </div>
            <div class="drinks-left-sidebar">
                <div class="shopping-cart" style="float: left;">
                    <div id="shoppingCartContent"></div>
                </div>
                <button type="button" class="checkout" onclick="location='/pizzerias/';"></button>
            </div>
        </div>
    </div>

    <script type="text/javascript">
//        jQuery(function ($) {
//            $(document).ready(function(){
//                $("#shoppingCartContent").load("/pv-shopping-cart-content/");
//            });
//        });
        
        jQuery(function ($) {
            $(document).ready(function(){
                $("#shoppingCartContent").load("/pv-shopping-cart-content/");
            });
			
			$(".popup-more-deal").bind("click", function(){
				$(".iframe-wrapper").height($(window).height() - 120);
				$("#pzIframe").attr("src","/popup-specials/");
				$(".overlay-mask").fadeIn(400, function(){
                    var pLeft = $("#pzIframe").offset().left + $("#pzIframe").width() - 36;
                    $(".iframe-close-button").css({"left":pLeft + "px"});
                });
			})

			$(".popup-more-pizza").bind("click", function(){
//				$(".iframe-wrapper").height($(window).height() - 120);
//				$("#pzIframe").attr("src","/make-pizza-overlay/");
//				$(".overlay-mask").fadeIn();
                location = "/make-pizza/";
			})
			
			$(".popup-more-drink").bind("click", function(){
//				$(".iframe-wrapper").height($(window).height() - 120);
//				$("#pzIframe").attr("src","/drinks-overlay/");
//				$(".overlay-mask").fadeIn();
                location  = "/drinks/";
			})
			
			$(window).bind("resize", function(){
				if($(".overlay-mask").is(":visible")){
					$(".iframe-wrapper").height($(window).height() - 120);
                    var pLeft = $("#pzIframe").offset().left + $("#pzIframe").width() - 36;
                    $(".iframe-close-button").css({"left":pLeft + "px"});
				}
			})
        });
		
		function closeMe(){
			jQuery("#pzIframe").attr("src","about:blank");
			jQuery(".overlay-mask").fadeOut();
		}
    
    </script>

<?php get_footer(); ?>
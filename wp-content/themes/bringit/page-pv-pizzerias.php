<?php

if(isset($_REQUEST[""])){
    
}elseif(isset($_REQUEST[""])){
    
}

?>
<div class="pz-list">

            <?php /* ======= Pizzeria ======= */ ?>
            <?php $vendors = Vendor::getVendorsByCity($city); //this will get all vendors that marked that they deliver to that region ?>
            <?php foreach($vendors as $vendor): ?>
            <?php $cart->defineVendor($vendor->getID()); ?>
            
            <ul class="pizzeria clearfix">
                <?php // Get Pizzeria Image Column ?>
                <li class="col1">
                    <img src="<?php echo $vendor->getLogo(); ?>" alt="" />
                </li>

                <li class="title-row clearfix">
                    <?php echo $vendor->getName();?>
                    <span class="reviews">
תגובות:
                        <a href="javascript:void(0);"><?php // Amount of reviews goes here ?></a>
                        <span class="pipe">&nbsp;|&nbsp;</span>
משלוח:
                        <?php //echo get_rate_stars(2, 3); ?> <?php // rating goes here ?>
                        <span class="pipe">&nbsp;|&nbsp;</span>
                        טעם:
                        <?php // echo get_rate_stars(2, 3);  ?> <?php // Rating goes here ?>
                    </span>
                </li>
                
                <?php // Kashrut and Open/Closed Column ?>
                <li class="col2">
                    <?php if( $vendor->getKashrut() != "לא כשר" ): ?>
                        <span class="bg-green">כשר</span>
                    <?php else: ?>
                        <span class="bg-red">לא כשר</span>
                    <?php endif; ?>
                    
                    <?php if( $vendor->getState() ): ?>
                        <span class="bg-green">פתוח</span>
                    <?php else: ?>
                        <span class="bg-red">סגור</span>
                    <?php endif; ?>
                </li>

                <?php // Shipping time Column ?>
                <li class="col3">
                    <?php if(!$vendor->isShipping($city,$region)): ?>
                    <div>
                    <span class="delivery-time">
                        <p class = 'no-delivery'>
                            לא מבצעים משלוחים
                        </p>
                    </span>
                    </div>
                    <?php else: ?>
                    <p class="top">
                        זמן למשלוח
                    </p>
                    <div>
                        <span class="delivery-time"><?php echo $vendor->getShippingTimeByRegion($city,$region); //Delivery time goes here ?></span>
                        <span class="delivery-time-suffix">דק'</span>
                    </div>
                    <?php endif; ?>
                </li>
                
                <?php // Missing Addson/Drinks Column ?>
                <li class="col4">
                    <?php $missing_addons = $vendor->getMissingAddons($vendor->getID()); ?>
                    <div>
                        תוספות:
                        <?php if(!$missing_addons): ?>
                        <span class="green">יש <?php // If all addons are available a confirmation text goes here ?></span>
                        <?php else: ?>
                        <span class = "red">חסר: <?php // else a list of addons that are not available will be displayed ?> 
                            <?php $i = 1; ?>
                            <?php foreach( $missing_addons as $missing_addon ): ?>
                                <?php if( $i != count($missing_addons) ): ?>
                                    <?php echo $missing_addon->getTitle() . ", "; ?>    
                                <?php else: ?>
                                    <?php echo $missing_addon->getTitle(); ?>
                                <?php endif; ?>
                                <?php $i++; ?>
                            <?php endforeach; ?>
                        </span>
                        <?php endif; ?>
                        <span class="jq-change-components change" vendor-id = "<?php echo $vendor->getID(); ?>">החלף</span>
                    </div>
                    <hr />
                    <div>
                        <?php $missing_drinks = $vendor->getMissingDrinks($vendor->getID()); ?>
                        שתיה:
                        <?php if( !$missing_drinks ): ?>
                        <span class="green">יש<?php // If all drinks are available a confirmation text goes here ?></span>
                        <?php else: ?>
                        <span class = "red">חסר: <?php // else a list of drinks that are not available will be displayed ?> 
                            <?php $i = 1; ?>
                            <?php foreach( $missing_drinks as $missing_drink ): ?>
                                <?php if( $i != count($missing_drinks) ): ?>
                                    <?php echo $missing_drink->getTitle() . ", "; ?>    
                                <?php else: ?>
                                    <?php echo $missing_drink->getTitle(); ?>
                                <?php endif; ?>
                                <?php $i++; ?>
                            <?php endforeach; ?>
                        </span>
                        <?php endif; ?>
                        <span class="jq-change-drinks change" vendor-id = "<?php echo $vendor->getID(); ?>">החלף</span>
                    </div>
                </li>

                <li class="col5">
                    <p>
                        מרכיבי המחיר
                    </p>
                    <?php $total_price = $cart->getTotalPriceByRegion($vendor,$city,$region,false); ?>
                    <?php $total_price_with_shipping = $cart->getTotalPriceByRegion($vendor,$city,$region,true); ?>
                    <p class="data">
                        עלות:
                        <span class="green"><?php echo $total_price; // Full order price without shipping price goes here?> &#8362;</span>
                    </p>
                    <?php  $shipping_price = $vendor->getShippingPriceByRegion($city,$region); //the second parameter determines if to return "free" or "0" in case of free shipping price; ?>
                    <?php  $minimum_order_price_for_free_shipping = $vendor->getMinimumOrderPriceForFreeShippingByRegion($city,$region); // ?>
                    
                    <?php if($vendor->isShipping($city,$region)): ?>
                    <p class="data">
                        משלוח: 
                        <?php if($vendor->getMinimumOrderPriceForFreeShippingByRegion($city,$region) < $total_price): ?>
                        <span class = "green">
                            
                            <span class="shadow">Free</span>
                            
                        </span>
                        <?php else: ?>
                        <span class="red"> 
                            
                            <span class="shadow"><?php  echo $shipping_price; // Shipping price goes here ?> &#8362; </span>
                            
                            <a class="price-tooltip" title="משלוח חינם אחל מ- <?php echo $minimum_order_price_for_free_shipping; // condition for free shipping goes here ?> ש&quot;ח">?</a>
                        </span>
                        <?php endif; ?>
                    </p>
                    <?php endif; ?>
                </li>

                <li class="col6">
                    <?php the_loading_animation(); ?>
                    <?php if($vendor->isShipping($city,$region) && 
                             $total_price < $vendor->getMinimumOrderPriceForFreeShippingByRegion($city,$region)): ?>
                        <div class="price"><?php  echo $total_price_with_shipping; // Full Order Price goes here ?> &#8362;</div> 
                    <?php else: ?>
                        <div class="price"><?php  echo $total_price; // Full Order Price goes here ?> &#8362;</div> 
                    <?php endif; ?>
                    
                    <button type="button" class="buy" vendor-id="<?php echo $vendor->getID(); ?>" onclick = "pizzeriasCheckout(event)">לרכישה</button>
                </li>
<!--

                <li class="col7specials">
                    <p>משתתף במבצע</p>
                </li>
                <li class="col8specials">
                    <p>
                        לחם שום מתנה
                        <span>בקנייה מעל 100 ש״ח (בין השעות 14:00 ל-14:31)</span>
                    </p>
                </li>
-->
            </ul>
            <?php /* ======= /Pizzeria ======= */ ?>
            <?php endforeach;?>
            
        </div>
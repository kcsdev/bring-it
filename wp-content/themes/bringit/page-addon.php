<?php
/**
Template Name: Addon page
 */
get_header(); ?>
<?php $additional_products = get_additional_products(-1,$_SESSION['vendor']); ?>
<?php $vendor = get_the_vendor($_SESSION['vendor']); ?>
    <div class="bi-section">
        <div class="bi-container clearfix addons">
            <div class="overlay-mask">
                <div class="iframe-wrapper">
                    <img src="<?php bloginfo('stylesheet_directory') ?>/assets/pop-up-close.png" alt="" class="close-iframe" />
                    <iframe id="pzIframe"></iframe>
                </div>
            </div>

            <div class="mask"></div>
            
            <div class="addons-pane clearfix">
                <p class="pizzeria-name"><?php echo $vendor->name; ?></p>
                <div class="title">
                    <img src="<?php bloginfo('stylesheet_directory') ?>/assets/title-addson-pop-up.png" alt="" />
                </div>

                <div id="addonsTiles" class="addons-tiles">
                    <?php foreach($additional_products as $additional_product): ?>
                    <div class="item">
                        <img src="<?php echo $additional_product->image; ?>" alt="" />
                        <p class="name"><?php echo $additional_product->title; ?></p>
                        <p class="descr"><?php echo $additional_product->description; ?></p>
                        <div class="bottom">
                            <span><?php echo $additional_product->price; ?> &#8362;</span>
                            <button onclick = "addAdditionalProductToCart(event)" additional-product-id = "<?php echo $additional_product->id; ?>" additional-product-title = "<?php echo $additional_product->title; ?>" type="button">הוסף</button>
                        </div>
                    </div>
                    <?php endforeach; ?>

<!--
                    <div class="item">
                        <img src="<?php // bloginfo('stylesheet_directory') ?>/assets/temp/adds-on-2.jpg" alt="" />
                        <p class="name">מיני פנקייקס</p>
                        <p class="descr">חביתית או חמיטה, הנקראת גם בלועזית פנקייק או קרפ</p>
                        <div class="bottom">
                            <span>16.50 &#8362;</span>
                            <button type="button">הוסף</button>
                        </div>
                    </div>
-->


                </div>
            </div>

            <div class="addons-left-sidebar">
                <div class="toggle-button">&lt;&lt;</div>
                <img src="<?php bloginfo('stylesheet_directory') ?>/assets/bike.png" alt="" class="bike" />
                <div class="shopping-cart">
                    <div id="shoppingCartContent"></div>
                    <button type="button" class="checkout"></button>
                </div>
            </div>

        </div>


    </div>
    <script type="text/javascript">
        jQuery(function ($) {
            $(".addons-left-sidebar .toggle-button").bind("click", function(){
                $self = $(this);
                var objMenu = $(".addons-left-sidebar");
                if($self.hasClass("open")) {
                    objMenu.animate({"left":"-310px"}, 200, function(){
                        $(".bi-section .bi-container.drinks .mask").fadeOut(200, function(){
                            $self.removeClass("open").text('<<');
                            objMenu.css({"left":""});
                        });
                    })
                } else {
                    $(".bi-section .bi-container.drinks .mask").fadeIn(200, function(){
                        objMenu.animate({"left":"0px"}, 200, function(){
                            $self.addClass("open").text('>>');
                        });
                    });
                }
            });

            $(".checkout").bind("click", function(){
                $(".iframe-wrapper").height($(window).height() - 90);
                $("#pzIframe").attr("src","/purchase-sign-in/");
                $(".overlay-mask").fadeIn();
            });

            $("img.close-iframe").bind("click", function(){
                $("#pzIframe").attr("src","");
                $(".overlay-mask").fadeOut();
            });
            $(document).ready(function(){
                $("#shoppingCartContent").load("/pv-shopping-cart-content/");
            });
        });
    </script>
<?php get_footer(); ?>
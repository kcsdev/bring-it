<?php
get_header('vendor');
$special_addson = $vendor->getSpecialAddson();
?>
    <div class="h-section down-space">
        <ul class="breadcrumbs">
            <li><a>מלאי</a></li>
            <li class="current"><a>תוספות אישיות</a></li>
        </ul>
    </div>

    <div class="h-section content-pane down-space clearfix">
        <form method="post" onsubmit="return false;">
            <div class="inner-block down-space">
                <div>
                    <label>תוספת חדשה:</label>&nbsp;
                    <input type="text" name="newClientAddOn" id="newClientAddOn" class="top-form-field" style="width:150px;" placeholder="שם תוספת" />
                    <!--&nbsp;
                    <input type="text" name="newClientAddOnDescr" id="newClientAddOnDescr" class="top-form-field" style="width:150px;" placeholder="תיאור תוספת" />-->
                    <!--&nbsp;
                    <ul class="input-file-block">
                        <li class="file-path">
                            <input type="text" id="fileUploadPath" name="fileUploadPath" placeholder="טעינת תמונה" readonly />
                        </li>
                        <li class="button">...
                            <input type="file" id="fileUpload" name="fileUpload" onchange="document.getElementById('fileUploadPath').value = this.value;" />
                        </li>
                    </ul>-->
                    &nbsp;
                    <!--<input type="file" name="newClientAddOn" id="newClientAddOn" />-->
                    <button type="submit" id = "newClientAddOnButton" class="top-form-button">הוסף</button>
                    <img src = "<?php echo LOADING_GIF ?>" class = "save-area-animation" id = "add-special-addon-button" />
                    <!--<button type="reset" class="top-form-button">אפס</button>-->
                </div>
            </div>
            <table class="stock-list down-space">
                <colgroup>
                    <col style="width:26%;" />
                    <col style="width:19%;" />
                    <col style="width:19%;" />
                    <col style="width:19%;" />
                    <col style="width:13%;" />
                </colgroup>
                <thead>
                    <tr>
                        <th>
                            שם
                        </th>
                        <th>
                            האם זמין לרכישה באתר?
                        </th>
                        <th>
                            האם נכלל במבצעים?
                        </th>
                        <th>
                            מחיר
                        </th>
                        <th>
סטטוס
                        </th>
                    </tr>
                </thead>
                <?php foreach($special_addson as $special_addon): ?>
                    <?php if(!$special_addon->isApproved()): ?>
                        <?php $disabled = "disabled"; ?>
                    <?php endif; ?>
                <tr special-addon-id = "<?php echo $special_addon->getID(); ?>" 
                    class = 'special-addon-row <?php echo $disabled; ?>' >
                    <td class="product-name">
                        <?php echo $special_addon->getName(); ?>
                    </td>
                    <td class="align-center">
                        <?php if($special_addon->inStock()): ?>
                            <?php $checked = "checked"; ?>
                        <?php else: ?>
                            <?php $checked = ""; ?>
                        <?php endif; ?>
                        <input type="checkbox" 
                               name="hotPeppers" 
                               id="in_stock_<?php echo $special_addon->getID(); ?>" 
                               <?php echo $checked; ?> />
                    </td>
                    <td class="align-center">
                        <?php if($special_addon->inBundles()): ?>
                            <?php $checked = "checked"; ?>
                        <?php else: ?>
                            <?php $checked = ""; ?>
                        <?php endif; ?>
                        <input type="checkbox" 
                               name="hotPeppers2" 
                               id="in_bundles_<?php echo $special_addon->getID(); ?>" 
                               <?php echo $checked; ?> />
                    </td>
                    <td class="align-center">
                        <input type="text" 
                               name="hot_peppers" 
                               id="price_<?php echo $special_addon->getID(); ?>" 
                               maxlength="5" 
                               value = "<?php echo $special_addon->getPrice(); ?>" />
                        &#8362;
                    </td>
                    <td class="align-center">
                    <?php if($special_addon->isApproved() == 1): ?>
                        <?php echo "ממתין לאישור"; ?>
                    <?php elseif($special_addon->isApproved() == 2): ?>
                        <?php echo "אושר"; ?>
                    <?php else: ?>
                        <?php echo "לא אושר"; ?>
                    <?php endif; ?>
                    </td>
                </tr>
                <?php endforeach; ?>
                
            </table>
            <div class="clearfix">
                <button type="submit" class="list-save" id = "list-save-special-addson">שמירה</button>
                <img src = "<?php echo LOADING_GIF ?>" class = "save-area-animation-absolute" id = "update-special-addon-button" />
            </div>
        </form>
    </div>

<?php
get_footer('vendor');
?>
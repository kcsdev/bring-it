<?php


function the_hp_title(){ 
    if(isset($_GET['pizzeria'])){ 
        
        $vendor = get_the_vendor($_GET['pizzeria']);
//        var_dump($vendor);
        echo "<div class = 'hp-vendor-title'>";
        echo $vendor->name;
        echo "</div>";
    }
}


/***************************************************************************/


function the_homepage_pizzerias(){
    $result = '';
    $vendors = Vendor::getVendors();
    foreach($vendors as $vendor) { 
        $x = rand(0,5);
        $y = 5 - $x;
        $time = rand(15,60);
        $result .= '<a href="?pizzeria='.$vendor->getID().'">';
        $result .= '<div class="pz-box">';
        $result .= '<div class="photo">';
//        $result .= '<img src="' . get_stylesheet_directory_uri() . '/assets/temp/pz-box-01.png" alt="" />';
        $result .= '<img src="'.$vendor->getLogo().'" alt="" />';
        $result .= '</div>';
        $result .= '<div class="details">';
        $result .= '<p class="name">'.$vendor->getName().'</p>';
        $result .= '<div class="row">';
        $result .= 'משלוח ';
        $result .= '<span>';
        $result .= get_rate_stars($x, $y);
        $result .= '</span>';
        $result .= '</div>';
        $result .= '<div class="row">';
        $result .= 'טעם ';
        $result .= '<span>';
        $result .= get_rate_stars($y, $x);
        $result .= '</span>';
        $result .= '</div>';
        $result .= '</div>';
        $result .= '<div class="time">';
        $result .= '<span class="one">זמן משלוח</span>';
        $result .= '<span class="two">'.$vendor->getShippingTime().'</span>';
        $result .= '<span class="three">דק\'</span>';
        $result .= '</div>';
        $result .= '</div>';
        $result .= '</a>';
    }
    echo $result;
}

?>
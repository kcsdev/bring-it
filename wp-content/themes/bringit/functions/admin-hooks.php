<?php
add_action( 'admin_bar_menu', 'vendorDashboard', 999 );

function vendorDashboard( $wp_admin_bar ) {
	$args = array(
		'id'    => 'vendor_dashboard',
		'title' => 'Vendor Dashboard',
		'href'  => 'http://new-bringit.c14.co.il/vendor-dashboard/',
		'meta'  => array( 'class' => 'my-toolbar-page' )
	);
	$wp_admin_bar->add_node( $args );
}

//add_action( 'admin_bar_menu', 'toolbar_link_to_mypage', 999 );
//
//function toolbar_link_to_mypage( $wp_admin_bar ) {
//	$args = array(
//		'id'    => 'my_page',
//		'title' => 'My Page',
//		'href'  => 'http://mysite.com/my-page/',
//		'meta'  => array( 'class' => 'my-toolbar-page' )
//	);
//	$wp_admin_bar->add_node( $args );
//}
?>
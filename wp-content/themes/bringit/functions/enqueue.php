<?php

function enqueue_js(){
//    wp_enqueue_script( 'bundle-mode-js', '/wp-content/themes/bringit/js/bundle/bundle.js', array("jquery"), '1.0.0', true );
    wp_enqueue_script( 'drink-js', '/wp-content/themes/bringit/js/drink/drink.js', array("jquery"), '1.0.0', true );
    wp_enqueue_script( 'pizzerias-checkout', '/wp-content/themes/bringit/js/pizzerias-checkout.js', array("jquery"), '1.0.0', true );
    wp_enqueue_script( 'page-make-pizza', '/wp-content/themes/bringit/js/page-make-pizza.js', array("jquery"), '1.0.0', true );
//    wp_enqueue_script( 'vendor-dashboard-shipping-regions', '/wp-content/themes/bringit/js/vendor-dashboard/shipping-regions.js', array("jquery"), '1.0.0', true );
    wp_enqueue_script( 'page-pizzerias', '/wp-content/themes/bringit/js/page-pizzerias.js', array("jquery"), '1.0.0', true );
    wp_enqueue_script( 'payment-type', '/wp-content/themes/bringit/js/payment-type.js', array("jquery"), '1.0.0', true );
    wp_enqueue_script( 'jqueryui-combobox', '/wp-content/themes/bringit/js/libs/jquery.ui.combobox.js', array("jquery"), '1.0.0', true );
    wp_enqueue_script( 'page-homepage', '/wp-content/themes/bringit/js/page-homepage.js', array("jquery"), '1.0.0', true );
}

add_action("wp_enqueue_scripts","enqueue_js");

function admin_enqueue_js(){ 
    wp_enqueue_script( 'jquery-ui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css', array("jquery"), '1.0.0', true );
}

function app_js_libs() { 
    wp_enqueue_script( 'jquery-ui', '//code.jquery.com/ui/1.11.4/jquery-ui.js' );
    wp_enqueue_script( 'jquery-ui-core', '/wp-includes/js/jquery/ui/core.min.js' );
    wp_enqueue_script( 'jquery-ui-position', '/wp-includes/js/jquery/ui/position.min.js' );
    wp_enqueue_script( 'jquery-ui-tooltip', '/wp-includes/js/jquery/ui/tooltip.min.js' );
}
add_action( 'wp_enqueue_scripts', 'app_js_libs' );
?>

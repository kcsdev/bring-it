<?php
function get_pb_addons($category){ // Return list of addons by the category defined; Only for Pizza Builder Page;
    $args = array(
        "post_type" =>  "addon",
        "status"    =>  "publish",
        "addon_cat" =>  $category
    );
    $addons = get_posts($args);
    return $addons;
}
//-----------------------------------------------------------------

function render_selection_group($component_name,$component_id,$component_title) { 
    $locations = array(
        'whole',
        'r-half',
        'l-half'
    );
    $quarters = array(
        'se',
        'sw',
        'ne',
        'nw'
    );
    echo '<ul class="selection" >';
    //
    foreach($locations as $location){
        echo '<li>';
        
        echo '<input ' .
            'id = "component-selection"' .
            'addon-WPID="'.$component_id.'" ' .
            'addon-name="'.$component_title.'" ' .
            'type="radio" ' .
            'class="component-selection component-selection-' . $component_name . '" ' .
            'data-component="' . $component_name . '" ' .
            'data-layer-name="'. $location .'_' . $component_name . '" ' .
            'name="selection_' . $component_name . '" ' .
            'addon-location="'. $location .'" ' .
            'data-waschecked="false" ' .
            'data-checked=false quarter=false />';
        
        echo '<label class="'. $location .'"></label>';
        
        echo '</li>';
    }
    echo '<li>';
    echo '<label class="quarter quarter-select quarter-selection_' . $component_name . '" data-selection-quarter="' . $component_name . '"></label>';
    echo '</li>';
    foreach($quarters as $quarter){
        echo '<li class="quarters-group quarters-visibility quarter-' . $component_name . ' quarters-start">';
        echo '<input ' .
            'addon-WPID="'.$component_id.'" ' .
            'addon-name="'.$component_name.'" ' . 
            'type="checkbox" ' .
            'id = "component-selection"' .
            'class="component-selection-qgroup quarters-group_' . $component_name . '" ' .
            'data-component="' . $component_name . '" ' .
            'data-layer-name="'. $quarter .'_' . $component_name . '" ' .
            'name="selection_' . $component_name . '" ' .
            'addon-location = "quarter-'. $quarter .'" ' .
            'data-waschecked="false" ' .
            'data-checked=false ' .
            'quarter=true />';
        
        echo '<label class="quarter-'. $quarter .'"></label>';
        echo '</li>';
    } 
    echo '</ul>';
} 
?>
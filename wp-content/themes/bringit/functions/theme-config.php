<?php 

define("VENDOR_REGIONS_TABLE","vendor_regions");
define("VENDOR_CITIES_TABLE","vendor_cities");
define("VENDOR_BUNDLES_TABLE","vendor_bundles");
define("VENDOR_DRINKS_TABLE","vendor_drinks");
define("VENDOR_ADDONS_TABLE","vendor_addons");
define("VENDOR_SPECIAL_ADDSON_TABLE","vendor_special_addson");
define("VENDOR_ADDITIONAL_PRODUCTS_TABLE","vendor_additional_products");
define("VENDOR_HOURS_TABLE","vendor_hours");
define("VENDOR_LOGOS_TABLE","vendor_logos");
define("VENDOR_PIZZA_PRICE_TABLE","vendor_pizza_price");

define("BRINGIT_CITIES","bi_cities");
define("BRINGIT_ORDERS","bi_orders");

define("LOADING_GIF","/wp-content/themes/bringit/assets/gif/gears.gif");
define("DEFAULT_VENDOR_LOGO_PATH", "/wp-content/themes/bringit/assets/bringit-top-logo.png");
?>
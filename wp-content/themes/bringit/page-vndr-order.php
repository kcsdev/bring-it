<?php
get_header('vendor');

$stat = false;
if(isset($_GET) && !empty($_GET)){
    $stat = true;
}
?>
<?php if(isset($_GET["order_id"])): ?>
    <?php $order = new Order($_GET["order_id"]); ?>
<?php endif; ?>
    <div class="h-section down-space">
        <ul class="breadcrumbs">
            <!--<li><a>הזמנות</a></li>
            <li><a>הזמנות חדשות</a></li>-->
            <?php if($stat): ?>
                <li><a href="/vndr-stat-orders/">הזמנות שלי</a></li>
            <?php endif; ?>
            <li class="current">
                <?php if(is_object($order)): ?>
                <a>הזמנה מס' <?php echo $order->getID(); ?>, 18:28, רח' <?php echo $order->getStreet(); ?> 125, <?php echo $order->getCity(); ?></a>
                <?php endif;?>
            </li>
        </ul>
    </div>

    <div class="h-section content-pane down-space">
        <ul class="coupon-for-client">
            <li>
                <a class="jq-coupon">
                    תן ללקוח זה קופון אישי לקניה הבאה
                </a>
                <ul class="jq-coupon-list">
                    <li>
                        <label for="addOn_one">
                            <input type="checkbox" name="addOn_one" id="addOn_one" />
לחם שום
                        </label>
                    </li>
                    <li>
                        <label for="addOn_two">
                            <input type="checkbox" name="addOn_two" id="addOn_two" />
סלט ירקות
                        </label>
                    </li>
                    <li>
                        <label for="addOn_three">
                            <input type="checkbox" name="addOn_three" id="addOn_three" />
                            רוטב חריף
                        </label>
                    </li>
                    <li class="btn-save"><button>אישור</button></li>
                </ul>
            </li>
        </ul>

        <ul class="order-details clearfix">
            <li>
                <h1 class="content">פרטי לקוח</h1>
                <dl>
                    <dt>שם לקוח</dt>
                    <dd><?php echo $order->getFirstName() . " " . $order->getLastName(); ?></dd>

                    <dt>כתובת</dt>
                    <dd>
                        <?php if(is_object($order)): ?>
                        רח' <?php echo $order->getStreet();?>
                        <?php echo $order->getBuildingNumber(); ?>
                        דירה
                        <?php echo $order->getApartmentNumber(); ?>
                        <?php endif; ?>
                    </dd>

                    <dt>טלפון</dt>
                    <dd><?php echo $order->getPhone(); ?></dd>

                    <dt>הערות לשליח</dt>
                    <dd>
                        <?php echo $order->getCourierComment(); ?>
                    </dd>
                </dl>
            </li>
            <li>
                <h1 class="content">פרטי הזמנה</h1>
                <dl>
                    <dt>מס' הזמנה</dt>
                    <dd>
                        <?php if(is_object($order)): ?>
                        <?php echo $order->getID(); ?>
                        &mdash;
                        <spam class="red bold">ממתינה לאישור</spam>
                        <?php endif; ?>
                    </dd>

                    <dt>תאריך/שעה</dt>
                    
                    <dd>
                        <?php if(is_object($order)): ?>
                        <?php echo $order->getOrderDate();?>
                        <?php endif; ?>
                    </dd>

                    <dt>שם פריט</dt>
                    <dd></dd>

                    <dt>תוספות</dt>
                    <dd>
                        <?php //$pizzas = $order->getPizzas(); ?>
                        <?php //$drinks = $order->getDrinks(); ?>
                        <?php //$additional_products = $order->getAdditionalProducts(); ?>
                        <?php //foreach($pizzas as $pizza): ?>
                        <?php //endforeach; ?>
                    </dd>
                </dl>
            </li>
        </ul>
        <?php if(!$stat) { ?>
        
        <div class="pizza-progress clearfix">
            <button class="confirm">אישור קבלת ההזמנה</button>
            <button class="en-route">השליח יצא</button>

            <ul>
                <li>
                    <span class="top"></span>
                    <span class="row confirm done">קבלה</span>
                    <?php if(is_object($order)): ?>
                    <span class="time-line"><?php echo $order->getTime();?></span>
                    <?php endif; ?>
                </li>
                <li>
                    <span class="top">&nbsp;</span>
                    <span class="row prep">הכנה</span>
                    <span class="time-line">14:33</span>
                </li>
                <li>
                    <span class="top">&nbsp;</span>
                    <span class="row oven">תנור</span>
                    <span class="time-line">14:48</span>
                </li>
                <li>
                    <span class="top"></span>
                    <span class="row delivery"></span>
                    <span class="time-line"></span>
                </li>
            </ul>
        </div>
        <?php } ?>
    </div>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $(".jq-coupon").bind("click", function(){
            $(".jq-coupon-list").toggle(200);
        });
    });
</script>
<?php
get_footer('vendor');
?>
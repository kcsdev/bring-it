<?php
/**
Template Name: Purchase Address List page
 */
$customer = new Customer(); 
$orders = $customer->getOrders();
get_header('blank'); ?>
<?php //var_dump($customer); ?>
    <div class="bi-section">
        <a class = "close-window" onclick="parent.closeMe();"></a>
        <div class="bi-container addr-list clearfix">

            <p class="pizzeria-name">היסטוריית הזמנות</p>

            <div class="list-title"></div>

            <table class = "order-list-table">
                <tr>
                    <th>מספר הזמנה</th>
                    <th>תאריך</th>
                    <th>שם</th>
                    <th>כתובת</th>
                    <th>סכום</th>
                </tr>
                <?php foreach($orders as $order): ?>
                
                <tr>
                    <td>
                        <a href = "/customer-order-details?order_id=<?php echo $order->getID(); ?>" style = "color:#fff">
                            <?php echo $order->getID(); ?>
                        </a>
                    </td>
                    
                    <td><?php echo $order->getOrderDate(); ?></td>
                    <td><?php echo $order->getFirstName() . " " . $order->getLastName(); ?></td>
                    <td><?php echo $order->getShippingAddress(); ?></td>
                    <td><?php echo $order->getOrderCost(); ?></td>
                    
                </tr>
                <?php endforeach; ?>
                
            </table>

        </div>
    </div>

<script type="text/javascript">
    jQuery(document).ready(function($){
       
    });
</script>
<?php get_footer('blank'); ?>
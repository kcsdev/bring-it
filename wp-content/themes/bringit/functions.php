<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 28/05/15
 * Time: 8:50 PM
 */
// Configurations
require_once("functions/theme-config.php");
// Post Types
require_once('post-types/addon.php');
require_once('post-types/canned-drink.php');
require_once('post-types/bottled-drink.php');
require_once('post-types/bundle.php');
require_once('post-types/additional-product.php');
require_once('post-types/special-drinks.php');
require_once('post-types/special-addons.php');
// Classes
require_once('classes/additional-products.php'); // NOTE Deprecated. Delete when replaced.
require_once('classes/additional-product.php');
require_once('classes/addon.php');
require_once('classes/address.php');
require_once('classes/auth.php');
require_once('classes/bundle.php');
require_once('classes/cart.php');
require_once('classes/customer.php');
require_once('classes/drink.php');
require_once('classes/order.php');
require_once('classes/pizza.php');
require_once('classes/secure.php');
require_once('classes/user.php');
require_once('classes/vendor.php');
require_once('classes/addon-client.php');
require_once('classes/db.php');
require_once('classes/day.php');
require_once('classes/error.php');
// Ajax
require_once('ajax/order.php');
require_once('ajax/auth.php');
require_once('ajax/cart.php');
require_once('ajax/customer.php');
require_once('ajax/vendor.php');
require_once('ajax/bundle.php');
require_once('ajax/address.php');
// Functions Libraries
require_once('app.php');
require_once('functions/homepage.php');
require_once('functions/make-pizza.php');
require_once('functions/global.php');
// Hooks
require_once('functions/enqueue.php');
require_once('functions/admin-hooks.php');



//var_dump($_SESSION);
add_filter('show_admin_bar', '__return_false');
?>
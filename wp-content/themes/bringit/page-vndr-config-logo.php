<?php
get_header('vendor');
?>
    <div class="h-section down-space">
        <ul class="breadcrumbs">
            <li><a>הגדרות</a></li>
            <li class="current"><a>לוגו אישי</a></li>
        </ul>
    </div>

    <div class="h-section content-pane down-space" id = "vndr-config-logo-content">
        <div id ="vndr-config-logo-content-inner" >
            <form method = "post"  enctype="multipart/form-data" action = "">
                <input type = "file" name="upload-img" />
                <button type = "submit" id = "upload-img" >Upload</button>
            </form>
            <div id = "response">
            <?php if(isset($_FILES['upload-img'])): ?>
                <?php echo "CHECK"; ?>
                <?php $vendor->setLogo(); ?>
            <?php endif; ?>
            </div>
            <div id = "uploaded-img"></div>
        </div>
    </div>
<script type="text/javascript">
    jQuery(document).ready(function($){
    });
</script>
<?php
get_footer('vendor');
?>
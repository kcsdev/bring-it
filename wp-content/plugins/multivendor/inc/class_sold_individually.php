<?php

class FPMultiVendorSoldIndividually {

    public function __construct() {
        if (get_option('fpmv_vendor_sell_individually') == 'yes') {
            add_filter('woocommerce_add_to_cart_validation', array($this, 'limit_single_purchase_in_cart'), 10, 5);
        }
    }

    public static function limit_single_purchase_in_cart($passed, $product_id, $quantity, $variation_id = '', $variations = '') {
        global $woocommerce;
        $getproductid = $product_id;

        $currentvendorlist = array();
        $cartvendorlists = array();
        $cart_contents_count = $woocommerce->cart->cart_contents_count;
        $currentproductidterm = wp_get_post_terms($product_id, 'multi_vendor', array("fields" => "all"));
        $countofcurrentproductterm = count($currentproductidterm);
        for ($i = 0; $i < $countofcurrentproductterm; $i++) {
            //var_dump($currentproductidterm[$i]->term_id);
            $currentvendorlist[] = $currentproductidterm[$i]->term_id;
        }
        $cart_contents = $woocommerce->cart->cart_contents;

        // Add Only Multi Vendor Product to Cart
        if (isset($cart_contents)) {
            foreach ($cart_contents as $each_contents) {
                $getproductidcart = $each_contents['product_id'];
                $cartvendorproducts = wp_get_post_terms($getproductidcart, 'multi_vendor', array('fields' => 'all'));
                $countofcartvendorproducts = count($cartvendorproducts);
                for ($j = 0; $j < $countofcartvendorproducts; $j++) {
                    $cartvendorlists[] = $cartvendorproducts[$j]->term_id;
                }
            }
        }

        $vendor_intersect = array_intersect((array) $currentvendorlist, (array) $cartvendorlists);
        // var_dump($vendor_intersect);
        //  var_dump(count($vendor_intersect));
        if (get_option('fpmv_vendor_restriction_type') == '2') {
            if ($cart_contents_count > 0) {

                //  var_dump($cartvendorlists);
                //var_dump($vendor_intersect);

                if (count($vendor_intersect) > 0) {
                    $passed = true;
                } else {
                    //  var_dump($vendor_intersect);
                    //wc_add_notice(__('google', 'multivendor'), 'error');
                    wc_add_notice(__(get_option('fpmv_error_message_for_vendor_level'), 'multivendor'), 'error');
                    $passed = false;
                }
                //var_dump($vendor_intersect);
            } else {
                $passed = true;
            }
        }

        // One Product Per Order
        if (get_option('fpmv_vendor_restriction_type') == 1) {

            if ($cart_contents_count > 0) {
                $woocommerce->cart->empty_cart();
                wc_add_notice(__(get_option('fpmv_error_message_for_product_level'), 'multivendor'), 'error');
                $passed = true;
            }
        }
        return $passed;
    }

}

new FPMultiVendorSoldIndividually();

<?php

/* Gravity Tab Settings */

class FPMultiVendorGravityTab {

    function __construct() {
        if (is_plugin_active('gravityforms/gravityforms.php')) {
            add_action('admin_init', array($this, 'multivendor_add_default_values_settings'));
            add_action('woocommerce_mv_settings_tabs_multivendor_gravity', array($this, 'multivendor_register_admin_settings'));
            add_action('woocommerce_update_options_multivendor_gravity', array($this, 'multivendor_update_values_settings'));
            add_filter('woocommerce_mv_settings_tabs_array', array($this, 'multivendor_messages_tab'), 100);
            add_action('gform_after_submission_' . get_option('fpmv_frontend_gravity_id'), array($this, 'process_gravity_vendor_application'), 10, 2);
        }
    }

    public static function multivendor_messages_tab($settings_tabs) {
        $settings_tabs['multivendor_gravity'] = __('Gravity', 'multivendor');
        return $settings_tabs;
    }

    public static function multivendor_gravity_admin_settings() {
        global $woocommerce;
        return apply_filters('woocommerce_multivendor_gravity', array(
            array(
                'name' => __('Gravity Settings', 'multivendor'),
                'type' => 'title',
                'id' => '_mv_title_gravity_settings'
            ),
            array(
                'name' => __('Gravity Form ID', 'multivendor'),
                'desc' => __('Customize Vendor Name in Front End Vendor Application', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_frontend_gravity_id',
                'std' => '',
                'type' => 'text',
                'newids' => 'fpmv_frontend_gravity_id',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Vendor Name Field ID', 'multivendor'),
                'desc' => __('Customize Vendor Name in Front End Vendor Application', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_frontend_gravity_vendor_name',
                'std' => '',
                'type' => 'text',
                'newids' => 'fpmv_frontend_gravity_vendor_name',
                'desc_tip' => true,
            ),
            array(
                'name' => __('About Vendor Field ID', 'multivendor'),
                'desc' => __('Customize About Vendor in Front End Vendor Application', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_frontend_gravity_about_vendor',
                'std' => '',
                'type' => 'text',
                'newids' => 'fpmv_frontend_gravity_about_vendor',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Message to Site Admin Field ID', 'multivendor'),
                'desc' => __('Customize Message to Site Admin in Front End Vendor Application', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_frontend_gravity_message_to_site_admin',
                'std' => '',
                'type' => 'text',
                'newids' => 'fpmv_frontend_gravity_message_to_site_admin',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Vendor Commission Rate Field ID', 'multivendor'),
                'desc' => __('Customize Vendor Commission Rate in Front End Vendor Application', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_frontend_gravity_vendor_commission_rate',
                'std' => '',
                'type' => 'text',
                'newids' => 'fpmv_frontend_gravity_vendor_commission_rate',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Vendor Payment Method Field ID', 'multivendor'),
                'desc' => __('Customize Vendor Payment Method Caption in Front End Vendor Application', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_frontend_gravity_vendor_payment_method',
                'std' => '',
                'type' => 'text',
                'newids' => 'fpmv_frontend_gravity_vendor_payment_method',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Vendor Paypal Address Field ID', 'multivendor'),
                'desc' => __('Customize Vendor Paypal Address in Front End Vendor Application', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_frontend_gravity_vendor_one_custom_paypal_address',
                'std' => '',
                'type' => 'text',
                'newids' => 'fpmv_frontend_gravity_vendor_one_custom_paypal_address',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Vendor Custom Payment Field ID', 'multivendor'),
                'desc' => __('Customize Vendor Custom Payment in Front End Vendor Application', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_frontend_gravity_vendor_custom_more_payment',
                'std' => '',
                'type' => 'text',
                'newids' => 'fpmv_frontend_gravity_vendor_custom_more_payment',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Vendor Logo/Shop Logo Field ID', 'multivendor'),
                'desc' => __('Customize Vendor Logo/Shop Logo Label in Front End Vendor Application', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_gravity_vendor_application_logo',
                'std' => '',
                'type' => 'text',
                'newids' => 'fpmv_gravity_vendor_application_logo',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Application Status Shortcode Gravity', 'multivendor'),
                'type' => 'title',
                'id' => '_mv_title_application_gravity_settings'
            ),
            array('type' => 'sectionend'),
        ));
    }

    public static function multivendor_register_admin_settings() {
        woocommerce_admin_fields(FPMultiVendorGravityTab::multivendor_gravity_admin_settings());
    }

    public static function multivendor_update_values_settings() {
        woocommerce_update_options(FPMultiVendorGravityTab::multivendor_gravity_admin_settings());
    }

    public static function multivendor_add_default_values_settings() {
        foreach (FPMultiVendorGravityTab::multivendor_gravity_admin_settings() as $settings) {
            if ((isset($settings['newids'])) && (isset($settings['std']))) {

                add_option($settings['newids'], $settings['std']);
            }
        }
    }

     public static function process_gravity_vendor_application($entry, $form) {
        global $wpdb;

        require_once(ABSPATH . "wp-admin" . '/includes/image.php');
        require_once(ABSPATH . "wp-admin" . '/includes/file.php');
        require_once(ABSPATH . "wp-admin" . '/includes/media.php');

        $vendorname = $entry[get_option('fpmv_frontend_gravity_vendor_name')];
        $aboutvendor = $entry[get_option('fpmv_frontend_gravity_about_vendor')];
        $messagevendor = $entry[get_option('fpmv_frontend_gravity_message_to_site_admin')];
        $vendorcommissionrate = $entry[get_option('fpmv_frontend_gravity_vendor_commission_rate')];
        $vendorpaymentoption = $entry[get_option('fpmv_frontend_gravity_vendor_payment_method')];
        $vendorcustompaymentvalue = $entry[get_option('fpmv_frontend_gravity_vendor_custom_more_payment')];
        $vendorpaypal = $entry[get_option('fpmv_frontend_gravity_vendor_one_custom_paypal_address')];
        $urllogo = $entry[get_option('fpmv_gravity_vendor_application_logo')];

        // update_option('mylogo', esc_url($urllogo));
        preg_match('/[^\?]+\.(jpg|jpe|jpeg|gif|png)/i', $urllogo, $matches);
        $url = $matches[0];
        $tmp = download_url($url);

        $post_id = 0;
        $desc = "Vendor Shop Logo";
        $file_array = array();
        $file_array['name'] = basename($matches[0]);
        $file_array['tmp_name'] = $tmp;


        if (is_wp_error($tmp)) {
            @unlink($file_array['tmp_name']);
            $file_array['tmp_name'] = '';
        }


        $attach_id = media_handle_sideload($file_array, 1);

        //var_dump($attach_id);

        if (is_wp_error($attach_id)) {
            @unlink($file_array['tmp_name']);
            $attach_id = '';
        }
        $vendoradmin = get_current_user_id();
        $username = get_userdata($vendoradmin);
        $setusernickname = $username->user_login;
        $table_name = $wpdb->prefix . "multi_vendor_submit_application";




        if (get_option('fpmv_approval_process_multi_vendor') == '1') {

            $vendorslugname = strtolower(str_replace(' ', '-', $vendorname));
            $testingterm = wp_insert_term(
                    "$vendorname", 'multi_vendor', array(
                'description' => $aboutvendor,
                'slug' => $vendorslugname,
            ));
            $vendoruser = new WP_User($vendoradmin);
            if (!empty($vendoruser->roles) && is_array($vendoruser->roles)) {

                foreach ($vendoruser->roles as $role) {
                    add_user_meta($vendoruser->ID, 'multivendor_backuprole', $role);
                    $exceptroles = array('administrator', 'shop_manager');
                    if (!in_array($role, $exceptroles)) {
                        $vendoruser->remove_role($role);
                        $vendoruser->add_role('multi_vendor');
                    }
                }
            }

            if (get_option('fpmv_vendor_app_approval_email_enable') == 'yes') {
                $mail_list = $vendoradmin;
                $status = 'Approved';
                include 'application_status_mail.php';
            }
            if (get_option('fpmv_vendor_app_approval_email_enable_admin') == 'yes') {

                $status = 'Approved';
                $getusernamebyid = get_user_by('id', $vendoradmin);
                $newvendorname = $getusernamebyid->user_login;
                $newvendorapplication = $vendorname;
                include 'application_status_mail_admin.php';
            }

            update_woocommerce_term_meta($testingterm['term_id'], 'thumbnail_id', $attach_id);
            update_woocommerce_term_meta($testingterm['term_id'], 'fp_mv_vendor_admins', serialize(array($vendoradmin)));
            update_woocommerce_term_meta($testingterm['term_id'], 'fp_mv_commissions_rate', $vendorcommissionrate);
            update_woocommerce_term_meta($testingterm['term_id'], 'mv_paypal_email', $vendorpaypal);
            update_woocommerce_term_meta($testingterm['term_id'], 'mv_payment_option', $vendorpaymentoption);
            update_woocommerce_term_meta($testingterm['term_id'], 'mv_custom_payment', $vendorcustompaymentvalue);
            $wpdb->insert($table_name, array('setvendorname' => $vendorname, 'setmessagetoreviewer' => $messagevendor, 'setvendordescription' => $aboutvendor, 'setcreatedvendorid' => $testingterm['term_id'], 'setvendorcustompayment' => $vendorcustompaymentvalue, 'vendorthumbnail' => $attach_id, 'setvendorpaymentoption' => $vendorpaymentoption, 'setusernickname' => $setusernickname, 'setvendorcommission' => $vendorcommissionrate, 'setvendorpaypalemail' => $vendorpaypal, 'setvendoradmins' => $vendoradmin, 'setvendordescription' => $aboutvendor, 'status' => get_option("fpmv_approval_process_multi_vendor") == "1" ? 'Approved' : 'Pending', 'date' => date('Y-m-d H:i:s')));
        } else {
            $wpdb->insert($table_name, array('setvendorname' => $vendorname, 'setmessagetoreviewer' => $messagevendor, 'setvendordescription' => $aboutvendor, 'setcreatedvendorid' => '0', 'setusernickname' => $setusernickname, 'setvendorcommission' => $vendorcommissionrate, 'vendorthumbnail' => $attach_id, 'setvendorcustompayment' => $vendorcustompaymentvalue, 'setvendorpaymentoption' => $vendorpaymentoption, 'setvendorpaypalemail' => $vendorpaypal, 'setvendoradmins' => $vendoradmin, 'setvendordescription' => $aboutvendor, 'status' => get_option("fpmv_approval_process_multi_vendor") == "1" ? 'Approved' : 'Pending', 'date' => date('Y-m-d H:i:s')));
        }

        if (get_option('fpmv_vendor_app_pending_email_enable') == 'yes') {
            $mail_list = $vendoradmin;
            $status = 'Pending';

            include 'application_status_mail.php';
        }
        if (get_option('fpmv_vendor_app_pending_email_enable_admin') == 'yes') {
            $status = 'Pending';
            $getusernamebyid = get_user_by('id', $vendoradmin);
            $newvendorname = $getusernamebyid->user_login;
            $newvendorapplication = $vendorname;
            include 'application_status_mail_admin.php';
        }
    }


}

new FPMultiVendorGravityTab();

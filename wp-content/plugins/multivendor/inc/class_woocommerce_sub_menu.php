<?php

class FPMultiVendorSubMenu {

    public function __construct() {
        add_action('admin_menu', array($this, 'add_sub_menu_to_woocommerce'));
        include_once('class_general_tab.php');
        include_once('class_single_product_page.php');
        include_once('class_my_account_tab.php');
        include_once('class_cart.php');
        include_once('class_vendor_capabilities.php');
        include_once('class_multi_vendor_status.php');
        include_once('class_shortcode.php');
        include_once('class_email_tab.php');
        include_once('class_vendor_applications_tab.php');
        include_once('class_commissions_tab.php');
        include_once('class_vendors_tab.php');
        include_once('class_reports_tab.php');
        include_once('class_messages.php');
        include_once('class_localization.php');
        include_once('class_gravity.php');
    }

    public static function add_sub_menu_to_woocommerce() {
        add_submenu_page('woocommerce', __('Socio Multi Vendor', 'multivendor'), __('Socio Multi Vendor', 'multivendor'), 'manage_options', 'multivendor_menu', array('FPMultiVendorSubMenu', 'my_custom_submenu_page_callback'));
    }

    public static function my_custom_submenu_page_callback() {
        global $woocommerce, $woocommerce_settings, $current_section, $current_tab;
        $tabs = "";
        do_action('woocommerce_mv_settings_start');
        $current_tab = ( empty($_GET['tab']) ) ? 'multivendor' : sanitize_text_field(urldecode($_GET['tab']));
        $current_section = ( empty($_REQUEST['section']) ) ? '' : sanitize_text_field(urldecode($_REQUEST['section']));
        if (!empty($_POST['save'])) {
            if (empty($_REQUEST['_wpnonce']) || !wp_verify_nonce($_REQUEST['_wpnonce'], 'woocommerce-settings'))
                die(__('Action failed. Please refresh the page and retry.', 'galaxyfunder'));

            if (!$current_section) {
//include_once('settings/settings-save.php');
                switch ($current_tab) {
                    default :
                        if (isset($woocommerce_settings[$current_tab]))
                            woocommerce_update_options($woocommerce_settings[$current_tab]);

// Trigger action for tab
                        do_action('woocommerce_update_options_' . $current_tab);
                        break;
                }

                do_action('woocommerce_update_options');

// Handle Colour Settings
                if ($current_tab == 'general' && get_option('woocommerce_frontend_css') == 'yes') {

                }
            } else {
// Save section onlys
                do_action('woocommerce_update_options_' . $current_tab . '_' . $current_section);
            }

// Clear any unwanted data
//$woocommerce->clear_product_transients();
            delete_transient('woocommerce_cache_excluded_uris');
// Redirect back to the settings page
            $redirect = esc_url_raw(add_query_arg(array('saved' => 'true')));
//  $redirect .= add_query_arg('noheader', 'true');

            if (isset($_POST['subtab'])) {
                wp_safe_redirect($redirect);
                exit;
            }
        }
// Get any returned messages
        $error = ( empty($_GET['wc_error']) ) ? '' : urldecode(stripslashes($_GET['wc_error']));
        $message = ( empty($_GET['wc_message']) ) ? '' : urldecode(stripslashes($_GET['wc_message']));

        if ($error || $message) {

            if ($error) {
                echo '<div id="message" class="error fade"><p><strong>' . esc_html($error) . '</strong></p></div>';
            } else {
                echo '<div id="message" class="updated fade"><p><strong>' . esc_html($message) . '</strong></p></div>';
            }
        } elseif (!empty($_GET['saved'])) {

            echo '<div id="message" class="updated fade"><p><strong>' . __('Your settings have been saved.', 'galaxyfunder') . '</strong></p></div>';
        }
        ?>
        <div class="wrap woocommerce">
            <form method="post" id="mainform" action="" enctype="multipart/form-data">
                <div class="icon32 icon32-woocommerce-settings" id="icon-woocommerce"><br /></div><h2 class="nav-tab-wrapper woo-nav-tab-wrapper">
                    <?php
                    $tabs = apply_filters('woocommerce_mv_settings_tabs_array', $tabs);

                    foreach ($tabs as $name => $label) {
                        // echo $current_tab;
                        echo '<a href="' . admin_url('admin.php?page=multivendor_menu&tab=' . $name) . '" class="nav-tab ';
                        if ($current_tab == $name)
                            echo 'nav-tab-active';
                        echo '">' . $label . '</a>';
                    }
                    do_action('woocommerce_mv_settings_tabs');
                    ?>
                </h2>

                <?php
                switch ($current_tab) :
                    case "crowdfunding_listtable" :
                        //      CrowdFunding::crowdfunding_adminpage();
                        break;
                    default :
                        do_action('woocommerce_mv_settings_tabs_' . $current_tab);
                        break;
                endswitch;
                ?>

                <p class="submit">
                    <?php if (!isset($GLOBALS['hide_save_button'])) : ?>
                        <input name="save" class="button-primary" type="submit" value="<?php _e('Save changes', 'woocommerce'); ?>" />

                    <?php endif; ?>
                    <input type="hidden" name="subtab" id="last_tab" />
                    <?php wp_nonce_field('woocommerce-settings', '_wpnonce', true, true); ?>
                </p>

            </form>
            <form method="post" id="mainforms" action="" enctype="multipart/form-data" style="float: left; margin-top: -52px; margin-left: 159px;">
                <input name="reset" class="button-secondary" type="submit" value="<?php _e('Reset', 'woocommerce'); ?>"/>
                <?php wp_nonce_field('woocommerce-reset_mv_settings', '_wpnonce', true, true); ?>
            </form>

        </div>
        <?php
    }

}

new FPMultiVendorSubMenu();

<?php

/*
 * Restriction for Media Files with User Role of Multi Vendor
 *
 */

class FPRestrictMediaFiles {

    public function __construct() {
        add_action('pre_get_posts', array($this, 'show_uploaded_files_from_multivendor'));
        add_filter('ajax_query_attachments_args', array($this, 'show_uploaded_files_from_multivendor_ajax'));
    }

    function show_uploaded_files_from_multivendor($wp_query) {
        global $current_user, $pagenow, $wpdb, $current_screen;
        if (is_admin() && self::mv_check_user_role('multi_vendor', $current_user->ID)) {
            if ('upload.php' == $pagenow) {
                $wp_query->set('author', $current_user->ID);
                return;
            }
        }
    }

    function show_uploaded_files_from_multivendor_ajax($query) {
        global $current_user, $pagenow, $wpdb, $current_screen;
        if (is_admin() && self::mv_check_user_role('multi_vendor', get_current_user_id())) {
            $query['author'] = get_current_user_id();
        }
        return $query;
    }

    public static function mv_check_user_role($role, $user_id = null) {
        if (is_numeric($user_id))
            $user = get_userdata($user_id);
        else
            $user = wp_get_current_user();
        if (empty($user))
            return false;

        return in_array($role, (array) $user->roles);
    }

}

new FPRestrictMediaFiles();

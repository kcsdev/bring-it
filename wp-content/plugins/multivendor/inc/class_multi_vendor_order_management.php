<?php

//** New Order Management for Multi Vendor User Role

class MultiVendor_Order_Management {

    public function __construct() {
        add_action('admin_menu', array($this, 'create_menu_separately'));

        add_action('wp_ajax_multivendor_role_update_order_status', array($this, 'get_response_from_ajax_multivendor'));

        add_action('add_meta_boxes', array($this, 'add_meta_box_for_order_table'));
        add_action('woocommerce_process_shop_order_meta', array($this, 'save_order_post'));
    }

// Create Menu Page
    public static function create_menu_separately() {
        /* Add menu for order Management */

        add_menu_page(__('Order Management', 'multivendor'), __('Order Management', 'multivendor'), 'manage_multivendor_order', 'multivendor_orders', array('MultiVendor_Order_Management', 'add_functionality_separately'), '', 25);
    }

// Get Vendor Count for Particular Order

    public static function get_vendor_count_order_management($orderid) {


        $order = new WC_Order($orderid);
        $vendordata = array();
        foreach ($order->get_items() as $eachitem) {
            $vendorlist = wp_get_post_terms($eachitem['product_id'], 'multi_vendor');
            foreach ($vendorlist as $eachvendor) {
                $vendordata[$eachvendor->term_id] = $eachvendor->name;
            }
        }
        return $vendordata;
    }

// Order Management Functionality
    public static function add_functionality_separately() {
        /* Add Functionality here for Order Management */

        if (is_user_logged_in()) {

            global $wpdb;
// Vendor Details shown only for the vendor Admins
            $taxonomies = array('multi_vendor');
            $args = array(
                'hide_empty' => false,
            );
            $termsid = array();
            $allvendoradmins = array();
            $getallvendors = get_terms($taxonomies, $args);
//echo get_current_user_id();
            foreach ($getallvendors as $eachvendor) {
                $vendoradminids = unserialize(get_woocommerce_term_meta($eachvendor->term_id, 'fp_mv_vendor_admins', true));
                if (in_array(get_current_user_id(), (array) $vendoradminids)) {
                    $termsid[] = $eachvendor->term_id;
                    $allvendoradmins[] = unserialize(get_woocommerce_term_meta($eachvendor->term_id, 'fp_mv_vendor_admins', true));
                }
            }

            $duecommission = array();
            $paidcommission = array();
            $multivendorduelist = $wpdb->get_results(
                    "SELECT *
	FROM " . $wpdb->prefix . "multi_vendor WHERE status='Due' and vendorid='21'"
            );
            $multivendorpaidlist = $wpdb->get_results(
                    "SELECT *
	FROM " . $wpdb->prefix . "multi_vendor WHERE status='Paid' and vendorid='16'"
            );

            if (!empty($multivendorduelist)) {
                foreach ($multivendorduelist as $eachdue) {
                    $duecommission[] = $eachdue->vendorcommission;
                }
            }
            if (!empty($multivendorpaidlist)) {
                foreach ($multivendorpaidlist as $eachpaid) {
                    $paidcommission[] = $eachpaid->vendorcommission;
                }
            }

// $combineddata = array_combine((array) $termsid, (array) $allvendoradmins);
            if (FPMultiVendorShortcodeFunction::check_if_current_user_is_vendor(get_current_user_id()) == '1') {
                if (!isset($_GET['orderid'])) {
                    ?>
                    <h3><?php _e('Order Management', 'multivendor'); ?></h3>
                    <?php
                    echo '<p> ' . __('Search:', 'multivendor') . '<input id="multivendor_filters" type="text"/>  ' . __('Page Size:', 'multivendor') . '
                <select id="multivendor_change-page-sizes">
                <option value="5">5</option>
                <option value="10">10</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select></p>';
                    ?>

                    <script type="text/javascript">
                        jQuery(document).ready(function () {
                            jQuery('.multivendor_myaccount').footable().bind('footable_filtering', function (e) {
                                var selected = jQuery('.filter-status').find(':selected').text();
                                if (selected && selected.length > 0) {
                                    e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
                                    e.clear = !e.filter;
                                }
                            });
                            jQuery('#multivendor_change-page-sizes').change(function (e) {
                                e.preventDefault();
                                var pageSize = jQuery(this).val();
                                jQuery('.footable').data('page-size', pageSize);
                                jQuery('.footable').trigger('footable_initialized');
                            });
                        });</script>
                    <table  data-filter = "#multivendor_filters" data-page-size="5" data-page-previous-text = "prev" data-filter-text-only = "true" data-page-next-text = "next" class = "wp-list-table widefat fixed posts multivendor_myaccount">
                        <thead><tr>
                                <th data-toggle="true" data-sort-initial = "true">
                                    <?php _e('Order ID', 'multivendor'); ?>
                                </th>
                                <th  data-toggle="true" >
                                    <?php _e('Product Name', 'multivendor'); ?>
                                </th>
                                <th  data-toggle="true" >
                                    <?php _e('Product Price', 'multivendor'); ?>
                                </th>
                                <th  data-toggle="true" >
                                    <?php _e('Vendor Name', 'multivendor'); ?>
                                </th>
                                <th  data-toggle="true">
                                    <?php _e('Vendor Commission Rate', 'multivendor'); ?>
                                </th>
                                <th  data-toggle="true">
                                    <?php _e('Vendor Commission', 'multivendor'); ?>
                                </th>
                                <th  data-toggle="true">
                                    <?php _e('Payment Status', 'multivendor'); ?>
                                </th>
                                <th data-toggle="true">
                                    <?php _e('Order Status', 'multivendor'); ?>
                                </th>
                                <th data-toggle="true">
                                    <?php _e('Update Status', 'multivendor'); ?>
                                </th>
                                <th  data-toggle="true">
                                    <?php _e('Date', 'multivendor'); ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($termsid as $termid) {
                                $multivendorlist = $wpdb->get_results(
                                        "SELECT *
	FROM " . $wpdb->prefix . "multi_vendor WHERE vendorid=$termid", ARRAY_A
                                );
//                var_dump($multivendorlist);
                                if (!empty($multivendorlist)) {
                                    foreach ($multivendorlist as $eacharray) {
                                        $orderid = $eacharray['orderid'];
                                        $order = new WC_Order($orderid);
                                        if ($order->status != '') {
                                            ?>
                                            <tr data-value="<?php echo $eacharray['id']; ?>">
                                                <td>
                                                    <?php echo $eacharray['orderid']; ?>
                                                </td>
                                                <td>
                                                    <?php echo get_the_title($eacharray['productid']); ?>
                                                </td>
                                                <td>
                                                    <?php echo FPMultiVendor::get_woocommerce_formatted_price($eacharray['productprice']); ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    $newvendorid = get_term_by('id', $eacharray['vendorid'], 'multi_vendor');
                                                    echo $newvendorid->name;
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php echo $eacharray['vendorcommissionrate'] ? $eacharray['vendorcommissionrate'] . "%" : "0%"; ?>
                                                </td>
                                                <td>
                                                    <?php echo $eacharray['vendorcommission'] ? FPMultiVendor::get_woocommerce_formatted_price($eacharray['vendorcommission']) : FPMultiVendor::get_woocommerce_formatted_price("0"); ?>
                                                </td>
                                                <td>
                                                    <?php echo $eacharray['status']; ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    $orderid = $eacharray['orderid'];
                                                    $order = new WC_Order($orderid);
                                                    echo ucwords($order->status);
                                                    ?>
                                                </td>
                                                <td>

                                                    <a href="admin.php?page=multivendor_orders&orderid=<?php echo $eacharray['orderid']; ?>"><?php _e('Update Status', 'multivendor'); ?></a>
                                                </td>
                                                <td>
                                                    <?php echo $eacharray['date']; ?>
                                                </td>

                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                            }
                            ?>
                        </tbody>

                    </table>
                    <table>
                        <tfoot>
                            <tr style="clear:both;">
                                <td>
                                    <div class="pagination pagination-centered"></div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    <?php
                } else {
                    $order = new WC_Order($_GET['orderid']);
                    ?>
                    <div class='wrap'>
                        <div id='poststuff'>
                            <div id='woocommerce-order-data' class='postbox'>
                                <div class='inside'>
                                    <div class='panel-wrap woocommerce'>
                                        <div id='order_data'>
                                            <h2><?php printf(__('Order %s details', 'multivendor'), esc_html($order->get_order_number())); ?></h2>
                                            <p><?php echo $order->payment_method_title; ?></p>
                                            <p class="order_number"><?php
                                                if ($ip_address = get_post_meta($_GET['orderid'], '_customer_ip_address', true)) {
                                                    echo __('Customer IP', 'woocommerce') . ': ' . esc_html($ip_address);
                                                }
                                                ?></p>
                                            <table cellspacing="0" cellpadding="0" style="width: 100%; vertical-align: top;" border="0">
                                                <tr>

                                                    <td valign="top" width="50%">
                                                        <h3><?php _e('Billing address', 'multivendor'); ?></h3>
                                                        <p><?php echo $order->get_formatted_billing_address(); ?></p>
                                                    </td>

                                                    <?php if (function_exists("wc_ship_to_billing_address_only")) { ?>
                                                        <?php if (!wc_ship_to_billing_address_only() && $order->needs_shipping_address() && ( $shipping = $order->get_formatted_shipping_address() )) : ?>

                                                            <td valign="top" width="50%">
                                                                <h3><?php _e('Shipping address', 'multivendor'); ?></h3>
                                                                <p><?php echo $shipping; ?></p>
                                                            </td>

                                                        <?php endif; ?>
                                                    <?php } else { ?>
                                                        <?php if (get_option('woocommerce_ship_to_billing_address_only') === 'no' && ( $shipping = $order->get_formatted_shipping_address() )) : ?>

                                                            <td valign="top" width="50%">
                                                                <h3><?php _e('Shipping address', 'multivendor'); ?></h3>
                                                                <p><?php echo $shipping; ?></p>
                                                            </td>

                                                        <?php endif; ?>
                                                    <?php } ?>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class='postbox'>
                                <h3><?php _e('Update Shipping Status Information', 'multivendor'); ?></h3>
                                <div class='inside'>
                                    <table style='width:100%' class='wp-list-table widefat fixed posts'>
                                        <thead>
                                        <th>
                                            <?php _e('Message', 'multivendor'); ?>
                                        </th>
                                        <th>
                                            <?php _e('Status', 'multivendor'); ?>
                                        </th>
                                        <th>
                                            <?php _e('Save Changes', 'multivendor'); ?>
                                        </th>
                                        </thead>
                                        <tr>
                                            <td>
                                                <textarea id='shippinginformationmessage' name='shippinginformationmessage' cols='40' rows='4'><?php echo get_post_meta($_GET['orderid'], 'add_shipping_details_to_order', true); ?></textarea>
                                            </td>
                                            <td>
                                                <?php
                                                $getcount = count(self::get_vendor_count_order_management($_GET['orderid']));
                                                ?>
                                                <input type='checkbox' name='shippingstatus' id='shippingstatus' value='1' <?php checked('1', $order->status == 'shipped' ? '1' : ''); ?>>
                                                <?php
                                                if (get_post_meta($_GET['orderid'], '_status_changed_to_shipped', true) == '1') {
                                                    _e('Shipped', 'multivendor');
                                                } else {
                                                    _e('Partially Shipped (because this order contain more than one vendor)', 'multivendor');
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <input type='submit' class='button-primary' name='shippingbutton' id='shippingbutton' value='Submit'/>
                                                <br>
                                                <div class='button_success'></div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>


                            <?php
                            global $wpdb;
// Vendor Details shown only for the vendor Admins
                            $taxonomies = array('multi_vendor');
                            $args = array(
                                'hide_empty' => false,
                            );
                            $termsid = array();
                            $allvendoradmins = array();
                            $getallvendors = get_terms($taxonomies, $args);
//echo get_current_user_id();
                            foreach ($getallvendors as $eachvendor) {
                                $vendoradminids = unserialize(get_woocommerce_term_meta($eachvendor->term_id, 'fp_mv_vendor_admins', true));
                                if (in_array(get_current_user_id(), (array) $vendoradminids)) {
                                    $termsid[] = $eachvendor->term_id;
                                    $allvendoradmins[] = unserialize(get_woocommerce_term_meta($eachvendor->term_id, 'fp_mv_vendor_admins', true));
                                }
                            }

                            $duecommission = array();
                            $paidcommission = array();
                            $multivendorduelist = $wpdb->get_results(
                                    "SELECT *
	FROM " . $wpdb->prefix . "multi_vendor WHERE status='Due' and vendorid='21'"
                            );
                            $multivendorpaidlist = $wpdb->get_results(
                                    "SELECT *
	FROM " . $wpdb->prefix . "multi_vendor WHERE status='Paid' and vendorid='16'"
                            );

                            if (!empty($multivendorduelist)) {
                                foreach ($multivendorduelist as $eachdue) {
                                    $duecommission[] = $eachdue->vendorcommission;
                                }
                            }
                            if (!empty($multivendorpaidlist)) {
                                foreach ($multivendorpaidlist as $eachpaid) {
                                    $paidcommission[] = $eachpaid->vendorcommission;
                                }
                            }
                            ?>
                            <div class='postbox'>
                                <h3><?php _e('Product Information', 'multivendor'); ?></h3>
                                <div class='inside'>
                                    <table style='width:100%' class='wp-list-table widefat fixed posts'>
                                        <thead>
                                        <th>
                                            <?php _e('Product Name', 'multivendor'); ?>
                                        </th>
                                        <th>
                                            <?php _e('Product Price', 'multivendor'); ?>
                                        </th>
                                        <th>
                                            <?php _e('Shipping Method', 'multivendor'); ?>
                                        </th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <?php
                                                foreach ($termsid as $termid) {
                                                    $multivendorlist = $wpdb->get_results(
                                                            "SELECT * FROM " . $wpdb->prefix . "multi_vendor WHERE vendorid=$termid", ARRAY_A);
//                var_dump($multivendorlist);
                                                    if (!empty($multivendorlist)) {
                                                        foreach ($multivendorlist as $eacharray) {
                                                            $orderid = $eacharray['orderid'];
                                                            $order = new WC_Order($orderid);
                                                            if ($order->status != '') {
                                                                ?>
                                                                <td>
                                                                    <?php echo get_the_title($eacharray['productid']); ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo FPMultiVendor::get_woocommerce_formatted_price($eacharray['productprice']); ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    $order = new WC_Order($eacharray['orderid']);
                                                                    echo $shipping_method = $order->get_shipping_method()
                                                                    ?>
                                                                </td>
                                                                <?php
                                                            }
                                                        }
                                                    }
                                                }
                                                ?>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    ?>
                    <script type='text/javascript'>
                        function redirectfunction() {
                            window.location = "admin.php?page=multivendor_orders";
                        }
                        jQuery(function () {
                            jQuery('#shippingbutton').click(function () {
                                var orderid = "<?php echo $_GET['orderid']; ?>";
                                var message = jQuery('#shippinginformationmessage').val();
                                var shipping_status = jQuery('#shippingstatus:checked').val();
                                var old_status = "<?php echo $order->status; ?>";
                                if (typeof (shipping_status) !== 'undefined') {
                                    message = message;
                                    shipping_status = shipping_status;

                                } else {
                                    message = message;
                                    shipping_status = '0';
                                }

                                var dataparam = ({
                                    action: 'multivendor_role_update_order_status',
                                    orderid: orderid,
                                    message: message,
                                    shipping_status: shipping_status,
                                    old_status: old_status,
                                });
                                jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparam,
                                        function (response) {
                                            var newresponse = response.replace(/\s/g, '');
                                            if (newresponse === 'success') {
                                                jQuery('.button_success').html('<?php _e('Data Successfully Updated', 'multivendor'); ?>');
                                                setTimeout(redirectfunction, '1000');
                                            }
                                        });
                                return false;

                            });
                        });
                    </script>
                    <?php
                }
            }
        }
    }

    public static function get_response_from_ajax_multivendor() {
        if (isset($_POST['orderid'])) {
            $orderid = $_POST['orderid'];
            $message = $_POST['message'];
            $shipping_status = $_POST['shipping_status'];
            $old_status = $_POST['old_status'];
            add_post_meta($orderid, 'old_mv_status', $old_status);

            update_post_meta($orderid, 'add_shipping_details_to_order', $message);

            $order_object = new WC_Order($orderid);
            if ($shipping_status == '1') {
                $get_count_vendors = count(self::get_vendor_count_order_management($orderid));
                $get_released_count = get_post_meta($orderid, 'after_changing_status', true) == '' ? 1 : get_post_meta($orderid, 'after_changing_status', true);
                if (($get_count_vendors == '1')) {
                    $order_object->update_status('shipped');
                    update_post_meta($orderid, '_status_changed_to_shipped', '1');
                } elseif ($get_count_vendors > 1) {
                    if (($get_count_vendors - $get_released_count) == 0) {
                        $order_object->update_status('shipped');
                        update_post_meta($orderid, '_status_changed_to_shipped', '1');
                    } else {
                        $order_object->update_status('awaiting_shipping');
                        update_post_meta($orderid, 'after_changing_status', $get_released_count + 1);
                    }
                }
            } else {
                $getoldstatusbkup = get_post_meta($orderid, 'old_mv_status', $old_status);
                $order_object->update_status($getoldstatusbkup);
            }
        }
        echo "success";
        exit();
    }

    public static function add_meta_box_for_order_table() {
        add_meta_box(__('Shipping Information', 'multivendor'), __('Shipping Information', 'multivendor'), array('MultiVendor_Order_Management', 'add_shipping_data_to_meta_box'), 'shop_order', 'normal', 'high');
    }

    public static function add_shipping_data_to_meta_box() {
        global $post;
        ?>
        <p class="form-field add_shipping_details_to_order_field ">
            <label for="add_shipping_details_to_order"><?php _e('Shipping Information', 'multivendor'); ?></label>
            <textarea class="short" name="add_shipping_details_to_order" id="add_shipping_details_to_order" placeholder="" rows="6" cols="1"><?php echo get_post_meta($post->ID, 'add_shipping_details_to_order', true); ?> </textarea>
        </p>
        <?php
    }

    public static function save_order_post($post_id) {
        update_post_meta($post_id, 'add_shipping_details_to_order', $_POST['add_shipping_details_to_order']);
    }

}

new MultiVendor_Order_Management();

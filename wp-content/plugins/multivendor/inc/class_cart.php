<?php

/* Cart Tab Settings */

class FPMultiVendorCartTab {

    public function __construct() {
        add_filter('woocommerce_mv_settings_tabs_array', array($this, 'multivendor_cart_tab'), 100);
        add_action('admin_init', array($this, 'multivendor_add_default_values_settings'));
        add_action('woocommerce_mv_settings_tabs_multivendor_cart', array($this, 'multivendor_register_admin_settings'));
        add_action('woocommerce_update_options_multivendor_cart', array($this, 'multivendor_update_values_settings'));
    }

    public static function multivendor_cart_tab($settings_tabs) {
        $settings_tabs['multivendor_cart'] = __('Cart', 'multivendor');
        return $settings_tabs;
    }

    public static function multivendor_cart_admin_settings() {
        global $woocommerce;
        return apply_filters('woocommerce_multivendor_cart', array(
            array(
                'name' => __('Cart Settings', 'multivendor'),
                'type' => 'title',
                'id' => '_mv_title_cart_settings'
            ),
            array(
                'name' => __('Enable Sell Individually', 'multivendor'),
                'desc' => __('Enable Sell Individually for Socio Multi Vendor', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_vendor_sell_individually',
                'css' => 'min-width:150px;',
                'std' => 'no',
                'type' => 'checkbox',
                'newids' => 'fpmv_vendor_sell_individually',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Restrict Product in Cart', 'multivendor'),
                'desc' => __('Select Restriction Option about Product in Cart by Product Level or Vendor Level', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_vendor_restriction_type',
                'css' => 'min-width:150px;',
                'std' => '1',
                'type' => 'select',
                'options' => array(
                    '1' => __('By Product', 'multivendor'),
                    '2' => __('By Vendor', 'multivendor'),
                ),
                'newids' => 'fpmv_vendor_restriction_type',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_mv_title_cart_settings'),
        ));
    }

    public static function multivendor_register_admin_settings() {
        woocommerce_admin_fields(FPMultiVendorCartTab::multivendor_cart_admin_settings());
    }

    public static function multivendor_update_values_settings() {
        woocommerce_update_options(FPMultiVendorCartTab::multivendor_cart_admin_settings());
    }

    public static function multivendor_add_default_values_settings() {
        foreach (FPMultiVendorCartTab::multivendor_cart_admin_settings() as $settings) {
            if ((isset($settings['newids'])) && (isset($settings['std']))) {
                //  if (get_option($settings['newids']) == FALSE) {
                add_option($settings['newids'], $settings['std']);
            }
        }
    }

}

new FPMultiVendorCartTab();

<?php

class FPMultiVendorEmailNotifications {
    /* Add Email Notification */

    public function __construct() {
        //add_action('wp_head', array($this, 'get_product_and_attributes'));
    }

    public static function send_email_new_order($order_id) {
        global $woocommerce;
        if (get_option('fpmv_vendor_new_order_email_enable') == 'yes') {

            $whole_vendor_details = FPMultiVendorEmailNotifications::mv_get_vendor_commissions($order_id);
            $order = new WC_Order($order_id);
            if (!empty($whole_vendor_details)) {
//mail things

                foreach ($whole_vendor_details as $vendor_cat => $vendor_cat_details) {
                    $subject = get_option('fpmv_vendor_new_order_email_subject');
                    $message = get_option('fpmv_vendor_new_order_email_message');
                    $users = maybe_unserialize(get_woocommerce_term_meta($vendor_cat, 'fp_mv_vendor_admins'));

                    foreach ($users as $user_id) {

                        $user_wmpl_lang = get_user_meta($user_id, 'mv_wpml_lang', true);
                        if (empty($user_wmpl_lang)) {
                            $user_wmpl_lang = 'en';
                        }
                        $subject = SocioMultiVendorWPMLSupport::fp_mv_get_wpml_text('fpmv_vendor_new_order_email_subject', $user_wmpl_lang, get_option('fpmv_vendor_new_order_email_subject'));
                        $message = SocioMultiVendorWPMLSupport::fp_mv_get_wpml_text('fpmv_vendor_new_order_email_message', $user_wmpl_lang, get_option('fpmv_vendor_new_order_email_message'));

                        $userdata = get_userdata($user_id);
                        $userdata->user_email;
                        ob_start();
                        ?>


                        <p><?php printf(__('You have received an order from %s. Their order is as follows:', 'woocommerce'), $order->billing_first_name . ' ' . $order->billing_last_name); ?></p>

                        <table cellspacing="0" cellpadding="6" id="sale_details" class="sale_details" style="width: 100%; border: 1px solid #eee;" border="1" bordercolor="#eee">
                            <thead>
                                <tr>
                                    <th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e('Order ID', 'multivendor'); ?></th>
                                    <th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e('Product Name', 'multivendor'); ?></th>
                                    <th scope="col" style="text-align:left; border:1px solid #eee;"><?php _e('Quantity', 'multivendor'); ?></th>
                                    <th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e('Commission', 'multivendor'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($vendor_cat_details as $vendor_cat_details_each) { ?>
                                    <tr>
                                        <td style="text-align:left; vertical-align:middle; border: 1px solid #eee; word-wrap:break-word;">#<?php echo $order_id; ?></td>
                                        <td style="text-align:left; vertical-align:middle; border: 1px solid #eee; word-wrap:break-word;"><?php echo self::get_product_and_attributes($order_id, $vendor_cat_details_each['product_id']); ?></td>
                                        <td style="text-align:left; vertical-align: middle; border:1px solid #eee; word-wrap: break-word;"><?php echo self::get_product_quantity_in_mail($order_id, $vendor_cat_details_each['product_id']); ?></td>
                                        <td style="text-align:left; vertical-align:middle; border: 1px solid #eee; word-wrap:break-word;"><?php echo FPMultiVendor::get_woocommerce_formatted_price($vendor_cat_details_each['commission']); ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th scope="row" colspan="3" style="text-align:left; border: 1px solid #eee;"><?php _e('Shipping Method', 'multivendor'); ?></th>
                                    <td style="text-align:left; border: 1px solid #eee;"><?php echo $order->get_shipping_method(); ?></td>
                                </tr>
                                <tr>
                                    <th scope="row" colspan="3" style="text-align:left; border: 1px solid #eee;"><?php _e('Payment Method', 'multivendor'); ?></th>
                                    <td style="text-align:left; border: 1px solid #eee;"><?php echo $order->payment_method_title; ?></td>
                                </tr>
                            </tfoot>
                        </table>
                        
                        
                        
                            <?php
                    }
                    ?>
                        <?php if (get_option('fpmv_vendor_ordernote_email_enable') == 'yes') { ?>
                         <h2><?php _e('Order Notes', 'woocommerce'); ?></h2>
                         <?php if ($order->customer_note) : ?>
                                <p><strong><?php _e('Notes:', 'woocommerce'); ?></strong> <?php echo $order->customer_note; ?></p>
                            <?php endif; ?>
                     <?php } ?>
                         <?php if (get_option('fpmv_vendor_customer_details') == 'yes') { ?>
                          <h2><?php _e('Customer details', 'woocommerce'); ?></h2>
                        <?php if ($order->billing_email) : ?>
                            <p><strong><?php _e('Email:', 'woocommerce'); ?></strong> <?php echo $order->billing_email; ?></p>
                        <?php endif; ?>
                        <?php if ($order->billing_phone) : ?>
                            <p><strong><?php _e('Tel:', 'woocommerce'); ?></strong> <?php echo $order->billing_phone; ?></p>
                        <?php endif; ?>
                    <?php } ?>
                    <table cellspacing="0" cellpadding="0" style="width: 100%; vertical-align: top;" border="0">
                        <tr>
                            <?php if (get_option('fpmv_vendor_billing_address') == 'yes') { ?>
                                <td valign="top" width="50%">
                                    <h3><?php _e('Billing address', 'woocommerce'); ?></h3>
                                    <p><?php echo $order->get_formatted_billing_address(); ?></p>
                                    
                                </td>
                            <?php } ?>
                            <?php if (function_exists("wc_ship_to_billing_address_only")) { ?>
                                <?php if (!wc_ship_to_billing_address_only() && $order->needs_shipping_address() && ( $shipping = $order->get_formatted_shipping_address() )) : ?>
                                    
                                    <?php if (get_option('fpmv_vendor_shipping_address') == 'yes') { ?>
                                        <td valign="top" width="50%">
                                            <h3><?php _e('Shipping address', 'woocommerce'); ?></h3>
                                            <p><?php echo $shipping; ?></p>
                                        </td>
                                    <?php } ?>
                                <?php endif; ?>
                            <?php } else { ?>
                                <?php if (get_option('woocommerce_ship_to_billing_address_only') === 'no' && ( $shipping = $order->get_formatted_shipping_address() )) : ?>
                                    <?php if (get_option('fpmv_vendor_shipping_address') == 'yes') { ?>
                                        <td valign="top" width="50%">
                                            <h3><?php _e('Shipping address', 'woocommerce'); ?></h3>
                                            <p><?php echo $shipping; ?></p>
                                        </td>
                                    <?php } ?>
                                <?php endif; ?>
                            <?php } ?>
                        </tr>
                    </table>
                    <?php
                    $sale_info = ob_get_clean();
                    $message = str_replace('{sale_details}', $sale_info, $message);
                    ob_start();
                    wc_get_template('emails/email-header.php', array('email_heading' => $subject));
                    echo $message;
                    wc_get_template('emails/email-footer.php');
                    $temp_message = ob_get_clean();


                    $headers = "MIME-Version: 1.0\r\n";
                    $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
                    $headers .= "From: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
                    $headers .= "Reply-To: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
                    if ((float) $woocommerce->version <= (float) ('2.2.0')) {

                        wp_mail($userdata->user_email, $subject, $temp_message, $headers);
                    } else {
                        $mailer = WC()->mailer();
                        $mailer->send($userdata->user_email, $subject, $temp_message, '', '');
                    }
                }
            }
        }
    }
    
    /* Get Product and its Attributes */

    public static function get_product_and_attributes($orderid, $currentproductid) {
//        $orderid = '199';
//        $currentproductid = '195';
        $order = new WC_Order($orderid);
        $itemdetails = '';
        foreach ($order->get_items() as $item_id => $item) {
            if ($item['product_id'] == $currentproductid) {
                $_product = $order->get_product_from_item($item);
                $item_meta = new WC_Order_Item_Meta($item['item_meta'], $_product);
                // Variation

                if ($item_meta->meta) {
                    $itemdetails = '<br/><small>' . nl2br($item_meta->display(true, true)) . '</small>';
                }
                return $item['name'] . $itemdetails;
            }
        }
    }

    public static function get_product_quantity_in_mail($orderid, $currentproductid) {
        $order = new WC_Order($orderid);
        $itemquantity = '';
        foreach ($order->get_items() as $item_id => $item) {
            if ($item['product_id'] == $currentproductid) {
                return $item['qty'];
            }
        }
    }

    public static function send_email_order_processing($order_id) {
        global $woocommerce;
        if (get_option('fpmv_vendor_processing_email_enable') == 'yes') {
            $order = new WC_Order($order_id);
            $whole_vendor_details = FPMultiVendorEmailNotifications::mv_get_vendor_commissions($order_id);
            if (!empty($whole_vendor_details)) {
//mail things
                foreach ($whole_vendor_details as $vendor_cat => $vendor_cat_details) {

                    $users = maybe_unserialize(get_woocommerce_term_meta($vendor_cat, 'fp_mv_vendor_admins'));

                    foreach ($users as $user_id) {
                        $user_wmpl_lang = get_user_meta($user_id, 'mv_wpml_lang', true);
                        if (empty($user_wmpl_lang)) {
                            $user_wmpl_lang = 'en';
                        }
                        $subject = SocioMultiVendorWPMLSupport::fp_mv_get_wpml_text('fpmv_vendor_order_processing_email_subject', $user_wmpl_lang, get_option('fpmv_vendor_order_processing_email_subject'));
                        $message = SocioMultiVendorWPMLSupport::fp_mv_get_wpml_text('fpmv_vendor_order_processing_email_message', $user_wmpl_lang, get_option('fpmv_vendor_order_processing_email_message'));
                        $userdata = get_userdata($user_id);
                        $userdata->user_email;
                        ob_start();
                        ?>
                        <table cellspacing="0" cellpadding="6" id="sale_details" class="sale_details" style="width: 100%; border: 1px solid #eee;" border="1" bordercolor="#eee">
                            <thead>
                                <tr>
                                    <th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e('Order ID', 'multivendor'); ?></th>
                                    <th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e('Product Name', 'multivendor'); ?></th>
                                    <th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e('Quantity', 'multivendor'); ?></th>
                                    <th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e('Commission', 'multivendor'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($vendor_cat_details as $vendor_cat_details_each) { ?>
                                    <tr>
                                        <td style="text-align:left; vertical-align:middle; border: 1px solid #eee; word-wrap:break-word;">#<?php echo $order_id; ?></td>
                                        <td style="text-align:left; vertical-align:middle; border: 1px solid #eee; word-wrap:break-word;"><?php echo self::get_product_and_attributes($order_id, $vendor_cat_details_each['product_id']); ?></td>
                                        <td style="text-align:left; vertical-align: middle; border:1px solid #eee; word-wrap: break-word;"><?php echo self::get_product_quantity_in_mail($order_id, $vendor_cat_details_each['product_id']); ?></td>
                                        <td style="text-align:left; vertical-align:middle; border: 1px solid #eee; word-wrap:break-word;"><?php echo FPMultiVendor::get_woocommerce_formatted_price($vendor_cat_details_each['commission']); ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th scope="row" colspan="3" style="text-align:left; border: 1px solid #eee;"><?php _e('Shipping Method', 'multivendor'); ?></th>
                                    <td style="text-align:left; border: 1px solid #eee;"><?php echo $order->get_shipping_method(); ?></td>
                                </tr>
                                <tr>
                                    <th scope="row" colspan="3" style="text-align:left; border: 1px solid #eee;"><?php _e('Payment Method', 'multivendor'); ?></th>
                                    <td style="text-align:left; border: 1px solid #eee;"><?php echo $order->payment_method_title; ?></td>
                                </tr>
                            </tfoot>
                        </table>
                        <?php ?>
                            <?php if (get_option('fpmv_vendor_processing_ordernote_email_enable') == 'yes') { ?>
                         <h2><?php _e('Order Notes', 'woocommerce'); ?></h2>
                         <?php if ($order->customer_note) : ?>
                                <p><strong><?php _e('Notes:', 'woocommerce'); ?></strong> <?php echo $order->customer_note; ?></p>
                            <?php endif; ?>
                     <?php } ?>
                        <?php if (get_option('fpmv_vendor_customer_details') == 'yes') { ?>
                            <h2><?php _e('Customer details', 'woocommerce'); ?></h2>

                            <?php if ($order->billing_email) : ?>
                                <p><strong><?php _e('Email:', 'woocommerce'); ?></strong> <?php echo $order->billing_email; ?></p>
                            <?php endif; ?>
                            <?php if ($order->billing_phone) : ?>
                                <p><strong><?php _e('Tel:', 'woocommerce'); ?></strong> <?php echo $order->billing_phone; ?></p>
                            <?php endif; ?>
                        <?php } ?>

                        <table cellspacing="0" cellpadding="0" style="width: 100%; vertical-align: top;" border="0">

                            <tr>

                                <td valign="top" width="50%">

                                    <h3><?php _e('Billing address', 'woocommerce'); ?></h3>

                                    <p><?php echo $order->get_formatted_billing_address(); ?></p>

                                </td>

                                <?php if (function_exists("wc_ship_to_billing_address_only")) { ?>
                                    <?php if (!wc_ship_to_billing_address_only() && $order->needs_shipping_address() && ( $shipping = $order->get_formatted_shipping_address() )) : ?>

                                        <td valign="top" width="50%">

                                            <h3><?php _e('Shipping address', 'woocommerce'); ?></h3>

                                            <p><?php echo $shipping; ?></p>

                                        </td>

                                    <?php endif; ?>
                                <?php } else { ?>
                                    <?php if (get_option('woocommerce_ship_to_billing_address_only') === 'no' && ( $shipping = $order->get_formatted_shipping_address() )) : ?>
                                        <?php if (get_option('fpmv_vendor_shipping_address') == 'yes') { ?>
                                            <td valign="top" width="50%">
                                                <h3><?php _e('Shipping address', 'woocommerce'); ?></h3>
                                                <p><?php echo $shipping; ?></p>
                                            </td>
                                        <?php } ?>
                                    <?php endif; ?>
                                <?php } ?>

                            </tr>

                        </table>
                        <?php
                        $sale_info = ob_get_clean();
                        $message = str_replace('{sale_details}', $sale_info, $message);
                        ob_start();
                        wc_get_template('emails/email-header.php', array('email_heading' => $subject));
                        echo $message;
                        wc_get_template('emails/email-footer.php');
                        $temp_message = ob_get_clean();
                        $headers = "MIME-Version: 1.0\r\n";
                        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
                        $headers .= "From: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
                        $headers .= "Reply-To: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
                        if ((float) $woocommerce->version <= (float) ('2.2.0')) {

                            wp_mail($userdata->user_email, $subject, $temp_message, $headers);
                        } else {
                            $mailer = WC()->mailer();
                            $mailer->send($userdata->user_email, $subject, $temp_message, '', '');
                        }
                    }
                }
            }
        }
    }

    public static function send_email_order_has_been_shipped($order_id) {
        global $woocommerce;

        if (get_option('fpmv_vendor_shipping_email_enable') == 'yes') {
            $order = new WC_Order($order_id);

            $user_id = $order->user_id;
            $userdata = get_userdata($order->user_id);
            $userdata->user_email;
            ob_start();
            ?>
            <table cellspacing="0" cellpadding="6" id="sale_details" class="sale_details" style="width: 100%; border: 1px solid #eee;" border="1" bordercolor="#eee">
                <tr>
                    <th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e('Product', 'woocommerce'); ?></th>
                    <th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e('Quantity', 'woocommerce'); ?></th>
                    <th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e('Price', 'woocommerce'); ?></th>
                </tr>
                <tbody>
                    <?php echo $order->email_order_items_table(true, false, true); ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th scope="row" colspan="2" style="text-align:left; border: 1px solid #eee;"><?php _e('Shipping Method', 'multivendor'); ?></th>
                        <td style="text-align:left; border: 1px solid #eee;"><?php echo $order->get_shipping_method(); ?></td>
                    </tr>
                    <tr>
                        <th scope="row" colspan="2" style="text-align:left; border: 1px solid #eee;"><?php _e('Payment Method', 'multivendor'); ?></th>
                        <td style="text-align:left; border: 1px solid #eee;"><?php echo $order->payment_method_title; ?></td>
                    </tr>
                </tfoot>
            </table>
            
                                <?php if (get_option('fpmv_vendor_shipping_ordernote_email_enable') == 'yes') { ?>
                         <h2><?php _e('Order Notes', 'woocommerce'); ?></h2>
                         <?php if ($order->customer_note) : ?>
                                <p><strong><?php _e('Notes:', 'woocommerce'); ?></strong> <?php echo $order->customer_note; ?></p>
                            <?php endif; ?>
                     <?php } ?>
            <?php if (get_option('fpmv_vendor_customer_details') == 'yes') { ?>
                              <h2><?php _e('Customer details', 'woocommerce'); ?></h2>

                <?php if ($order->billing_email) : ?>
                    <p><strong><?php _e('Email:', 'woocommerce'); ?></strong> <?php echo $order->billing_email; ?></p>
                <?php endif; ?>
                <?php if ($order->billing_phone) : ?>
                    <p><strong><?php _e('Tel:', 'woocommerce'); ?></strong> <?php echo $order->billing_phone; ?></p>
                <?php endif; ?>
            <?php } ?>
            <table cellspacing="0" cellpadding="0" style="width: 100%; vertical-align: top;" border="0">

                <tr>

                    <td valign="top" width="50%">

                        <h3><?php _e('Billing address', 'woocommerce'); ?></h3>

                        <p><?php echo $order->get_formatted_billing_address(); ?></p>

                    </td>
                    <?php if (function_exists("wc_ship_to_billing_address_only")) { ?>
                        <?php if (!wc_ship_to_billing_address_only() && $order->needs_shipping_address() && ( $shipping = $order->get_formatted_shipping_address() )) : ?>

                            <td valign="top" width="50%">

                                <h3><?php _e('Shipping address', 'woocommerce'); ?></h3>

                                <p><?php echo $shipping; ?></p>

                            </td>

                        <?php endif; ?>
                    <?php } else { ?>
                        <?php if (get_option('woocommerce_ship_to_billing_address_only') === 'no' && ( $shipping = $order->get_formatted_shipping_address() )) : ?>
                            <?php if (get_option('fpmv_vendor_shipping_address') == 'yes') { ?>
                                <td valign="top" width="50%">
                                    <h3><?php _e('Shipping address', 'woocommerce'); ?></h3>
                                    <p><?php echo $shipping; ?></p>
                                </td>
                            <?php } ?>
                        <?php endif; ?>
                    <?php } ?>

                </tr>

            </table>
            <?php
            $user_wmpl_lang = get_user_meta($user_id, 'mv_wpml_lang', true);
            if (empty($user_wmpl_lang)) {
                $user_wmpl_lang = 'en';
            }
            $subject = SocioMultiVendorWPMLSupport::fp_mv_get_wpml_text('fpmv_vendor_shipping_email_subject', $user_wmpl_lang, get_option('fpmv_vendor_shipping_email_subject'));
            $message = SocioMultiVendorWPMLSupport::fp_mv_get_wpml_text('fpmv_vendor_shipping_email_message', $user_wmpl_lang, get_option('fpmv_vendor_shipping_email_message'));

            $sale_info = ob_get_clean();
            $message = str_replace('{orderitems}', $sale_info, $message);
            $message = str_replace('{ordershippingdetails}', get_post_meta($order_id, 'add_shipping_details_to_order', true), $message);
            ob_start();
            wc_get_template('emails/email-header.php', array('email_heading' => $subject));
            echo $message;
            wc_get_template('emails/email-footer.php');
            $temp_message = ob_get_clean();


            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
            $headers .= "From: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
            $headers .= "Reply-To: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
            // wp_mail($userdata->user_email, $subject, $temp_message, $headers);
            if ((float) $woocommerce->version <= (float) ('2.2.0')) {
                wp_mail($userdata->user_email, $subject, $temp_message, $headers);
            } else {
                $mailer = WC()->mailer();
                $mailer->send($userdata->user_email, $subject, $temp_message, '', '');
            }
        }
    }

    public static function send_email_order_completed($order_id) {
        global $woocommerce;
        if (get_option('fpmv_vendor_completed_email_enable') == 'yes') {
            $order = new WC_Order($order_id);
            $whole_vendor_details = FPMultiVendorEmailNotifications::mv_get_vendor_commissions($order_id);
            if (!empty($whole_vendor_details)) {
//mail things
                foreach ($whole_vendor_details as $vendor_cat => $vendor_cat_details) {
                    $subject = get_option('fpmv_vendor_order_completed_email_subject');
                    $message = get_option('fpmv_vendor_order_completed_email_message');
                    $users = maybe_unserialize(get_woocommerce_term_meta($vendor_cat, 'fp_mv_vendor_admins'));

                    foreach ($users as $user_id) {

                        $userdata = get_userdata($user_id);
                        $userdata->user_email;
                        ob_start();
                        ?>
                        <table cellspacing="0" cellpadding="6" id="sale_details" class="sale_details" style="width: 100%; border: 1px solid #eee;" border="1" bordercolor="#eee">
                            <thead>
                                <tr>
                                    <th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e('Order ID', 'multivendor'); ?></th>
                                    <th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e('Product Name', 'multivendor'); ?></th>
                                    <th scope="col" style="text-align:left; border:1px solid #eee;"><?php _e('Quantity', 'multivendor'); ?></th>
                                    <th scope="col" style="text-align:left; border: 1px solid #eee;"><?php _e('Commission', 'multivendor'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($vendor_cat_details as $vendor_cat_details_each) { ?>
                                    <tr>
                                        <td style="text-align:left; vertical-align:middle; border: 1px solid #eee; word-wrap:break-word;">#<?php echo $order_id; ?></td>
                                        <td style="text-align:left; vertical-align:middle; border: 1px solid #eee; word-wrap:break-word;"><?php echo self::get_product_and_attributes($order_id, $vendor_cat_details_each['product_id']); ?></td>
                                        <td style="text-align:left; vertical-align:middle; border:1px solid #eee; word-wrap: break-word;"><?php echo self::get_product_quantity_in_mail($order_id, $vendor_cat_details_each['product_id']); ?></td>
                                        <td style="text-align:left; vertical-align:middle; border: 1px solid #eee; word-wrap:break-word;"><?php echo FPMultiVendor::get_woocommerce_formatted_price($vendor_cat_details_each['commission']); ?></td>
                                        
                                    </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th scope="row" colspan="3" style="text-align:left; border: 1px solid #eee;"><?php _e('Shipping Method', 'multivendor'); ?></th>
                                    <td style="text-align:left; border: 1px solid #eee;"><?php echo $order->get_shipping_method(); ?></td>
                                </tr>
                                <tr>
                                    <th scope="row" colspan="3" style="text-align:left; border: 1px solid #eee;"><?php _e('Payment Method', 'multivendor'); ?></th>
                                    <td style="text-align:left; border: 1px solid #eee;"><?php echo $order->payment_method_title; ?></td>
                                </tr>
                            </tfoot>
                        </table>
                        <?php ?>
                     <?php if (get_option('fpmv_vendor_completed_ordernote_email_enable') == 'yes') { ?>
                         <h2><?php _e('Order Notes', 'woocommerce'); ?></h2>
                         <?php if ($order->customer_note) : ?>
                                <p><strong><?php _e('Notes:', 'woocommerce'); ?></strong> <?php echo $order->customer_note; ?></p>
                            <?php endif; ?>
                                <?php } ?>
                     <?php if (get_option('fpmv_vendor_customer_details') == 'yes') { ?>
                              <h2><?php _e('Customer details', 'woocommerce'); ?></h2>
                            <?php if ($order->billing_email) : ?>
                                <p><strong><?php _e('Email:', 'woocommerce'); ?></strong> <?php echo $order->billing_email; ?></p>
                            <?php endif; ?>
                            <?php if ($order->billing_phone) : ?>
                                <p><strong><?php _e('Tel:', 'woocommerce'); ?></strong> <?php echo $order->billing_phone; ?></p>
                            <?php endif; ?>
                               <?php } ?>
                        <table cellspacing="0" cellpadding="0" style="width: 100%; vertical-align: top;" border="0">

                            <tr>

                                <td valign="top" width="50%">

                                    <h3><?php _e('Billing address', 'woocommerce'); ?></h3>

                                    <p><?php echo $order->get_formatted_billing_address(); ?></p>

                                </td>
                                <?php if (function_exists("wc_ship_to_billing_address_only")) { ?>
                                    <?php if (!wc_ship_to_billing_address_only() && $order->needs_shipping_address() && ( $shipping = $order->get_formatted_shipping_address() )) : ?>

                                        <td valign="top" width="50%">

                                            <h3><?php _e('Shipping address', 'woocommerce'); ?></h3>

                                            <p><?php echo $shipping; ?></p>

                                        </td>

                                    <?php endif; ?>
                                <?php } else { ?>
                                    <?php if (get_option('woocommerce_ship_to_billing_address_only') === 'no' && ( $shipping = $order->get_formatted_shipping_address() )) : ?>
                                        <?php if (get_option('fpmv_vendor_shipping_address') == 'yes') { ?>
                                            <td valign="top" width="50%">
                                                <h3><?php _e('Shipping address', 'woocommerce'); ?></h3>
                                                <p><?php echo $shipping; ?></p>
                                            </td>
                                        <?php } ?>
                                    <?php endif; ?>
                                <?php } ?>

                            </tr>

                        </table>
                        <?php
                        $user_wmpl_lang = get_user_meta($user_id, 'mv_wpml_lang', true);
                        if (empty($user_wmpl_lang)) {
                            $user_wmpl_lang = 'en';
                        }
                        $subject = SocioMultiVendorWPMLSupport::fp_mv_get_wpml_text('fpmv_vendor_order_completed_email_subject', $user_wmpl_lang, get_option('fpmv_vendor_order_completed_email_subject'));
                        $message = SocioMultiVendorWPMLSupport::fp_mv_get_wpml_text('fpmv_vendor_order_completed_email_message', $user_wmpl_lang, get_option('fpmv_vendor_order_completed_email_message'));

                        $sale_info = ob_get_clean();
                        $message = str_replace('{sale_details}', $sale_info, $message);
                        ob_start();
                        wc_get_template('emails/email-header.php', array('email_heading' => $subject));
                        echo $message;
                        wc_get_template('emails/email-footer.php');
                        $temp_message = ob_get_clean();


                        $headers = "MIME-Version: 1.0\r\n";
                        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
                        $headers .= "From: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
                        $headers .= "Reply-To: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";



                        if ((float) $woocommerce->version <= (float) ('2.2.0')) {

                            wp_mail($userdata->user_email, $subject, $temp_message, $headers);
                        } else {
                            $mailer = WC()->mailer();
                            $mailer->send($userdata->user_email, $subject, $temp_message, '', '');
                        }
                    }
                }
            }
        }
    }

    public static function mv_get_vendor_commissions($order_id) {
        global $wpdb;
        $order = new WC_Order($order_id);
        $vendorids = array();
        $vendormasterlist = array();
        $currentvendordata = array();
        $vendor_admins_presented = array();
        $whole_vendor_details = array();
        $order_total = $order->get_total();
        foreach ($order->get_items() as $item) {
            if ($item['product_id'] != '') {
                $vendorlist = wp_get_post_terms($item['product_id'], 'multi_vendor');
                $getvendorcount = count($vendorlist);
// var_dump($vendorlist);
                if ($getvendorcount > 1) {
                    if (( $item['variation_id'] != '0' ) && ($item['variation_id'] != '')) {
                        $variable_product1 = new WC_Product_Variation($item['variation_id']);
                        $newparentid = $variable_product1->parent->id;
                        if ($newparentid == $item['product_id']) {
                            if (get_option('fpmv_vendor_calculate_product_price') == '1') {
                                $getregularprice = $variable_product1->regular_price;
                            } else {
                                $getregularprice = $variable_product1->price;
                            }
                        } else {
                            if (get_option('fpmv_vendor_calculate_product_price') == '1') {
                                $getregularprice = get_post_meta($item['product_id'], '_regular_price', true);
                            } else {
                                $getregularprice = get_post_meta($item['product_id'], '_price', true);
                            }
                        }
                    } else {
                        if (get_option('fpmv_vendor_calculate_product_price') == '1') {
                            $getregularprice = get_post_meta($item['product_id'], '_regular_price', true);
                        } else {
                            $getregularprice = get_post_meta($item['product_id'], '_price', true);
                        }
                    }
                    if (!empty($vendorlist)) {
                        if (get_post_meta($item['product_id'], '_fp_multi_vendor_commission_percentage', true) != '') {
                            for ($i = 0; $i < $getvendorcount; $i++) {
                                $getcommissionrate = get_post_meta($item['product_id'], '_fp_multi_vendor_commission_percentage', true);
                                $commissionaverage = $getregularprice * $getcommissionrate;
                                $commissionamount = $commissionaverage / 100;
                                $commissionamount = $commissionamount / $getvendorcount;
                                $table_name = $wpdb->prefix . "multi_vendor";
//         $wpdb->insert($table_name, array('orderid' => $order_id, 'productid' => $item['product_id'], 'qty' => $item['qty'], 'productname' => get_the_title($item['product_id']), 'productprice' => $getregularprice, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => $commissionamount * $item['qty'], 'vendorname' => $vendorlist[$i]->name, 'paidcommission' => '', 'vendorid' => $vendorlist[$i]->term_id, 'status' => 'Due', 'date' => date('Y-m-d H:i:s')));

                                $each_product_ven_commission = $commissionamount * $item['qty'];
                                $each_product_v_term_id[] = $vendorlist[$i]->term_id;
                                $whole_vendor_details[$vendorlist[$i]->term_id][] = array('product_id' => $item['product_id'], 'commission' => $commissionamount * $item['qty']);
                                $pr = get_the_title($item['product_id']);
                            }
                        } elseif (get_option('fpmv_vendor_commission_rate_in_percentage') != '') {
                            for ($i = 0; $i < $getvendorcount; $i++) {
                                $getcommissionrate = get_option('fpmv_vendor_commission_rate_in_percentage');
                                $commissionaverage = $getregularprice * $getcommissionrate;
                                $commissionamount = $commissionaverage / 100;
                                $commissionamount = $commissionamount / $getvendorcount;
                                $table_name = $wpdb->prefix . "multi_vendor";
                                $each_product_ven_commission = $commissionamount * $item['qty'];
                                $each_product_v_term_id[] = $vendorlist[$i]->term_id;
                                $whole_vendor_details[$vendorlist[$i]->term_id][] = array('product_id' => $item['product_id'], 'commission' => $commissionamount * $item['qty']);
//      $wpdb->insert($table_name, array('orderid' => $order_id, 'productid' => $item['product_id'], 'qty' => $item['qty'], 'productname' => get_the_title($item['product_id']), 'productprice' => $getregularprice, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => $commissionamount * $item['qty'], 'vendorname' => $vendorlist[$i]->name, 'paidcommission' => '', 'vendorid' => $vendorlist[$i]->term_id, 'status' => 'Due', 'date' => date('Y-m-d H:i:s')));
                            }
                        } else {
                            for ($i = 0; $i < $getvendorcount; $i++) {
                                $getcommissionrate = 50;
                                $commissionaverage = $getregularprice * $getcommissionrate;
                                $commissionamount = $commissionaverage / 100;
                                $commissionamount = $commissionamount / $getvendorcount;
                                $table_name = $wpdb->prefix . "multi_vendor";
                                $each_product_ven_commission = $commissionamount * $item['qty'];
                                $each_product_v_term_id[] = $vendorlist[$i]->term_id;
                                $whole_vendor_details[$vendorlist[$i]->term_id][] = array('product_id' => $item['product_id'], 'commission' => $commissionamount * $item['qty']);
//      $wpdb->insert($table_name, array('orderid' => $order_id, 'productid' => $item['product_id'], 'qty' => $item['qty'], 'productname' => get_the_title($item['product_id']), 'productprice' => $getregularprice, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => $commissionamount * $item['qty'], 'vendorname' => $vendorlist[$i]->name, 'paidcommission' => '', 'vendorid' => $vendorlist[$i]->term_id, 'status' => 'Due', 'date' => date('Y-m-d H:i:s')));
                            }
                        }
                    }
                } else {
                    $vendor_list = wp_get_post_terms($item['product_id'], 'multi_vendor');
                    if ($item['variation_id'] != '' && $item['variation_id'] != '0' ) {
                        $variable_product1 = new WC_Product_Variation($item['variation_id']);
                        $newparentid = $variable_product1->parent->id;
                        if ($newparentid == $item['product_id']) {
                            if (get_option('fpmv_vendor_calculate_product_price') == '1') {
                                $getregularprice = $variable_product1->regular_price;
                            } else {
                                $getregularprice = $variable_product1->price;
                            }
                        } else {
                            if (get_option('fpmv_vendor_calculate_product_price') == '1') {
                                $getregularprice = get_post_meta($item['product_id'], '_regular_price', true);
                            } else {
                                $getregularprice = get_post_meta($item['product_id'], '_price', true);
                            }
                        }
                    } else {
                        if (get_option('fpmv_vendor_calculate_product_price') == '1') {
                            $getregularprice = get_post_meta($item['product_id'], '_regular_price', true);
                        } else {
                            $getregularprice = get_post_meta($item['product_id'], '_price', true);
                        }
                    }


                    if (!empty($vendor_list)) {
                        if (get_post_meta($item['product_id'], '_fp_multi_vendor_commission_percentage', true) != '') {


                            $getvendorid = $vendor_list[0]->term_id;
                            $getcommissionrate = get_post_meta($item['product_id'], '_fp_multi_vendor_commission_percentage', true);
                            $commissionaverage = $getregularprice * $getcommissionrate;
                            $commissionamount = $commissionaverage / 100;
                            $vendorids[] = $getvendorid;
                            $vendorcommission[] = $commissionamount;
                            $vendormasterlist[] = array('orderid' => $order_id, 'productid' => $item['product_id'], 'productprice' => $getregularprice, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => $commissionamount * $item['qty'], 'vendorid' => $vendor_list[0]->name, 'date' => date('Y-m-d H:i:s'));

//delete_woocommerce_term_meta($vendor_list[0]->term_id, 'mycommissionlist');
//delete_woocommerce_term_meta($vendor_list[0]->term_id, 'totalcommissions');

                            $oldvendormeta = (array) get_woocommerce_term_meta($vendor_list[0]->term_id, 'mycommissionlist', true);
                            $currentvendordata = array(array('orderid' => $order_id, 'productid' => $item['product_id'], 'productprice' => $getregularprice, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => $commissionamount * $item['qty'], 'vendorid' => $vendor_list[0]->name, 'date' => date('Y-m-d H:i:s')));
                            $updatedvendordata = array_merge($oldvendormeta, $currentvendordata);
                            update_woocommerce_term_meta($vendor_list[0]->term_id, 'mycommissionlist', array_filter($updatedvendordata));

                            $table_name = $wpdb->prefix . "multi_vendor";
                            $each_product_ven_commission = $commissionamount * $item['qty'];
                            $each_product_v_term_id[] = $vendor_list[0]->term_id;
                            $whole_vendor_details[$vendor_list[0]->term_id][] = array('product_id' => $item['product_id'], 'commission' => $commissionamount * $item['qty']);
//     $wpdb->insert($table_name, array('orderid' => $order_id, 'productid' => $item['product_id'], 'qty' => $item['qty'], 'productname' => get_the_title($item['product_id']), 'productprice' => $getregularprice, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => $commissionamount * $item['qty'], 'vendorname' => $vendor_list[0]->name, 'paidcommission' => '', 'vendorid' => $vendor_list[0]->term_id, 'status' => 'Due', 'date' => date('Y-m-d H:i:s')));
                        } elseif (get_woocommerce_term_meta($vendor_list[0]->term_id, 'fp_mv_commissions_rate', true) != '') {
//category percentage

                            $getvendorid = $vendor_list[0]->term_id;
                            $getcommissionrate = get_woocommerce_term_meta($vendor_list[0]->term_id, 'fp_mv_commissions_rate', true);
                            $commissionaverage = $getregularprice * $getcommissionrate;
                            $commissionamount = $commissionaverage / 100;
                            $vendorids[] = $getvendorid;
                            $vendorcommission[] = $commissionamount;
                            $vendormasterlist[] = array('orderid' => $order_id, 'productid' => $item['product_id'], 'productname' => get_the_title($item['product_id']), 'productprice' => $getregularprice, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => $commissionamount * $item['qty'], 'vendorid' => $vendor_list[0]->name, 'date' => date('Y-m-d H:i:s'));

//delete_woocommerce_term_meta($vendor_list[0]->term_id, 'mycommissionlist');
//delete_woocommerce_term_meta($vendor_list[0]->term_id, 'totalcommissions');

                            $oldvendormeta = (array) get_woocommerce_term_meta($vendor_list[0]->term_id, 'mycommissionlist', true);
                            $currentvendordata = array(array('orderid' => $order_id, 'productid' => $item['product_id'], 'productname' => get_the_title($item['product_id']), 'productprice' => $getregularprice, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => $commissionamount * $item['qty'], 'vendorid' => $vendor_list[0]->name, 'date' => date('Y-m-d H:i:s')));
                            $updatedvendordata = array_merge($oldvendormeta, $currentvendordata);
                            update_woocommerce_term_meta($vendor_list[0]->term_id, 'mycommissionlist', array_filter($updatedvendordata));

                            $table_name = $wpdb->prefix . "multi_vendor";
                            $each_product_ven_commission = $commissionamount * $item['qty'];
                            $each_product_v_term_id[] = $vendor_list[0]->term_id;
                            $whole_vendor_details[$vendor_list[0]->term_id][] = array('product_id' => $item['product_id'], 'commission' => $commissionamount * $item['qty']);
//     $wpdb->insert($table_name, array('orderid' => $order_id, 'productid' => $item['product_id'], 'qty' => $item['qty'], 'productname' => get_the_title($item['product_id']), 'productprice' => $getregularprice, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => $commissionamount * $item['qty'], 'vendorname' => $vendor_list[0]->name, 'paidcommission' => '', 'vendorid' => $vendor_list[0]->term_id, 'status' => 'Due', 'date' => date('Y-m-d H:i:s')));
                        } elseif (get_option('fpmv_vendor_commission_rate_in_percentage') != '') {
//Global Percentage;
                            $getvendorid = $vendor_list[0]->term_id;
                            $getcommissionrate = get_option('fpmv_vendor_commission_rate_in_percentage');
                            $commissionaverage = $getregularprice * $getcommissionrate;
                            $commissionamount = $commissionaverage / 100;
                            $vendorids[] = $getvendorid;
                            $vendorcommission[] = $commissionamount;
                            $vendormasterlist[] = array('orderid' => $order_id, 'productid' => $item['product_id'], 'productname' => get_the_title($item['product_id']), 'productprice' => $getregularprice, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => $commissionamount * $item['qty'], 'vendorid' => $vendor_list[0]->name, 'date' => date('Y-m-d H:i:s'));

//delete_woocommerce_term_meta($vendor_list[0]->term_id, 'mycommissionlist');
//delete_woocommerce_term_meta($vendor_list[0]->term_id, 'totalcommissions');

                            $oldvendormeta = (array) get_woocommerce_term_meta($vendor_list[0]->term_id, 'mycommissionlist', true);
                            $currentvendordata = array(array('orderid' => $order_id, 'productid' => $item['product_id'], 'productname' => get_the_title($item['product_id']), 'productprice' => $getregularprice, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => $commissionamount * $item['qty'], 'vendorid' => $vendor_list[0]->name, 'date' => date('Y-m-d H:i:s')));
                            $updatedvendordata = array_merge($oldvendormeta, $currentvendordata);
                            update_woocommerce_term_meta($vendor_list[0]->term_id, 'mycommissionlist', array_filter($updatedvendordata));

                            $table_name = $wpdb->prefix . "multi_vendor";
                            $each_product_ven_commission = $commissionamount * $item['qty'];
                            $each_product_v_term_id[] = $vendor_list[0]->term_id;
                            $whole_vendor_details[$vendor_list[0]->term_id][] = array('product_id' => $item['product_id'], 'commission' => $commissionamount * $item['qty']);
//    $wpdb->insert($table_name, array('orderid' => $order_id, 'productid' => $item['product_id'], 'qty' => $item['qty'], 'productname' => get_the_title($item['product_id']), 'productprice' => $getregularprice, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => $commissionamount * $item['qty'], 'vendorname' => $vendor_list[0]->name, 'paidcommission' => '', 'vendorid' => $vendor_list[0]->term_id, 'status' => 'Due', 'date' => date('Y-m-d H:i:s')));
                        } else {
                            $getvendorid = $vendor_list[0]->term_id;
                            $getcommissionrate = 50;
                            $commissionaverage = $getregularprice * $getcommissionrate;
                            $commissionamount = $commissionaverage / 100;
                            $vendorids[] = $getvendorid;
                            $vendorcommission[] = $commissionamount;
                            $vendormasterlist[] = array('orderid' => $order_id, 'productid' => $item['product_id'], 'productname' => get_the_title($item['product_id']), 'productprice' => $getregularprice, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => $commissionamount * $item['qty'], 'vendorid' => $vendor_list[0]->name, 'date' => date('Y-m-d H:i:s'));

//delete_woocommerce_term_meta($vendor_list[0]->term_id, 'mycommissionlist');
//delete_woocommerce_term_meta($vendor_list[0]->term_id, 'totalcommissions');

                            $oldvendormeta = (array) get_woocommerce_term_meta($vendor_list[0]->term_id, 'mycommissionlist', true);
                            $currentvendordata = array(array('orderid' => $order_id, 'productid' => $item['product_id'], 'productprice' => $getregularprice, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => $commissionamount * $item['qty'], 'vendorid' => $vendor_list[0]->name, 'date' => date('Y-m-d H:i:s')));
                            $updatedvendordata = array_merge($oldvendormeta, $currentvendordata);
                            update_woocommerce_term_meta($vendor_list[0]->term_id, 'mycommissionlist', array_filter($updatedvendordata));

                            $table_name = $wpdb->prefix . "multi_vendor";
                            $each_product_ven_commission = $commissionamount * $item['qty'];
                            $each_product_v_term_id[] = $vendor_list[0]->term_id;
                            $whole_vendor_details[$vendor_list[0]->term_id][] = array('product_id' => $item['product_id'], 'commission' => $commissionamount * $item['qty']);
//  $wpdb->insert($table_name, array('orderid' => $order_id, 'productid' => $item['product_id'], 'qty' => $item['qty'], 'productname' => get_the_title($item['product_id']), 'productprice' => $getregularprice, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => $commissionamount * $item['qty'], 'vendorname' => $vendor_list[0]->name, 'paidcommission' => '', 'vendorid' => $vendor_list[0]->term_id, 'status' => 'Due', 'date' => date('Y-m-d H:i:s')));
                        }
                    }
                }
            }
        }

        return $whole_vendor_details;
    }

}

new FPMultiVendorEmailNotifications();
add_action('woocommerce_thankyou', array('FPMultiVendorEmailNotifications', 'send_email_new_order'));
add_action('woocommerce_order_status_shipped', array('FPMultiVendorEmailNotifications', 'send_email_order_has_been_shipped'));
if (get_option('fp_multi_vendor_order_status_control') != '') {
    foreach (get_option('fp_multi_vendor_order_status_control') as $values) {
        if ($values == 'processing') {
            add_action('woocommerce_order_status_processing', array('FPMultiVendorEmailNotifications', 'send_email_order_processing'));
        }
        if ($values == 'completed') {
            add_action('woocommerce_order_status_completed', array('FPMultiVendorEmailNotifications', 'send_email_order_completed'));
        }
    }
}
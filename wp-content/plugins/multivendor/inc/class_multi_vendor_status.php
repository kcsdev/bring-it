<?php
/* Status Tab Settings */

class FPMultiVendorStatusTab {

    public function __construct() {
        add_filter('woocommerce_mv_settings_tabs_array', array($this, 'multivendor_status_tab'), 100);
        add_action('admin_init', array($this, 'multivendor_add_default_values_settings'));
        add_action('woocommerce_mv_settings_tabs_multivendor_status', array($this, 'multivendor_register_admin_settings'));
        add_action('woocommerce_update_options_multivendor_status', array($this, 'multivendor_update_values_settings'));
        add_action('admin_head', array($this, 'multivendor_add_chosen_to_status'));
    }

    public static function multivendor_status_tab($settings_tabs) {
        $settings_tabs['multivendor_status'] = __('Status', 'multivendor');
        return $settings_tabs;
    }

    public static function multivendor_add_chosen_to_status() {
        global $woocommerce;
        if (isset($_GET['tab'])) {
            if ($_GET['tab'] == 'multivendor_status') {
                ?>
                <script type="text/javascript">
                    jQuery(document).ready(function () {
                <?php if ((float) $woocommerce->version <= (float) ('2.2.0')) { ?>
                            jQuery('#fp_multi_vendor_order_status_control').chosen();
                <?php } else { ?>
                            jQuery('#fp_multi_vendor_order_status_control').select2();
                <?php } ?>
                    });
                </script>
                <?php
            }
        }
    }

    public static function multivendor_status_admin_settings() {
        global $woocommerce;
        $newcombinedarray = '';
        if (function_exists('wc_get_order_statuses')) {
            $orderstatus = str_replace('wc-', '', array_keys(wc_get_order_statuses()));
            $orderslugs = array_values(wc_get_order_statuses());
            $newcombinedarray = array_combine((array) $orderstatus, (array) $orderslugs);
        } else {
            $taxonomy = 'shop_order_status';
            $orderstatus = '';
            $orderslugs = '';

            $term_args = array(
                'hide_empty' => false,
                'orderby' => 'date',
                    //'order' => 'DESC'
            );
            $tax_terms = get_terms($taxonomy, $term_args);
            foreach ($tax_terms as $getterms) {
                $orderstatus[] = $getterms->name;
                $orderslugs[] = $getterms->slug;
            }
            $newcombinedarray = array_combine((array) $orderslugs, (array) $orderstatus);
        }

        return apply_filters('woocommerce_multivendor_status', array(
            array(
                'name' => __('Status Settings', 'multivendor'),
                'type' => 'title',
                'desc' => __('Here the Commission can be calculated is based on Order Status', 'multivendor'),
                'id' => '_mv_title_status_settings'
            ),
            array(
                'name' => __('Calculate Commission for Vendor when Order Status becomes ', 'multivendor'),
                'desc' => __('Here the Commission can be calculated is based on Order Status', 'multivendor'),
                'tip' => '',
                'id' => 'fp_multi_vendor_order_status_control',
                'css' => 'min-width:150px;',
                'std' => array('completed'),
                'type' => 'multiselect',
                'options' => $newcombinedarray,
                'newids' => 'fp_multi_vendor_order_status_control',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_mv_title_status_settings'),
        ));
    }

    public static function multivendor_register_admin_settings() {
        woocommerce_admin_fields(FPMultiVendorStatusTab::multivendor_status_admin_settings());
    }

    public static function multivendor_update_values_settings() {
        woocommerce_update_options(FPMultiVendorStatusTab::multivendor_status_admin_settings());
    }

    public static function multivendor_add_default_values_settings() {
        foreach (FPMultiVendorStatusTab::multivendor_status_admin_settings() as $settings) {
            if ((isset($settings['newids'])) && (isset($settings['std']))) {
                if (get_option($settings['newids']) == FALSE) {
                    add_option($settings['newids'], $settings['std']);
                }
            }
        }
    }

}

new FPMultiVendorStatusTab();

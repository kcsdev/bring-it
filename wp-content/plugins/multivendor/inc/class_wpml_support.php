<?php

class SocioMultiVendorWPMLSupport {

    function __construct() {
        add_action('wp_head', array($this, 'register_user_lang'));
        add_action('admin_init', array($this, 'mv_register_template_for_wpml'));
    }

    function register_user_lang() {
        $user_id = get_current_user_id();
        $meta_key = 'mv_wpml_lang';
        if (function_exists('icl_register_string')) {
            $currentuser_lang = isset($_SESSION['wpml_globalcart_language']) ? $_SESSION['wpml_globalcart_language'] : ICL_LANGUAGE_CODE;
        } else {
            $currentuser_lang = 'en';
        }
        $meta_value = $currentuser_lang;
        update_user_meta($user_id, $meta_key, $meta_value);
    }

    // registering mail templates strings
    function mv_register_template_for_wpml() {

        if (function_exists('icl_register_string')) {

            global $wpdb;
            $context = 'Socio Multi Vendor';
            $email_options = array('fpmv_vendor_new_order_email_subject' => 'fpmv_vendor_new_order_email_message',
                'fpmv_vendor_order_processing_email_subject' => 'fpmv_vendor_order_processing_email_message',
                'fpmv_vendor_order_completed_email_subject' => 'fpmv_vendor_order_completed_email_message',
                'fpmv_vendor_application_email_subject' => 'fpmv_vendor_application_email_message',
                'fpmv_vendor_application_email_subject_admin' => 'fpmv_vendor_application_email_message_admin',
                'fpmv_produt_email_subject' => 'fpmv_product__email_message',
                'fpmv_vendor_shipping_email_subject' => 'fpmv_vendor_shipping_email_message',
            );

            foreach ($email_options as $eachsubject => $eachmessage) {
                $name_msg = $eachmessage;
                $value_msg = get_option($eachmessage);
                icl_register_string($context, $name_msg, $value_msg); //for registering message
                $name_sub = $eachsubject;
                $value_sub = get_option($eachsubject);
                icl_register_string($context, $name_sub, $value_sub); //for registering subject
            }
        }
    }

    // getting the registered strings from wpml table
    public static function fp_mv_get_wpml_text($option_name, $language, $message) {
        if (function_exists('icl_register_string')) {
            if ($language == 'en') {
                return $message;
            } else {
                global $wpdb;
                $context = 'Socio Multi Vendor';

                $res = $wpdb->get_results($wpdb->prepare("
            SELECT s.name, s.value, t.value AS translation_value, t.status
            FROM  {$wpdb->prefix}icl_strings s
            LEFT JOIN {$wpdb->prefix}icl_string_translations t ON s.id = t.string_id
            WHERE s.context = %s
                AND (t.language = %s OR t.language IS NULL)
            ", $context, $language), ARRAY_A);
                foreach ($res as $each_entry) {
                    if ($each_entry['name'] == $option_name) {
                        if ($each_entry['status'] == '1') {
                            $translated = $each_entry['translation_value'];
                        } else {
                            $translated = $each_entry['value'];
                        }
                    }
                }
                return $translated;
            }
        } else {
            return $message;
        }
    }

}

new SocioMultiVendorWPMLSupport();

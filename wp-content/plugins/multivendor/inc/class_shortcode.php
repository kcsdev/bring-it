<?php

/* Shortcode Tab Settings */

class FPMultiVendorShortcodeTab {

    public function __construct() {
        add_filter('woocommerce_mv_settings_tabs_array', array($this, 'multivendor_shortcode_tab'), 100);
        add_action('admin_init', array($this, 'multivendor_add_default_values_settings'));
        add_action('woocommerce_mv_settings_tabs_multivendor_shortcode', array($this, 'multivendor_register_admin_settings'));
        add_action('woocommerce_update_options_multivendor_shortcode', array($this, 'multivendor_update_values_settings'));
        add_action('woocommerce_admin_field_mv_list_of_shortcodes', array($this, 'multivendor_list_shortcodes'));
    }

    public static function multivendor_shortcode_tab($settings_tabs) {
        $settings_tabs['multivendor_shortcode'] = __('Shortcode', 'multivendor');
        return $settings_tabs;
    }

    public static function multivendor_list_shortcodes() {
        ?>
        <style type="text/css">
            p.submit {
                display:none;
            }
            #mainforms {
                display:none;
            }
        </style>
        <h3>
            [vendor_sales_log] - For Vendor Sales and Commission
        </h3>
        <h3>
            [mv_vendor_application] - For Submitting Vendor Application from Frontend
        </h3>
        <h3>
            [mv_vendor_track_application] - For Track Vendor Application
        </h3>
        <h3>
            {sale_details} - For Sale details in Order Status Email
        </h3>
        <h3>
            {application_status} - For to Get Application Status in Email
        </h3>
        <h3>
            {vendor_admin_name} - For to Get Vendor Admin name  in Email
        </h3>
        <h3>
            {vendor_shop_name} - For to Get Vendor Shop Name which is given in Vendor application form
        </h3>
        <h3>
            {product_status} - For to Get Product Status in Email
        </h3>
        <h3>
            {product_title} - For to Get Title of the Product in Email
        </h3>
        <?php

    }

    public static function multivendor_shortcode_admin_settings() {
        global $woocommerce;
        return apply_filters('woocommerce_multivendor_shortcode', array(
            array(
                'name' => __('Shortcodes', 'multivendor'),
                'type' => 'title',
                'id' => '_mv_title_shortcode_settings'
            ),
            array(
                'type' => 'mv_list_of_shortcodes',
            ),
            array('type' => 'sectionend', 'id' => '_mv_title_shortcode_settings'),
        ));
    }

    public static function multivendor_register_admin_settings() {
        woocommerce_admin_fields(FPMultiVendorShortcodeTab::multivendor_shortcode_admin_settings());
    }

    public static function multivendor_update_values_settings() {
        woocommerce_update_options(FPMultiVendorShortcodeTab::multivendor_shortcode_admin_settings());
    }

    public static function multivendor_add_default_values_settings() {
        foreach (FPMultiVendorShortcodeTab::multivendor_shortcode_admin_settings() as $settings) {
            if ((isset($settings['newids'])) && (isset($settings['std']))) {
                add_option($settings['newids'], $settings['std']);
            }
        }
    }

}

new FPMultiVendorShortcodeTab();

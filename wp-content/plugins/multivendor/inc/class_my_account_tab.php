<?php

/* My Account Tab Settings */

class FPMultiVendorMyAccountTab {

    public function __construct() {
        add_filter('woocommerce_mv_settings_tabs_array', array($this, 'multivendor_myaccount_tab'), 100);
        add_action('admin_init', array($this, 'multivendor_add_default_values_settings'));
        add_action('woocommerce_mv_settings_tabs_multivendor_myaccount', array($this, 'multivendor_register_admin_settings'));
        add_action('woocommerce_update_options_multivendor_myaccount', array($this, 'multivendor_update_values_settings'));
    }

    public static function multivendor_myaccount_tab($settings_tabs) {
        $settings_tabs['multivendor_myaccount'] = __('My Account', 'multivendor');
        return $settings_tabs;
    }

    public static function multivendor_myaccount_admin_settings() {
        global $woocommerce;
        return apply_filters('woocommerce_multivendor_myaccount', array(
            array(
                'name' => __('My Account Settings', 'multivendor'),
                'type' => 'title',
                'desc' => '<h3>Use this Shortcode [vendor_sales_log] to display Vendor Sales Log on Any Page/Post of your WordPress Site </h3>',
                'id' => '_mv_vendor_myaccount_settings'
            ),
            array(
                'name' => __('Display Vendor Sales Log in My Account Page', 'multivendor'),
                'desc' => __('Enable/Disable Vendor Sales Log Displaying in My Account Page', 'multivendor'),
                'tip' => '',
                'id' => 'fp_enable_disable_vendor_sales_log',
                'css' => 'min-width:150px;',
                'std' => '1',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'multivendor'),
                    '2' => __('Hide', 'multivendor'),
                ),
                'newids' => 'fp_enable_disable_vendor_sales_log',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Show/Hide Vendor Application Status in My Account Page', 'multivendor'),
                'desc' => __('Show/Hide Vendor Application Status in My Account Page', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_show_hide_vendor_application_status',
                'css' => 'min-width:150px;',
                'std' => '1',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'multivendor'),
                    '2' => __('Hide', 'multivendor'),
                ),
                'newids' => 'fpmv_show_hide_vendor_application_status',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend'),
            array(
                'type' => 'title',
                'name' => __('Customize Column Name in a Log', 'multivendor'),
                'id' => '_mv_vendor_customize_column_name',
            ),
            array(
                'name' => __('My Socio Log', 'multivendor'),
                'desc' => __('Customize My Socio Log in My Account Page', 'multivendor'),
                'tip' => '',
                'id' => 'fp_customize_my_socio_log_for_myaccount',
                'css' => 'min-width:150px;',
                'std' => 'My Socio Log',
                'type' => 'textarea',
                'newids' => 'fp_customize_my_socio_log_for_myaccount',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Order ID', 'multivendor'),
                'desc' => __('Customize Order ID Column Name in My Account Page', 'multivendor'),
                'tip' => '',
                'id' => 'fp_customize_orderid_for_myaccount',
                'css' => 'min-width:150px;',
                'std' => 'Order ID',
                'type' => 'text',
                'newids' => 'fp_customize_orderid_for_myaccount',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Product Name', 'multivendor'),
                'desc' => __('Customize Product Name Column Name in My Account Page', 'multivendor'),
                'tip' => '',
                'id' => 'fp_customize_productname_for_myaccount',
                'css' => 'min-width:150px;',
                'std' => 'Product Name',
                'type' => 'text',
                'newids' => 'fp_customize_productname_for_myaccount',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Product Price', 'multivendor'),
                'desc' => __('Customize Product Price in Column Name of My Account Page', 'multivendor'),
                'tip' => '',
                'id' => 'fp_customize_productprice_for_myaccount',
                'css' => 'min-width:150px;',
                'std' => 'Product Price',
                'type' => 'text',
                'newids' => 'fp_customize_productprice_for_myaccount',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Vendor Commission Rate', 'multivendor'),
                'desc' => __('Customize Vendor Commission Rate in Column Name of My Account Page', 'multivendor'),
                'tip' => '',
                'id' => 'fp_customize_vendorcommissionrate_for_myaccount',
                'css' => 'min-width:150px;',
                'std' => 'Vendor Commission Rate',
                'type' => 'text',
                'newids' => 'fp_customize_vendorcommissionrate_for_myaccount',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Vendor Commission', 'multivendor'),
                'desc' => __('Customize Vendor Commission in Column Name of My Account Page', 'multivendor'),
                'tip' => '',
                'id' => 'fp_customize_vendorcommission_for_myaccount',
                'css' => 'min-width:150px;',
                'std' => 'Vendor Commission',
                'type' => 'text',
                'newids' => 'fp_customize_vendorcommission_for_myaccount',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Vendor Name', 'multivendor'),
                'desc' => __('Customize Vendor Name in Column Name of My Account Page', 'multivendor'),
                'tip' => '',
                'id' => 'fp_customize_vendorname_for_myaccount',
                'css' => 'min-width:150px;',
                'std' => 'Vendor Name',
                'type' => 'text',
                'newids' => 'fp_customize_vendorname_for_myaccount',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Status', 'multivendor'),
                'desc' => __('Customize Status in Column Name of My Account Page', 'multivendor'),
                'tip' => '',
                'id' => 'fp_customize_status_for_myaccount',
                'css' => 'min-width:150px;',
                'std' => 'Status',
                'type' => 'text',
                'newids' => 'fp_customize_status_for_myaccount',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Date', 'multivendor'),
                'desc' => __('Customize Date Column Name of My Account Page', 'multivendor'),
                'tip' => '',
                'id' => 'fp_customize_date_for_myaccount',
                'css' => 'min-width:150px;',
                'std' => 'Date',
                'type' => 'text',
                'newids' => 'fp_customize_date_for_myaccount',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Total Commission Earned for each Vendor', 'multivendor'),
                'desc' => __('Customize Total Commission Earned for each Vendor in My Account Page', 'multivendor'),
                'tip' => '',
                'id' => 'fp_customize_total_commissionmessage_for_myaccount',
                'css' => 'min-width:150px;',
                'std' => 'Total Commission Earned for each Vendor',
                'type' => 'textarea',
                'newids' => 'fp_customize_total_commissionmessage_for_myaccount',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Vendor Name', 'multivendor'),
                'desc' => __('Customize Vendor Name in My Account Page', 'multivendor'),
                'tip' => '',
                'id' => 'fp_customize_total_vendorname_for_myaccount',
                'css' => 'min-width:150px;',
                'std' => 'Vendor Name',
                'type' => 'text',
                'newids' => 'fp_customize_total_vendorname_for_myaccount',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Vendor Total Earned Commission', 'multivendor'),
                'desc' => __('Customize Vendor Total Earned Commission in My Account Page', 'multivendor'),
                'tip' => '',
                'id' => 'fp_customize_total_vendorearnedcommission',
                'css' => 'min-width:150px;',
                'std' => 'Total Vendor Earned Commission',
                'type' => 'text',
                'newids' => 'fp_customize_total_vendorearnedcommission',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Vendor Due Commission', 'multivendor'),
                'desc' => __('Customize Vendor Due Commission in My Account Page', 'multivendor'),
                'tip' => '',
                'id' => 'fp_customize_total_vendorduecommission_for_myaccount',
                'css' => 'min-width:150px',
                'std' => 'Vendor Due Commission',
                'type' => 'text',
                'newids' => 'fp_customize_total_vendorduecommission_for_myaccount',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Vendor Paid Commission', 'multivendor'),
                'desc' => __('Customize Vendor Paid Commission in My Account Page', 'multivendor'),
                'tip' => '',
                'id' => 'fp_customize_total_vendorpaidcommission_for_myaccount',
                'css' => 'min-width:150px;',
                'std' => 'Vendor Paid Commission',
                'type' => 'text',
                'newids' => 'fp_customize_total_vendorpaidcommission_for_myaccount',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Vendor URL', 'multivendor'),
                'desc' => __('Customize Vendor URL in My Account Page', 'multivendor'),
                'tip' => '',
                'id' => 'fp_vendor_url',
                'css' => 'min-width:150px;',
                'std' => 'Vendor URL',
                'type' => 'text',
                'newids' => 'fp_vendor_url',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Vendor URL Text', 'multivendor'),
                'desc' => __('Customize Vendor URL Text in My Account Page', 'multivendor'),
                'tip' => '',
                'id' => 'fp_vendor_url_text',
                'css' => 'min-width:150px;',
                'std' => 'Go to Vendor URL',
                'type' => 'text',
                'newids' => 'fp_vendor_url_text',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_mv_vendor_customize_column_name'),
            array('type' => 'sectionend', 'id' => '_mv_vendor_myaccount_settings'),
        ));
    }

    public static function multivendor_register_admin_settings() {
        woocommerce_admin_fields(FPMultiVendorMyAccountTab::multivendor_myaccount_admin_settings());
    }

    public static function multivendor_update_values_settings() {
        woocommerce_update_options(FPMultiVendorMyAccountTab::multivendor_myaccount_admin_settings());
    }

    public static function multivendor_add_default_values_settings() {

        foreach (FPMultiVendorMyAccountTab::multivendor_myaccount_admin_settings() as $settings) {
            if ((isset($settings['newids'])) && (isset($settings['std']))) {
                if (get_option($settings['newids']) == FALSE) {
                    add_option($settings['newids'], $settings['std']);
                }
            }
        }
    }

}

new FPMultiVendorMyAccountTab();

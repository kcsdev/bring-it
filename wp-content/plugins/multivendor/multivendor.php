<?php

/**
 * Plugin Name: Socio Multi Vendor
 * Plugin URI:
 * Description: Socio Multi Vendor is the WooCommerce Extension to your site into a Multi Vendor WordPress site.
 * Version: 3.8
 * Author: Fantastic Plugins
 * Author URI:
 */
class FPMultiVendor {

    public function __construct() {
        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        add_filter('init', array($this, 'prevent_header_already_sent_problem'));
        add_action('init', array($this, 'check_woocommerce_is_active'));
        register_activation_hook(__FILE__, array($this, 'multi_vendor_create_table_in_wordpress'));
        register_activation_hook(__FILE__, array($this, 'multi_vendor_create_table_for_submit_vendor_application'));
        add_filter('template_include', array($this, 'create_custom_template_for_taxonomies'));
        add_action('woocommerce_archive_description', array($this, 'woocommerce_taxonomy_archive_description'));

        include_once ('inc/class_add_fields_to_category.php');
        include_once('inc/class_woocommerce_sub_menu.php');
        if (isset($_GET['page']) || (isset($_GET['taxonomy']))) {
            if ((@$_GET['page'] == 'multivendor_menu') || (@$_GET['taxonomy'] == 'multi_vendor')) {
                add_filter('woocommerce_screen_ids', array($this, 'multi_vendor_load_default_enqueues'), 9, 1);
            }
        }
        add_action('admin_enqueue_scripts', array($this, 'multi_vendor_enqueue_scripts'));
        add_action('woocommerce_product_options_general_product_data', array($this, 'multi_vendor_add_field_admin_single_product_page'));
        add_action('woocommerce_process_product_meta', array($this, 'multi_vendor_save_field_admin_single_product_page'));

        include_once('inc/class_multi_vendor_main_function.php');
        include_once('inc/class_wp_admin_list_table.php');
        include_once('inc/class_frontend_shortcode_function.php');

        add_action('plugins_loaded', array($this, 'multivendor_translate_file'));
        add_action('wp_enqueue_scripts', array($this, 'multivendor_enqueue_scripts'));
        add_action('admin_enqueue_scripts', array($this, 'multivendor_enqueue_scripts'));

        add_action('admin_init', array($this, 'reset_multivendor_admin_settings'));

        include_once('inc/class_front_submit_vendor_application.php');
        include_once('inc/class_admin_vendor_application_list.php');
        include_once('inc/class_email_notifications_vendor.php');
        include_once('inc/product_status_mail.php');
        if (get_option('fpmv_enable_seller_info_tab_in_single_product_page') == 'yes') {
            add_filter('woocommerce_product_tabs', array($this, 'multi_vendor_product_tab'));
        }
        register_activation_hook(__FILE__, array($this, 'multi_vendor_create_custom_role_wordpress'));

        //add_action('init', array($this, 'multi_vendor_create_custom_role_wordpress'));


        add_action('admin_init', array($this, 'exclude_product_vendor_display'), 999);
        include_once('inc/class_sold_individually.php');
        include_once('inc/class_restrict_media_files_filter.php');
        include_once('inc/class_wpml_support.php');
        include_once('inc/class_multi_vendor_shipping_order_status.php');

        include_once('inc/class_multi_vendor_order_management.php');



        add_action('add_meta_boxes', array($this, 'add_meta_box_for_order_table'));

        add_action('pre_get_posts', array($this, 'products_for_current_author'));
        add_filter('post_row_actions', array($this, 'remove_quick_edit'), 10, 1);
        add_action('admin_menu', array($this, 'adjust_the_wp_menu'), 999);

        /* Backward Compatibility for Socio Multi Vendor with old version of WooCommerce */

        include_once('inc/wc_settings_backward_compatibility.php');

        add_filter('woocommerce_prevent_admin_access', array($this, 'multivendor_redirection_to_admin'), 10, 1);
    }

    /* Check WooCommerce is Active or Not */

    public static function check_woocommerce_is_active() {
        if (is_multisite()) {
//var_dump(is_plugin_active_for_network('woocommerce/woocommerce.php'));
//var_dump(is_plugin_active('woocommerce/woocommerce.php'));
            if (!is_plugin_active_for_network('woocommerce/woocommerce.php') && (!is_plugin_active('woocommerce/woocommerce.php'))) {
                if (is_admin()) {
                    $variable = "<div class='error'><p> Multi Vendor Plugin Requires WooCommerce must be Active!!! </p></div>";
                    echo $variable;
                }
                return;
            }
        } else {
            if (!is_plugin_active('woocommerce/woocommerce.php')) {
                if (is_admin()) {
                    $variable = "<div class='error'><p> Multi Vendor Plugin Requires WooCommerce must be Active!!! </p></div>";
                    echo $variable;
                }
                return;
            }
        }
    }

    /* Load Template for Custom Taxonomies */

    public static function create_custom_template_for_taxonomies($template) {
        if (is_tax('multi_vendor')) {
            $template = untrailingslashit(plugin_dir_path(__FILE__)) . '/woocommerce/templates/taxonomy-multi_vendor.php';
        }
        return $template;
    }

    /* Load Description to Taxonomy Archive */

    public static function woocommerce_taxonomy_archive_description() {
        if (is_tax(array('multi_vendor')) && get_query_var('paged') == 0) {
            $term = get_queried_object();

            $image = '';
            $thumbnail_id = absint(get_woocommerce_term_meta($term->term_id, 'thumbnail_id', true));
            if ($thumbnail_id)
                $image = wp_get_attachment_url($thumbnail_id);
            else
                $image = '';

            $description = apply_filters('the_content', term_description());
            if ($image != '') {
                $getimagefromurl = "<img src=" . $image . " >";
            } else {
                $getimagefromurl = '';
            }
            if ($description) {
                echo '<div class="term-description">' . $getimagefromurl . $description . '</div>';
            }
        }
    }

    /* Prevent Header Already Sent Problem */

    public static function prevent_header_already_sent_problem() {
        ob_start();
    }

    /* Load Javascript and CSS from WooCommerce */

    public static function multi_vendor_load_default_enqueues() {
        global $my_admin_page;
        if (isset($_GET['page'])) {
            if (($_GET['page'] == 'multivendor_menu')) {
                $array[] = 'woocommerce_page_' . $_GET['page'];
                return $array;
            } else {
                $array[] = '';
                return $array;
            }
        }
        if (isset($_GET['taxonomy'])) {
            if (($_GET['taxonomy'] == 'multi_vendor')) {
                $array[] = 'edit-multi_vendor';
                return $array;
            } else {
                $array[] = '';
                return $array;
            }
        }
    }

    public static function multi_vendor_product_tab($tabs) {
        global $post;
        $vendorlist = wp_get_post_terms($post->ID, 'multi_vendor');
        $getvendorcount = count($vendorlist);
        if ($getvendorcount > 0) {
            $tabs['seller_info'] = array(
                'title' => get_option('fpmv_customize_seller_info_tab_name_single_product_page'),
                'priority' => 50,
                'callback' => array('FPMultiVendor', 'multi_vendor_product_tab_content'),
            );
        }
        return $tabs;
    }

    public static function multi_vendor_product_tab_content() {
        global $post;

        $vendorlist = wp_get_post_terms($post->ID, 'multi_vendor');


        $getvendorcount = count($vendorlist);
        if ($getvendorcount > 0) {
            ?>
            <h3><?php _e('Vendor Name', 'multivendor'); ?></h3>
            <?php
            foreach ($vendorlist as $eachvendor) {
                $termlink = get_term_link($eachvendor->slug, 'multi_vendor');

                if (!is_wp_error($termlink)) {
                    global $woocommerce, $product;
                    foreach (get_objects_in_term($eachvendor->term_id, 'multi_vendor') as $value) {
                        $product_object = new WC_Product($value);
                        $product_object->get_average_rating();
                        $overallobject[] = $product_object->get_average_rating();
                    }
                    $getcountvalues = count(array_filter($overallobject));

                    $getsumofcounts = array_sum($overallobject);
                    if ($getcountvalues != 0) {
                        $average = number_format($getsumofcounts / $getcountvalues, 2);
                    } else {
                        $average = 0;
                    }
                    ?>
                    <div class="vendorinfo vendorinfo<?php echo $eachvendor->term_id; ?>" style="width:100%;">
                        <a href="<?php echo $termlink; ?>" target="_blank"><?php echo $eachvendor->name; ?></a> <?php echo '<div class="star-rating"><span title="' . sprintf(__('Average Rating %s out of 5', 'woocommerce'), $average) . '" style="width:' . ( ( $average / 5 ) * 100 ) . '%"><strong itemprop="ratingValue" class="rating">' . $average . '</strong> ' . __('out of 5', 'woocommerce') . '</span></div>'; ?> <br>
                        <?php if ($eachvendor->description != '') { ?>
                            <?php echo nl2br($eachvendor->description); ?>
                        <?php } ?>
                        <hr>
                    </div>
                    <?php
                }
            }
        }
    }

    public static function multi_vendor_enqueue_scripts() {
        wp_register_script('multi_vendor_chosen_enqueue', plugins_url('multivendor/js/chosen.jquery.js'));
        wp_register_style('multi_vendor_chosen_style_enqueue', plugins_url('multivendor/css/chosen.css'));
        if (isset($_GET['taxonomy'])) {
            if ($_GET['taxonomy'] == 'multi_vendor') {
                wp_enqueue_script('multi_vendor_chosen_enqueue');
                wp_enqueue_style('multi_vendor_chosen_style_enqueue');
                wp_enqueue_media();
            }
        }
    }

    /* From Version 3.3 */

    public static function remove_quick_edit($actions) {
        global $current_user;
        if (is_admin() && FPRestrictMediaFiles::mv_check_user_role('multi_vendor', $current_user->ID)) {
            unset($actions['inline hide-if-no-js']);
        }
        return $actions;
    }

    /* Check the Current User is Multivendor to Avoid Redirection */

    public static function multivendor_redirection_to_admin($prevent_access) {
        $current_user_id = get_current_user_id();
        // Check if the current user as multi vendor role
        if (FPRestrictMediaFiles::mv_check_user_role('multi_vendor', $current_user_id)) {
            $prevent_access = false;
        }
        return $prevent_access;
    }

    /* Add Additional Field in Admin Single Product Page */

    public static function multi_vendor_add_field_admin_single_product_page() {

        global $current_user;
        if (is_admin() && FPRestrictMediaFiles::mv_check_user_role('multi_vendor', $current_user->ID)) {
            ?>
            <script type="text/javascript">
                jQuery(document).ready(function () {
            <?php
            if (get_option('fpmv_show_hide_commission_field_in_single_product') == 'hide') {
                ?>
                        jQuery('._fp_multi_vendor_commission_percentage_field').hide();

            <?php } ?>
                    jQuery('#_fp_multi_vendor_commission_percentage').attr('readonly', true);
                });
            </script>
            <?php
        }
        woocommerce_wp_text_input(
                array(
                    'id' => '_fp_multi_vendor_commission_percentage',
                    'name' => '_fp_multi_vendor_commission_percentage',
                    'label' => __('Vendor Commission in Percentage(%)', 'multivendor'),
                )
        );
    }

    /* Save the meta data in Admin Single Product Page */

    public static function multi_vendor_save_field_admin_single_product_page($post_id) {
        $multivendorpercentage = $_POST['_fp_multi_vendor_commission_percentage'];
        update_post_meta($post_id, '_fp_multi_vendor_commission_percentage', $multivendorpercentage);
    }

    public static function multivendor_translate_file() {
        load_plugin_textdomain('multivendor', false, dirname(plugin_basename(__FILE__)) . '/languages');
    }

    /* Resetting the Data */

    public static function reset_multivendor_admin_settings() {
        if (!empty($_POST['reset'])) {
            if (empty($_REQUEST['_wpnonce']) || !wp_verify_nonce($_REQUEST['_wpnonce'], 'woocommerce-reset_mv_settings'))
                die(__('Action failed. Please refresh the page and retry.', 'multivendor'));

            foreach (FPMultiVendorGeneralTab::multivendor_general_admin_settings() as $setting) {
                if (isset($setting['newids']) && ($setting['std'])) {
                    delete_option($setting['newids']);
                    add_option($setting['newids'], $setting['std']);
                }
            }

            foreach (FPMultiVendorMyAccountTab::multivendor_myaccount_admin_settings() as $setting) {
                if (isset($setting['newids']) && ($setting['std'])) {
                    delete_option($setting['newids']);
                    add_option($setting['newids'], $setting['std']);
                }
            }

            foreach (FPMultiVendorStatusTab::multivendor_status_admin_settings() as $setting) {
                if (isset($setting['newids']) && ($setting['std'])) {
                    delete_option($setting['newids']);
                    add_option($setting['newids'], $setting['std']);
                }
            }

            foreach (FPMultiVendorMessagesTab::multivendor_messages_admin_settings() as $setting) {
                if (isset($setting['newids']) && ($setting['std'])) {
                    delete_option($setting['newids']);
                    add_option($setting['newids'], $setting['std']);
                }
            }

            foreach (FPMultiVendorLocalizationTab::multivendor_localization_admin_settings() as $setting) {
                if (isset($setting['newids']) && ($setting['std'])) {
                    delete_option($setting['newids']);
                    add_option($setting['newids'], $setting['std']);
                }
            }

            foreach (FPMultiVendorCartTab::multivendor_cart_admin_settings() as $setting) {
                if (isset($setting['newids']) && ($setting['std'])) {
                    delete_option($setting['newids']);
                    add_option($setting['newids'], $setting['std']);
                }
            }

            foreach (FPMultiVendorEmailNotiTab::multivendor_email_noti_settings() as $setting) {
                if (isset($setting['newids']) && ($setting['std'])) {
                    delete_option($setting['newids']);
                    add_option($setting['newids'], $setting['std']);
                }
            }

            FPMultiVendor::multi_vendor_create_custom_role_wordpress();

//$woocommerce->clear_product_transients();
            delete_transient('woocommerce_cache_excluded_uris');
// Redirect back to the settings page
            $redirect = esc_url_raw(add_query_arg(array('saved' => 'true')));
//  $redirect .= add_query_arg('noheader', 'true');
            if (isset($_POST['reset'])) {
                wp_safe_redirect($redirect);
                exit;
            }
        }
    }

    /* Enqueue Script in Frontend */

    public static function multivendor_enqueue_scripts() {
        wp_register_script('multivendor_footable', plugins_url('multivendor/js/footable.js'));
        wp_register_script('multivendor_footable_sort', plugins_url('multivendor/js/footable.sort.js'));
        wp_register_script('multivendor_footable_paging', plugins_url('multivendor/js/footable.paginate.js'));
        wp_register_script('multivendor_footable_filter', plugins_url('multivendor/js/footable.filter.js'));
        wp_register_style('multivendor_footable_css', plugins_url('multivendor/css/footable.core.css'));
        wp_register_style('multivendor_bootstrap_css', plugins_url('multivendor/css/bootstrap.css'));
        wp_register_script('multivendor_jquery_validation', plugins_url('multivendor/js/jquery.validate.js'));

        wp_enqueue_script('jquery');
        wp_enqueue_script('multivendor_jquery_validation');
        wp_enqueue_script('multivendor_footable');
        wp_enqueue_script('multivendor_footable_sort');
        wp_enqueue_script('multivendor_footable_paging');
        wp_enqueue_script('multivendor_footable_filter');
        wp_enqueue_style('multivendor_footable_css');
        wp_enqueue_style('multivendor_bootstrap_css');
        wp_enqueue_script('jquery-form', array('jquery'), false, true);
    }

    public static function multi_vendor_create_custom_role_wordpress() {
        global $wpdb;
        global $wp_roles;

        if (class_exists('WP_Roles')) {
            if (!isset($wp_roles)) {
                $wp_roles = new WP_Roles();
            }
        }
        remove_role('multi_vendor');

        add_role('multi_vendor', 'Multi Vendor', array(
            'level_0' => true,
            'read' => true,
            'edit_posts' => get_option('fpmv_create_blog_post_for_multivendor_user_role') == 'yes' ? true : false,
            'publish_posts' => get_option('fpmv_publish_post_instantly_for_multivendor_user_role') == 'yes' ? true : false,
            'import' => false,
            'export' => false,
            'assign_product_terms' => true,
            'upload_files' => true,
            'edit_product' => false,
            'read_product' => false,
            'edit_products' => get_option('fpmv_hide_add_products_for_multivendor_user_role') == 'yes' ? true : false,
            'edit_others_products' => false,
            'publish_products' => get_option('fpmv_publish_product_instantly_for_multivendor_user_role') == 'yes' ? true : false,
            'delete_products' => get_option('fpmv_enable_delete_products_for_multivendor_user_role') == 'yes' ? true : false,
            'delete_published_products' => get_option('fpmv_delete_published_product_for_multivendor_user_role') == 'yes' ? true : false,
            'delete_others_products' => false,
            'edit_published_products' => get_option('fpmv_edit_published_product_for_multivendor_user_role') == 'yes' ? true : false,
            'manage_multivendor_order' => get_option('fpmv_enable_order_management_for_multivendor_user_role') == 'yes' ? true : false,
        ));
    }

    public static function exclude_vendor_products($exclusions, $args) {
        $exclude = get_user_meta(get_current_user_id(), 'excludedtermid', true);
        $excluding_other_vendors = wp_parse_id_list($exclude);
        foreach ($excluding_other_vendors as $exclude_vendor) {
            if (empty($exclusions))
                $exclusions = ' AND ( t.term_id <> ' . intval($exclude_vendor) . ' ';
            else
                $exclusions .= ' AND t.term_id <> ' . intval($exclude_vendor) . ' ';
        }

        if (!empty($exclusions))
            $exclusions .= ')';

        return $exclusions;
    }

    public static function add_meta_box_for_order_table() {
        add_meta_box(__('Vendor Details', 'multivendor'), __('Vendor Details', 'multivendor'), array('FPMultiVendor', 'vendor_order_details'), 'shop_order', 'normal', 'high');
    }

    public static function vendor_order_details($post) {
        //
        global $wpdb;
        $table_name = $wpdb->prefix . 'multi_vendor';
        $getallresults = $wpdb->get_results("SELECT * FROM $table_name WHERE orderid=$post->ID", ARRAY_A);

        $orderid = $post->ID;
        $order = new WC_Order($orderid);
        $vendordata = array();
        foreach ($order->get_items() as $eachitem) {
            $vendorlist = wp_get_post_terms($eachitem['product_id'], 'multi_vendor');

            foreach ($vendorlist as $eachvendor) {
                $vendordata[$eachvendor->term_id] = $eachvendor->name;
            }
        }
        // var_dump($getallresults);
        if (empty($vendordata)) {
            _e('Sorry no Vendor Associated for this Order', 'multivendor');
        } else {
            ?>
            <h4><?php _e('Vendor Details', 'multivendor'); ?></h4>
            <?php
            if (is_array($vendordata)) {
                //var_dump($getallresults);
                foreach ($vendordata as $eachresults => $key) {
                    ?>
                    <p>
                        <strong>
                            <?php _e('Vendor Name :', 'multivendor'); ?>
                        </strong>
                        <strong>
                            <?php echo $key; ?>
                        </strong>
                    </p>
                    <p>
                        <strong>
                            <?php _e('Vendor ID :', 'multivendor'); ?>
                        </strong>
                        <strong>
                            <?php echo $eachresults; ?>
                        </strong>
                    </p>

                    <?php
                }
            }
        }
    }

    public static function exclude_product_vendor_display() {
        $vendoruser = new WP_User(get_current_user_id());
        if (!empty($vendoruser->roles) && is_array($vendoruser->roles)) {
            foreach ($vendoruser->roles as $role) {
                if ($role == 'multi_vendor') {
                    $getmytaxonomy = array('multi_vendor');
                    $arguments = array(
                        'hide_empty' => false,
                    );
                    $termsid = array();
                    $includetermsid = array();
                    $getallvendors = get_terms($getmytaxonomy, $arguments);
                    foreach ($getallvendors as $eachvendor) {
                        $vendoradminids = unserialize(get_woocommerce_term_meta($eachvendor->term_id, 'fp_mv_vendor_admins', true));
// var_dump($vendoradminids);

                        if (!in_array(get_current_user_id(), (array) $vendoradminids)) {
                            $termsid[] = $eachvendor->term_id;
                        }

                        if (in_array(get_current_user_id(), (array) $vendoradminids)) {
                            $includetermsid[] = $eachvendor->term_id;
                        }
                    }

//  var_dump($includetermsid);
// var_dump($termsid);
                    $implode = implode(',', $termsid);
//  var_dump($implode);
                    update_user_meta(get_current_user_id(), 'excludedtermid', $implode);

                    add_filter('list_terms_exclusions', array('FPMultiVendor', 'exclude_vendor_products'), 10, 2);
                }
            }
        }
    }

    function adjust_the_wp_menu() {
        global $current_user;
        if (is_admin() && FPRestrictMediaFiles::mv_check_user_role('multi_vendor', $current_user->ID)) {
            remove_submenu_page('woocommerce', 'wc-settings'); // Latest Version of WooCommerce
            remove_submenu_page('woocommerce', 'woocommerce_settings'); //Backward Compatibility Old Version
            remove_submenu_page('woocommerce', 'wc-status');
            remove_submenu_page('woocommerce', 'woocommerce_status');
            remove_submenu_page('woocommerce', 'wc-addons');
            remove_submenu_page('woocommerce', 'woocommerce_addons');
            remove_submenu_page('woocommerce', 'woocommerce');
        }
    }

    public static function products_for_current_author($query) {


        if ($query->is_admin) {
            global $pagenow;
// var_dump($currentscreenid);
            global $user_ID;
            // var_dump($pagenow);
            if (($pagenow == 'edit.php')) {
                if (!current_user_can('manage_options')) {
                    if (is_admin() && FPRestrictMediaFiles::mv_check_user_role('multi_vendor', $user_ID)) {
// if (isset($_GET['post_type'])) {
//   if ($_GET['post_type'] != 'shop_order') {
//$query->set('author', $user_ID);
                        $taxonomies = array('multi_vendor');
                        $args = array(
                            'hide_empty' => false,
                        );
                        $termsid = array();
                        $termsslug = array();
                        $allvendoradmins = array();
                        $getallvendors = get_terms($taxonomies, $args);

                        $getonlyids = get_terms($taxonomies, array('hide_empty' => false, 'fields' => 'ids'));

//echo get_current_user_id();
                        foreach ($getallvendors as $eachvendor) {
                            $vendoradminids = unserialize(get_woocommerce_term_meta($eachvendor->term_id, 'fp_mv_vendor_admins', true));
                            if (in_array($user_ID, (array) $vendoradminids)) {
                                $termsid[] = $eachvendor->term_id;
                                $termsslug[] = $eachvendor->slug;
                                $allvendoradmins[] = unserialize(get_woocommerce_term_meta($eachvendor->term_id, 'fp_mv_vendor_admins', true));
                            }
                        }

//var_dump($termsslug);
                        $query->get('post_type');

                        $query->set('author', '');
                        $query->set('tax_query', array(
                            'relation' => 'OR',
                            array(
                                'taxonomy' => 'multi_vendor',
                                'field' => 'term_id',
                                'terms' => $getonlyids,
                                'operator' => 'IN',
                            )
                        ));
                        add_filter('views_edit-product', array('FPMultiVendor', 'get_corresponding_post_count'));
                    }
                }
//   }
// }
            }
        }


        return $query;
    }

    public static function get_corresponding_post_count($views) {
        global $current_user, $wp_query;
        unset($views['mine']);
        $types = array(
            array('status' => NULL),
            array('status' => 'publish'),
            array('status' => 'trash'),
            array('status' => 'draft'),
            array('status' => 'pending'),
        );
        foreach ($types as $type) {
            $query = array(
                'author' => $current_user->ID,
                'post_type' => 'product',
                'post_status' => $type['status']
            );
            $result = new WP_Query($query);
            if ($type['status'] == NULL):
                @$class = ($wp_query->query_vars['post_status'] == NULL) ? ' class="current"' : '';
                $views['all'] = sprintf(
                        '<a href="%1$s"%2$s>%4$s <span class="count">(%3$d)</span></a>', admin_url('edit.php?post_type=product'), $class, $result->found_posts, __('All')
                );
            elseif ($type['status'] == 'publish'):
                @$class = ($wp_query->query_vars['post_status'] == 'publish') ? ' class="current"' : '';
                $views['publish'] = sprintf(
                        '<a href="%1$s"%2$s>%4$s <span class="count">(%3$d)</span></a>', admin_url('edit.php?post_status=publish&post_type=product'), $class, $result->found_posts, __('Publish')
                );
            elseif ($type['status'] == 'pending'):
                @$class = ($wp_query->query_vars['post_status'] == 'pending') ? ' class="current"' : '';
                $views['pending'] = sprintf(
                        '<a href="%1$s"%2$s>%4$s <span class="count">(%3$d)</span></a>', admin_url('edit.php?post_status=pending&post_type=product'), $class, $result->found_posts, __('Pending')
                );
            elseif ($type['status'] == 'trash'):
                @$class = ($wp_query->query_vars['post_status'] == 'trash') ? ' class="current"' : '';
                $views['trash'] = sprintf(
                        '<a href="%1$s"%2$s>%4$s <span class="count">(%3$d)</span></a>', admin_url('edit.php?post_status=trash&post_type=product'), $class, $result->found_posts, __('Trash')
                );
            elseif ($type['status'] == 'draft'):
                @$class = ($wp_query->query_vars['post_status'] == 'draft') ? ' class="current"' : '';
                $views['draft'] = sprintf(
                        '<a href="%1$s"%2$s>%4$s <span class="count">(%3$d)</span></a>', admin_url('edit.php?post_status=draft&post_type=product'), $class, $result->found_posts, __('Draft')
                );
            endif;
        }
        return $views;
    }

    public static function multi_vendor_create_table_in_wordpress() {
        global $wpdb;
        $charset_collate = '';
        $table_name = $wpdb->prefix . "multi_vendor";
        if (!empty($wpdb->charset)) {
            $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
        }

        if (!empty($wpdb->collate)) {
            $charset_collate .= " COLLATE {$wpdb->collate}";
        }

        $sql = "CREATE TABLE $table_name (
  id mediumint(9) NOT NULL AUTO_INCREMENT,
  orderid VARCHAR(200),
  productid VARCHAR(200),
  productname VARCHAR(200),
  productprice VARCHAR(100),
  discountedproductprice VARCHAR(100) DEFAULT 0,
  discountamount VARCHAR(100) DEFAULT 0,
  qty VARCHAR(100),
  shippingcost VARCHAR(100) DEFAULT 0,
  vendorcommissionrate VARCHAR(100),
  vendorcommission VARCHAR(100),
  vendorid VARCHAR(100),
  vendorname VARCHAR(100),
  paidcommission VARCHAR(100) DEFAULT 0,
  duecommission VARCHAR(100) DEFAULT 0,
  status VARCHAR(100),
  date VARCHAR(100),
  UNIQUE KEY id (id)
) $charset_collate;";
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta($sql);
    }

    public static function multi_vendor_create_table_for_submit_vendor_application() {
        global $wpdb;
        $charset_collate = '';
        $table_name = $wpdb->prefix . "multi_vendor_submit_application";
        if (!empty($wpdb->charset)) {
            $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
        }

        if (!empty($wpdb->collate)) {
            $charset_collate .= " COLLATE {$wpdb->collate}";
        }
        $sql = "CREATE TABLE $table_name (
  id mediumint(9) NOT NULL AUTO_INCREMENT,
  setvendorname VARCHAR(200),
  setvendoradmins VARCHAR(400),
  setusernickname VARCHAR(400),
  setvendordescription LONGTEXT,
  setvendorcommission VARCHAR(200),
  setvendorpaypalemail VARCHAR(200),
  setvendorcustompayment LONGTEXT,
  setvendorpaymentoption VARCHAR(200),
  setcreatedvendorid VARCHAR(300),
  setmessagetoreviewer LONGTEXT,
  getmessagefromreviewer LONGTEXT,
  vendorthumbnail LONGTEXT,
  status VARCHAR(200),
  date VARCHAR(300),
  UNIQUE KEY id (id)
) $charset_collate;";
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta($sql);
    }

    public static function get_woocommerce_formatted_price($price) {
        if (function_exists('woocommerce_price')) {
            return woocommerce_price($price);
        } else {
            if (function_exists('wc_price')) {
                return wc_price($price);
            }
        }
    }

}

new FPMultiVendor();
